﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour {

	// Use this for initialization
	void Awake () {
		Debug.Log("Awake");
	}

	void OnEnable () {
		Debug.Log("OnEnable");
	}

	void Start () {
		int sum = 1234/10;
		Debug.Log("Start "+sum);
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log("Update");
	}

	void LateUpdate () {
		Debug.Log("LateUpdate");
	}

	void OnGUI() {
		Debug.Log("OnGUI");
	}

	void OnApplicationQuit () {
		Debug.Log("OnApplicationQuit");
	}

	void OnDisable () {
		Debug.Log("OnDisable");
	}

	void OnDestroy () {
		Debug.Log("OnDestroy");
	}
}
