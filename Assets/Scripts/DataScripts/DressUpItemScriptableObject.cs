﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DressUpItemScriptableObject : MonoBehaviour {

    public string sceneName;

    public List <DressUpItemObject> elements;

    public void RefreshStylePoints()
    {
        foreach (var item in elements)
        {
            item.RefreshStylePoints();
        }
    }
}
