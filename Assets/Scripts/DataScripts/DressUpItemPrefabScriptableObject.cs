﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[CreateAssetMenu(fileName = "DataAsset", menuName = "DressUp Item Data/Dress Up Item Prefab")]
public class DressUpItemPrefabScriptableObject : ScriptableObject {

    public string sceneName;

    public List <DressUpItemPrefab> elements;

    public void RefreshStylePoints()
    {
        foreach (var item in elements)
        {
            item.RefreshStylePoints();
        }
    }
}
