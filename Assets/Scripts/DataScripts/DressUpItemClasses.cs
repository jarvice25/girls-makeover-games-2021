﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum EItemStyles {
    Classy = 0,
    Punk = 1,
};

[System.Serializable]
public enum ECategoryTier {
    None = 0,
    Low = 5,
    Mid = 10,
    High = 20
};

[System.Serializable]
public enum EValueMultiplier {
    None = 0,
    Free = 2,
    Video = 5,
    Exp = 10
};

[System.Serializable]
public class DressUpItemSprite {

    public List<EItemStyles> styles;
    public Sprite item;
    public Sprite thumbnail;
    //ItemValue
    [ShowOnlyAttribute,SerializeField]
    int stylePoints;
    public ECategoryTier categoryTier;
    public EValueMultiplier valueMultiplier;
    public int unlockLevel;

    public bool HasStyle(int styleIndex)
    {
        foreach (var style in styles)
        {
            if (styleIndex == (int)style)
                return true;
        }
        return false;
    }

    public int StylePoints 
    {
        get
        {
            return stylePoints;
        }
        set
        {
            stylePoints = value;
        }
    }

    public void RefreshStylePoints()
    {
        StylePoints = (int)categoryTier * (int)valueMultiplier;
//        Debug.Log("StylePoints refreshed " + stylePoints);
    }
}

[System.Serializable]
public class DressUpItemPrefab {

    public List<EItemStyles> styles;
    public GameObject item;
    public Sprite thumbnail;
    //ItemValue
    [ShowOnlyAttribute,SerializeField]
    int stylePoints;
    public ECategoryTier categoryTier;
    public EValueMultiplier valueMultiplier;
    public int unlockLevel;

    public bool HasStyle(int styleIndex)
    {
        foreach (var style in styles)
        {
            if (styleIndex == (int)style)
                return true;
        }
        return false;
    }

    public int StylePoints 
    {
        get
        {
            return stylePoints;
        }
        set
        {
            stylePoints = value;
        }
    }

    public void RefreshStylePoints()
    {
        StylePoints = (int)categoryTier * (int)valueMultiplier;
        Debug.Log("StylePoints refreshed " + stylePoints);
    }
}

[System.Serializable]
public class DressUpItemObject {

    public string itemName;
    public List<EItemStyles> styles;
    public Object[] item;
    public Sprite thumbnail;
    //ItemValue
    [ShowOnlyAttribute,SerializeField]
    int stylePoints;
    public ECategoryTier categoryTier;
    public EValueMultiplier valueMultiplier;
    public int unlockLevel;

    public bool HasStyle(int styleIndex)
    {
        foreach (var style in styles)
        {
            if (styleIndex == (int)style)
                return true;
        }
        return false;
    }

    public int StylePoints 
    {
        get
        {
            return stylePoints;
        }
        set
        {
            stylePoints = value;
        }
    }

    public void RefreshStylePoints()
    {
        StylePoints = (int)categoryTier * (int)valueMultiplier;
        //        Debug.Log("StylePoints refreshed " + stylePoints);
    }
}
