﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwimmingSuit : DressUpItemPrefabScriptableObject {

	public List<GameObject> swimmingSuit;
}