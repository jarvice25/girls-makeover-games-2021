﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu (fileName = "Accessories", menuName = "DressUp Item Data/Accessories")]
public class Accessories : ScriptableObject {

    public List<Sprite> accessoire;
}
