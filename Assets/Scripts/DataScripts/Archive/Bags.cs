﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bags : DressUpItemSpriteScriptableObject {

	public List<Sprite> bags;
}
