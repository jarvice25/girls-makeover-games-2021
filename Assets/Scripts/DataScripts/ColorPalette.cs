﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ColorPalette", menuName = "DressUp Item Data/Color Palette")]
public class ColorPalette : ScriptableObject {

    public List<Color> colors;
}