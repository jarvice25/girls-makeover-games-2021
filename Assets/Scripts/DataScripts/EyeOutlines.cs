﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "EyeOutlines",menuName = "DressUp Item Data/Eye Outlines Holder")]
public class EyeOutlines : ScriptableObject {

    public Sprite[] eyeOutlinesOpen;
    public Sprite[] eyeOutlinesClosed;
    public Texture2D[] eyeLiner;
}
