﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[CreateAssetMenu(fileName = "DataAsset", menuName = "DressUp Item Data/Dress Up Item Sprite")]
public class DressUpItemSpriteScriptableObject : ScriptableObject {

    public string sceneName;
    
    public List <DressUpItemSprite> elements;

    public void RefreshStylePoints()
    {
        foreach (var item in elements)
        {
            item.RefreshStylePoints();
        }
    }
}
