﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ContestOpponent", menuName = "DressUp Item Data/Contest Opponent DressUp Data")]
public class CharacterDressUpData : ScriptableObject 
{
    //TODO videti kako ovo da se odradi... verovatno da se podese samo boje, mozda preko color palette

    #region Accessories
    public DressUpItemSprite earringsSprite;
    public DressUpItemSprite neclessSprite;
    public DressUpItemSprite sunglassesSprite;
    public DressUpItemSprite tiaraSprite;
    #endregion


    #region DressUP
    public DressUpItemPrefab hairPrefab;
    public DressUpItemPrefab blousePrefab;
    public DressUpItemPrefab jacketPrefab;
    public DressUpItemPrefab dressPrefab;
    public DressUpItemPrefab pantsPrefab;
    public DressUpItemPrefab shoesPrefab;
    public DressUpItemPrefab shirtPrefab;
    public DressUpItemPrefab skirtPrefab;
    public DressUpItemPrefab shortsPrefab;
    public DressUpItemPrefab socksPrefab;
    public DressUpItemSprite bagSprite;
    #endregion

    public void RefreshStylePoints()
    {
        earringsSprite.RefreshStylePoints();
        neclessSprite.RefreshStylePoints();
        sunglassesSprite.RefreshStylePoints();
        tiaraSprite.RefreshStylePoints();

        hairPrefab.RefreshStylePoints();
        blousePrefab.RefreshStylePoints();
        jacketPrefab.RefreshStylePoints();
        dressPrefab.RefreshStylePoints();
        pantsPrefab.RefreshStylePoints();
        shoesPrefab.RefreshStylePoints();
        shirtPrefab.RefreshStylePoints();
        skirtPrefab.RefreshStylePoints();
        shortsPrefab.RefreshStylePoints();
        socksPrefab.RefreshStylePoints();
        bagSprite.RefreshStylePoints();
    }

//    #region jewelry section
//    public int jewelryLiquidIndex;  //-1-no ring, 0-silver, 1 - gold
//    public int jewelryDiamond;
//    #endregion
}

public class CharacterMakeUpData {

    #region MakeUp
    public Color eyeColor;
    public Sprite leftEyeShadowSprite;
    public Sprite rightEyeShadowSprite;
    public Sprite leftEyeLinerSprite;
    public Sprite rightEyeLinerSprite;
    public Sprite leftBlushSprite;
    public Sprite rightBlushSprite;
    public Sprite leftMascaraOpenSprite;
    public Sprite leftMascaraClosedSprite;
    public Sprite rightMascaraOpenSprite;
    public Sprite rightMascaraClosedSprite;
    public Color leftMascaraColor;
    public Color rightMascaraColor;
    public Color upperLipColor;
    public Color lowerLipColor;
    public bool[] shimmers = new bool[6]; //FIXME da duzina niza ne bude HC
    #endregion

    public CharacterMakeUpData()
    {

        Texture2D tex = new Texture2D (128, 128, TextureFormat.RGBA32, false);
        Color fillColor = new Color(1,1,1,0);
        Color[] fillPixels = new Color[tex.width * tex.height];
        for (int i = 0; i < fillPixels.Length; i++)
        {
            fillPixels[i] = fillColor;
        }
        tex.SetPixels(fillPixels);
        tex.Apply();
        tex.filterMode = FilterMode.Point;
        Sprite emptySprite = Sprite.Create(tex,new Rect(0.0f, 0.0f, 128, 128),new Vector2(0.5f,0.5f));
        
        leftEyeShadowSprite = emptySprite;
        rightEyeShadowSprite = emptySprite;
        leftEyeLinerSprite = emptySprite;
        rightEyeLinerSprite = emptySprite;
        leftBlushSprite = emptySprite;
        rightBlushSprite = emptySprite;

        //This should be overriden
        eyeColor = Color.blue;
        leftMascaraOpenSprite = emptySprite;
        leftMascaraClosedSprite = emptySprite;
        leftMascaraColor = Color.black;
        rightMascaraOpenSprite = emptySprite;
        rightMascaraClosedSprite = emptySprite;
        rightMascaraColor = Color.black;
        upperLipColor = Color.red;
        lowerLipColor = Color.red;
    }
}