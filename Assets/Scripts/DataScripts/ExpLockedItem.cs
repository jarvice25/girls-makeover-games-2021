﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpLockedItem {

    public string itemName;
    public string sceneName;
    public int unlockLevel;
    public Sprite itemImage;

    public ExpLockedItem(string itmName, string scnName, int level, Sprite image)
    {
        itemName = itmName;
        sceneName = scnName;
        unlockLevel = level;
        itemImage = image;
    }
}
