﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimEventsHair : MonoBehaviour {

	public void ReturnCanPlayAnimationToTrue()
	{
		if(Application.loadedLevelName.Equals("HairAndMakeUp"))
		{
			HairAndMakeUpManager.canPlayAnimation = true;
		}
		else if(Application.loadedLevelName.Equals("BeautySalon"))
		{
			BeautySalonManager.canPlayAnimation = true;
		}

	}
}
