﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
//using System.Diagnostics;

public class HairAndMakeUpManager : MonoBehaviour
{

    int currentNubOfScene = 0;
    int currentSlideToolIndex = -1;
    public static bool canPlayAnimation = true;
    public static GameObject lastObject;
    public GameObject lastEyeColor, lastEyeShadowColor, lastEyeLinerColor, lastMascaraColor, lastBlushColor, lastLipStickColor, lastHairStyle;
    public Transform ToolsHolder, SlideToolTransform;
    public GameObject ButtonLeft;
    //FIXME Nidza
    [Header("Character")]
    public Transform characterHolder;
    public GameObject[] characterPrefabs;
    [Header("Hairs")]
    public DressUpItemPrefabScriptableObject hair;
    [Header("SelectionSprites")]
    public Sprite normalSprite;
    public Sprite selectedSprite;

    //Delegates
    public delegate void NextSceneDel();
    public event NextSceneDel OnNextScene;
    public delegate void PreviousSceneDel();
    public event PreviousSceneDel OnPreviousScene;

    GameObject character;
    // Use this for initialization
    //FIXME Nidza
    IEnumerator Start()
    {
        character = Instantiate(characterPrefabs[GlobalVariables.selectedCharacterIndex[GlobalVariables.currentPlayerIndex]], characterHolder) as GameObject;
        character.transform.localScale = new Vector3(3.5f, 3.5f, 1f);
        character.transform.localPosition = new Vector3(5000f, 0f, 0f);
        yield return null;
        //Set character for BeautySalon
        character.GetComponent<CharacterSetUp>().SetUpCharacter(GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex], GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex]);
        character.transform.localPosition = Vector3.zero;
        character.transform.GetChild(0).GetComponent<Animator>().Play("CharacterArriving");

        //        GlobalVariables.curentNativeAd = ToolsHolder.GetChild(currentNubOfScene).GetComponentInChildren<FacebookNativeAd>();
        //        if (GlobalVariables.curentNativeAd != null)
        //            GlobalVariables.curentNativeAd.LoadAdWithDelay(1f);
        ButtonLeft.SetActive(false);
    }

    public void NextScene()
    {
        //        GameObject.Find("Canvas").GetComponent<MenuManager>().AddExp(GlobalVariables.CollectXpFromBuffer());

        DisableButtons();
        //FIXME Nidza
        canPlayAnimation = true;

        if (lastObject != null)
            lastObject.SetActive(false);

        for (int i = 0; i < 7; i++)
        {
            if (SlideToolTransform.GetChild(i).gameObject.activeSelf)
            {
                SlideToolTransform.parent.parent.GetComponent<Animator>().Play("SideLeftToolDeparting");
                break;
            }
        }
        if (currentSlideToolIndex != -1)
            SlideToolTransform.GetChild(currentSlideToolIndex).gameObject.SetActive(false);
        currentSlideToolIndex = -1;
        if (currentNubOfScene < ToolsHolder.childCount - 2)
        {
            ToolsHolder.GetChild(currentNubOfScene).GetComponent<Animator>().Play("ScenesDepart");

            //            if (GlobalVariables.curentNativeAd != null)
            //                GlobalVariables.curentNativeAd.HideNativeAd();

            currentNubOfScene++;

            //            GlobalVariables.curentNativeAd = ToolsHolder.GetChild(currentNubOfScene).GetComponentInChildren<FacebookNativeAd>();
            //            if (GlobalVariables.curentNativeAd != null)
            //                GlobalVariables.curentNativeAd.LoadAdWithDelay(1f);

            if (currentNubOfScene == 1)
                ButtonLeft.SetActive(true);
            ToolsHolder.GetChild(currentNubOfScene).GetComponent<Animator>().Play("ScenesArrival");
            //FIXME Nidza
            if (OnNextScene != null)
                OnNextScene();
        }
        else
        {
            //            if (GlobalVariables.curentNativeAd != null)
            //                GlobalVariables.curentNativeAd.HideNativeAd();
            if (AdsManager.Instance != null)
                AdsManager.Instance.ShowInterstitial();

            Debug.Log("Ucitaj novu scenu");
            if (GlobalVariables.currentPlayerIndex == 0)
            {
                if (AdsManager.Instance != null)
                    AdsManager.Instance.ShowInterstitial();
                GameObject.Find("Canvas").GetComponent<MenuManager>().LoadAdequateScene("DressUp");
            }
            else
            {
                if (AdsManager.Instance != null)
                    AdsManager.Instance.ShowInterstitial();
                GameObject.Find("Canvas").GetComponent<MenuManager>().LoadAdequateScene("DressUpPunk");
            }
        }

        if (currentNubOfScene == 4)
        {
            if (AdsManager.Instance != null)
            {
                AdsManager.Instance.ShowInterstitial();
            }
        }

        Invoke("EnableButtons", 1f);
    }

    void EnableButtons()
    {
        ButtonLeft.transform.parent.Find("ButtonArrowRight").GetComponentInChildren<Button>().enabled = true;
        ButtonLeft.GetComponentInChildren<Button>().enabled = true;
    }

    void DisableButtons()
    {
        ButtonLeft.transform.parent.Find("ButtonArrowRight").GetComponentInChildren<Button>().enabled = false;
        ButtonLeft.GetComponentInChildren<Button>().enabled = false;
    }

    public void PreviousScene()
    {
        DisableButtons();
        //FIXME Nidza
        canPlayAnimation = true;

        if (lastObject != null)
            lastObject.SetActive(false);

        for (int i = 0; i < 7; i++)
        {
            if (SlideToolTransform.GetChild(i).gameObject.activeSelf)
            {
                SlideToolTransform.parent.parent.GetComponent<Animator>().Play("SideLeftToolDeparting");
                break;
            }
        }
        if (currentSlideToolIndex != -1)
            SlideToolTransform.GetChild(currentSlideToolIndex).gameObject.SetActive(false);
        currentSlideToolIndex = -1;
        if (currentNubOfScene > 0)
        {
            ToolsHolder.GetChild(currentNubOfScene).GetComponent<Animator>().Play("ScenesArrivalReverse");

            //            if (GlobalVariables.curentNativeAd != null)
            //                GlobalVariables.curentNativeAd.HideNativeAd();

            currentNubOfScene--;

            //            GlobalVariables.curentNativeAd = ToolsHolder.GetChild(currentNubOfScene).GetComponentInChildren<FacebookNativeAd>();
            //            if (GlobalVariables.curentNativeAd != null)
            //                GlobalVariables.curentNativeAd.LoadAdWithDelay(1f);

            if (currentNubOfScene == 0)
            {
                ButtonLeft.SetActive(false);
            }
            ToolsHolder.GetChild(currentNubOfScene).GetComponent<Animator>().Play("ScenesDepartReverse");
            //FIXME Nidza
            if (OnPreviousScene != null)
                OnPreviousScene();
        }

        //        GameObject.Find("Canvas").GetComponent<MenuManager>().AddExp(GlobalVariables.CollectXpFromBuffer());

        Invoke("EnableButtons", 1f);
    }

    public void SelectObject()
    {
        if (lastObject != null)
            lastObject.SetActive(false);
        EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).GetChild(0).gameObject.SetActive(true);
        lastObject = EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).GetChild(0).gameObject;

        if (SoundManager.Instance != null)
            SoundManager.Instance.Play_ButtonClick();
    }

    public void ChangeSlideTool(int index)
    {
        if (index != currentSlideToolIndex)
            StartCoroutine("ChangeSlideToolCoorutine", index);

        if (index == 0)
            GameObject.Find("Canvas/HandTutorialHolder/AnimationHolder").GetComponent<Animator>().SetInteger("TutorialState", 1);

        if (SoundManager.Instance != null)
            SoundManager.Instance.Play_ButtonClick5();
    }

    IEnumerator ChangeSlideToolCoorutine(int index)
    {
        while (!canPlayAnimation)
            yield return null;

        if (currentSlideToolIndex != -1)
        {
            SlideToolTransform.GetChild(currentSlideToolIndex).gameObject.SetActive(false);
        }

        currentSlideToolIndex = index;
        SlideToolTransform.GetChild(currentSlideToolIndex).gameObject.SetActive(true);
        canPlayAnimation = false;
        SlideToolTransform.parent.parent.GetComponent<Animator>().Play("SideLeftToolArriving");

    }

    public void SelectEyeColor()
    {
        if (lastEyeColor != null)
        {
            lastEyeColor.SetActive(false);
            lastEyeColor.transform.parent.GetChild(2).GetComponent<Image>().sprite = normalSprite;
            EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(2).GetComponent<Image>().sprite = selectedSprite;
        }
        EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).gameObject.SetActive(true);
        lastEyeColor = EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).gameObject;

        if (SoundManager.Instance != null)
            SoundManager.Instance.Play_ButtonClick();

        GlobalVariables.AddXpToBuffer(EventSystem.current.currentSelectedGameObject.transform.parent.name);
        GlobalVariables.AddContestVotesItem(EventSystem.current.currentSelectedGameObject.transform.parent.gameObject);
    }

    public void SelectEyeShadowColor()
    {
        if (lastEyeShadowColor != null)
        {
            lastEyeShadowColor.SetActive(false);
            lastEyeShadowColor.transform.parent.GetChild(2).GetComponent<Image>().sprite = normalSprite;
            EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(2).GetComponent<Image>().sprite = selectedSprite;
        }
        EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).gameObject.SetActive(true);
        lastEyeShadowColor = EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).gameObject;

        if (SoundManager.Instance != null)
            SoundManager.Instance.Play_ButtonClick();

        GlobalVariables.AddXpToBuffer(EventSystem.current.currentSelectedGameObject.transform.parent.name);
    }

    public void SelectMascaraColor()
    {
        if (lastMascaraColor != null)
        {
            lastMascaraColor.SetActive(false);
            lastMascaraColor.transform.parent.GetChild(2).GetComponent<Image>().sprite = normalSprite;
            EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(2).GetComponent<Image>().sprite = selectedSprite;
        }
        EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).gameObject.SetActive(true);
        lastMascaraColor = EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).gameObject;

        if (SoundManager.Instance != null)
            SoundManager.Instance.Play_ButtonClick();

        GlobalVariables.AddXpToBuffer(EventSystem.current.currentSelectedGameObject.transform.parent.name);
    }

    public void SelectBlushColor()
    {
        if (lastBlushColor != null)
        {
            lastBlushColor.SetActive(false);
            lastBlushColor.transform.parent.GetChild(2).GetComponent<Image>().sprite = normalSprite;
            EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(2).GetComponent<Image>().sprite = selectedSprite;
        }
        EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).gameObject.SetActive(true);
        lastBlushColor = EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).gameObject;

        if (SoundManager.Instance != null)
            SoundManager.Instance.Play_ButtonClick();

        GlobalVariables.AddXpToBuffer(EventSystem.current.currentSelectedGameObject.transform.parent.name);
    }

    public void SelectEyeLinerColor()
    {
        if (lastEyeLinerColor != null)
        {
            lastEyeLinerColor.SetActive(false);
            lastEyeLinerColor.transform.parent.GetChild(2).GetComponent<Image>().sprite = normalSprite;
            EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(2).GetComponent<Image>().sprite = selectedSprite;
        }
        EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).gameObject.SetActive(true);
        lastEyeLinerColor = EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).gameObject;

        if (SoundManager.Instance != null)
            SoundManager.Instance.Play_ButtonClick();

        GlobalVariables.AddXpToBuffer(EventSystem.current.currentSelectedGameObject.transform.parent.name);
    }

    public void SelectLipStickColor()
    {
        if (lastLipStickColor != null)
        {
            lastLipStickColor.SetActive(false);
            lastLipStickColor.transform.parent.GetChild(2).GetComponent<Image>().sprite = normalSprite;
            EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(2).GetComponent<Image>().sprite = selectedSprite;
        }
        EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).gameObject.SetActive(true);
        lastLipStickColor = EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).gameObject;

        if (SoundManager.Instance != null)
            SoundManager.Instance.Play_ButtonClick();

        GlobalVariables.AddXpToBuffer(EventSystem.current.currentSelectedGameObject.transform.parent.name);
    }

    public void SelectHairStyle()
    {
        if (lastHairStyle != null)
        {
            lastHairStyle.SetActive(false);
            lastHairStyle.transform.parent.GetChild(2).gameObject.SetActive(false);
        }
        EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).gameObject.SetActive(true);
        EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(2).gameObject.SetActive(true);
        lastHairStyle = EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).gameObject;

        if (SoundManager.Instance != null)
            SoundManager.Instance.Play_ButtonClick();

        GlobalVariables.AddXpToBuffer(EventSystem.current.currentSelectedGameObject.transform.parent.name);
    }

    public void HideSideMenu()
    {
        canPlayAnimation = true;
        for (int i = 0; i < SlideToolTransform.childCount; i++)
        {
            if (SlideToolTransform.GetChild(i).gameObject.activeSelf)
            {
                SlideToolTransform.parent.parent.GetComponent<Animator>().Play("SideLeftToolDeparting");
                SlideToolTransform.GetChild(i).gameObject.SetActive(false);
                break;
            }
        }
        currentSlideToolIndex = -1;
    }

    public int GetCurrentScene()
    {
        return currentNubOfScene;
    }

    void OnDisable()
    {
        OnNextScene = null;
    }
}
