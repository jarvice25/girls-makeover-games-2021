﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
///<summary>
///<para>Scene:All/NameOfScene/NameOfScene1,NameOfScene2,NameOfScene3...</para>
///<para>Object:N/A</para>
///<para>Description: Sample Description </para>
///</summary>

public class ItemDragHandeler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {


	public bool canDrag;
    public bool freezeItem;
	public GameObject itemBeingDragged;
	public GameObject[] itemToHideOnDrag;
	public GameObject ObjectToInstantiate;
	Vector3 startPosition;
	GameObject newItem;
    //FIXME Nidza
    public delegate void ItemPickedUpDel();
    public event ItemPickedUpDel OnPickedUp;
    public delegate void ItemReleasedDel();
    public event ItemReleasedDel OnReleased;

	#region IBeginDragHandler implementation
	public void OnBeginDrag (PointerEventData eventData)
	{
		if(canDrag)
		{
			if(itemToHideOnDrag!=null)
			{
				for(int i=0;i<itemToHideOnDrag.Length;i++)
				{
					itemToHideOnDrag[i].SetActive(false);
				}
			}

            if(itemBeingDragged.transform.Find("InstanceHolder").childCount > 1)
                DestroyImmediate(itemBeingDragged.transform.Find("InstanceHolder").GetChild(0).gameObject);

			Vector3 screenPoint = (Input.mousePosition);
			screenPoint.z = 50.0f; //distance of the plane from the camera
			newItem = Instantiate(ObjectToInstantiate) as GameObject;
            newItem.transform.SetParent(itemBeingDragged.transform.Find("InstanceHolder"));
            //FIXME Nidza
            newItem.transform.SetAsFirstSibling();
            newItem.transform.localScale = Vector3.one;
            newItem.transform.GetComponent<RectTransform>().anchoredPosition3D = Vector3.zero;
            itemBeingDragged.transform.position = Camera.main.ScreenToWorldPoint (screenPoint);

            //FIXME Nidza
            if(OnPickedUp != null)
                OnPickedUp();
		}
	}
	#endregion

	#region IDragHandler implementation

	public void OnDrag (PointerEventData eventData)
	{
		if(canDrag)
		{
            Vector3 screenPoint = (Input.mousePosition);
			screenPoint.z = 50.0f; //distance of the plane from the camera
            //Top drag limit so we dont go under the banner
            if (screenPoint.y >= Screen.height * 0.7f)
                screenPoint.y = Screen.height * 0.7f;
			itemBeingDragged.transform.position = Camera.main.ScreenToWorldPoint (screenPoint);
		}
	}

	#endregion

	#region IEndDragHandler implementation

	public void OnEndDrag (PointerEventData eventData)
	{
		if(canDrag)
		{
            //FIXME Nidza
            if(OnReleased != null)
                OnReleased();
            
			RepositionObject();
		}
	}

	#endregion

	// Use this for initialization
	void Start () {
        startPosition = itemBeingDragged.transform.position;
	}

	public void RepositionObject()
	{
        if (freezeItem)
            return;
        Debug.Log("Reposition obj");
        itemBeingDragged.transform.position = startPosition;
		if(itemToHideOnDrag!=null)
		{
			for(int i=0;i<itemToHideOnDrag.Length;i++)
			{
				itemToHideOnDrag[i].SetActive(true);
			}
		}

        transform.GetChild(0).GetComponent<Animator>().Play("ItemArriving");
//		Debug.Log("RepositionObject "+itemBeingDragged.transform.GetChild(0).gameObject.name);
        if(itemBeingDragged.transform.Find("InstanceHolder").childCount > 1)
            DestroyImmediate(itemBeingDragged.transform.Find("InstanceHolder").GetChild(0).gameObject);
	}

    // We call same logic as OnEndDrag
    void OnApplicationPause(bool paused)
    {
        if(canDrag && paused)
        {
            //FIXME Nidza
            if(OnReleased != null)
                OnReleased();

            RepositionObject();
        }
    }

}

