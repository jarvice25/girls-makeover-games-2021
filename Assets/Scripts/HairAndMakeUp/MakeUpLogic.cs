﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using AdvancedMobilePaint;
using Anima2D;

public class MakeUpLogic : MonoBehaviour
{

    public Transform instanceHolder;
    [Header("Pattern textures")]
    public Texture2D eyeShadowPatternBaseTex;
    public Texture2D blushPatternBaseTex;
    Texture2D eyeLinerPatternBaseTex;
    //    [Header ("Eye outlines")]
    //    public Sprite[] openEyeOutlines;
    //    public Sprite[] closedEyeOutlines;
    [Header("Color palettes")]
    public ColorPalette eyeBallsColorPalette;
    public ColorPalette eyeShadowColorPalette;
    public ColorPalette eyeLinerColorPalette;
    public ColorPalette mascaraColorPalette;
    public ColorPalette blushColorPalette;
    public Color lipstickDefaultColor;
    public ColorPalette lipstickColorPalette;

    [Header("Sounds Holder")]
    public Transform soundsHolder;

    HairAndMakeUpManager hamm;
    CharacterSetUp characterSetUp;
    Transform itemPointOfAction;
    int currentPatternColorIndex;
    Texture2D currentPattern;
    //References and holders
    //    EyeOutlines eyeOutlines;
    AdvancedMobilePaint.AdvancedMobilePaint leftEyeShadowPaint;
    AdvancedMobilePaint.AdvancedMobilePaint rightEyeShadowPaint;
    AdvancedMobilePaint.AdvancedMobilePaint leftEyeLinerPaint;
    AdvancedMobilePaint.AdvancedMobilePaint rightEyeLinerPaint;
    AdvancedMobilePaint.AdvancedMobilePaint leftBlushPaint;
    AdvancedMobilePaint.AdvancedMobilePaint rightBlushPaint;
    //    Transform shimmersHolder;
    Transform mascaraParticlesHolder;
    Transform lipstickHolder;

    ItemDragHandeler[] allDraggableItems;
    Transform allButtonsHolder;
    Color clearColor = new Color(1, 1, 1, 0);
    Color currentMascaraColor = Color.black;
    Color currentLipstickColor;
    Image itemLipstickColorImage;
    Animator tutorialAnimator;
    AudioSource currentAudioSource;
    //HairStyles
    int currentHairTypeIndex;
    int currentHairStyleIndex;

    //names of last selected
    [HideInInspector]
    public GameObject eyeShadowColorObj;
    public GameObject eyeLinerColorObj;
    public GameObject blushColorObj;
    public GameObject mascaraColorObj;
    public GameObject lipstickColorObj;
    Button btn;

    void Awake()
    {
        tutorialAnimator = GameObject.Find("Canvas/HandTutorialHolder/AnimationHolder").GetComponent<Animator>();

        if (PlayerPrefs.HasKey("MakeUpTutorialShown"))
        {
            if (PlayerPrefs.GetString("MakeUpTutorialShown") == "true")
                tutorialAnimator.gameObject.SetActive(false);
        }

        hamm = GameObject.Find("HairAndMakeUpManager").GetComponent<HairAndMakeUpManager>();
        hamm.OnNextScene += delegate
            {
                if (hamm.GetCurrentScene() > 1)
                {
                    tutorialAnimator.gameObject.SetActive(false);
                    PlayerPrefs.SetString("MakeUpTutorialShown", "true");
                    PlayerPrefs.Save();
                }
                else
                {
                    tutorialAnimator.SetInteger("TutorialState", 3);
                }
            };

        hamm.OnPreviousScene += delegate
            {
                tutorialAnimator.gameObject.SetActive(false);
                PlayerPrefs.SetString("MakeUpTutorialShown", "true");
                PlayerPrefs.Save();
            };

        allDraggableItems = FindObjectsOfType<ItemDragHandeler>();
        allButtonsHolder = GameObject.Find("Canvas/HairAndMakeUp/AnimationHolder/SideToolsHolder/LeftToolsHolder/ToolsBG/Content").transform;
        //        Debug.Log("Brat naso itema : " + allDraggableItems.Length);
    }

    IEnumerator Start()
    {
        // Here we wait for end of frame because character needs to be instantiated
        yield return new WaitForEndOfFrame();
        characterSetUp = GameObject.Find("CharacterHolder").transform.GetChild(0).GetComponent<CharacterSetUp>();
        //        eyeOutlines = Resources.Load<EyeOutlines>("Outlines/EyeOutlines"+GlobalVariables.selectedCharacterIndex.ToString());
        eyeLinerPatternBaseTex = characterSetUp.eyeOutlines.eyeLiner[0];
        //Get references
        leftEyeShadowPaint = GameObject.Find("MakeUpTargetsHolder/LeftEyeShadow").GetComponent<AdvancedMobilePaint.AdvancedMobilePaint>();
        rightEyeShadowPaint = GameObject.Find("MakeUpTargetsHolder/RightEyeShadow").GetComponent<AdvancedMobilePaint.AdvancedMobilePaint>();
        leftEyeLinerPaint = GameObject.Find("MakeUpTargetsHolder/LeftEyeLiner").GetComponent<AdvancedMobilePaint.AdvancedMobilePaint>();
        rightEyeLinerPaint = GameObject.Find("MakeUpTargetsHolder/RightEyeLiner").GetComponent<AdvancedMobilePaint.AdvancedMobilePaint>();
        leftBlushPaint = GameObject.Find("MakeUpTargetsHolder/LeftBlush").GetComponent<AdvancedMobilePaint.AdvancedMobilePaint>();
        rightBlushPaint = GameObject.Find("MakeUpTargetsHolder/RightBlush").GetComponent<AdvancedMobilePaint.AdvancedMobilePaint>();
        //        shimmersHolder = GameObject.Find("MakeUpTargetsHolder/ShimmersHolder").transform;
        lipstickHolder = GameObject.Find("MakeUpTargetsHolder/Lipstick").transform;
        itemLipstickColorImage = GameObject.Find("Canvas/HairAndMakeUp/AnimationHolder/ToolsHolder/AnimationHolder/Scene7/LipstickHolder/AnimationHolder/Body/LipstickColor").GetComponent<Image>();
        mascaraParticlesHolder = GameObject.Find("MakeUpTargetsHolder/MascaraParticlesHolder").transform;

        yield return new WaitForEndOfFrame();
        //SetUpShimmers
        foreach (Transform shimmer in characterSetUp.shimmersHolder)
        {
            shimmer.gameObject.AddComponent<ColliderHelperClass>();
        }

        yield return new WaitForEndOfFrame();
        //SetUpLipstick
        lipstickHolder.GetChild(0).gameObject.AddComponent<ColliderHelperClass>();
        lipstickHolder.GetChild(1).gameObject.AddComponent<ColliderHelperClass>();

        //Creating itemPointForAction
        itemPointOfAction = new GameObject().transform;
        itemPointOfAction.SetParent(instanceHolder);
        itemPointOfAction.localPosition = Vector3.zero;
        itemPointOfAction.gameObject.AddComponent<Rigidbody2D>();
        itemPointOfAction.GetComponent<Rigidbody2D>().isKinematic = true;
        itemPointOfAction.gameObject.AddComponent<CircleCollider2D>();
        itemPointOfAction.GetComponent<CircleCollider2D>().radius = 0.1f;
        itemPointOfAction.GetComponent<Collider2D>().enabled = false;
        itemPointOfAction.GetComponent<Collider2D>().isTrigger = true;
        itemPointOfAction.name = "ItemPointOfAction";
        //Set up transparent texture and order in layer
        SetUpPaintForGameObject(leftEyeShadowPaint, AdvancedMobilePaint.PaintUtils.ConvertSpriteToTexture2D(GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].leftEyeShadowSprite), 13);
        SetUpPaintForGameObject(rightEyeShadowPaint, AdvancedMobilePaint.PaintUtils.ConvertSpriteToTexture2D(GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].rightEyeShadowSprite), 13);
        SetUpPaintForGameObject(leftEyeLinerPaint, AdvancedMobilePaint.PaintUtils.ConvertSpriteToTexture2D(GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].leftEyeLinerSprite), 13);
        SetUpPaintForGameObject(rightEyeLinerPaint, AdvancedMobilePaint.PaintUtils.ConvertSpriteToTexture2D(GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].rightEyeLinerSprite), 13);
        SetUpPaintForGameObject(leftBlushPaint, AdvancedMobilePaint.PaintUtils.ConvertSpriteToTexture2D(GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].leftBlushSprite), 13);
        SetUpPaintForGameObject(rightBlushPaint, AdvancedMobilePaint.PaintUtils.ConvertSpriteToTexture2D(GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].rightBlushSprite), 13);
        //Init GlobalVariables
        yield return new WaitForEndOfFrame();
        if (GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].eyeColor == Color.blue)
            ColorUtility.TryParseHtmlString("#2EC2FEFF", out GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].eyeColor);
        GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].leftMascaraOpenSprite = characterSetUp.eyeOutlines.eyeOutlinesOpen[0];
        GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].leftMascaraClosedSprite = characterSetUp.eyeOutlines.eyeOutlinesClosed[0];
        GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].rightMascaraOpenSprite = characterSetUp.eyeOutlines.eyeOutlinesOpen[0];
        GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].rightMascaraClosedSprite = characterSetUp.eyeOutlines.eyeOutlinesClosed[0];
        if (GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].upperLipColor == Color.red)
            GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].upperLipColor = characterSetUp.upperLipHolder.GetComponent<Anima2D.SpriteMeshInstance>().color;
        if (GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].lowerLipColor == Color.red)
            GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].lowerLipColor = characterSetUp.lowerLipHolder.GetComponent<Anima2D.SpriteMeshInstance>().color;

        yield return new WaitForEndOfFrame();
        SetItemOnSceneSwitch();
        //        ItemsMakeUpReleased();

        //Set up Buttons
        SubscribeToButtons();
        //Subscribe methods
        SubscribeMethods();

        yield return new WaitForSeconds(1f);
        PlaySentence("TimeToEnhanceNaturalBeauty");
    }

    #region OnPickedUpMakeUpMethods
    public void ItemEyeShadowRemoverPickedUp()
    {
        CharacterEyesClosed();
        //Set clear color
        leftEyeShadowPaint.SetVectorBrush(VectorBrush.Circle, 20, 20, clearColor, null, false, false, false, true);
        rightEyeShadowPaint.SetVectorBrush(VectorBrush.Circle, 20, 20, clearColor, null, false, false, false, true);
        //Enable paint
        leftEyeShadowPaint.drawEnabled = true;
        leftEyeShadowPaint.GetComponent<MeshCollider>().enabled = true;
        rightEyeShadowPaint.drawEnabled = true;
        rightEyeShadowPaint.GetComponent<MeshCollider>().enabled = true;
        EnableShimmerColliders();
        tutorialAnimator.gameObject.SetActive(false);
    }

    public void ItemEyeShadowPickedUp()
    {
        CharacterEyesClosed();
        itemPointOfAction.position += new Vector3(-0.3f, 0.3f, 0);
        //Set up left eye shadow paint
        leftEyeShadowPaint.raySource = itemPointOfAction;
        leftEyeShadowPaint.useAlternativeRay = true;
        leftEyeShadowPaint.drawEnabled = true;
        leftEyeShadowPaint.GetComponent<MeshCollider>().enabled = true;
        //Set up right eye shadow paint
        rightEyeShadowPaint.raySource = itemPointOfAction;
        rightEyeShadowPaint.useAlternativeRay = true;
        rightEyeShadowPaint.drawEnabled = true;
        rightEyeShadowPaint.GetComponent<MeshCollider>().enabled = true;
        EnableShimmerColliders();
        tutorialAnimator.gameObject.SetActive(false);
    }

    public void ItemEyeShadowShimmerPickedUp()
    {
        CharacterEyesClosed();
        itemPointOfAction.position += new Vector3(-0.5f, 0.5f, 0);
        EnableShimmerColliders();
        tutorialAnimator.gameObject.SetActive(false);
    }

    public void ItemEyeLinerRemoverPickedUp()
    {
        itemPointOfAction.position += new Vector3(0, 0, 0);
        //Set clear color
        leftEyeLinerPaint.SetVectorBrush(VectorBrush.Circle, 20, 20, clearColor, null, false, false, false, true);
        rightEyeLinerPaint.SetVectorBrush(VectorBrush.Circle, 20, 20, clearColor, null, false, false, false, true);
        //Enable paint
        leftEyeLinerPaint.drawEnabled = true;
        leftEyeLinerPaint.GetComponent<MeshCollider>().enabled = true;
        rightEyeLinerPaint.drawEnabled = true;
        rightEyeLinerPaint.GetComponent<MeshCollider>().enabled = true;
        EnableShimmerColliders();
    }

    public void ItemEyeLinerPickedUp()
    {
        itemPointOfAction.position += new Vector3(-0.53f, 0.8f, 0);
        //Set up left eye liner paint
        leftEyeLinerPaint.raySource = itemPointOfAction;
        leftEyeLinerPaint.useAlternativeRay = true;
        leftEyeLinerPaint.drawEnabled = true;
        leftEyeLinerPaint.GetComponent<MeshCollider>().enabled = true;
        //Set up right eye liner paint
        rightEyeLinerPaint.raySource = itemPointOfAction;
        rightEyeLinerPaint.useAlternativeRay = true;
        rightEyeLinerPaint.drawEnabled = true;
        rightEyeLinerPaint.GetComponent<MeshCollider>().enabled = true;
        EnableShimmerColliders();
    }

    public void ItemMascaraRemoverPickedUp()
    {
        itemPointOfAction.position += new Vector3(0, 0, 0);
        EnableShimmerColliders();
    }

    public void ItemMascaraPickedUp()
    {
        itemPointOfAction.position += new Vector3(-0.5f, 0.5f, 0);
        EnableShimmerColliders();
    }

    public void ItemBlushRemoverPickedUp()
    {
        //Set clear color
        leftBlushPaint.SetVectorBrush(VectorBrush.Circle, 20, 20, clearColor, null, false, false, false, true);
        rightBlushPaint.SetVectorBrush(VectorBrush.Circle, 20, 20, clearColor, null, false, false, false, true);
        //Enable paint
        leftBlushPaint.drawEnabled = true;
        leftBlushPaint.GetComponent<MeshCollider>().enabled = true;
        rightBlushPaint.drawEnabled = true;
        rightBlushPaint.GetComponent<MeshCollider>().enabled = true;
        EnableShimmerColliders();
    }

    public void ItemBlushPickedUp()
    {
        itemPointOfAction.position += new Vector3(-0.4f, 0.6f, 0);
        //Set up left blush paint
        leftBlushPaint.raySource = itemPointOfAction;
        leftBlushPaint.useAlternativeRay = true;
        leftBlushPaint.drawEnabled = true;
        leftBlushPaint.GetComponent<MeshCollider>().enabled = true;
        //Set up right blush paint
        rightBlushPaint.raySource = itemPointOfAction;
        rightBlushPaint.useAlternativeRay = true;
        rightBlushPaint.drawEnabled = true;
        rightBlushPaint.GetComponent<MeshCollider>().enabled = true;
        EnableShimmerColliders();
    }

    public void ItemBlushShimmerPickedUp()
    {
        itemPointOfAction.position += new Vector3(-0.4f, 0.6f, 0);
        EnableShimmerColliders();
    }

    public void ItemLipstickRemoverPickedUp()
    {
        itemPointOfAction.position += new Vector3(0, 0, 0);
        lipstickHolder.GetChild(0).GetComponent<Collider2D>().enabled = true;
        lipstickHolder.GetChild(1).GetComponent<Collider2D>().enabled = true;
        EnableShimmerColliders();
    }

    public void ItemLipstickPickedUp()
    {
        itemPointOfAction.position += new Vector3(-0.45f, 0.65f, 0);
        lipstickHolder.GetChild(0).GetComponent<Collider2D>().enabled = true;
        lipstickHolder.GetChild(1).GetComponent<Collider2D>().enabled = true;
        itemPointOfAction.GetComponent<Collider2D>().enabled = true;
        itemPointOfAction.GetComponent<CircleCollider2D>().radius = 0.05f;
        //Set lipstick color on instantiated lipstick object
        itemPointOfAction.transform.parent.GetChild(0).GetChild(0).GetComponent<Image>().color = currentLipstickColor;
        EnableShimmerColliders();
    }

    public void ItemLipstickShimmerPickedUp()
    {
        itemPointOfAction.position += new Vector3(-0.35f, 0.55f, 0);
        EnableShimmerColliders();
    }

    public void ItemHairSprayPickedUp()
    {
        itemPointOfAction.position += new Vector3(-0.35f, 1f, 0);
        EnableShimmerColliders();
    }
    #endregion  //OnPickedUpMakeUpMethods

    #region OnReleasedMethods
    public void ItemsMakeUpReleased()
    {
        CharacterEyesOpen();
        itemPointOfAction.localPosition = Vector3.zero;
        itemPointOfAction.GetComponent<CircleCollider2D>().radius = 0.1f;
        //eye shadow
        leftEyeShadowPaint.drawEnabled = false;
        leftEyeShadowPaint.GetComponent<MeshCollider>().enabled = false;
        rightEyeShadowPaint.drawEnabled = false;
        rightEyeShadowPaint.GetComponent<MeshCollider>().enabled = false;
        //eye liner
        leftEyeLinerPaint.drawEnabled = false;
        leftEyeLinerPaint.GetComponent<MeshCollider>().enabled = false;
        rightEyeLinerPaint.drawEnabled = false;
        rightEyeLinerPaint.GetComponent<MeshCollider>().enabled = false;
        //blush
        leftBlushPaint.drawEnabled = false;
        leftBlushPaint.GetComponent<MeshCollider>().enabled = false;
        rightBlushPaint.drawEnabled = false;
        rightBlushPaint.GetComponent<MeshCollider>().enabled = false;
        //lipstick
        lipstickHolder.GetChild(0).GetComponent<Collider2D>().enabled = false;
        lipstickHolder.GetChild(1).GetComponent<Collider2D>().enabled = false;
        StopApplyingLipstick();
        //Shimmers
        DisableShimmerColliders();
    }
    #endregion //OnReleasedMethods

    #region ButtonCalls
    public void SetEyeBallsColor()
    {
        Debug.Log("Ochi");
        characterSetUp.leftEyeHolder.Find("EyeBall_L").GetComponent<SpriteRenderer>().color = eyeBallsColorPalette.colors[hamm.lastEyeColor.transform.parent.GetSiblingIndex()];
        characterSetUp.rightEyeHolder.Find("EyeBall_R").GetComponent<SpriteRenderer>().color = eyeBallsColorPalette.colors[hamm.lastEyeColor.transform.parent.GetSiblingIndex()];
        GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].eyeColor = eyeBallsColorPalette.colors[hamm.lastEyeColor.transform.parent.GetSiblingIndex()];

        tutorialAnimator.SetInteger("TutorialState", 2);
        PlaySentence("WowSuchMesmerizingEyes");
    }

    public void SetEyeShadowColor()
    {
        //Just in case of memory leak
        currentPattern = null;
        leftEyeShadowPaint.pattenTexture = null;
        rightEyeShadowPaint.pattenTexture = null;

        currentPattern = PaintUtils.CombineColorAndTex(128, 128, eyeShadowColorPalette.colors[hamm.lastEyeShadowColor.transform.parent.GetSiblingIndex()], eyeShadowPatternBaseTex);
        leftEyeShadowPaint.SetVectorBrush(VectorBrush.Circle, 9, 9, Color.white, currentPattern, true, false, false, true);
        rightEyeShadowPaint.SetVectorBrush(VectorBrush.Circle, 9, 9, Color.white, PaintUtils.FlippTexture(currentPattern), true, false, false, true);

        eyeShadowColorObj = hamm.lastEyeShadowColor.transform.parent.gameObject;
    }

    public void SetEyeLinerColor()
    {
        //Just in case of memory leak
        currentPattern = null;
        leftEyeLinerPaint.pattenTexture = null;
        rightEyeLinerPaint.pattenTexture = null;

        currentPattern = PaintUtils.CombineColorAndTex(128, 128, eyeLinerColorPalette.colors[hamm.lastEyeLinerColor.transform.parent.GetSiblingIndex()], eyeLinerPatternBaseTex);
        leftEyeLinerPaint.SetVectorBrush(VectorBrush.Circle, 5, 5, Color.white, currentPattern, false, false, false, true);
        rightEyeLinerPaint.SetVectorBrush(VectorBrush.Circle, 5, 5, Color.white, PaintUtils.FlippTexture(currentPattern), false, false, false, true);

        eyeLinerColorObj = hamm.lastEyeLinerColor.transform.parent.gameObject;
    }

    public void SetBlushColor()
    {
        //Just in case of memory leak
        currentPattern = null;
        leftBlushPaint.pattenTexture = null;
        rightBlushPaint.pattenTexture = null;

        currentPattern = PaintUtils.CombineColorAndTex(128, 128, blushColorPalette.colors[hamm.lastBlushColor.transform.parent.GetSiblingIndex()], blushPatternBaseTex);
        leftBlushPaint.SetVectorBrush(VectorBrush.Circle, 12, 12, Color.white, currentPattern, true, false, false, true);
        rightBlushPaint.SetVectorBrush(VectorBrush.Circle, 12, 12, Color.white, PaintUtils.FlippTexture(currentPattern), true, false, false, true);

        blushColorObj = hamm.lastBlushColor.transform.parent.gameObject;
    }

    public void SetLipstickColor()
    {
        if (currentLipstickColor == null)
            currentLipstickColor = lipstickDefaultColor;
        else
            currentLipstickColor = lipstickColorPalette.colors[hamm.lastLipStickColor.transform.parent.GetSiblingIndex()];

        itemLipstickColorImage.color = lipstickColorPalette.colors[hamm.lastLipStickColor.transform.parent.GetSiblingIndex()];

        lipstickColorObj = hamm.lastLipStickColor.transform.parent.gameObject;

        PlaySentence("WhatABeautyfullLipstickColor");
    }

    public void SetMascaraColor()
    {
        currentMascaraColor = mascaraColorPalette.colors[EventSystem.current.currentSelectedGameObject.transform.parent.GetSiblingIndex()];
        mascaraColorObj = EventSystem.current.currentSelectedGameObject.transform.parent.gameObject;
    }

    public void SetHairPreset()
    {
        int buttonIndex = hamm.lastHairStyle.transform.parent.GetSiblingIndex();//-1;
        hamm.characterHolder.GetChild(0).GetComponent<CharacterSetUp>().SetHair(hamm.hair.elements[buttonIndex].item);

        GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].hairPrefab = hamm.hair.elements[buttonIndex];
        PlaySentence("MyHairLooksAmazing");
    }
    #endregion  //ButtonCalls

    #region ColliderHelperClassCalls
    public void ApplyLipstick(bool upper)
    {
        if (upper)
            StartCoroutine(CApplyLipstick(currentLipstickColor, characterSetUp.upperLipHolder.GetComponent<SpriteMeshInstance>()));
        else
            StartCoroutine(CApplyLipstick(currentLipstickColor, characterSetUp.lowerLipHolder.GetComponent<SpriteMeshInstance>()));
    }

    public void RemoveLipstick()
    {
        StopAllCoroutines();
        StartCoroutine(CApplyLipstick(lipstickDefaultColor, characterSetUp.upperLipHolder.GetComponent<SpriteMeshInstance>()));
        StartCoroutine(CApplyLipstick(lipstickDefaultColor, characterSetUp.lowerLipHolder.GetComponent<SpriteMeshInstance>()));
    }

    public void StopApplyingLipstick()
    {
        StopAllCoroutines();
    }

    IEnumerator CApplyLipstick(Color lipstickColor, SpriteMeshInstance lip)
    {
        Color lipColor = lip.color;
        float ElapsedTime = 0.0f;
        float TotalTime = 1.0f;
        while (ElapsedTime < TotalTime)
        {
            ElapsedTime += Time.deltaTime;
            lip.color = Color.Lerp(lipColor, lipstickColor, (ElapsedTime / TotalTime));
            yield return null;

            if (lip.transform.name.Contains("Up"))
                GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].upperLipColor = lip.color;
            else
                GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].lowerLipColor = lip.color;
        }

        yield return new WaitForEndOfFrame();


    }

    public void SetEyeOutlineSize(int i, bool leftEye, bool playParticle)
    {
        Debug.Log("SetEyeOutline");
        if (leftEye)
        {
            //Set left eye outline size
            characterSetUp.leftEyeHolder.Find("S_EyeOutlineClosed_L").GetComponent<SpriteRenderer>().sprite = characterSetUp.eyeOutlines.eyeOutlinesClosed[i];
            characterSetUp.leftEyeHolder.Find("S_EyeOutlineClosed_L").GetComponent<SpriteRenderer>().color = currentMascaraColor;
            characterSetUp.leftEyeHolder.Find("S_EyeOutlineOpen_L").GetComponent<SpriteRenderer>().sprite = characterSetUp.eyeOutlines.eyeOutlinesOpen[i];
            characterSetUp.leftEyeHolder.Find("S_EyeOutlineOpen_L").GetComponent<SpriteRenderer>().color = currentMascaraColor;
            //Save
            GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].leftMascaraClosedSprite = characterSetUp.eyeOutlines.eyeOutlinesClosed[i];
            GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].leftMascaraOpenSprite = characterSetUp.eyeOutlines.eyeOutlinesOpen[i];
            GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].leftMascaraColor = currentMascaraColor;
            //Play particle
            if (playParticle)
                mascaraParticlesHolder.transform.GetChild(0).GetComponent<Animator>().Play("MoveParticle", 0, 0);
        }
        else
        {
            //Set right eye outline size
            characterSetUp.rightEyeHolder.Find("S_EyeOutlineClosed_R").GetComponent<SpriteRenderer>().sprite = characterSetUp.eyeOutlines.eyeOutlinesClosed[i];
            characterSetUp.rightEyeHolder.Find("S_EyeOutlineClosed_R").GetComponent<SpriteRenderer>().color = currentMascaraColor;
            characterSetUp.rightEyeHolder.Find("S_EyeOutlineOpen_R").GetComponent<SpriteRenderer>().sprite = characterSetUp.eyeOutlines.eyeOutlinesOpen[i];
            characterSetUp.rightEyeHolder.Find("S_EyeOutlineOpen_R").GetComponent<SpriteRenderer>().color = currentMascaraColor;
            //Save
            GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].rightMascaraClosedSprite = characterSetUp.eyeOutlines.eyeOutlinesClosed[i];
            GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].rightMascaraOpenSprite = characterSetUp.eyeOutlines.eyeOutlinesOpen[i];
            GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].rightMascaraColor = currentMascaraColor;
            //Play particle
            if (playParticle)
                mascaraParticlesHolder.transform.GetChild(1).GetComponent<Animator>().Play("MoveParticle", 0, 0);
        }
    }
    public void SetDefaultOutline(bool leftEye)
    {
        if (leftEye)
        {
            //Set left eye outline size
            characterSetUp.leftEyeHolder.Find("S_EyeOutlineClosed_L").GetComponent<SpriteRenderer>().sprite = characterSetUp.eyeOutlines.eyeOutlinesClosed[0];
            characterSetUp.leftEyeHolder.Find("S_EyeOutlineClosed_L").GetComponent<SpriteRenderer>().color = Color.black;
            characterSetUp.leftEyeHolder.Find("S_EyeOutlineOpen_L").GetComponent<SpriteRenderer>().sprite = characterSetUp.eyeOutlines.eyeOutlinesOpen[0];
            characterSetUp.leftEyeHolder.Find("S_EyeOutlineOpen_L").GetComponent<SpriteRenderer>().color = Color.black;
        }
        else
        {
            //Set right eye outline size
            characterSetUp.rightEyeHolder.Find("S_EyeOutlineClosed_R").GetComponent<SpriteRenderer>().sprite = characterSetUp.eyeOutlines.eyeOutlinesClosed[0];
            characterSetUp.rightEyeHolder.Find("S_EyeOutlineClosed_R").GetComponent<SpriteRenderer>().color = Color.black;
            characterSetUp.rightEyeHolder.Find("S_EyeOutlineOpen_R").GetComponent<SpriteRenderer>().sprite = characterSetUp.eyeOutlines.eyeOutlinesOpen[0];
            characterSetUp.rightEyeHolder.Find("S_EyeOutlineOpen_R").GetComponent<SpriteRenderer>().color = Color.black;
        }
    }
    #endregion //ColliderHelperClassCalls

    #region helperMethods
    //FIXME this is used for setting up the AMP for the first time for each item
    void SetItemOnSceneSwitch()
    {
        switch (hamm.GetCurrentScene())
        {
            case 0:
                //                SetEyeBallsColor();
                break;
            case 1:
                PlaySentence("LetsPutSomeColorOnMyEyelids");
                SetEyeShadowColor();

                break;
            case 2:
                break;
            case 3:
                PlaySentence("EyelinerShouldMakeMyEyesPop");
                SetEyeLinerColor();

                break;
            case 4:
                currentMascaraColor = mascaraColorPalette.colors[0];
                mascaraColorObj = hamm.lastMascaraColor.transform.parent.gameObject;
                break;
            case 5:
                PlaySentence("NowTheEyeMakeUpIsComplete");
                SetBlushColor();
                break;
            case 6:
                SetLipstickColor();
                characterSetUp.headTowelHolder.SetActive(true);
                characterSetUp.hairHolder.SetActive(false);
                break;
            case 7:
                SaveMakeUp();
                characterSetUp.headTowelHolder.SetActive(false);
                characterSetUp.hairHolder.SetActive(true);
                break;
            default:
                Debug.Log("Scene not handeled: " + hamm.GetCurrentScene());
                break;
        }
    }

    void SubscribeMethods()
    {
        //        Debug.Log("Ce sabskrajbuje");
        hamm.OnNextScene += SetItemOnSceneSwitch;
        hamm.OnPreviousScene += SetItemOnSceneSwitch;

        foreach (ItemDragHandeler item in allDraggableItems)
        {
            item.OnPickedUp += SoundManager.Instance.Play_ItemPickUp;

            switch (item.name)
            {
                case "EyeShadowRemoverHolder":
                    item.OnPickedUp += ItemEyeShadowRemoverPickedUp;
                    item.OnReleased += SetEyeShadowColor;
                    break;
                case "EyeShadowHolder":
                    item.OnPickedUp += ItemEyeShadowPickedUp;
                    break;
                case "EyeShadowShimmerHolder":
                case "EyeLinerShimmerHolder":
                    item.OnPickedUp += ItemEyeShadowShimmerPickedUp;
                    break;
                case "EyeLinerHolder":
                    item.OnPickedUp += ItemEyeLinerPickedUp;
                    break;
                case "EyeLinerRemoverHolder":
                    item.OnPickedUp += ItemEyeLinerRemoverPickedUp;
                    item.OnReleased += SetEyeLinerColor;
                    break;
                case "MascaraSize1Holder":
                case "MascaraSize2Holder":
                case "MascaraSize3Holder":
                    item.OnPickedUp += ItemMascaraPickedUp;
                    break;
                case "MascaraRemoverHolder":
                    item.OnPickedUp += ItemMascaraRemoverPickedUp;
                    break;
                case "BlushRemoverHolder":
                    item.OnPickedUp += ItemBlushRemoverPickedUp;
                    item.OnReleased += SetBlushColor;
                    break;
                case "BlushHolder":
                    item.OnPickedUp += ItemBlushPickedUp;
                    break;
                case "BlushShimmerHolder":
                    item.OnPickedUp += ItemBlushShimmerPickedUp;
                    break;
                case "LipstickRemoverHolder":
                    item.OnPickedUp += ItemLipstickRemoverPickedUp;
                    break;
                case "LipstickHolder":
                    item.OnPickedUp += ItemLipstickPickedUp;
                    break;
                case "LipstickShimmerHolder":
                    item.OnPickedUp += ItemLipstickShimmerPickedUp;
                    break;
                case "HairSprayShimmerHolder":
                    item.OnPickedUp += ItemHairSprayPickedUp;
                    break;
                default:
                    Debug.Log("Item not handeled: " + item.name);
                    break;
            }
            //Same method for all items;
            item.OnReleased += ItemsMakeUpReleased;
        }
    }

    void UnsubscribeMethods()
    {
        hamm.OnNextScene -= SetItemOnSceneSwitch;
        hamm.OnPreviousScene -= SetItemOnSceneSwitch;

        foreach (ItemDragHandeler item in allDraggableItems)
        {
            switch (item.name)
            {
                case "EyeShadowRemoverHolder":
                    item.OnPickedUp -= ItemEyeShadowRemoverPickedUp;
                    item.OnReleased -= SetEyeShadowColor;
                    break;
                case "EyeShadowHolder":
                    item.OnPickedUp -= ItemEyeShadowPickedUp;
                    break;
                case "EyeShadowShimmerHolder":
                case "EyeLinerShimmerHolder":
                    item.OnPickedUp -= ItemEyeShadowShimmerPickedUp;
                    item.OnReleased -= ItemsMakeUpReleased;
                    break;
                case "EyeLinerHolder":
                    item.OnPickedUp -= ItemEyeLinerPickedUp;
                    break;
                case "EyeLinerRemoverHolder":
                    item.OnPickedUp -= ItemEyeLinerRemoverPickedUp;
                    item.OnReleased -= SetEyeLinerColor;
                    break;
                case "MascaraSize1Holder":
                case "MascaraSize2Holder":
                case "MascaraSize3Holder":
                    item.OnPickedUp -= ItemMascaraPickedUp;
                    break;
                case "MascaraRemoverHolder":
                    item.OnPickedUp -= ItemMascaraRemoverPickedUp;
                    break;
                case "BlushRemoverHolder":
                    item.OnPickedUp -= ItemBlushRemoverPickedUp;
                    item.OnReleased -= SetBlushColor;
                    break;
                case "BlushHolder":
                    item.OnPickedUp -= ItemBlushPickedUp;
                    break;
                case "BlushShimmerHolder":
                    item.OnPickedUp -= ItemBlushShimmerPickedUp;
                    break;
                case "LipstickRemoverHolder":
                    item.OnPickedUp -= ItemLipstickRemoverPickedUp;
                    item.OnReleased -= SetLipstickColor;
                    break;
                case "LipstickHolder":
                    item.OnPickedUp -= ItemLipstickPickedUp;
                    break;
                case "LipstickShimmerHolder":
                    item.OnPickedUp -= ItemLipstickShimmerPickedUp;
                    break;
                case "HairSprayShimmerHolder":
                    item.OnPickedUp -= ItemHairSprayPickedUp;
                    break;
                default:
                    Debug.Log("Item not handeled: " + item.name);
                    break;
            }
            //Same method for all items;
            item.OnReleased -= ItemsMakeUpReleased;
        }
        //        Debug.Log("Time end: " + Time.deltaTime.ToString());
    }

    void SubscribeToButtons()
    {
        //        Debug.Log("Ce sabskrajbuje buttons");

        //Sub to eye color buttons
        foreach (Transform eyeBallsButtons in allButtonsHolder.Find("EyeColorsHolder").GetChild(0))
        {
            eyeBallsButtons.GetChild(3).GetComponent<Button>().onClick.AddListener(SetEyeBallsColor);
            if (eyeBallsButtons.GetChild(1).GetComponent<Image>().color == Color.white)
                eyeBallsButtons.GetChild(1).GetComponent<Image>().color = eyeBallsColorPalette.colors[eyeBallsButtons.GetSiblingIndex()];
        }
        //Sub to eye shadow color buttons
        foreach (Transform eyeShadowButtons in allButtonsHolder.Find("EyeShadowColorsHolder").GetChild(0))
        {
            eyeShadowButtons.GetChild(3).GetComponent<Button>().onClick.AddListener(SetEyeShadowColor);
            if (eyeShadowButtons.GetChild(1).GetComponent<Image>().color == Color.white)
                eyeShadowButtons.GetChild(1).GetComponent<Image>().color = eyeShadowColorPalette.colors[eyeShadowButtons.GetSiblingIndex()];
        }
        //Sub to eye liner color buttons
        foreach (Transform eyeLinerButtons in allButtonsHolder.Find("EyeLinerColorsHolder").GetChild(0))
        {
            eyeLinerButtons.GetChild(3).GetComponent<Button>().onClick.AddListener(SetEyeLinerColor);
            if (eyeLinerButtons.GetChild(1).GetComponent<Image>().color == Color.white)
                eyeLinerButtons.GetChild(1).GetComponent<Image>().color = eyeLinerColorPalette.colors[eyeLinerButtons.GetSiblingIndex()];
        }
        //Sub to mascara color buttons
        foreach (Transform mascaraButtons in allButtonsHolder.Find("MascaraColorsHolder").GetChild(0))
        {
            mascaraButtons.GetChild(3).GetComponent<Button>().onClick.AddListener(SetMascaraColor);
            if (mascaraButtons.GetChild(1).GetComponent<Image>().color == Color.white)
                mascaraButtons.GetChild(1).GetComponent<Image>().color = mascaraColorPalette.colors[mascaraButtons.GetSiblingIndex()];
        }
        //Sub to blush color buttons
        foreach (Transform blushButtons in allButtonsHolder.Find("BlushColorsHolder").GetChild(0))
        {
            blushButtons.GetChild(3).GetComponent<Button>().onClick.AddListener(SetBlushColor);
            if (blushButtons.GetChild(1).GetComponent<Image>().color == Color.white)
                blushButtons.GetChild(1).GetComponent<Image>().color = blushColorPalette.colors[blushButtons.GetSiblingIndex()];

            Color c = blushButtons.GetChild(1).GetComponent<Image>().color;
            c.a = 1;
            blushButtons.GetChild(1).GetComponent<Image>().color = c;
        }
        //Sub to lipstick color buttons
        foreach (Transform lipstickButtons in allButtonsHolder.Find("LipStickColorsHolder").GetChild(0))
        {
            lipstickButtons.GetChild(3).GetComponent<Button>().onClick.AddListener(SetLipstickColor);
            if (lipstickButtons.GetChild(1).GetComponent<Image>().color == Color.white)
                lipstickButtons.GetChild(1).GetComponent<Image>().color = lipstickColorPalette.colors[lipstickButtons.GetSiblingIndex()];
        }
        //Sub to hair style color buttons
        foreach (Transform hairStyle in allButtonsHolder.Find("HairStylesHolder").GetChild(0))
        {
            hairStyle.GetChild(4).GetComponent<Button>().onClick.AddListener(SetHairPreset);
        }

        transform.Find("PopUps/PopUpDialogRepeat/AnimationHolder/Body/ButtonsHolder/ButtonYes").GetComponent<Button>().onClick.AddListener(GlobalVariables.InitializeMainCharacterMakeUpData);
    }

    void UnsubscribeFromButtons()
    {
        //Unsub from eye color buttons
        foreach (Transform eyeBallsButtons in allButtonsHolder.Find("EyeColorsHolder").GetChild(0))
        {
            eyeBallsButtons.GetChild(3).GetComponent<Button>().onClick.RemoveListener(SetEyeBallsColor);
        }
        //Unsub from eye shadow color buttons
        foreach (Transform eyeShadowButtons in allButtonsHolder.Find("EyeShadowColorsHolder").GetChild(0))
        {
            eyeShadowButtons.GetChild(3).GetComponent<Button>().onClick.RemoveListener(SetEyeShadowColor);
        }
        //Unsub from eye liner color buttons
        foreach (Transform eyeLinerButtons in allButtonsHolder.Find("EyeLinerColorsHolder").GetChild(0))
        {
            eyeLinerButtons.GetChild(3).GetComponent<Button>().onClick.RemoveListener(SetEyeLinerColor);
        }
        //Unsub from mascara color buttons
        foreach (Transform mascaraButtons in allButtonsHolder.Find("MascaraColorsHolder").GetChild(0))
        {
            mascaraButtons.GetChild(3).GetComponent<Button>().onClick.RemoveListener(SetMascaraColor);
        }
        //Unsub from blush color buttons
        foreach (Transform blushButtons in allButtonsHolder.Find("BlushColorsHolder").GetChild(0))
        {
            blushButtons.GetChild(3).GetComponent<Button>().onClick.RemoveListener(SetBlushColor);
        }
        //Unsub from lipstick color buttons
        foreach (Transform lipstickButtons in allButtonsHolder.Find("LipStickColorsHolder").GetChild(0))
        {
            lipstickButtons.GetChild(3).GetComponent<Button>().onClick.RemoveListener(SetLipstickColor);
        }
        //Unsub from hair style color buttons
        foreach (Transform hairStyle in allButtonsHolder.Find("HairStylesHolder").GetChild(0))
        {
            hairStyle.GetChild(4).GetComponent<Button>().onClick.RemoveListener(SetHairPreset);
        }

        transform.Find("PopUps/PopUpDialogRepeat/AnimationHolder/Body/ButtonsHolder/ButtonYes").GetComponent<Button>().onClick.RemoveListener(GlobalVariables.InitializeMainCharacterMakeUpData);
    }

    void SetUpPaintForGameObject(AdvancedMobilePaint.AdvancedMobilePaint amp, Texture2D tex, int orderInLayer)
    {
        //        Debug.Log("Podesio paint");
        amp.GetComponent<MeshRenderer>().sortingOrder = orderInLayer;
        if (tex == null)
            amp.SetDrawingTexture(PaintUtils.GenerateColoredTex(128, 128, new Color(1, 1, 1, 0)));
        else
            amp.SetDrawingTexture(tex);
        amp.drawEnabled = false;
    }

    void CharacterEyesClosed()
    {
        characterSetUp.transform.GetChild(0).GetComponent<Animator>().enabled = false;
        //Close left eye
        characterSetUp.leftEyeHolder.Find("S_EyeClosedShadow_L").gameObject.SetActive(true);
        characterSetUp.leftEyeHolder.Find("S_EyeOutlineClosed_L").gameObject.SetActive(true);
        characterSetUp.leftEyeHolder.Find("S_EyeOutlineOpen_L").gameObject.SetActive(false);
        characterSetUp.leftEyeHolder.Find("S_EyeWhite_L").gameObject.SetActive(false);
        characterSetUp.leftEyeHolder.Find("EyeBall_L").gameObject.SetActive(false);
        //Close right eye
        characterSetUp.rightEyeHolder.Find("S_EyeClosedShadow_R").gameObject.SetActive(true);
        characterSetUp.rightEyeHolder.Find("S_EyeOutlineClosed_R").gameObject.SetActive(true);
        characterSetUp.rightEyeHolder.Find("S_EyeOutlineOpen_R").gameObject.SetActive(false);
        characterSetUp.rightEyeHolder.Find("S_EyeWhite_R").gameObject.SetActive(false);
        characterSetUp.rightEyeHolder.Find("EyeBall_R").gameObject.SetActive(false);
    }

    void CharacterEyesOpen()
    {
        //Open left eye
        characterSetUp.leftEyeHolder.Find("S_EyeClosedShadow_L").gameObject.SetActive(false);
        characterSetUp.leftEyeHolder.Find("S_EyeOutlineClosed_L").gameObject.SetActive(false);
        characterSetUp.leftEyeHolder.Find("S_EyeOutlineOpen_L").gameObject.SetActive(true);
        characterSetUp.leftEyeHolder.Find("S_EyeWhite_L").gameObject.SetActive(true);
        characterSetUp.leftEyeHolder.Find("EyeBall_L").gameObject.SetActive(true);
        //Open right eye
        characterSetUp.rightEyeHolder.Find("S_EyeClosedShadow_R").gameObject.SetActive(false);
        characterSetUp.rightEyeHolder.Find("S_EyeOutlineClosed_R").gameObject.SetActive(false);
        characterSetUp.rightEyeHolder.Find("S_EyeOutlineOpen_R").gameObject.SetActive(true);
        characterSetUp.rightEyeHolder.Find("S_EyeWhite_R").gameObject.SetActive(true);
        characterSetUp.rightEyeHolder.Find("EyeBall_R").gameObject.SetActive(true);

        characterSetUp.transform.GetChild(0).GetComponent<Animator>().enabled = true;
    }

    // When Shimmer or Remover item is picked up
    void EnableShimmerColliders()
    {
        itemPointOfAction.GetComponent<Collider2D>().enabled = true;

        foreach (Transform shimmer in characterSetUp.shimmersHolder)
        {
            shimmer.GetComponent<Collider2D>().enabled = true;
        }
    }

    // When Shimmer or Remover item is released
    public void DisableShimmerColliders()
    {
        itemPointOfAction.GetComponent<Collider2D>().enabled = false;

        foreach (Transform shimmer in characterSetUp.shimmersHolder)
        {
            shimmer.GetComponent<Collider2D>().enabled = false;
        }
    }

    public void SaveMakeUp()
    {
        //TODO Odraditi deo da se ovojene teksture prebace u sprajtove, povezu sa karakterom i sacuvaju za prenos po scenama
        GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].leftEyeShadowSprite = Sprite.Create(leftEyeShadowPaint.tex, new Rect(0.0f, 0.0f, leftEyeShadowPaint.tex.width, leftEyeShadowPaint.tex.height), new Vector2(0.5f, 0.5f));
        GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].rightEyeShadowSprite = Sprite.Create(rightEyeShadowPaint.tex, new Rect(0.0f, 0.0f, rightEyeShadowPaint.tex.width, rightEyeShadowPaint.tex.height), new Vector2(0.5f, 0.5f));
        GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].leftEyeLinerSprite = Sprite.Create(leftEyeLinerPaint.tex, new Rect(0.0f, 0.0f, leftEyeLinerPaint.tex.width, leftEyeLinerPaint.tex.height), new Vector2(0.5f, 0.5f));
        GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].rightEyeLinerSprite = Sprite.Create(rightEyeLinerPaint.tex, new Rect(0.0f, 0.0f, rightEyeLinerPaint.tex.width, rightEyeLinerPaint.tex.height), new Vector2(0.5f, 0.5f));
        GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].leftBlushSprite = Sprite.Create(leftBlushPaint.tex, new Rect(0.0f, 0.0f, leftBlushPaint.tex.width, leftBlushPaint.tex.height), new Vector2(0.5f, 0.5f));
        GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].rightBlushSprite = Sprite.Create(rightBlushPaint.tex, new Rect(0.0f, 0.0f, rightBlushPaint.tex.width, rightBlushPaint.tex.height), new Vector2(0.5f, 0.5f));

        Debug.Log("Safe make ap");
    }

    void PlaySentence(string sentence)
    {
        if (soundsHolder.Find(sentence) != null)
        {
            if (currentAudioSource != null)
                currentAudioSource.Stop();

            currentAudioSource = soundsHolder.Find(sentence).GetComponent<AudioSource>();
            currentAudioSource.Play();
        }
    }
    #endregion //HelperMethods

    void OnDisable()
    {
        //TODO Unsubscribe
        UnsubscribeMethods();
        UnsubscribeFromButtons();
    }

    void OnDestroy()
    {
        //TODO Unsubscribe
        UnsubscribeMethods();
        UnsubscribeFromButtons();
    }

}

public class ColliderHelperClass : MonoBehaviour
{

    ParticleSystem shimmerParticle;
    MakeUpLogic mul;
    bool leftCollider;

    void Awake()
    {
        mul = GameObject.Find("Canvas").GetComponent<MakeUpLogic>();
        if (transform.childCount > 0)
            shimmerParticle = transform.GetChild(0).GetComponent<ParticleSystem>();
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (name.Contains("Left"))
            leftCollider = true;
        else
            leftCollider = false;

        //        Debug.Log("Current obj " + EventSystem.current.currentSelectedGameObject.transform.parent.name);
        if (EventSystem.current.currentSelectedGameObject == null)
            return;
        //Check if should enable shimmer
        if (EnableShimmerCheck(EventSystem.current.currentSelectedGameObject.transform.parent))
        {
            if (shimmerParticle != null)
            {
                shimmerParticle.gameObject.SetActive(true);
                GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].shimmers[transform.GetSiblingIndex()] = true;
            }
        }
        //Check if should disable shimmer
        if (DisableShimmerCheck(EventSystem.current.currentSelectedGameObject.transform.parent))
        {
            if (shimmerParticle != null)
            {
                shimmerParticle.gameObject.SetActive(false);
                GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].shimmers[transform.GetSiblingIndex()] = false;
            }
        }
        //Check if eye outline size needs to be changed
        if (name.Contains("EyeShimmer") && other.transform.parent.GetChild(0).name.Contains("MascaraSize1"))
        {
            mul.SetEyeOutlineSize(0, leftCollider, true);
            GlobalVariables.AddXpToBuffer("Mascara");
            GlobalVariables.AddContestVotesItem(mul.mascaraColorObj);
        }
        else if (name.Contains("EyeShimmer") && other.transform.parent.GetChild(0).name.Contains("MascaraSize2"))
        {
            mul.SetEyeOutlineSize(1, leftCollider, true);
            GlobalVariables.AddXpToBuffer("Mascara");
            GlobalVariables.AddContestVotesItem(mul.mascaraColorObj);
        }
        else if (name.Contains("EyeShimmer") && other.transform.parent.GetChild(0).name.Contains("MascaraSize3"))
        {
            mul.SetEyeOutlineSize(2, leftCollider, true);
            GlobalVariables.AddXpToBuffer("Mascara");
            GlobalVariables.AddContestVotesItem(mul.mascaraColorObj);
        }
        else if (name.Contains("EyeShimmer") && EventSystem.current.currentSelectedGameObject.transform.parent.name.Contains("MascaraRemoverHolder"))
        {
            mul.SetDefaultOutline(leftCollider);
            GlobalVariables.AddXpToBuffer("Remover");
        }
        //Check if it is lipstick
        if (other.transform.parent.GetChild(0).name.Contains("LipStick"))
        {
            if (name.Contains("Upper"))
                mul.ApplyLipstick(true);
            if (name.Contains("Lower"))
                mul.ApplyLipstick(false);
            GlobalVariables.AddXpToBuffer("Lipstick");
            GlobalVariables.AddContestVotesItem(mul.lipstickColorObj);
        }
        else if (EventSystem.current.currentSelectedGameObject.transform.parent.name.Contains("LipstickRemoverHolder"))
        {
            mul.RemoveLipstick();
            GlobalVariables.AddXpToBuffer("Remover");
        }

        //Check for votes and xp
        if (other.transform.parent.GetChild(0).name.Contains("EyeShadow") && name.Contains("Eye"))
        {
            GlobalVariables.AddContestVotesItem(mul.eyeShadowColorObj);
            GlobalVariables.AddXpToBuffer("EyeShadow");
        }
        else if (other.transform.parent.GetChild(0).name.Contains("EyeLiner") && name.Contains("Eye"))
        {
            GlobalVariables.AddContestVotesItem(mul.eyeLinerColorObj);
            GlobalVariables.AddXpToBuffer("EyeLiner");
        }
        else if (other.transform.parent.GetChild(0).name.Contains("Blush") && name.Contains("Blush"))
        {
            GlobalVariables.AddContestVotesItem(mul.blushColorObj);
            GlobalVariables.AddXpToBuffer("Blush");
        }
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        //Check if it is lipstick
        if (other.transform.parent.GetChild(0).name.Contains("LipStick"))
        {
            mul.StopApplyingLipstick();
        }
    }

    bool EnableShimmerCheck(Transform go)
    {
        //Check if dragged itme is "Shimmer" type
        Debug.Log(go.name + "       " + name);
        if (go.name.Contains("Shimmer"))
        {
            GlobalVariables.AddXpToBuffer("Shimmer");
            //Check if current "Shimmer" item colided with apropriate object
            if ((go.name.Contains("EyeShadow") || go.name.Contains("EyeLiner")) && name.Contains("EyeShimmerHolder"))
                return true;

            if (go.name.Contains("Blush") && name.Contains("BlushShimmerHolder"))
                return true;

            if (go.name.Contains("Lipstick") && name.Contains("LipstickShimmerHolder"))
                return true;

            if (go.name.Contains("Hair") && name.Contains("Hair"))
                return true;
        }
        return false;
    }

    bool DisableShimmerCheck(Transform go)
    {
        //Check if dragged itme is "Remover" type
        //        Debug.Log(go.name + "       "+ name );
        if (go.name.Contains("Remover"))
        {
            GlobalVariables.AddXpToBuffer("Remover");
            //Check if current "Remover" item colided with apropriate object
            if ((go.name.Contains("EyeShadow") || go.name.Contains("EyeLiner")) && name.Contains("EyeShimmerHolder"))
                return true;

            if (go.name.Contains("Blush") && name.Contains("BlushShimmerHolder"))
                return true;

            if (go.name.Contains("Lipstick") && name.Contains("LipstickShimmerHolder"))
                return true;
        }

        return false;
    }
}