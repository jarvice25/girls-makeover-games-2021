﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectObject : MonoBehaviour, IPointerDownHandler {

	public void OnPointerDown (PointerEventData eventData)
	{
		if(HairAndMakeUpManager.lastObject!=null)
			HairAndMakeUpManager.lastObject.SetActive(false);
		Debug.Log(this.name);
		transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
		HairAndMakeUpManager.lastObject =transform.GetChild(0).GetChild(0).gameObject;
	}
		
}
