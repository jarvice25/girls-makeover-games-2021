﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AdvancedMobilePaint;
using System.IO;

public class ContestManager : MonoBehaviour
{

    [Header("Characters")]
    public GameObject[] characterPrefabs;

    //    [Header ("Opponents")]
    //    public CharacterDressUpData[] opponentsGroup0;
    //    public CharacterDressUpData[] opponentsGroup1;
    //    public CharacterDressUpData[] opponentsGroup2;
    //    public CharacterDressUpData[] opponentsGroup3;

    [Header("Stands")]
    public Transform[] standsHolder;
    public Animator bottomButtonsAnim;
    public Text[] points;
    public Text[] names;

    [Header("Make Up - Colors")]
    public ColorPalette eyeBallsColorPalette;
    public ColorPalette eyeShadowColorPalette;
    public ColorPalette eyeLinerColorPalette;
    public ColorPalette mascaraColorPalette;
    public ColorPalette blushColorPalette;
    public ColorPalette lipstickColorPalette;
    [Header("Make Up - Textures")]
    public Texture2D eyeShadowPatternBaseTex;
    public Texture2D blushPatternBaseTex;
    Texture2D eyeLinerPatternBaseTex;

    //    CharacterDressUpData[][] opponentGroups;
    CharacterDressUpData[] opponentData;
    //    List <string> opponentNames = new List<string>(){
    //        "Kim",
    //        "Michelle",
    //        "Cathy",
    //        "Carrie",
    //        "Stephanie",
    //        "Bella",
    //        "Kendall",
    //        "Kirsten",
    //        "Jenny",
    //        "Tanya"
    //    };


    List<Contestants> contestants = new List<Contestants>();
    int playerPlacement;
    MenuManager menuManager;

    IEnumerator Start()
    {
        bottomButtonsAnim = GameObject.Find("Canvas/ButtonsHolderBottom").GetComponent<Animator>();
        menuManager = GameObject.Find("Canvas").GetComponent<MenuManager>();
        //FIXME test      
        //        CharacterDressUpData[][] testOpponentGroups = new CharacterDressUpData[4][];
        //        testOpponentGroups[0] = Resources.LoadAll<CharacterDressUpData>("ContestOpponents/Prom");
        //        testOpponentGroups[1] = Resources.LoadAll<CharacterDressUpData>("ContestOpponents/BackToSchool");
        //        testOpponentGroups[2] = Resources.LoadAll<CharacterDressUpData>("ContestOpponents/DateNight");
        //        testOpponentGroups[3] = Resources.LoadAll<CharacterDressUpData>("ContestOpponents/Cheerleading");
        //
        //        for (int i = 0; i < testOpponentGroups.Length; i++)
        //        {
        //            foreach (var item in testOpponentGroups[i])
        //            {
        //                GameObject characterGo = Instantiate(characterPrefabs[Random.Range(0,characterPrefabs.Length)],new Vector3 (200f*i,3333f,3333f),Quaternion.identity) as GameObject;
        //                CharacterSetUp characterSetUp = characterGo.transform.GetComponent<CharacterSetUp>();
        //
        //                eyeLinerPatternBaseTex = characterSetUp.eyeOutlines.eyeLiner[0];
        //
        //                characterSetUp.SetUpCharacter(item,GenerateMakeUpData());
        //                characterGo = null;
        //            }
        //        }

        //        //Get 2 radnom opponents
        //        opponentData = new CharacterDressUpData[2];
        //        //TODO fill opponent data
        ////        int opponentsInCategory = new DirectoryInfo("Assets/Resources/ContestOpponents/"+characterContestQuest.ToString()).GetFiles().Length/2;    //delimo sa 2 jer racuna i meta fajlove
        //        //FIXME DirectoryInfo ne moze da nadje folder kad se izbilduje na android uredjaju pa je hotfix HC 8
        //        opponentData[0] = Resources.Load<CharacterDressUpData>("ContestOpponents/"+characterContestQuest.ToString()+"/ContestOpponent_"+Random.Range(0,8));
        //        opponentData[1] = Resources.Load<CharacterDressUpData>("ContestOpponents/"+characterContestQuest.ToString()+"/ContestOpponent_"+Random.Range(0,8));

        yield return null;
        InstantiateCharacter(characterPrefabs[GlobalVariables.selectedCharacterIndex[0]], 0, EItemStyles.Classy);    //player 1
        yield return null;
        ChangeSortingLayer(standsHolder[0].GetChild(0).gameObject, "HomeScreenCharacter");
        yield return null;
        InstantiateCharacter(characterPrefabs[GlobalVariables.selectedCharacterIndex[1]], 1, EItemStyles.Punk);    //player 2
                                                                                                                   //        yield return null;
                                                                                                                   //        InstantiateCharacter(characterPrefabs[Random.Range(0,characterPrefabs.Length)],opponentData[0]);
                                                                                                                   //        yield return null;
                                                                                                                   //        InstantiateCharacter(characterPrefabs[Random.Range(0,characterPrefabs.Length)],opponentData[1]);

        //TODO Odrediti pozicije ucesnicima
        Resources.UnloadUnusedAssets();

        yield return null;
        //FIXME TEST
        contestants[0].name = GlobalVariables.selectedCharacterName[0];
        contestants[1].name = GlobalVariables.selectedCharacterName[1];

        for (int i = 0; i < names.Length; i++)
        {
            names[i].text = contestants[i].name;
        }
        //        ContestantsPlacement();
        //        ChangeSortingLayer(standsHolder[2].GetChild(0).gameObject);

        yield return new WaitForSeconds(1f);
        menuManager.transform.Find("LoadingMenu").gameObject.SetActive(true);
        menuManager.transform.Find("LoadingMenu/LoadingHolder/AnimationHolder").GetComponent<Animator>().Play("LoadingGameplayDeparting");
    }

    void InstantiateCharacter(GameObject player, int playerIndex, EItemStyles characterContestQuest)
    {
        GameObject characterGo = Instantiate(player) as GameObject;
        characterGo.transform.SetParent(standsHolder[playerIndex]);
        characterGo.transform.localScale = new Vector3(0.7f, 0.7f, 1f);
        characterGo.transform.localPosition = Vector3.zero;

        //Init GlobalVariables
        if (GlobalVariables.mainCharacterMakeUpData[playerIndex].eyeColor == Color.blue)
            ColorUtility.TryParseHtmlString("#2EC2FEFF", out GlobalVariables.mainCharacterMakeUpData[playerIndex].eyeColor);
        GlobalVariables.mainCharacterMakeUpData[playerIndex].leftMascaraOpenSprite = characterGo.GetComponent<CharacterSetUp>().eyeOutlines.eyeOutlinesOpen[0];
        GlobalVariables.mainCharacterMakeUpData[playerIndex].leftMascaraClosedSprite = characterGo.GetComponent<CharacterSetUp>().eyeOutlines.eyeOutlinesClosed[0];
        GlobalVariables.mainCharacterMakeUpData[playerIndex].rightMascaraOpenSprite = characterGo.GetComponent<CharacterSetUp>().eyeOutlines.eyeOutlinesOpen[0];
        GlobalVariables.mainCharacterMakeUpData[playerIndex].rightMascaraClosedSprite = characterGo.GetComponent<CharacterSetUp>().eyeOutlines.eyeOutlinesClosed[0];
        if (GlobalVariables.mainCharacterMakeUpData[playerIndex].upperLipColor == Color.red)
            GlobalVariables.mainCharacterMakeUpData[playerIndex].upperLipColor = characterGo.GetComponent<CharacterSetUp>().upperLipHolder.GetComponent<Anima2D.SpriteMeshInstance>().color;
        if (GlobalVariables.mainCharacterMakeUpData[playerIndex].lowerLipColor == Color.red)
            GlobalVariables.mainCharacterMakeUpData[playerIndex].lowerLipColor = characterGo.GetComponent<CharacterSetUp>().lowerLipHolder.GetComponent<Anima2D.SpriteMeshInstance>().color;

        characterGo.GetComponent<CharacterSetUp>().SetUpCharacter(GlobalVariables.mainCharacterDressUpData[playerIndex], GlobalVariables.mainCharacterMakeUpData[playerIndex]);
        Contestants c = new Contestants();
        c.name = GlobalVariables.selectedCharacterName[GlobalVariables.currentPlayerIndex];
        c.votes = GetCharacterVotes(playerIndex) + GlobalVariables.GetBeautyAndMakeUpVotes(playerIndex);
        c.contestant = characterGo;
        c.isPlayer = true;
        contestants.Add(c);

        characterGo.transform.GetChild(0).GetComponent<Animator>().Play("CharacterIdleDressUp", 0, Random.Range(0f, 1f));
        characterGo = null;
    }

    //    void InstantiateCharacter(GameObject opponent, CharacterDressUpData dressUpData)
    //    {
    //        GameObject characterGo = Instantiate(opponent,new Vector3 (3333f,3333f,3333f),Quaternion.identity) as GameObject;
    //        CharacterSetUp characterSetUp = characterGo.transform.GetComponent<CharacterSetUp>();
    //        
    //        eyeLinerPatternBaseTex = characterSetUp.eyeOutlines.eyeLiner[0];
    //
    //        characterSetUp.SetUpCharacter(dressUpData,GenerateMakeUpData());
    //        Contestants c = new Contestants();
    //        c.name = PullNameFromList();
    //        c.votes = GetCharacterVotes(characterSetUp, dressUpData) + Random.Range(3,17)*10;    //FIXME dodati odgovarajuci random jer opponents ne dobijaju points za beauty i makeup 25-160
    //        c.contestant = characterGo;
    //        contestants.Add(c);
    //
    //        characterGo.transform.GetChild(0).GetComponent<Animator>().Play("CharacterIdleDressUp",0,Random.Range(0f,1f));
    //        characterGo = null;
    //    }


    int GetCharacterVotes(int playerIndex)
    {
        CharacterDressUpData dressUpData = GlobalVariables.mainCharacterDressUpData[playerIndex];
        int votes = 0;

        //HairVotes
        if (dressUpData.hairPrefab != null)
            votes += dressUpData.hairPrefab.StylePoints;
        //Accessories votes
        if (dressUpData.earringsSprite != null)
            votes += dressUpData.earringsSprite.StylePoints;
        if (dressUpData.neclessSprite != null)
            votes += dressUpData.neclessSprite.StylePoints;
        if (dressUpData.sunglassesSprite != null)
            votes += dressUpData.sunglassesSprite.StylePoints;
        if (dressUpData.tiaraSprite != null)
            votes += dressUpData.tiaraSprite.StylePoints;
        //        //DressUp votes
        if (dressUpData.blousePrefab != null)
            votes += dressUpData.blousePrefab.StylePoints;
        if (dressUpData.jacketPrefab != null)
            votes += dressUpData.jacketPrefab.StylePoints;
        if (dressUpData.dressPrefab != null)
            votes += dressUpData.dressPrefab.StylePoints;
        if (dressUpData.pantsPrefab != null)
            votes += dressUpData.pantsPrefab.StylePoints;
        if (dressUpData.shoesPrefab != null)
            votes += dressUpData.shoesPrefab.StylePoints;
        if (dressUpData.shirtPrefab != null)
            votes += dressUpData.shirtPrefab.StylePoints;
        if (dressUpData.skirtPrefab != null)
            votes += dressUpData.skirtPrefab.StylePoints;
        if (dressUpData.shortsPrefab != null)
            votes += dressUpData.shortsPrefab.StylePoints;
        if (dressUpData.socksPrefab != null)
            votes += dressUpData.socksPrefab.StylePoints;
        if (dressUpData.bagSprite != null)
            votes += dressUpData.bagSprite.StylePoints;

        Debug.Log("DA votes " + votes);
        return votes;
    }

    CharacterMakeUpData GenerateMakeUpData()
    {
        CharacterMakeUpData generatedMakeUpData = new CharacterMakeUpData();

        generatedMakeUpData.eyeColor = eyeBallsColorPalette.colors[Random.Range(0, eyeBallsColorPalette.colors.Count)];
        generatedMakeUpData.upperLipColor = generatedMakeUpData.lowerLipColor = lipstickColorPalette.colors[Random.Range(0, lipstickColorPalette.colors.Count)];

        Texture2D tempTex = PaintUtils.CombineColorAndTex(128, 128, eyeShadowColorPalette.colors[Random.Range(0, eyeShadowColorPalette.colors.Count)], eyeShadowPatternBaseTex);
        generatedMakeUpData.leftEyeShadowSprite = Sprite.Create(tempTex, new Rect(0.0f, 0.0f, eyeShadowPatternBaseTex.width, eyeShadowPatternBaseTex.height), new Vector2(0.5f, 0.5f));
        tempTex = PaintUtils.FlippTexture(tempTex);
        generatedMakeUpData.rightEyeShadowSprite = Sprite.Create(tempTex, new Rect(0.0f, 0.0f, eyeShadowPatternBaseTex.width, eyeShadowPatternBaseTex.height), new Vector2(0.5f, 0.5f));

        tempTex = PaintUtils.CombineColorAndTex(128, 128, eyeLinerColorPalette.colors[Random.Range(0, eyeLinerColorPalette.colors.Count)], eyeLinerPatternBaseTex);
        generatedMakeUpData.leftEyeLinerSprite = Sprite.Create(tempTex, new Rect(0.0f, 0.0f, eyeLinerPatternBaseTex.width, eyeLinerPatternBaseTex.height), new Vector2(0.5f, 0.5f));
        tempTex = PaintUtils.FlippTexture(tempTex);
        generatedMakeUpData.rightEyeLinerSprite = Sprite.Create(tempTex, new Rect(0.0f, 0.0f, eyeLinerPatternBaseTex.width, eyeLinerPatternBaseTex.height), new Vector2(0.5f, 0.5f));

        tempTex = PaintUtils.CombineColorAndTex(128, 128, blushColorPalette.colors[Random.Range(0, blushColorPalette.colors.Count)], blushPatternBaseTex);
        generatedMakeUpData.leftBlushSprite = Sprite.Create(tempTex, new Rect(0.0f, 0.0f, blushPatternBaseTex.width, blushPatternBaseTex.height), new Vector2(0.5f, 0.5f));
        tempTex = PaintUtils.FlippTexture(tempTex);
        generatedMakeUpData.rightBlushSprite = Sprite.Create(tempTex, new Rect(0.0f, 0.0f, blushPatternBaseTex.width, blushPatternBaseTex.height), new Vector2(0.5f, 0.5f));

        return generatedMakeUpData;
    }

    //    void ContestantsPlacement()
    //    {
    //        playerPlacement = 0;
    //        contestants.Sort(((x, y) => x.votes.CompareTo(y.votes)));
    //
    //        for (int i = 0; i < contestants.Count; i++)
    //        {
    //            names[i].text = contestants[i].name;
    //            points[i].text = contestants[i].votes.ToString();
    //            contestants[i].contestant.transform.SetParent(standsHolder[i]);
    //            contestants[i].contestant.transform.localScale = new Vector3(66f,66f,1f);
    //            contestants[i].contestant.transform.localPosition = Vector2.zero;
    //            if (contestants[i].isPlayer)
    //                playerPlacement = i;
    //        }
    //    }

    public void EditPlayer(int playerIndex)
    {
        GlobalVariables.readyToContest = true;
        GlobalVariables.currentPlayerIndex = playerIndex;
        if (playerIndex == 0)
            menuManager.LoadAdequateScene("DressUp");
        else
            menuManager.LoadAdequateScene("DressUpPunk");
    }

    public void BattleButton()
    {
        Button btn = GameObject.Find("Canvas/PopUps").transform.Find("PopUpMessage/AnimationHolder/Body/ButtonsHolder/ButtonOk").GetComponent<Button>();
        menuManager.ShowPopUpMessage("Battle!", "Let's see who is the winner of this fashion battle?");
        btn.onClick.RemoveAllListeners();
        btn.onClick.AddListener(delegate { this.CancelInvoke("ShowResults"); });
        btn.onClick.AddListener(delegate { this.ShowResults(); });

        Invoke("ShowResults", 3f);
    }

    void ShowResults()
    {
        menuManager.ClosePopUpMenu(GameObject.Find("Canvas/PopUps/PopUpMessage"));
        StopCoroutine(ShowBattleResult());
        StartCoroutine(ShowBattleResult());
    }

    IEnumerator ShowBattleResult()
    {
        GameObject.Find("Canvas/UIButtonsHolder/ButtonsHolder/ButtonsHolderBattle FINAL/ButtonsMenuHolder/ButtonMenu/Button").GetComponent<Button>().interactable = false;
        foreach (Text txt in points)
        {
            txt.text = "0";
        }

        yield return new WaitForSeconds(1f);

        bottomButtonsAnim.Play("BottomButtonsShowScore");

        yield return new WaitForSeconds(0.25f);

        if (SoundManager.Instance != null)
            SoundManager.Instance.Play_Voting();

        float f = 0;
        while (f < 1)
        {
            yield return new WaitForSeconds(0.05f);
            points[0].text = ((int)Mathf.Lerp(0, contestants[0].votes, f)).ToString();
            points[1].text = ((int)Mathf.Lerp(0, contestants[1].votes, f)).ToString();
            f += 0.025f;
        }

        GameObject.Find("Canvas/PopUps").transform.Find("PopUpBattle/AnimationHolder/Body/ButtonsHolder/ButtonYes").GetComponent<Button>().onClick.RemoveAllListeners();
        GameObject.Find("Canvas/PopUps").transform.Find("PopUpBattle/AnimationHolder/Body/ButtonsHolder/ButtonNo").GetComponent<Button>().onClick.RemoveAllListeners();
        GameObject.Find("Canvas/PopUps").transform.Find("PopUpBattle/AnimationHolder/Body/ButtonsHolder/ButtonYes").GetComponent<Button>().onClick.AddListener(delegate { menuManager.LoadAdequateScene("HomeScreen"); });
        GameObject.Find("Canvas/PopUps").transform.Find("PopUpBattle/AnimationHolder/Body/ButtonsHolder/ButtonYes").GetComponent<Button>().onClick.AddListener(delegate { GlobalVariables.ResetGlobalVariables(); });
        GameObject.Find("Canvas/PopUps").transform.Find("PopUpBattle/AnimationHolder/Body/ButtonsHolder/ButtonNo").GetComponent<Button>().onClick.AddListener(delegate { bottomButtonsAnim.Play("BottomButtonsArrive"); });
        yield return new WaitForSeconds(3f);
        DeclareWinner();
        yield return new WaitForSeconds(0.5f);
        GameObject.Find("Canvas/UIButtonsHolder/ButtonsHolder/ButtonsHolderBattle FINAL/ButtonsMenuHolder/ButtonMenu/Button").GetComponent<Button>().interactable = true;
    }
    //    public void AddContestExp()
    //    {
    //        switch (playerPlacement)
    //        {
    //            case 0:
    //                menuManager.AddExp(GlobalVariables.currentLvl*100);
    //                break;
    //            case 1:
    //                menuManager.AddExp(GlobalVariables.currentLvl*50);
    //                break;
    //            case 2:
    //                menuManager.AddExp(GlobalVariables.currentLvl*25);
    //                break;
    //            default:
    //                break;
    //        }
    //    }


    //    string PullNameFromList()
    //    {
    //        string s = opponentNames[Random.Range(0,opponentNames.Count)];
    //        opponentNames.Remove(s);
    //        return s;
    //    }

    void ChangeSortingLayer(GameObject prnt)
    {
        Canvas[] tmpCanvases;
        SpriteRenderer[] tmpSR;
        Anima2D.SpriteMeshInstance[] tmpSMI;
        string sortingLayerName = "HomeScreenCharacter";

        tmpCanvases = prnt.GetComponentsInChildren<Canvas>(true);

        if (tmpCanvases != null || tmpCanvases.Length < 1)
        {
            foreach (Canvas canvas in tmpCanvases)
            {
                canvas.sortingLayerID = SortingLayer.NameToID(sortingLayerName);
            }
        }

        tmpSR = prnt.GetComponentsInChildren<SpriteRenderer>(true);

        if (tmpCanvases != null || tmpCanvases.Length < 1)
        {
            foreach (SpriteRenderer sr in tmpSR)
            {
                sr.sortingLayerID = SortingLayer.NameToID(sortingLayerName);
            }
        }

        tmpSMI = prnt.GetComponentsInChildren<Anima2D.SpriteMeshInstance>(true);

        if (tmpCanvases != null || tmpCanvases.Length < 1)
        {
            foreach (Anima2D.SpriteMeshInstance smi in tmpSMI)
            {
                smi.sortingLayerID = SortingLayer.NameToID(sortingLayerName);
            }
        }

        tmpCanvases = null;
    }

    void ChangeSortingLayer(GameObject prnt, string sortingLayerName)
    {

        Canvas[] tmpCanvases = prnt.GetComponentsInChildren<Canvas>(true);

        if (tmpCanvases != null || tmpCanvases.Length < 1)
        {
            foreach (Canvas canvas in tmpCanvases)
            {
                canvas.sortingLayerID = SortingLayer.NameToID(sortingLayerName);
            }
        }

        SpriteRenderer[] tmpSR = prnt.GetComponentsInChildren<SpriteRenderer>(true);

        if (tmpCanvases != null || tmpCanvases.Length < 1)
        {
            foreach (SpriteRenderer sr in tmpSR)
            {
                sr.sortingLayerID = SortingLayer.NameToID(sortingLayerName);
            }
        }

        Anima2D.SpriteMeshInstance[] tmpSMI = prnt.GetComponentsInChildren<Anima2D.SpriteMeshInstance>(true);

        if (tmpCanvases != null || tmpCanvases.Length < 1)
        {
            foreach (Anima2D.SpriteMeshInstance smi in tmpSMI)
            {
                smi.sortingLayerID = SortingLayer.NameToID(sortingLayerName);
            }
        }

        Debug.Log("Sorting layer changed");

        tmpCanvases = null;
        tmpSMI = null;
        tmpSR = null;
    }

    void DeclareWinner()
    {
        if (contestants[0].votes > contestants[1].votes)
            menuManager.ShowPopUpBattle("Winner!", "The winner of this battle of styles is: \n" + contestants[0].name + "\nThis time, classy girl wins!\nDo you want a rematch?");
        else if (contestants[0].votes < contestants[1].votes)
            menuManager.ShowPopUpBattle("Winner!", "And the winner is: \n" + contestants[1].name + "\nPunk style won the first prize this time!\nAre you ready for a rematch?");
        else
            menuManager.ShowPopUpBattle("Winner!", "Both of our contestants look amazing! Its a draw!\nDo you want to try again?");
    }
}

public class Contestants
{

    public string name;
    public bool isPlayer = false;
    public GameObject contestant;
    public int votes;
}

