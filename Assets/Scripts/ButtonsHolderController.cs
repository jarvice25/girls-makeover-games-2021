﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonsHolderController : MonoBehaviour {
    
    string nameOfCurrentScene;
    MenuManager menuManager;
    Transform popUps;
    bool buttonsExpanded;

    void Awake () 
    {
        menuManager = GameObject.Find("Canvas").GetComponent<MenuManager>();
        popUps = GameObject.Find("Canvas/PopUps").transform;

        if (Camera.main.aspect > 2f || Camera.main.aspect < 0.5f)
        {
            transform.localScale = new Vector3(0.9f,0.9f,0.9f);
            RectTransform rt = GetComponent<RectTransform>();
            rt.offsetMax = new Vector2(70,0);
            rt.offsetMin = new Vector2(-70,0);
        }

        nameOfCurrentScene = SceneManager.GetActiveScene().name;
        
        switch(nameOfCurrentScene)
        {
            case "JewelryDesign":
            case "Map":
                transform.Find("ButtonArrowLeft").gameObject.SetActive(false);
                transform.Find("ButtonArrowRight").gameObject.SetActive(false);
                break;
            case "Accessories":
            case "AccessoriesPunk":
            case "DressUp":
            case "DressUpPunk":
                transform.Find("ButtonArrowLeft").gameObject.SetActive(false);
                break;
        }
    }

    public void ShowHideMenu()
    {
        menuManager.ButtonClickSound2();

        if(buttonsExpanded)
        {
            transform.Find("ButtonsMenuHolder").GetComponent<Animator>().Play("MenuButtonsHide");
        }
        else
        {
            transform.Find("ButtonsMenuHolder").GetComponent<Animator>().Play("MenuButtonsExpand");
        }
        buttonsExpanded = !buttonsExpanded;
    }

    public void HomeButtonClick()
    {
        menuManager.ButtonClickSound2();
        menuManager.ShowPopUpMenu(popUps.Find("PopUpDialogHome").gameObject);
    }

    public void RepeatButtonClick()
    {
        menuManager.ButtonClickSound2();
        menuManager.ShowPopUpMenu(popUps.Find("PopUpDialogRepeat").gameObject);
    }

    public void MoreAppsButtonClick()
    {
        menuManager.ButtonClickSound2();
        menuManager.ShowPopUpMenu(popUps.Find("PopUpCrossPromotionOfferWall").gameObject);
    }
}
