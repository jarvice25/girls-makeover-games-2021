﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

///<summary>
///<para>Scene:All/NameOfScene/NameOfScene1,NameOfScene2,NameOfScene3...</para>
///<para>Object:N/A</para>
///<para>Description: Sample Description </para>
///</summary>

public class Loading : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Invoke ("Load",1);
	}

	void Load()
	{
		Application.LoadLevel(MenuManager.nameOfSceneToLoad);
	}
}
