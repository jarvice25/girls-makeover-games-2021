﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollRectButtons : MonoBehaviour {

    public Button buttonIncrement;
    public Button buttonDecrement;
    public float speed = 1f;
    public float step = 0.1f;
    ScrollRect target;

    void Awake()
    {
        target = GetComponent<ScrollRect>();
    }

    public void Increment()
    {
        if (target == null || !target.enabled || !gameObject.activeSelf)
            return;
        target.enabled = false;
//        target.verticalNormalizedPosition = Mathf.Clamp(target.verticalNormalizedPosition + step, 0, 1);
        StartCoroutine(lerpTowardsTargetPosition(Mathf.Clamp(target.verticalNormalizedPosition + step, 0, 1)));
//        buttonIncrement.interactable = target.verticalNormalizedPosition != 1;
//        buttonDecrement.interactable = true;
    }

    public void Decrement()
    {
        if (target == null || !target.enabled || !gameObject.activeSelf)
            return;
        target.enabled = false;
//        target.verticalNormalizedPosition = Mathf.Clamp(target.verticalNormalizedPosition - step, 0, 1);
        StartCoroutine(lerpTowardsTargetPosition(Mathf.Clamp(target.verticalNormalizedPosition - step, 0, 1)));
//        buttonDecrement.interactable = target.verticalNormalizedPosition != 0;
//        buttonIncrement.interactable = true;
    }

    IEnumerator lerpTowardsTargetPosition (float targetPos)
    {
        while (Mathf.Abs(target.verticalNormalizedPosition - targetPos) > step*0.01f)
        {
            target.verticalNormalizedPosition = Mathf.Lerp(target.verticalNormalizedPosition,targetPos,speed*Time.deltaTime);
            yield return null;
        }
        target.verticalNormalizedPosition = targetPos;
        yield return null;
        target.enabled = true;
    }
}
