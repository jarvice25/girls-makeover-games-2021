﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Anima2D;
using UnityEngine.EventSystems;

public class DressUpAnima2D : MonoBehaviour
{

    //FIXME Nidza
    CharacterSetUp characterSetUp;

    [Header("BLOUSES")]
    public DressUpItemPrefabScriptableObject blouses;
    //  public Blouses blouses;
    //  public GameObject blousesInspectorHolder;
    public Transform ItemsScrollBlousesHolder;
    [Space(10)]
    [Header("DRESS")]
    public DressUpItemPrefabScriptableObject dresses;
    //    public Dress dresses;
    //	public GameObject dress1InspectorHolder,dress2InspectorHolder,dress3InspectorHolder,dress4InspectorHolder;
    public Transform ItemsScrollDressHolder;
    [Space(10)]
    [Header("JACKETS")]
    public DressUpItemPrefabScriptableObject jackets;
    //	public Jackets jackets;
    //	public GameObject jackets1InspectorHolder,jackets2InspectorHolder;
    public Transform ItemsScrollJacketsHolder;
    [Space(10)]
    [Header("PANTS")]
    public DressUpItemPrefabScriptableObject pants;
    //	public Pants pants;
    //	public GameObject pants1InspectorHolder, pants2InspectorHolder;
    public Transform ItemsScrollPantsHolder;
    [Space(10)]
    [Header("SHIRTS")]
    public DressUpItemPrefabScriptableObject shirts;
    //	public Shirts shirts;
    //	public GameObject shirts1InspectorHolder, shirts2InspectorHolder;
    public Transform ItemsScrollShirtHolder;
    [Space(10)]
    [Header("SHOES")]
    public DressUpItemPrefabScriptableObject shoes;
    //	public Shoes shoes;
    //	public GameObject shoes1InspectorHolder, shoes2InspectorHolder, shoes3InspectorHolder;
    public Transform ItemsScrollShoesHolder;
    [Space(10)]
    [Header("SHORTS")]
    public DressUpItemPrefabScriptableObject shorts;
    //	public Shorts shorts;
    //	public GameObject shorts1InspectorHolder, shorts2InspectorHolder, shorts3InspectorHolder;
    public Transform ItemsScrollShortsHolder;
    [Space(10)]
    [Header("SOCKS")]
    public DressUpItemPrefabScriptableObject socks;
    //	public Socks socks;
    //	public GameObject socks1InspectorHolder, socks2InspectorHolder;
    public Transform ItemsScrollSocksHolder;
    [Space(10)]
    [Header("SKIRTS")]
    public DressUpItemPrefabScriptableObject skirts;
    //	public Skirts skirts;
    //	public GameObject skirts1InspectorHolder,skirts2InspectorHolder;
    public Transform ItemsScrollSkirtsHolder;
    [Header("Bags&PamPams")]
    public DressUpItemSpriteScriptableObject bags;
    //    public Bags bags;
    //  public GameObject bagsInspectorHolder, pamPamsLeftInspectorHolder, pamPamsRightInspectorHolder;
    [Space(5)]
    public Transform ItemsScrollBagsHolder, ItemsScrollPamsHolder;
    [Space(5)]
    public Transform CategoriesHolder, CategoriesLeftHolder, CategoriesIconsHolder;
    [Space(5)]
    public Animator anim, zoomButton, photoButton, particlesButton;
    [Space(5)]
    public Transform character, background;
    [Space(5)]
    public Animator bgAnimator;
    [Space(10)]
    public GameObject bgParticles;
    public GameObject photoParticlesHolder;

    [Header("Sounds Holder")]
    public Transform soundsHolder;

    int currentCategory = 99; // 0-dresses,1-shirts,2-pants,3-skirts,4-blouses,5-shorts,6-jackets,7-shoes,8-socks,9-bags,10-pam pams
    [Space(10)]
    public float speedMultiplier;
    public AnimationCurve test;

    [Header("Character")]
    public Transform characterHolder;
    public GameObject[] characterPrefabs;

    bool photoMode = false;
    Coroutine deactivateCoroutine;
    Coroutine zoomCoroutine;

    Transform selectParticlesHolder;
    Animator tutorialAnimator;

    int lastShirt, lastPants;

    public static int currentParticle = 0;

    void Awake()
    {
        tutorialAnimator = GameObject.Find("Canvas/HandTutorialHolder/AnimationHolder").GetComponent<Animator>();
        if (PlayerPrefs.HasKey("DressUpTutorialShown"))
        {
            if (PlayerPrefs.GetString("DressUpTutorialShown") == "true")
                tutorialAnimator.gameObject.SetActive(false);
        }



        GameObject characterGo = Instantiate(characterPrefabs[GlobalVariables.selectedCharacterIndex[GlobalVariables.currentPlayerIndex]], characterHolder) as GameObject;
        character = characterGo.transform;
        anim = character.GetChild(0).GetComponent<Animator>();
        anim.Play("CharacterIdleDressUp");
        characterGo = null;
        character.localScale = new Vector3(0.6f, 0.6f, 1f);
        character.localPosition = new Vector3(-0.22f, 0.18f, 0);

        selectParticlesHolder = character.transform.GetChild(0).GetChild(0).Find("SelectParticlesHolder");


        //ZoomOut
        GlobalVariables.ZoomedIn = false;
        character.localPosition = new Vector3(-0.22f, 0.18f, 0);
        background.localPosition = Vector3.zero;
        character.localScale = new Vector3(0.6f, 0.6f, 0.6f);
        background.localScale = Vector3.one;
        //        GlobalVariables.curentNativeAd = GameObject.Find("Canvas/MainMenu/AnimationHolder").GetComponentInChildren<FacebookNativeAd>();
        //        if (GlobalVariables.curentNativeAd != null)
        //            GlobalVariables.curentNativeAd.LoadAdWithDelay(1f);
    }

    IEnumerator Start()
    {

        GameObject.Find("Canvas/PopUps/PopUpDialogRepeat/AnimationHolder/Body/ButtonsHolder/ButtonYes").GetComponent<UnityEngine.UI.Button>().onClick.AddListener(GlobalVariables.MainCharacterDefaultDressUpData);
        characterSetUp = character.GetComponent<CharacterSetUp>();

        //Init GlobalVariables
        yield return new WaitForEndOfFrame();
        if (GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].eyeColor == Color.blue)
            ColorUtility.TryParseHtmlString("#2EC2FEFF", out GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].eyeColor);
        GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].leftMascaraOpenSprite = characterSetUp.eyeOutlines.eyeOutlinesOpen[0];
        GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].leftMascaraClosedSprite = characterSetUp.eyeOutlines.eyeOutlinesClosed[0];
        GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].rightMascaraOpenSprite = characterSetUp.eyeOutlines.eyeOutlinesOpen[0];
        GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].rightMascaraClosedSprite = characterSetUp.eyeOutlines.eyeOutlinesClosed[0];
        if (GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].upperLipColor == Color.red)
            GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].upperLipColor = characterSetUp.upperLipHolder.GetComponent<Anima2D.SpriteMeshInstance>().color;
        if (GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].lowerLipColor == Color.red)
            GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].lowerLipColor = characterSetUp.lowerLipHolder.GetComponent<Anima2D.SpriteMeshInstance>().color;

        yield return new WaitForEndOfFrame();
        characterSetUp.SetUpCharacter(GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex], GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex]);

    }

    public void ChangeShoes()//(int shoeIndex)
    {
        int shoeIndex = EventSystem.current.currentSelectedGameObject.transform.parent.GetSiblingIndex();

        if (GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].shoesPrefab != shoes.elements[shoeIndex])
        {
            foreach (Transform item in ItemsScrollShoesHolder)
            {
                item.GetChild(0).gameObject.SetActive(false);
                item.GetChild(2).gameObject.SetActive(false);
            }

            GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].shoesPrefab = shoes.elements[shoeIndex];

            characterSetUp.SetShoes(shoes.elements[shoeIndex].item);

            ItemsScrollShoesHolder.GetChild(shoeIndex).GetChild(0).gameObject.SetActive(true);
            ItemsScrollShoesHolder.GetChild(shoeIndex).GetChild(2).gameObject.SetActive(true);

            selectParticlesHolder.Find("SelectParticleFeet").GetComponent<ParticleSystem>().Play();

            GlobalVariables.AddXpToBuffer("Shoes");
            soundsHolder.GetChild(Random.Range(0, soundsHolder.childCount)).GetComponent<AudioSource>().Play();
        }
    }

    public void ChangeSocks()//(int socksIndex)
    {
        int socksIndex = EventSystem.current.currentSelectedGameObject.transform.parent.GetSiblingIndex() - 1;

        foreach (Transform item in ItemsScrollSocksHolder)
        {
            item.GetChild(0).gameObject.SetActive(false);
            item.GetChild(2).gameObject.SetActive(false);
        }

        if (socksIndex < 0)
        {
            characterSetUp.SetSocks(null);
            GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].socksPrefab = null;
        }
        else if (GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].socksPrefab != socks.elements[socksIndex])// && socksIndex>=0)
        {
            GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].socksPrefab = socks.elements[socksIndex];
            characterSetUp.SetSocks(socks.elements[socksIndex].item);

            if (socksIndex >= 20)
                selectParticlesHolder.Find("SelectParticleLegs").GetComponent<ParticleSystem>().Play();
            else
                selectParticlesHolder.Find("SelectParticleFeet").GetComponent<ParticleSystem>().Play();

            GlobalVariables.AddXpToBuffer("Socks");
            soundsHolder.GetChild(Random.Range(0, soundsHolder.childCount)).GetComponent<AudioSource>().Play();
        }

        ItemsScrollSocksHolder.GetChild(socksIndex + 1).GetChild(0).gameObject.SetActive(true);
        ItemsScrollSocksHolder.GetChild(socksIndex + 1).GetChild(2).gameObject.SetActive(true);
    }

    public void ChangeDress()//(int dressIndex)
    {
        int dressIndex = EventSystem.current.currentSelectedGameObject.transform.parent.GetSiblingIndex();


        if (GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].dressPrefab != dresses.elements[dressIndex])
        {
            foreach (Transform item in ItemsScrollDressHolder)
            {
                item.GetChild(0).gameObject.SetActive(false);
                item.GetChild(2).gameObject.SetActive(false);
            }

            GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].dressPrefab = dresses.elements[dressIndex];

            characterSetUp.SetDress(dresses.elements[dressIndex].item);

            if (dressIndex < 8)
                selectParticlesHolder.Find("SelectParticleDressShortWide").GetComponent<ParticleSystem>().Play();
            else if (dressIndex < 16)
                selectParticlesHolder.Find("SelectParticleDressShort").GetComponent<ParticleSystem>().Play();
            else if (dressIndex < 24)
                selectParticlesHolder.Find("SelectParticleDressLongWide").GetComponent<ParticleSystem>().Play();
            else
                selectParticlesHolder.Find("SelectParticleDressLong").GetComponent<ParticleSystem>().Play();

            ItemsScrollDressHolder.GetChild(dressIndex).GetChild(0).gameObject.SetActive(true);
            ItemsScrollDressHolder.GetChild(dressIndex).GetChild(2).gameObject.SetActive(true);
            HideClothesForFullBodyClothesSelected();

            GlobalVariables.AddXpToBuffer("Dress");
            soundsHolder.GetChild(Random.Range(0, soundsHolder.childCount)).GetComponent<AudioSource>().Play();
        }

        tutorialAnimator.gameObject.SetActive(false);
        PlayerPrefs.SetString("DressUpTutorialShown", "true");
        PlayerPrefs.Save();
    }

    public void ChangeShirts()//(int shirtIndex)
    {
        int shirtIndex = EventSystem.current.currentSelectedGameObject.transform.parent.GetSiblingIndex();

        if (GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].shirtPrefab != shirts.elements[shirtIndex])
        {
            foreach (Transform item in ItemsScrollShirtHolder)
            {
                item.GetChild(0).gameObject.SetActive(false);
                item.GetChild(2).gameObject.SetActive(false);
            }

            GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].shirtPrefab = shirts.elements[shirtIndex];
            lastShirt = shirtIndex;

            characterSetUp.shirts1InspectorHolder.SetActive(true);

            characterSetUp.SetShirt(shirts.elements[shirtIndex].item);

            ItemsScrollShirtHolder.GetChild(shirtIndex).GetChild(0).gameObject.SetActive(true);
            ItemsScrollShirtHolder.GetChild(shirtIndex).GetChild(2).gameObject.SetActive(true);
            selectParticlesHolder.Find("SelectParticleShirts").GetComponent<ParticleSystem>().Play();

            HideClothesForUpperBodyClothesSelected("Shirt");
            GlobalVariables.AddXpToBuffer("Shirts");
            soundsHolder.GetChild(Random.Range(0, soundsHolder.childCount)).GetComponent<AudioSource>().Play();
        }
    }

    public void ChangePants()//(int pantsIndex)
    {
        int pantsIndex = EventSystem.current.currentSelectedGameObject.transform.parent.GetSiblingIndex();


        if (GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].pantsPrefab != pants.elements[pantsIndex])
        {
            foreach (Transform item in ItemsScrollPantsHolder)
            {
                item.GetChild(0).gameObject.SetActive(false);
                item.GetChild(2).gameObject.SetActive(false);
            }


            GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].pantsPrefab = pants.elements[pantsIndex];
            lastPants = pantsIndex;
            characterSetUp.SetPants(pants.elements[pantsIndex].item);

            ItemsScrollPantsHolder.GetChild(pantsIndex).GetChild(0).gameObject.SetActive(true);
            ItemsScrollPantsHolder.GetChild(pantsIndex).GetChild(2).gameObject.SetActive(true);
            selectParticlesHolder.Find("SelectParticleLegs").GetComponent<ParticleSystem>().Play();

            HideClothesForLowerBodyClothesSelected("Pants");
            GlobalVariables.AddXpToBuffer("Pants");
            soundsHolder.GetChild(Random.Range(0, soundsHolder.childCount)).GetComponent<AudioSource>().Play();
        }

    }

    public void ChangeBlouses()//(int blousesIndex)
    {
        int blousesIndex = EventSystem.current.currentSelectedGameObject.transform.parent.GetSiblingIndex();

        if (GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].blousePrefab != blouses.elements[blousesIndex])
        {
            foreach (Transform item in ItemsScrollBlousesHolder)
            {
                item.GetChild(0).gameObject.SetActive(false);
                item.GetChild(2).gameObject.SetActive(false);
            }
            GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].blousePrefab = blouses.elements[blousesIndex];

            characterSetUp.blousesInspectorHolder.SetActive(true);


            characterSetUp.SetBlause(blouses.elements[blousesIndex].item);

            ItemsScrollBlousesHolder.GetChild(blousesIndex).GetChild(0).gameObject.SetActive(true);
            ItemsScrollBlousesHolder.GetChild(blousesIndex).GetChild(2).gameObject.SetActive(true);
            selectParticlesHolder.Find("SelectParticleBlouse").GetComponent<ParticleSystem>().Play();

            HideClothesForUpperBodyClothesSelected("Blouse");
            GlobalVariables.AddXpToBuffer("Blouses");
            soundsHolder.GetChild(Random.Range(0, soundsHolder.childCount)).GetComponent<AudioSource>().Play();
        }
    }

    public void ChangeSkirts()//(int skirtsIndex)
    {
        int skirtsIndex = EventSystem.current.currentSelectedGameObject.transform.parent.GetSiblingIndex();

        if (GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].skirtPrefab != skirts.elements[skirtsIndex])
        {
            foreach (Transform item in ItemsScrollSkirtsHolder)
            {
                item.GetChild(0).gameObject.SetActive(false);
                item.GetChild(2).gameObject.SetActive(false);
            }

            GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].skirtPrefab = skirts.elements[skirtsIndex];
            characterSetUp.SetSkirt(skirts.elements[skirtsIndex].item);

            //            if(skirtsIndex<2)
            selectParticlesHolder.Find("SelectParticleSkirts").GetChild(1).gameObject.SetActive(true);
            //            else
            //                selectParticlesHolder.Find("SelectParticleSkirts").GetChild(1).gameObject.SetActive(false);

            ItemsScrollSkirtsHolder.GetChild(skirtsIndex).GetChild(0).gameObject.SetActive(true);
            ItemsScrollSkirtsHolder.GetChild(skirtsIndex).GetChild(2).gameObject.SetActive(true);

            selectParticlesHolder.Find("SelectParticleSkirts").GetComponent<ParticleSystem>().Play();

            HideClothesForLowerBodyClothesSelected("Skirt");
            GlobalVariables.AddXpToBuffer("Skirts");
            soundsHolder.GetChild(Random.Range(0, soundsHolder.childCount)).GetComponent<AudioSource>().Play();
        }
    }

    public void ChangeShorts()//(int shortsIndex)
    {
        int shortsIndex = EventSystem.current.currentSelectedGameObject.transform.parent.GetSiblingIndex();

        if (GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].shortsPrefab != shorts.elements[shortsIndex])
        {
            foreach (Transform item in ItemsScrollShortsHolder)
            {
                item.GetChild(0).gameObject.SetActive(false);
                item.GetChild(2).gameObject.SetActive(false);
            }


            GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].shortsPrefab = shorts.elements[shortsIndex];
            characterSetUp.SetShorts(shorts.elements[shortsIndex].item);

            ItemsScrollShortsHolder.GetChild(shortsIndex).GetChild(0).gameObject.SetActive(true);
            ItemsScrollShortsHolder.GetChild(shortsIndex).GetChild(2).gameObject.SetActive(true);
            selectParticlesHolder.Find("SelectParticleShorts").GetComponent<ParticleSystem>().Play();

            HideClothesForLowerBodyClothesSelected("Shorts");
            GlobalVariables.AddXpToBuffer("Shorts");
            soundsHolder.GetChild(Random.Range(0, soundsHolder.childCount)).GetComponent<AudioSource>().Play();
        }
    }

    public void ChangeJackets()//(int jacketsIndex)
    {
        int jacketsIndex = EventSystem.current.currentSelectedGameObject.transform.parent.GetSiblingIndex() - 1;

        foreach (Transform item in ItemsScrollJacketsHolder)
        {
            item.GetChild(0).gameObject.SetActive(false);
            item.GetChild(2).gameObject.SetActive(false);
        }

        if (jacketsIndex < 0)
        {
            characterSetUp.SetJacket(null);
            GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].jacketPrefab = null;
        }
        else if (GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].jacketPrefab != jackets.elements[jacketsIndex]) //&& jacketsIndex >= 0)
        {
            GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].jacketPrefab = jackets.elements[jacketsIndex];

            characterSetUp.SetJacket(jackets.elements[jacketsIndex].item);

            if (jacketsIndex < 16)
            {
                selectParticlesHolder.Find("SelectParticleJackets").GetChild(3).gameObject.SetActive(true);
                selectParticlesHolder.Find("SelectParticleJackets").GetChild(4).gameObject.SetActive(true);
            }
            else
            {
                selectParticlesHolder.Find("SelectParticleJackets").GetChild(3).gameObject.SetActive(false);
                selectParticlesHolder.Find("SelectParticleJackets").GetChild(4).gameObject.SetActive(false);
            }

            selectParticlesHolder.Find("SelectParticleJackets").GetComponent<ParticleSystem>().Play();

            GlobalVariables.AddXpToBuffer("Blouses");
            soundsHolder.GetChild(Random.Range(0, soundsHolder.childCount)).GetComponent<AudioSource>().Play();
        }
        ItemsScrollJacketsHolder.GetChild(jacketsIndex + 1).GetChild(0).gameObject.SetActive(true);
        ItemsScrollJacketsHolder.GetChild(jacketsIndex + 1).GetChild(2).gameObject.SetActive(true);
    }

    //	bagsInspectorHolder, pamPamsLeftInspectorHolder, pamPamsRightInspectorHolder;
    public void ChangeBagsOrPamPams()//(int bagsIndex)
    {
        int bagsIndex = EventSystem.current.currentSelectedGameObject.transform.parent.GetSiblingIndex() - 1;

        foreach (Transform item in ItemsScrollBagsHolder)
        {
            item.GetChild(0).gameObject.SetActive(false);
            item.GetChild(2).gameObject.SetActive(false);
        }

        if (bagsIndex < 0)
        {
            characterSetUp.SetBagOrPamPams(null);
            GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].bagSprite = null;
        }
        else// if(bagsIndex<10)
        {
            soundsHolder.GetChild(Random.Range(0, soundsHolder.childCount)).GetComponent<AudioSource>().Play();
            characterSetUp.SetBagOrPamPams(bags.elements[bagsIndex].item);

            GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].bagSprite = bags.elements[bagsIndex];
            GlobalVariables.AddXpToBuffer("Bags");
        }

        ItemsScrollBagsHolder.GetChild(bagsIndex + 1).GetChild(0).gameObject.SetActive(true);
        ItemsScrollBagsHolder.GetChild(bagsIndex + 1).GetChild(2).gameObject.SetActive(true);
    }

    public void ShowCategory(int indexOfCategory)
    {
        if (indexOfCategory == 0)
        {
            tutorialAnimator.SetInteger("TutorialState", 1);
        }
        else
        {
            tutorialAnimator.gameObject.SetActive(false);
            PlayerPrefs.SetString("DressUpTutorialShown", "true");
            PlayerPrefs.Save();
        }

        if (indexOfCategory != currentCategory)
        {
            if (currentCategory <= 10)
            {
                CategoriesIconsHolder.GetChild(currentCategory).GetChild(0).GetChild(0).gameObject.SetActive(true);
                CategoriesIconsHolder.GetChild(currentCategory).GetChild(0).GetChild(1).gameObject.SetActive(false);
                HideCurrentCategory();
            }
            currentCategory = indexOfCategory;
            CategoriesIconsHolder.GetChild(currentCategory).GetChild(0).GetChild(0).gameObject.SetActive(false);
            CategoriesIconsHolder.GetChild(currentCategory).GetChild(0).GetChild(1).gameObject.SetActive(true);
            CategoriesHolder.GetChild(currentCategory).gameObject.SetActive(true);
            CategoriesHolder.GetChild(currentCategory).GetChild(0).GetComponent<Animator>().Play("ArrivingRightToLeft");
            ChangeCategoryPosition();
        }
    }

    void HideCurrentCategory()
    {
        CategoriesHolder.GetChild(currentCategory).GetChild(0).GetComponent<Animator>().Play("DepartingRightToLeft");
        if (deactivateCoroutine != null)
            StopCoroutine(deactivateCoroutine);
        deactivateCoroutine = StartCoroutine(DeactivateCategory(currentCategory));
    }

    IEnumerator DeactivateCategory(int category)
    {
        yield return new WaitForSeconds(0.7f);
        CategoriesHolder.GetChild(category).gameObject.SetActive(false);
        Debug.Log("Deactivated");
    }

    void Test()
    {
        characterSetUp.dress1InspectorHolder.SetActive(true);
        characterSetUp.dress2InspectorHolder.SetActive(false);
        characterSetUp.dress3InspectorHolder.SetActive(false);
        characterSetUp.dress4InspectorHolder.SetActive(false);

        characterSetUp.shirts1InspectorHolder.SetActive(true);
        characterSetUp.shirts2InspectorHolder.SetActive(false);

        characterSetUp.pants1InspectorHolder.SetActive(true);
        characterSetUp.pants2InspectorHolder.SetActive(false);

        characterSetUp.blousesInspectorHolder.SetActive(false);

        characterSetUp.skirts1InspectorHolder.SetActive(false);
        characterSetUp.skirts2InspectorHolder.SetActive(false);
        characterSetUp.shorts1InspectorHolder.SetActive(true);
        characterSetUp.shorts2InspectorHolder.SetActive(false);
        characterSetUp.shorts3InspectorHolder.SetActive(false);

        characterSetUp.jackets1InspectorHolder.SetActive(true);
        characterSetUp.jackets2InspectorHolder.SetActive(false);


        characterSetUp.dress1InspectorHolder.SetActive(true);
        characterSetUp.dress2InspectorHolder.SetActive(false);
        characterSetUp.dress3InspectorHolder.SetActive(false);
        characterSetUp.dress4InspectorHolder.SetActive(false);
        characterSetUp.skirts1InspectorHolder.SetActive(false);
        characterSetUp.pants1InspectorHolder.SetActive(true);
        characterSetUp.pants2InspectorHolder.SetActive(false);
    }


    public void ChangeZoomIcon()
    {
        zoomButton.Play("ZoomClicked");
        if (zoomCoroutine != null)
            StopCoroutine(zoomCoroutine);
        zoomCoroutine = StartCoroutine("ZoomInOut");
    }

    IEnumerator ZoomInOut()
    {
        float curveTime = 0;
        float curveAmount = test.Evaluate(curveTime);
        Debug.Log("curve amount " + curveAmount);
        if (GlobalVariables.ZoomedIn)
        {
            GlobalVariables.ZoomedIn = false;
            while (curveAmount < 1)
            {
                curveTime += Time.deltaTime * speedMultiplier;
                curveAmount = test.Evaluate(curveTime);
                //haljina
                character.localPosition = Vector3.Lerp(character.localPosition, new Vector3(-0.22f, 0.18f, 0), curveTime);
                background.localPosition = Vector3.Lerp(background.localPosition, Vector3.zero, curveTime);

                character.localScale = Vector3.Lerp(character.localScale, new Vector3(0.6f, 0.6f, 0.6f), curveTime);
                background.localScale = Vector3.Lerp(background.localScale, Vector3.one, curveTime);
                yield return null;
            }
        }
        else
        {
            GlobalVariables.ZoomedIn = true;
            while (curveAmount < 1)
            {
                curveTime += Time.deltaTime * speedMultiplier;
                curveAmount = test.Evaluate(curveTime);

                switch (currentCategory)
                {
                    case 0:
                    case 9:
                    case 10:
                        //haljina
                        character.localPosition = Vector3.Lerp(character.localPosition, new Vector3(-0.22f, 0.18f, 0), curveTime);
                        background.localPosition = Vector3.Lerp(background.localPosition, Vector3.zero, curveTime);

                        character.localScale = Vector3.Lerp(character.localScale, new Vector3(0.6f, 0.6f, 0.6f), curveTime);
                        background.localScale = Vector3.Lerp(background.localScale, Vector3.one, curveTime);
                        break;
                    case 1:
                    case 4:
                    case 6:
                        //Blouse
                        character.localPosition = Vector3.Lerp(character.localPosition, new Vector3(-0.22f, -2.2f, 0), curveTime);
                        background.localPosition = Vector3.Lerp(background.localPosition, new Vector3(0, -460, 0), curveTime);

                        character.localScale = Vector3.Lerp(character.localScale, new Vector3(1.1f, 1.1f, 1.1f), curveTime);
                        background.localScale = Vector3.Lerp(background.localScale, new Vector3(1.85f, 1.85f, 1.85f), curveTime);
                        break;
                    case 2:
                    case 5:
                    case 8:
                        //Jeans
                        character.localPosition = Vector3.Lerp(character.localPosition, new Vector3(-0.22f, 1.5f, 0), curveTime);
                        background.localPosition = Vector3.Lerp(background.localPosition, new Vector3(0, 252, 0), curveTime);

                        character.localScale = Vector3.Lerp(character.localScale, Vector3.one, curveTime);
                        background.localScale = Vector3.Lerp(background.localScale, new Vector3(1.77f, 1.77f, 1.77f), curveTime);
                        break;
                    case 3:
                        //Skirt
                        character.localPosition = Vector3.Lerp(character.localPosition, new Vector3(-0.32f, 1.2f, 0), curveTime);
                        background.localPosition = Vector3.Lerp(background.localPosition, new Vector3(-10, 190, 0), curveTime);

                        character.localScale = Vector3.Lerp(character.localScale, new Vector3(0.87f, 0.87f, 0.87f), curveTime);
                        background.localScale = Vector3.Lerp(background.localScale, new Vector3(1.54f, 1.54f, 1.54f), curveTime);
                        break;
                    case 7:
                        //cipele
                        character.localPosition = Vector3.Lerp(character.localPosition, new Vector3(-0.38f, 3, 0), curveTime);
                        background.localPosition = Vector3.Lerp(background.localPosition, new Vector3(-21, 538, 0), curveTime);

                        character.localScale = Vector3.Lerp(character.localScale, new Vector3(1.14f, 1.14f, 1.14f), curveTime);
                        background.localScale = Vector3.Lerp(background.localScale, new Vector3(2.02f, 2.02f, 2.02f), curveTime);
                        break;
                }
                yield return null;
            }
        }
        yield return null;
    }

    void ChangeCategoryPosition()
    {
        if (zoomCoroutine != null)
            StopCoroutine(zoomCoroutine);
        zoomCoroutine = StartCoroutine("ChangeCategoryPositionIEnumerator");
    }

    IEnumerator ChangeCategoryPositionIEnumerator()
    {
        Debug.Log("Changet category zoom" + GlobalVariables.ZoomedIn);
        float curveTime = 0;
        float curveAmount = test.Evaluate(curveTime);

        while (curveAmount < 1)
        {
            curveTime += Time.deltaTime * speedMultiplier;
            curveAmount = test.Evaluate(curveTime);
            if (GlobalVariables.ZoomedIn)
            {
                switch (currentCategory)
                {
                    case 0:
                    case 9:
                    case 10:
                        //haljina
                        character.localPosition = Vector3.Lerp(character.localPosition, new Vector3(-0.22f, 0.18f, 0), curveTime);
                        background.localPosition = Vector3.Lerp(background.localPosition, Vector3.zero, curveTime);

                        character.localScale = Vector3.Lerp(character.localScale, new Vector3(0.6f, 0.6f, 0.6f), curveTime);
                        background.localScale = Vector3.Lerp(background.localScale, Vector3.one, curveTime);
                        break;
                    case 1:
                    case 4:
                    case 6:
                        //Blouse
                        character.localPosition = Vector3.Lerp(character.localPosition, new Vector3(-0.22f, -2.2f, 0), curveTime);
                        background.localPosition = Vector3.Lerp(background.localPosition, new Vector3(0, -460, 0), curveTime);

                        character.localScale = Vector3.Lerp(character.localScale, new Vector3(1.1f, 1.1f, 1.1f), curveTime);
                        background.localScale = Vector3.Lerp(background.localScale, new Vector3(1.85f, 1.85f, 1.85f), curveTime);
                        break;
                    case 2:
                    case 5:
                    case 8:
                        //Jeans
                        character.localPosition = Vector3.Lerp(character.localPosition, new Vector3(-0.22f, 1.5f, 0), curveTime);
                        background.localPosition = Vector3.Lerp(background.localPosition, new Vector3(0, 252, 0), curveTime);

                        character.localScale = Vector3.Lerp(character.localScale, Vector3.one, curveTime);
                        background.localScale = Vector3.Lerp(background.localScale, new Vector3(1.77f, 1.77f, 1.77f), curveTime);
                        break;
                    case 3:
                        //Skirt
                        character.localPosition = Vector3.Lerp(character.localPosition, new Vector3(-0.32f, 1.2f, 0), curveTime);
                        background.localPosition = Vector3.Lerp(background.localPosition, new Vector3(-10, 190, 0), curveTime);

                        character.localScale = Vector3.Lerp(character.localScale, new Vector3(0.87f, 0.87f, 0.87f), curveTime);
                        background.localScale = Vector3.Lerp(background.localScale, new Vector3(1.54f, 1.54f, 1.54f), curveTime);
                        break;
                    case 7:
                        //cipele
                        character.localPosition = Vector3.Lerp(character.localPosition, new Vector3(-0.38f, 3, 0), curveTime);
                        background.localPosition = Vector3.Lerp(background.localPosition, new Vector3(-21, 538, 0), curveTime);

                        character.localScale = Vector3.Lerp(character.localScale, new Vector3(1.14f, 1.14f, 1.14f), curveTime);
                        background.localScale = Vector3.Lerp(background.localScale, new Vector3(2.02f, 2.02f, 2.02f), curveTime);
                        break;
                }
            }
            yield return null;
        }
        yield return null;
    }

    #region HideAppropriateClothes
    //Gasimo odgovarajucu odecu kada je selektovana odeca za celo telo (haljine)
    void HideClothesForFullBodyClothesSelected()
    {
        HideBlouses();
        HideShirts();
        HidePants();
        HideSkirts();
        HideShorts();
    }
    //Gasimo odgovarajucu oecu kada je selektovana odeca za gornji deo tela (majice i bluze)
    void HideClothesForUpperBodyClothesSelected(string partType)
    {
        HideDress();

        switch (partType)
        {
            case "Blouse":
                HideShirts();
                break;
            case "Shirt":
                HideBlouses();
                break;
            default:
                break;
        }

        //Show lower boddy clothes if none is active
        if (GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].pantsPrefab == null && GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].skirtPrefab == null && GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].shortsPrefab == null)
        {
            characterSetUp.SetPants(pants.elements[lastPants].item);
            GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].pantsPrefab = pants.elements[lastPants];
            ItemsScrollPantsHolder.GetChild(lastPants).GetChild(0).gameObject.SetActive(true);
        }
    }
    //Gasimo odgovarajucu oecu kada je selektovana odeca za donji deo tela (pantalone, suknje, sorcevi)
    void HideClothesForLowerBodyClothesSelected(string partType)
    {
        HideDress();
        switch (partType)
        {
            case "Pants":
                HideShorts();
                HideSkirts();
                break;
            case "Shorts":
                HidePants();
                HideSkirts();
                break;
            case "Skirt":
                HidePants();
                HideShorts();
                break;
            default:
                break;
        }

        //Show upper boddy clothes if none is active
        if (GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].shirtPrefab == null && GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].blousePrefab == null)
        {
            characterSetUp.SetShirt(shirts.elements[lastShirt].item);
            GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].shirtPrefab = shirts.elements[lastShirt];
            ItemsScrollShirtHolder.GetChild(lastShirt).GetChild(0).gameObject.SetActive(true);
        }
    }

    void HideDress()
    {
        foreach (Transform item in ItemsScrollDressHolder)
        {
            item.GetChild(0).gameObject.SetActive(false);
        }
        //Hide full body clothes
        characterSetUp.SetDress(null);
        GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].dressPrefab = null;
    }

    void HideShirts()
    {
        foreach (Transform item in ItemsScrollShirtHolder)
        {
            item.GetChild(0).gameObject.SetActive(false);
        }
        //Hide shirts
        characterSetUp.SetShirt(null);
        GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].shirtPrefab = null;
    }

    void HideBlouses()
    {
        foreach (Transform item in ItemsScrollBlousesHolder)
        {
            item.GetChild(0).gameObject.SetActive(false);
        }
        //Hide blouses
        characterSetUp.SetBlause(null);
        GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].blousePrefab = null;
    }

    void HidePants()
    {
        foreach (Transform item in ItemsScrollPantsHolder)
        {
            item.GetChild(0).gameObject.SetActive(false);
        }
        //Hide pants
        characterSetUp.SetPants(null);
        GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].pantsPrefab = null;
    }

    void HideSkirts()
    {
        foreach (Transform item in ItemsScrollSkirtsHolder)
        {
            item.GetChild(0).gameObject.SetActive(false);
        }
        //Hide skirts
        characterSetUp.SetSkirt(null);
        GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].skirtPrefab = null;
    }

    void HideShorts()
    {
        foreach (Transform item in ItemsScrollShortsHolder)
        {
            item.GetChild(0).gameObject.SetActive(false);
        }
        //Hide shorts
        characterSetUp.SetShorts(null);
        GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].shortsPrefab = null;
    }
    #endregion

    public void GoToPhotoMode()
    {
        //        GameObject.Find("Canvas").GetComponent<MenuManager>().AddExp(GlobalVariables.CollectXpFromBuffer());

        //        if (photoMode)
        //        {
        //TODO Finish
        if (AdsManager.Instance != null)
            AdsManager.Instance.ShowInterstitial();

        //            Debug.Log("Ucitaj novu scenu");
        if (GlobalVariables.currentPlayerIndex == 0)
        {
            if (AdsManager.Instance != null)
                AdsManager.Instance.ShowInterstitial();
            GameObject.Find("Canvas").GetComponent<MenuManager>().LoadAdequateScene("Accessories");
        }
        else
        {
            if (AdsManager.Instance != null)
                AdsManager.Instance.ShowInterstitial();
            GameObject.Find("Canvas").GetComponent<MenuManager>().LoadAdequateScene("AccessoriesPunk");
        }
        //        }
        //        else
        //        {
        //            //Handle particles
        //            bgParticles.GetComponent<ParticleSystem>().Stop();
        //            PhotoParticlesEmission(photoParticlesHolder.transform.GetChild(currentParticle).gameObject, true);
        //            //Handle zoom
        //            if (GlobalVariables.ZoomedIn)
        //            {
        //                GameObject.Find("Canvas/ButtonsHolderDressUp FINAL/ButtonZoom/Icon").transform.GetChild(0).gameObject.SetActive(true);
        //                GameObject.Find("Canvas/ButtonsHolderDressUp FINAL/ButtonZoom/Icon").transform.GetChild(1).gameObject.SetActive(false);
        //                ChangeZoomIcon();
        //            }
        //            //Hide current category
        //            if (currentCategory <= 10)
        //                HideCurrentCategory();
        //            CategoriesLeftHolder.GetComponentInChildren<Animator>().Play("CategoriesDeparting");
        //            //Change bg
        //            bgAnimator.Play("DressUpBgFadeIn");
        //            photoMode = true;
        //        }


    }

    public void GoBackToDressUp()
    {
        //Handle particles
        bgParticles.GetComponent<ParticleSystem>().Play();
        foreach (Transform particleHolder in photoParticlesHolder.transform)
        {
            PhotoParticlesEmission(particleHolder.gameObject, false);
        }
        //Show last selected category
        int tmpIndex = currentCategory;
        currentCategory = 99;
        ShowCategory(tmpIndex);
        CategoriesLeftHolder.GetComponentInChildren<Animator>().Play("CategoriesArriving");
        //Change bg
        bgAnimator.Play("DressUpBgFadeOut");
        photoMode = false;
    }

    public void ChangeParticles()
    {
        particlesButton.Play("ParticlesClicked");
        if (currentParticle >= photoParticlesHolder.transform.childCount - 1)
            currentParticle = 0;
        else
            currentParticle++;

        foreach (Transform particleHolder in photoParticlesHolder.transform)
        {
            PhotoParticlesEmission(particleHolder.gameObject, false);
        }

        PhotoParticlesEmission(photoParticlesHolder.transform.GetChild(currentParticle).gameObject, true);
    }

    void PhotoParticlesEmission(GameObject particles, bool emit)
    {
        if (emit)
        {
            particles.GetComponent<ParticleSystem>().Play();
            foreach (ParticleSystem ps in particles.GetComponentsInChildren<ParticleSystem>())
            {
                ps.Play();
            }
        }
        else
        {
            particles.GetComponent<ParticleSystem>().Stop();
            foreach (ParticleSystem ps in particles.GetComponentsInChildren<ParticleSystem>())
            {
                ps.Stop();
            }
        }
    }
}
