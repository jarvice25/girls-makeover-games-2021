﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Security.Cryptography;
using System.Text;

///<summary>
///<para>Scene:All/NameOfScene/NameOfScene1,NameOfScene2,NameOfScene3...</para>
///<para>Object:N/A</para>
///<para>Description: Sample Description </para>
///</summary>

public class DressUpLogic : MonoBehaviour {

	// TEMP
	Color deselectColor = new Color(1,1,1,0.5f);
	int nmbclick = 0;                   

	// TEMP




	int currentCategory = 0; // hair -1; dress -2, Shirt-3,Pants-4,HeadWear-5,Bag-6,Shoes-7
	[System.Serializable]
	public struct CharacterStruct
	{
		public Image dress;
		public Image pants;
		public Image shirt;
		public Image PamPamLeft;
		public Image PamPamRight;
		public Image frontHair;
		public Image backHair;
		public Image sockLeft;
		public Image sockRight;
		public Image shoeLeft;
		public Image shoeRight;
	}
	[HideInInspector]
	public Animator categoryDressAnimator, categoryPantsAnimator, categoryShirtAnimator, categoryHeadWearAnimator, categoryBagAnimator, categoryHairAnimator, categoryShoesAnimator;
	[HideInInspector]
	public Image categoryDressButton, categoryPantsButton, categoryShirtButton, categoryHeadWearButton, categoryBagButton, categoryHairButton, categoryShoesButton;
	public CharacterStruct Character;
	// Use this for initialization
	void Start () {
		ShowDresses();
	}

	public void ChangeDress(Sprite newDress)
	{
		Character.dress.sprite = newDress;

		Character.dress.gameObject.GetComponent<Animator>().Play("ChangeItem");
		if(!Character.dress.enabled)
		{
			Character.pants.enabled = false;
			Character.shirt.enabled = false;
			Character.dress.enabled = true;
		}
	}

	public void ChangePants(Sprite newPants)
	{
		Character.pants.gameObject.GetComponent<Animator>().Play("ChangeItem");
		Character.pants.sprite = newPants;
		if(!Character.pants.enabled)
		{
			Character.pants.enabled = true;
			Character.shirt.enabled = true;
			Character.dress.enabled = false;
		}
	}

	public void ChangeShirt(Sprite newShirt)
	{
		Character.shirt.gameObject.GetComponent<Animator>().Play("ChangeItem");
		Character.shirt.sprite = newShirt;
		if(!Character.shirt.enabled)
		{
			Character.pants.enabled = true;
			Character.shirt.enabled = true;
			Character.dress.enabled = false;
		}
	}

	public void ChangeSockLeft(Sprite newSockLeft)
	{
		Character.sockLeft.gameObject.GetComponent<Animator>().Play("ChangeItem");
		Character.sockLeft.sprite = newSockLeft;
	}

	public void ChangeSockRight(Sprite newSockRight)
	{
		Character.sockRight.gameObject.GetComponent<Animator>().Play("ChangeItem");
		Character.sockRight.sprite = newSockRight;
	}

	public void ChangePamPamLeft(Sprite newPamPamLeft)
	{
		Character.PamPamLeft.gameObject.GetComponent<Animator>().Play("ChangeItem");
		Character.PamPamLeft.sprite = newPamPamLeft;
	}

	public void ChangePamPamRight(Sprite newPamPamRight)
	{
		Character.PamPamRight.gameObject.GetComponent<Animator>().Play("ChangeItem");
		Character.PamPamRight.sprite = newPamPamRight;
	}

	public void ChangeFrontHair(Sprite newFrontHair)
	{
		Character.frontHair.sprite = newFrontHair;
	}

	public void ChangeBackHair(Sprite newBackHair)
	{
		Character.backHair.sprite = newBackHair;
	}

	public void ChangeLeftShoe(Sprite newShoeLeft)
	{
		Character.shoeLeft.gameObject.GetComponent<Animator>().Play("ChangeItem");
		Character.shoeLeft.sprite = newShoeLeft;
	}

	public void ChangeRightShoe(Sprite newShoeRight)
	{
		Character.shoeRight.gameObject.GetComponent<Animator>().Play("ChangeItem");
		Character.shoeRight.sprite = newShoeRight;
	}

	public void ShowDresses()
	{
		if(currentCategory>0 && currentCategory!=2)
		{
			HideCurrentCategory();
		}
		currentCategory = 2;
		categoryDressButton.color = Color.white; 
		categoryDressAnimator.Play("Arive");
	}

	public void ShowPants()
	{
		if(currentCategory>0 && currentCategory!=4)
		{
			HideCurrentCategory();
		}
		currentCategory = 4;
		categoryPantsButton.color = Color.white; 
		categoryPantsAnimator.Play("Arive");
	}

	public void ShowShirts()
	{
		if(currentCategory>0 && currentCategory!=3)
		{
			HideCurrentCategory();
		}
		currentCategory = 3;
		categoryShirtButton.color = Color.white; 
		categoryShirtAnimator.Play("Arive");
	}

	public void ShowHeadWears()
	{
		if(currentCategory>0 && currentCategory!=5)
		{
			HideCurrentCategory();
		}
		currentCategory = 5;
		categoryHeadWearButton.color = Color.white; 
		categoryHeadWearAnimator.Play("Arive");
	}

	public void ShowBags()
	{
		if(currentCategory>0 && currentCategory!=6)
		{
			HideCurrentCategory();
		}
		currentCategory = 6;
		categoryBagButton.color = Color.white; 
		categoryBagAnimator.Play("Arive");
	}

	public void ShowHairs()
	{
		if(currentCategory>0 && currentCategory!=1)
		{
			HideCurrentCategory();
		}
		currentCategory = 1;
		categoryHairButton.color = Color.white; 
		categoryHairAnimator.Play("Arive");
	}

	public void ShowShoes()
	{
		if(currentCategory>0 && currentCategory!=7)
		{
			HideCurrentCategory();
		}
		currentCategory = 7;
		categoryShoesButton.color = Color.white; 
		categoryShoesAnimator.Play("Arive");
	}
	// hair -1; dress -2, Shirt-3,Pants-4,HeadWear-5,Bag-6,Shoes-7
	
	void HideCurrentCategory()
	{
		switch(currentCategory)
		{
		case 1:
			categoryHairButton.color = deselectColor;
			categoryHairAnimator.Play("Depart");
			break;
		case 2:
			categoryDressButton.color = deselectColor;
			categoryDressAnimator.Play("Depart");
			break;
		case 3:
			categoryShirtButton.color = deselectColor;
			categoryShirtAnimator.Play("Depart");
			break;
		case 4:
			categoryPantsButton.color = deselectColor;
			categoryPantsAnimator.Play("Depart");
			break;
		case 5:
			categoryHeadWearButton.color = deselectColor;
			categoryHeadWearAnimator.Play("Depart");
			break;
		case 6:
			categoryBagButton.color = deselectColor;
			categoryBagAnimator.Play("Depart");
			break;
		case 7:
			categoryShoesButton.color = deselectColor;
			categoryShoesAnimator.Play("Depart");
			break;
		}
	}

}
