﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Anima2D;
#if UNITY_EDITOR
using UnityEditor;
using System.IO;
#endif
///<summary>
///<para>Scene:All/NameOfScene/NameOfScene1,NameOfScene2,NameOfScene3...</para>
///<para>Object:N/A</para>
///<para>Description: Sample Description </para>
///</summary>

public class CharacterSetUp : MonoBehaviour {
    
    //    [Header ("Accesories")]
    [HideInInspector]
    public GameObject LeftEaring,
    RightEaring,
    Necless,
    Sunglass,
    Tiara;
    
    //    [Header ("Ring")]
    [HideInInspector]
    public Transform ringHolder;
    
    //    [Header ("DressUp")]
    [HideInInspector]
    public GameObject blousesInspectorHolder,
    dress1InspectorHolder,
    dress2InspectorHolder,
    dress3InspectorHolder,
    dress4InspectorHolder,
    jackets1InspectorHolder,
    jackets2InspectorHolder,
    pants1InspectorHolder,
    pants2InspectorHolder,
    shirts1InspectorHolder,
    shirts2InspectorHolder,
    shoes1InspectorHolder,
    shoes2InspectorHolder,
    shoes3InspectorHolder,
    shorts1InspectorHolder,
    shorts2InspectorHolder,
    shorts3InspectorHolder,
    socks1InspectorHolder,
    socks2InspectorHolder,
    skirts1InspectorHolder,
    skirts2InspectorHolder,
    bagsInspectorHolder,
    pamPamsLeftInspectorHolder,
    pamPamsRightInspectorHolder;
    
    //    [Header ("MakeUp")]
    [HideInInspector]
    public Transform leftEyeHolder,
    rightEyeHolder,
    leftBlushHolder,
    rightBlushHolder,
    upperLipHolder,
    lowerLipHolder,
    shimmersHolder;
    
    //    [Header ("Beauty Salon")]
    [HideInInspector]
    public Transform leftBrowHairsHolder,
    rightBrowHairsHolder;
    
    //    [Header ("Other")]
    [HideInInspector]
    public GameObject hairHolder,
    clothesHolder,
    headTowelHolder,
    bodyTowelHolder,
    shadowHolder,
    pimplesHolder,
    mythsHolder;
    
    //    [Header ("New hair initialization")]
    //    public int initialHairLenght;
    //    public int initialHairStructure;
    //    public Color initialHairColor;
    
    Transform faceSprites;
    
    //    [Header ("Hair")]
    //    public DressUpItemPrefabScriptableObject hair; 
    
    [Header ("Eye Outlines")]
    public EyeOutlines eyeOutlines;
    
    //    [Header ("Accessories data")]
    //    public DressUpItemSpriteScriptableObject earringsData;
    //    public DressUpItemSpriteScriptableObject neclessesData;
    //    public DressUpItemSpriteScriptableObject sunglassesData;
    //    public DressUpItemSpriteScriptableObject tiarasData;
    
    //    [Header ("Ring")]
    //    public ColorPalette diamondColorPalette;
    
    //    [Header ("DressUp data")]
    //    public DressUpItemPrefabScriptableObject blouses;
    //    public DressUpItemPrefabScriptableObject dresses;
    //    public DressUpItemPrefabScriptableObject jackets;
    //    public DressUpItemPrefabScriptableObject pants;
    //    public DressUpItemPrefabScriptableObject shirts;
    //    public DressUpItemPrefabScriptableObject shoes;
    //    public DressUpItemPrefabScriptableObject shorts;
    //    public DressUpItemPrefabScriptableObject socks;
    //    public DressUpItemPrefabScriptableObject skirts;
    //    public DressUpItemSpriteScriptableObject bags;
    
    
    //    string[] newHairSaveStrings;
    
    //    void Awake()
    //    {
    //        if (GlobalVariables.newHairSaveString != "")
    //            newHairSaveStrings = GlobalVariables.newHairSaveString.Split('_');
    //        
    //        SetUpNewHair();
    //    }
    
    
    #region SetUp methods on scene start
    public void SetUpCharacter(CharacterDressUpData dressUpData, CharacterMakeUpData makeUpData)
    {
        switch (Application.loadedLevelName)
        {
            case "BeautySalon":
            case "BeautySalonPunk":
                HideClothes();
                //Show pimples and Myths... Will be deactivated from BeautySalonLogic
                //                pimplesHolder.SetActive(true);
                mythsHolder.SetActive(true);
                leftBrowHairsHolder.gameObject.SetActive(true);
                rightBrowHairsHolder.gameObject.SetActive(true);
                hairHolder.SetActive(false);
                headTowelHolder.SetActive(true);
                break;
            case "HairAndMakeUp":
            case "HairAndMakeUpPunk":
                ScaleShimmerParticles(3.5f);
                HideClothes();
                MoveHairBonesZ();
                // first we set appropriate hair and then we hide it with towel
                if(dressUpData.hairPrefab != null)
                    SetHair(dressUpData.hairPrefab.item);
                hairHolder.SetActive(false);
                headTowelHolder.SetActive(true);
                break;
            case "Accessories":
            case "AccessoriesPunk":
                ScaleShimmerParticles(3f);
                SetMakeUp(makeUpData);
                SetAccessories(dressUpData);
                SetDressUp(dressUpData);
                //Disable pam pam and bag animations in this scene
                transform.GetChild(0).GetComponent<Animator>().Play("New State", 3);
                transform.GetChild(0).GetComponent<Animator>().Play("New State",2);
                if(dressUpData.hairPrefab != null)
                    SetHair(dressUpData.hairPrefab.item);
                break;
            case "DressUp":
            case "DressUpPunk":
                ScaleShimmerParticles(0.6f);
                SetMakeUp(makeUpData);
                SetAccessories(dressUpData);
                SetDressUp(dressUpData);
                if(dressUpData.hairPrefab != null)
                    SetHair(dressUpData.hairPrefab.item);
                break;
            case "Battle":
                ScaleShimmerParticles(0.6f);
                SetMakeUp(makeUpData);
                SetAccessories(dressUpData);
                SetDressUp(dressUpData);
                if(dressUpData.hairPrefab != null)
                    SetHair(dressUpData.hairPrefab.item);
                break;
            default:
                break;
        }
    }
    //TODO da se prosledi parametar umesto GlobalVariables
    public void SetMakeUp(CharacterMakeUpData data)
    {
        //EnableMakeUpItems
        leftEyeHolder.Find("S_EyeShadow_L").gameObject.SetActive(true);
        rightEyeHolder.Find("S_EyeShadow_R").gameObject.SetActive(true);
        leftEyeHolder.Find("S_EyeLiner_L").gameObject.SetActive(true);
        rightEyeHolder.Find("S_EyeLiner_R").gameObject.SetActive(true);
        leftBlushHolder.gameObject.SetActive(true);
        rightBlushHolder.gameObject.SetActive(true);
        //EyeColor
        if (data.eyeColor != null)
        {
            leftEyeHolder.Find("EyeBall_L").GetComponent<SpriteRenderer>().color = data.eyeColor;
            rightEyeHolder.Find("EyeBall_R").GetComponent<SpriteRenderer>().color = data.eyeColor;
        }
        //Eye Shadow
        if (data.leftEyeShadowSprite != null)
        {
            leftEyeHolder.Find("S_EyeShadow_L").GetComponent<SpriteRenderer>().sprite = data.leftEyeShadowSprite;
            leftEyeHolder.Find("S_EyeShadow_L").GetComponent<SpriteRenderer>().color = Color.white;
        }
        if (data.rightEyeShadowSprite != null)
        {
            rightEyeHolder.Find("S_EyeShadow_R").GetComponent<SpriteRenderer>().sprite = data.rightEyeShadowSprite;
            rightEyeHolder.Find("S_EyeShadow_R").GetComponent<SpriteRenderer>().color = Color.white;
        }
        //Eye Liner
        if (data.leftEyeLinerSprite != null)
        {
            leftEyeHolder.Find("S_EyeLiner_L").GetComponent<SpriteRenderer>().sprite = data.leftEyeLinerSprite;
            leftEyeHolder.Find("S_EyeLiner_L").GetComponent<SpriteRenderer>().color = Color.white;
        }
        if (data.rightEyeLinerSprite != null)
        {
            rightEyeHolder.Find("S_EyeLiner_R").GetComponent<SpriteRenderer>().sprite = data.rightEyeLinerSprite;
            rightEyeHolder.Find("S_EyeLiner_R").GetComponent<SpriteRenderer>().color = Color.white;
        }
        //Blush
        if(data.leftBlushSprite != null)
        {
            leftBlushHolder.GetComponent<SpriteRenderer>().sprite = data.leftBlushSprite;
            leftBlushHolder.GetComponent<SpriteRenderer>().color = Color.white;
        }
        if (data.rightBlushSprite != null)
        {
            rightBlushHolder.GetComponent<SpriteRenderer>().sprite = data.rightBlushSprite;
            rightBlushHolder.GetComponent<SpriteRenderer>().color = Color.white;
        }
        //Mascara
        if (data.leftMascaraClosedSprite != null && data.leftMascaraOpenSprite != null && data.leftMascaraColor != null)
        {
            leftEyeHolder.Find("S_EyeOutlineClosed_L").GetComponent<SpriteRenderer>().sprite = data.leftMascaraClosedSprite;
            leftEyeHolder.Find("S_EyeOutlineOpen_L").GetComponent<SpriteRenderer>().sprite = data.leftMascaraOpenSprite;
            leftEyeHolder.Find("S_EyeOutlineOpen_L").GetComponent<SpriteRenderer>().color = data.leftMascaraColor;
        }
        if (data.rightMascaraClosedSprite != null && data.rightMascaraOpenSprite != null && data.rightMascaraColor != null)
        {
            rightEyeHolder.Find("S_EyeOutlineClosed_R").GetComponent<SpriteRenderer>().sprite = data.rightMascaraClosedSprite;
            rightEyeHolder.Find("S_EyeOutlineOpen_R").GetComponent<SpriteRenderer>().sprite = data.rightMascaraOpenSprite;
            rightEyeHolder.Find("S_EyeOutlineOpen_R").GetComponent<SpriteRenderer>().color = data.rightMascaraColor;
        }
        //Lipstick
        if(data.upperLipColor != null)
            upperLipHolder.GetComponent<SpriteMeshInstance>().color = data.upperLipColor;
        if(data.lowerLipColor != null)
            lowerLipHolder.GetComponent<SpriteMeshInstance>().color = data.lowerLipColor;
        //Shimmers
        for (int i = 0; i < shimmersHolder.childCount; i++)
        {
            shimmersHolder.GetChild(i).GetChild(0).gameObject.SetActive(data.shimmers[i]);
        }
    }
    
    //    void SetNewHair(int hairIndex)
    //    {
    //        //FIXME Prebaciti kosu da bude na istom principu kao i ostali DressUp
    ////        if (GlobalVariables.hairPresetIndex != -1)
    ////        {
    //            //TODO pass hair preset
    //            //Update indices
    //        int currentHairTypeIndex = hairIndex/4;
    //        int currentHairStyleIndex = hairIndex%4;
    //
    //            //Enable new hair preset and disable NewHairHolder
    //            hairHolder.transform.GetChild(currentHairTypeIndex).GetChild(currentHairStyleIndex).gameObject.SetActive(true);
    //            hairHolder.transform.GetChild(currentHairTypeIndex).gameObject.SetActive(true);
    //            hairHolder.transform.Find("NewHairHolder").gameObject.SetActive(false);
    //
    ////        }
    ////        else
    ////        {
    ////            if (newHairSaveStrings != null)
    ////            {
    ////                string[] tmpStrings;
    ////                for (int i = 0; i < hairHolder.transform.Find("NewHairHolder").childCount; i++)
    ////                {
    ////                    tmpStrings = newHairSaveStrings[i].Split('!');
    ////
    ////                    initialHairLenght = int.Parse(tmpStrings[0]);
    ////                    initialHairStructure = int.Parse(tmpStrings[1]);
    ////                    ColorUtility.TryParseHtmlString(tmpStrings[2], out initialHairColor);
    ////
    ////
    ////                    hairHolder.transform.Find("NewHairHolder").GetChild(i).GetComponent<HairPiece>().InitializeHair(initialHairLenght, initialHairStructure, initialHairColor);
    ////                }
    ////            }
    ////        }
    //    }
    
    
    
    void HideClothes()
    {
        for (int i = 0; i < clothesHolder.transform.childCount; i++)
        {
            clothesHolder.transform.GetChild(i).gameObject.SetActive(false);
        }
        bodyTowelHolder.SetActive(true);
    }
    
    void ScaleShimmerParticles(float scale)
    {
        foreach (Transform sh in shimmersHolder)
        {
            sh.GetChild(0).localScale = new Vector3(scale,scale,scale);
        }
    }
    
    void MoveHairBonesZ()
    {
        Transform tmpTransform;
        tmpTransform = transform.Find("AnimationHolder/BonesHolder/B_Hip/B_Spine1/B_Spine2/B_Spine3/B_Neck/B_Head/HairStyle1Bones");
        tmpTransform.position = new Vector3(tmpTransform.position.x,tmpTransform.position.y,tmpTransform.position.z-0.1f);
        
        tmpTransform = transform.Find("AnimationHolder/BonesHolder/B_Hip/B_Spine1/B_Spine2/B_Spine3/B_Neck/B_Head/HairStyle2Bones");
        tmpTransform.position = new Vector3(tmpTransform.position.x,tmpTransform.position.y,tmpTransform.position.z-0.1f);
        
        tmpTransform = transform.Find("AnimationHolder/BonesHolder/B_Hip/B_Spine1/B_Spine2/B_Spine3/B_Neck/B_Head/HairStyle3Bones");
        tmpTransform.position = new Vector3(tmpTransform.position.x,tmpTransform.position.y,tmpTransform.position.z-0.1f);
        
        tmpTransform = transform.Find("AnimationHolder/BonesHolder/B_Hip/B_Spine1/B_Spine2/B_Spine3/B_Neck/B_Head/HairStyle4Bones");
        tmpTransform.position = new Vector3(tmpTransform.position.x,tmpTransform.position.y,tmpTransform.position.z-0.1f);
        
        tmpTransform = transform.Find("AnimationHolder/BonesHolder/B_Hip/B_Spine1/B_Spine2/B_Spine3/B_Neck/B_Head/HairStyle5Bones");
        tmpTransform.position = new Vector3(tmpTransform.position.x,tmpTransform.position.y,tmpTransform.position.z-0.1f);
        
        tmpTransform = transform.Find("AnimationHolder/BonesHolder/B_Hip/B_Spine1/B_Spine2/B_Spine3/B_Neck/B_Head/HairStyle6Bones");
        tmpTransform.position = new Vector3(tmpTransform.position.x,tmpTransform.position.y,tmpTransform.position.z-0.1f);
    }
    
    void SetDressUp(CharacterDressUpData data)
    {
        if (data.blousePrefab != null)
            SetBlause(data.blousePrefab.item);
        else
            SetBlause(null);
        if (data.jacketPrefab != null)
            SetJacket(data.jacketPrefab.item);
        else
            SetJacket(null);
        if (data.dressPrefab != null)
            SetDress(data.dressPrefab.item);
        else
            SetDress(null);
        if (data.pantsPrefab != null)
            SetPants(data.pantsPrefab.item);
        else
            SetPants(null);
        if (data.shoesPrefab != null)
            SetShoes(data.shoesPrefab.item);
        else
            SetShoes(null);
        if (data.shirtPrefab != null)
            SetShirt(data.shirtPrefab.item);
        else
            SetShirt(null);
        if (data.skirtPrefab != null)
            SetSkirt(data.skirtPrefab.item);
        else
            SetSkirt(null);
        if (data.shortsPrefab != null)
            SetShorts(data.shortsPrefab.item);
        else
            SetShorts(null);
        if (data.socksPrefab != null)
            SetSocks(data.socksPrefab.item);
        else
            SetSocks(null);
        if (data.bagSprite != null)
            SetBagOrPamPams(data.bagSprite.item);
        else
            SetBagOrPamPams(null);
    }
    #endregion //SetUp methods on scene start
    #region Accessories and Jewlery Mehtods
    public void SetAccessories(CharacterDressUpData data)
    {
        if (data.earringsSprite != null)
        {
            LeftEaring.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = data.earringsSprite.item;
            RightEaring.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = data.earringsSprite.item;
            LeftEaring.SetActive(true);
            RightEaring.SetActive(true);
        }
        else
        {
            LeftEaring.SetActive(false);
            RightEaring.SetActive(false);
        }
        if (data.neclessSprite != null)
        {
            Necless.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = data.neclessSprite.item;
            Necless.SetActive(true);
        }
        else
            Necless.SetActive(false);
        if(data.sunglassesSprite != null)
        {
            Sunglass.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = data.sunglassesSprite.item;
            Sunglass.SetActive(true);
        }
        else
            Sunglass.SetActive(false);
        if (data.tiaraSprite != null)
        {
            Tiara.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = data.tiaraSprite.item;
            Tiara.SetActive(true);
        }
        else
            Tiara.SetActive(false);
    }
    //
    //    public void SetRing(int liquid, int diamond)
    //    {
    //        switch (liquid)
    //        {
    //            case -1:
    //                ringHolder.gameObject.SetActive(false);
    //                break;
    //            case 0:
    //                ringHolder.gameObject.SetActive(true);
    //                ringHolder.GetChild(1).gameObject.SetActive(true);
    //                ringHolder.GetChild(2).gameObject.SetActive(false);
    //                break;
    //            case 1:
    //                ringHolder.gameObject.SetActive(true);
    //                ringHolder.GetChild(1).gameObject.SetActive(false);
    //                ringHolder.GetChild(2).gameObject.SetActive(true);
    //                break;
    //            default:
    //                Debug.Log("Wrong ring index");
    //                break;
    //        }
    //
    //        if(diamond >= 0)
    //            ringHolder.GetChild(0).GetComponent<SpriteRenderer>().color = diamondColorPalette.colors[diamond];
    //    }
    #endregion //Accessories and Jewlery Mehtods
    
    #region DressUpMethods
    public void SetHair(GameObject hairPrefab)
    {
        Tiara.transform.localPosition = new Vector3(1.139f,-0.016f,0.135f);
        
        if(hairPrefab.name.Contains("Hair1S"))
        {
            ChangeHair("Hair1H",hairPrefab);
        }
        else if(hairPrefab.name.Contains("Hair2"))
        {
            ChangeHair("Hair2",hairPrefab);
        }
        else if(hairPrefab.name.Contains("Hair3"))
        {
            ChangeHair("Hair3",hairPrefab);
        }
        else if(hairPrefab.name.Contains("Hair4"))
        {
            ChangeHair("Hair4",hairPrefab);
        }
        else if(hairPrefab.name.Contains("Hair5"))
        {
            ChangeHair("Hair5",hairPrefab);
        }
        else if(hairPrefab.name.Contains("Hair6"))
        {
            ChangeHair("Hair6",hairPrefab);
        }
        else if(hairPrefab.name.Contains("Hair7"))
        {
            ChangeHair("Hair7",hairPrefab);
        }
        else if(hairPrefab.name.Contains("Hair8"))
        {
            ChangeHair("Hair8",hairPrefab);
            Tiara.transform.localPosition = new Vector3(1.039f,-0.016f,0.135f);
        }
        else if(hairPrefab.name.Contains("Hair9"))
        {
            ChangeHair("Hair9",hairPrefab);
            Tiara.transform.localPosition = new Vector3(1.039f,-0.016f,0.135f);
        }
        else
        {
            ChangeHair("Hair10",hairPrefab);
            Tiara.transform.localPosition = new Vector3(1.039f,-0.016f,0.135f);
        }
    }
    
    public void SetShoes(GameObject shoePrefab)
    {
        shoes1InspectorHolder.SetActive(false);
        shoes2InspectorHolder.SetActive(false);
        shoes3InspectorHolder.SetActive(false);
        
        if (shoePrefab == null)
            return;
        
        if(shoePrefab.name.Contains("Boots"))
        {
            shoes1InspectorHolder.SetActive(true);
            ReplaceSpriteMeshes(shoes1InspectorHolder.transform,shoePrefab);
        }
        else if(shoePrefab.name.Contains("HighHeels"))
        {
            shoes2InspectorHolder.SetActive(true);
            ReplaceSpriteMeshes(shoes2InspectorHolder.transform,shoePrefab);
        }
        else
        {
            shoes3InspectorHolder.SetActive(true);
            ReplaceSpriteMeshes(shoes3InspectorHolder.transform,shoePrefab);
        }
    }
    
    public void SetSocks(GameObject socksPrefab)
    {
        socks1InspectorHolder.SetActive(false);
        socks2InspectorHolder.SetActive(false);
        
        if (socksPrefab == null)
            return;
        
        if(socksPrefab.name.Contains("PantyHoes"))
        {
            socks2InspectorHolder.SetActive(true);
            ReplaceSpriteMeshes(socks2InspectorHolder.transform,socksPrefab);
        }
        else
        {
            socks1InspectorHolder.SetActive(true);
            ReplaceSpriteMeshes(socks1InspectorHolder.transform,socksPrefab);
        }
    }
    
    public void SetDress(GameObject dressPrefab)
    {
        dress1InspectorHolder.SetActive(false);
        dress2InspectorHolder.SetActive(false);
        dress3InspectorHolder.SetActive(false);
        dress4InspectorHolder.SetActive(false);
        
        if (dressPrefab == null)
            return;
        
        if(dressPrefab.name.Contains("Dress1"))
        {
            dress1InspectorHolder.SetActive(true);
            ReplaceSpriteMeshes(dress1InspectorHolder.transform,dressPrefab);
            
        }
        else if(dressPrefab.name.Contains("Dress2"))
        {
            dress2InspectorHolder.SetActive(true);
            ReplaceSpriteMeshes(dress2InspectorHolder.transform,dressPrefab);
        }
        else if(dressPrefab.name.Contains("Dress3"))
        {
            dress3InspectorHolder.SetActive(true);
            ReplaceSpriteMeshes(dress3InspectorHolder.transform,dressPrefab);
        }
        else
        {
            dress4InspectorHolder.SetActive(true);
            ReplaceSpriteMeshes(dress4InspectorHolder.transform,dressPrefab);
        }
    }
    
    public void SetShirt(GameObject shirtPrefab)
    {
        shirts1InspectorHolder.SetActive(false);
        shirts2InspectorHolder.SetActive(false);
        
        if (shirtPrefab == null)
            return;
        
        if(shirtPrefab.name.Contains("Shirt1"))
        {
            shirts1InspectorHolder.SetActive(true);
            ReplaceSpriteMeshes(shirts1InspectorHolder.transform,shirtPrefab);
        }
        else 
        {
            shirts2InspectorHolder.SetActive(true);
            ReplaceSpriteMeshes(shirts2InspectorHolder.transform,shirtPrefab);
        }
    }
    
    public void SetPants(GameObject pantsPrefab)
    {
        Transform tempTransform; 
        pants1InspectorHolder.SetActive(false);
        pants2InspectorHolder.SetActive(false);
        
        if (pantsPrefab == null)
            return;
        
        if(pantsPrefab.name.Contains("Jeans1"))
        {
            pants2InspectorHolder.SetActive(true);
            ReplaceSpriteMeshes(pants2InspectorHolder.transform,pantsPrefab);
        }
        else 
        {
            pants1InspectorHolder.SetActive(true);
            ReplaceSpriteMeshes(pants1InspectorHolder.transform,pantsPrefab);
        }
    }
    
    public void SetBlause(GameObject blousesPrefab)
    {
        blousesInspectorHolder.SetActive(false);
        if (blousesPrefab != null)
        {
            blousesInspectorHolder.SetActive(true);
            ReplaceSpriteMeshes(blousesInspectorHolder.transform,blousesPrefab);
        }
    }
    
    public void SetSkirt(GameObject skirtsPrefab)
    {
        skirts1InspectorHolder.SetActive(false);
        skirts2InspectorHolder.SetActive(false);
        
        if (skirtsPrefab == null)
            return;
        
        if(skirtsPrefab.name.Contains("Dress5"))
        {
            skirts2InspectorHolder.SetActive(true);
            ReplaceSpriteMeshes(skirts2InspectorHolder.transform,skirtsPrefab);
        }
        else
        {
            skirts1InspectorHolder.SetActive(true);
            ReplaceSpriteMeshes(skirts1InspectorHolder.transform,skirtsPrefab);
        }
    }
    
    public void SetShorts(GameObject shortsPrefab)
    {
        shorts1InspectorHolder.SetActive(false);
        shorts2InspectorHolder.SetActive(false);
        shorts3InspectorHolder.SetActive(false);

        if (shortsPrefab == null)
            return;
        
        if(shortsPrefab.name.Contains("Shorts1"))
        {
            shorts1InspectorHolder.SetActive(true);
            ReplaceSpriteMeshes(shorts1InspectorHolder.transform,shortsPrefab);
        }
        else if(shortsPrefab.name.Contains("ShortsLong"))
        {
            shorts2InspectorHolder.SetActive(true);
            ReplaceSpriteMeshes(shorts2InspectorHolder.transform,shortsPrefab);
        }
        else 
        {
            shorts3InspectorHolder.SetActive(true);
            ReplaceSpriteMeshes(shorts3InspectorHolder.transform,shortsPrefab);
        }
    }
    
    public void SetJacket (GameObject jacketsPrefab)
    {
        jackets1InspectorHolder.SetActive(false);
        jackets2InspectorHolder.SetActive(false);
        
        if (jacketsPrefab == null)
            return;
        
        if(jacketsPrefab.name.Contains("ZJacket"))
        {
            jackets1InspectorHolder.SetActive(true);
            ReplaceSpriteMeshes(jackets1InspectorHolder.transform,jacketsPrefab);
        }
        else
        {
            jackets2InspectorHolder.SetActive(true);
            ReplaceSpriteMeshes(jackets2InspectorHolder.transform,jacketsPrefab);
        }
    }
    
    public void SetBagOrPamPams(Sprite bagsSprite)
    {
        Animator anim = transform.GetChild(0).GetComponent<Animator>();
        
        bagsInspectorHolder.SetActive(false);
        
        if (bagsSprite == null)
        {
            anim.Play("New State", 3);
            anim.Play("New State",2);
            return;
        }
        
        //        if(bagsIndex<10)
        //        {
        anim.Play("New State", 3);
        anim.Play("CharacterBagHolding", 2, Random.Range(0f,1f));
        bagsInspectorHolder.SetActive(true);
        pamPamsLeftInspectorHolder.SetActive(false);
        pamPamsRightInspectorHolder.SetActive(false);
        
        bagsInspectorHolder.GetComponent<SpriteRenderer>().sprite = bagsSprite;
        //        }
        //        else
        //        {
        //            anim.Play("New State", 2);
        //            anim.Play("PamPamHolding",3, Random.Range(0f,1f));
        //
        //            bagsInspectorHolder.SetActive(false);
        //            pamPamsLeftInspectorHolder.SetActive(true);
        //            pamPamsRightInspectorHolder.SetActive(true);
        //
        //            pamPamsLeftInspectorHolder.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = bags.elements[bagsIndex].item;
        //            pamPamsRightInspectorHolder.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = bags.elements[bagsIndex].item;
        //        }
    }
    
    void ReplaceSpriteMeshes(Transform tempTransform, GameObject itemPrefab)
    {
        for (int i = 0; i < tempTransform.childCount; i++)
        {
            if (tempTransform.GetChild(i).GetComponent<SpriteMeshInstance>() != null)
            {
                if(itemPrefab != null)
                    tempTransform.GetChild(i).GetComponent<SpriteMeshInstance>().spriteMesh = itemPrefab.transform.GetChild(i).GetComponent<SpriteMeshInstance>().spriteMesh;
            }
        }
    }
    #endregion //DressUpMethods
    
    public void GetAllReferences()
    {
        faceSprites = transform.Find("AnimationHolder/BonesHolder/B_Hip/B_Spine1/B_Spine2/B_Spine3/B_Neck/B_Head/FaceSprites");
        //Accessories
        LeftEaring = faceSprites.Find("Ear_L/EaringHolderL").gameObject;
        RightEaring = faceSprites.Find("Ear_R/EaringHolderR").gameObject;
        Necless = transform.Find("AnimationHolder/BonesHolder/B_Hip/B_Spine1/B_Spine2/B_Spine3/NecklessHolder").gameObject;
        Sunglass = faceSprites.Find("GlassessHolder").gameObject;
        Tiara = faceSprites.Find("TiaraHolder").gameObject;
        //Ring
        ringHolder = transform.Find("AnimationHolder/BonesHolder/B_Hip/B_Spine1/B_Spine2/B_Spine3/B_Lung_R/B_ArmUpper_R/B_ArmLower_R/B_Hand_R/B_Hand_RF4_1/RingHolder");
        //DressUp
        clothesHolder = transform.Find("AnimationHolder/SpriteMeshesHolder/ClothesHolder").gameObject;
        blousesInspectorHolder = clothesHolder.transform.Find("BlousesHolder 8/Blouse1Holder/Blouse1Style1").gameObject;
        dress1InspectorHolder = clothesHolder.transform.Find("DressHolder 20/Dress1Holder 4/Dress1Style1").gameObject;
        dress2InspectorHolder = clothesHolder.transform.Find("DressHolder 20/Dress2Holder 4/Dress2Style1").gameObject;
        dress3InspectorHolder = clothesHolder.transform.Find("DressHolder 20/Dress3Holder 8/Dress3Style1").gameObject;
        dress4InspectorHolder = clothesHolder.transform.Find("DressHolder 20/Dress4Holder 4/Dress4Style1").gameObject;
        jackets1InspectorHolder = clothesHolder.transform.Find("JacketsHolder 8/JacketHolder 4/Jacket1Holder/Jacket1Style1").gameObject;
        jackets2InspectorHolder = clothesHolder.transform.Find("JacketsHolder 8/FurCoatHolder 4/FurCoat1Holder/FurCoat1Style1").gameObject;
        pants1InspectorHolder = clothesHolder.transform.Find("PantsHolder 9/Pants1Holder/Pants1Style1").gameObject;
        pants2InspectorHolder = clothesHolder.transform.Find("PantsHolder 9/Jeans1Holder/Jeans1Style1").gameObject;
        shirts1InspectorHolder = clothesHolder.transform.Find("ShirtsHolder 12/Shirt1Holder 6/Shirt1Style1").gameObject;
        shirts2InspectorHolder = clothesHolder.transform.Find("ShirtsHolder 12/Shirt2Holder 6/Shirt2Style1").gameObject;
        shoes1InspectorHolder = clothesHolder.transform.Find("ShoesHolder 18/BootsHolder 6/Boots1Holder/Boots1Style1").gameObject;
        shoes2InspectorHolder = clothesHolder.transform.Find("ShoesHolder 18/SnickersHolder 6/Snickers1Holder/Snickers1Style1").gameObject;
        shoes3InspectorHolder = clothesHolder.transform.Find("ShoesHolder 18/HighHeelsHolder 6/HighHeels1Holder/HighHeels1Style1").gameObject;
        shorts1InspectorHolder = clothesHolder.transform.Find("ShortsHolder 18/Shorts1Holder/Shorts1Style1").gameObject;
        shorts2InspectorHolder = clothesHolder.transform.Find("ShortsHolder 18/ShortsLong1Holder/ShortsLong1Style1").gameObject;
        shorts3InspectorHolder = clothesHolder.transform.Find("ShortsHolder 18/ShortsWide1Holder/ShortsWide1Style1").gameObject;
        socks1InspectorHolder = clothesHolder.transform.Find("SocksHolder 3/SocksLongHolder/SocksLong1Holder/SocksLong1Style1").gameObject;
        socks2InspectorHolder = clothesHolder.transform.Find("PantyHoesHolder 3/PantyHoes1Holder/PantyHoes1Style1").gameObject;
        skirts1InspectorHolder = clothesHolder.transform.Find("SkirtHolder 4/SkirtStyle1").gameObject;
        skirts2InspectorHolder = clothesHolder.transform.Find("SkirtHolder 4/SkirtStyle2").gameObject;
        bagsInspectorHolder = transform.Find("AnimationHolder/BonesHolder/B_Hip/B_Spine1/B_Spine2/B_Spine3/B_Lung_R/B_ArmUpper_R/B_ArmLower_R/B_Hand_R/BagHolder/Bags01_0").gameObject;
        pamPamsLeftInspectorHolder = transform.Find("AnimationHolder/BonesHolder/B_Hip/B_Spine1/B_Spine2/B_Spine3/B_Lung_L/B_ArmUpper_L/B_ArmLower_L/B_Hand_L/PamPamsHolder_L").gameObject;
        pamPamsRightInspectorHolder = transform.Find("AnimationHolder/BonesHolder/B_Hip/B_Spine1/B_Spine2/B_Spine3/B_Lung_R/B_ArmUpper_R/B_ArmLower_R/B_Hand_R/PamPamsHolder_R").gameObject;
        //MakeUp
        leftEyeHolder = faceSprites.Find("EyesHolder/EyeHolder_L");
        rightEyeHolder = faceSprites.Find("EyesHolder/EyeHolder_R");
        leftBlushHolder = faceSprites.Find("Blush_L");
        rightBlushHolder = faceSprites.Find("Blush_R");
        upperLipHolder = transform.Find("AnimationHolder/SpriteMeshesHolder/BodyHolder/LipsColorsHolder/Lips1Style1/LipUpStyle1");
        lowerLipHolder = transform.Find("AnimationHolder/SpriteMeshesHolder/BodyHolder/LipsColorsHolder/Lips1Style1/LipDownStyle1");
        shimmersHolder = faceSprites.transform.parent.Find("ShimmersHolder");
        //Beauty Salon
        leftBrowHairsHolder = faceSprites.Find("EyeBrowasHolder/EyeBrowHolder_L/EyeBrow_L/EyeBrowHairsHolder_L/HairsHolder");
        rightBrowHairsHolder = faceSprites.Find("EyeBrowasHolder/EyeBrowHolder_R/EyeBrow_R/EyeBrowHairsHolder_R/HairsHolder");
        //Other
        hairHolder = transform.Find("AnimationHolder/SpriteMeshesHolder/HairHolder").gameObject;
        headTowelHolder = clothesHolder.transform.Find("HeadTowelHolder").gameObject;
        bodyTowelHolder = clothesHolder.transform.Find("BodyTowelHolder").gameObject;
        shadowHolder = transform.Find("AnimationHolder/ShadowHolder").gameObject;
        pimplesHolder = faceSprites.Find("PimplesHolder").gameObject;
        mythsHolder = faceSprites.Find("Nose/Myths").gameObject;
        Debug.Log("Get references");
        
    }
    
    void ChangeHair(string hairType, GameObject hairPrefab)
    {
        Transform hairTypeHolder = null;
        foreach (Transform child in hairHolder.transform)
        {
            if (child.name.Contains(hairType))
            {
                child.gameObject.SetActive(true);
                hairTypeHolder = child;
            }
            else
                child.gameObject.SetActive(false);
        }
        if (hairTypeHolder != null)
        {
            for (int i = 0; i < hairTypeHolder.GetChild(0).childCount; i++)
            {
                hairTypeHolder.GetChild(0).GetChild(i).GetComponent<SpriteMeshInstance>().spriteMesh = hairPrefab.transform.GetChild(i).GetComponent<SpriteMeshInstance>().spriteMesh;
            }
        }
        else
            Debug.Log("There is no child object that contains: " + hairType);
    }
    
    //    void SetUpNewHair()
    //    {
    //        foreach (Transform hairPiece in hairHolder.transform.Find("NewHairHolder"))
    //        {
    //            if (hairPiece.GetComponent<HairPiece>() == null)
    //            {
    //                hairPiece.gameObject.AddComponent<HairPiece>();
    //            }
    //        }
    //    }
}
/// <summary>
/// Helper class for handling new hair.
/// </summary>
public class HairPiece : MonoBehaviour {
    
    //    CharacterSetUp characterSetUp;
    int currentHairLenghtIndex;
    int currentHairStructureIndex;
    Color currentHairColor = Color.white;
    
    public static bool itemPickedUp = false;
    
    static HairPiece currentHairPice;
    
    //    void Start()
    //    {
    //        HairSalonLogic hsl = GameObject.FindObjectOfType<HairSalonLogic>();
    //        if (hsl != null)
    //            hsl.onHairWet.AddListener(ChangeHairStructure);
    //    }
    //
    //    void OnDestroy()
    //    {
    //        HairSalonLogic hsl = GameObject.FindObjectOfType<HairSalonLogic>();
    //        if (hsl != null)
    //            hsl.onHairWet.RemoveListener(ChangeHairStructure);
    //    }
    
    #region OnMouse events
    void OnMouseEnter()
    {
        if (itemPickedUp)
        {
            Debug.Log("OnMouseEnter");
            currentHairPice = this;
            transform.GetChild(currentHairLenghtIndex).GetChild(currentHairStructureIndex).GetComponent<SpriteMeshInstance>().color = currentHairColor * 0.7f;
        }
    }
    
    void OnMouseExit()
    {
        if (itemPickedUp)
        {
            Debug.Log("OnMouseExit");
            transform.GetChild(currentHairLenghtIndex).GetChild(currentHairStructureIndex).GetComponent<SpriteMeshInstance>().color = currentHairColor;
            currentHairPice = null;
        }
    }
    #endregion //OnMouse events
    
    #region Setters
    public void ChangeHairLenght(int newHairLenghtIndex)
    {
        transform.GetChild(currentHairLenghtIndex).GetChild(currentHairStructureIndex).GetComponent<SpriteMeshInstance>().color = currentHairColor;
        
        if (newHairLenghtIndex >= 0 && newHairLenghtIndex < transform.childCount && newHairLenghtIndex != currentHairLenghtIndex)
        {
            StartCoroutine(CFadeHair(transform.GetChild(currentHairLenghtIndex).GetChild(currentHairStructureIndex).gameObject,false));
            StartCoroutine(CFadeHair(transform.GetChild(newHairLenghtIndex).GetChild(currentHairStructureIndex).gameObject,true));
            currentHairLenghtIndex = newHairLenghtIndex;
        }
    }
    
    public void ChangeHairStructure(int newHairStructureIndex)
    {
        transform.GetChild(currentHairLenghtIndex).GetChild(currentHairStructureIndex).GetComponent<SpriteMeshInstance>().color = currentHairColor;
        
        if (newHairStructureIndex >= 0 && newHairStructureIndex < transform.GetChild(currentHairLenghtIndex).childCount && newHairStructureIndex != currentHairStructureIndex)
        {
            StartCoroutine(CFadeHair(transform.GetChild(currentHairLenghtIndex).GetChild(currentHairStructureIndex).gameObject,false));
            StartCoroutine(CFadeHair(transform.GetChild(currentHairLenghtIndex).GetChild(newHairStructureIndex).gameObject,true));
            currentHairStructureIndex = newHairStructureIndex;
        }
    }
    /// <summary>
    /// Changes the color of the hair by lerping it from current to new color.
    /// </summary>
    /// <param name="newColor">New color.</param>
    public void ChangeHairColor(Color newColor)
    {
        StartCoroutine(CLeprColor(newColor));
    }
    /// <summary>
    /// Instantly sets the color of the hair.
    /// </summary>
    /// <param name="newColor">New color.</param>
    public void InitializeHair(int lenghtIndex, int structIndex,Color newColor)
    {
        transform.GetChild(lenghtIndex).GetChild(structIndex).gameObject.SetActive(false);
        currentHairLenghtIndex = lenghtIndex;
        currentHairStructureIndex = structIndex;
        currentHairColor = newColor;
        //FIXME disable other
        transform.GetChild(lenghtIndex).GetChild(structIndex).gameObject.SetActive(true);
        transform.GetChild(lenghtIndex).GetChild(structIndex).GetComponent<SpriteMeshInstance>().color = currentHairColor;
    }
    
    public static void ResetCurrentHairPiece()
    {
        currentHairPice = null;
    }
    #endregion //Setters
    
    #region Getters
    public static HairPiece GetCurrentHairPiece()
    {
        return currentHairPice;
    }
    
    public int GetLenght()
    {
        return currentHairLenghtIndex;
    }
    
    public int GetStructure()
    {
        return currentHairStructureIndex;
    }
    
    public string GetColorHtml()
    {
        return ColorUtility.ToHtmlStringRGBA(currentHairColor);
    }
    #endregion //Getters
    
    #region HelperMethods
    IEnumerator CLeprColor(Color newColor)
    {
        float ElapsedTime = 0.0f;
        float TotalTime = 1.0f;
        while (ElapsedTime < TotalTime) {
            ElapsedTime += Time.deltaTime;
            transform.GetChild(currentHairLenghtIndex).GetChild(currentHairStructureIndex).GetComponent<SpriteMeshInstance>().color = Color.Lerp(currentHairColor, newColor, (ElapsedTime / TotalTime));
            yield return null;
        }
        yield return new WaitForEndOfFrame();
        currentHairColor = newColor;
    }
    
    IEnumerator CFadeHair(GameObject hair, bool fadeIn)
    {
        Debug.Log("Fading hair " + fadeIn);
        float ElapsedTime = 0.0f;
        float TotalTime = 2.0f;
        Color tempColor = currentHairColor;
        float targetAlpha = 0;
        float startAlpha = 0;
        
        if (fadeIn)
        {
            hair.SetActive(true);
            targetAlpha = 1;
        }
        else
        {
            startAlpha = currentHairColor.a;
        }
        
        while (ElapsedTime < TotalTime) {
            ElapsedTime += Time.deltaTime;
            tempColor.a = Mathf.Lerp(startAlpha, targetAlpha, (ElapsedTime / TotalTime));
            hair.GetComponent<SpriteMeshInstance>().color = tempColor;
            yield return null;
        }
        yield return new WaitForEndOfFrame();
        //        Debug.Log("Fade done : " + hair.GetComponent<SpriteMeshInstance>().color);
        if (!fadeIn)
            hair.SetActive(false);
    }
    #endregion //HleperMethods
}

/// <summary>
/// Character set up editor extension for GetReferences button.
/// </summary>
#if UNITY_EDITOR
[CustomEditor(typeof(CharacterSetUp))]
public class CharacterSetUpEditor : Editor {

//    ContestOpponentDressUpData asset;

public override void OnInspectorGUI()
{
CharacterSetUp myScript = (CharacterSetUp)target;

GUIStyle style = new GUIStyle(GUI.skin.button);
style.normal.textColor = Color.green;

if(GUILayout.Button("Get All References",style))
{
myScript.GetAllReferences();
}

DrawDefaultInspector();
}


}
#endif