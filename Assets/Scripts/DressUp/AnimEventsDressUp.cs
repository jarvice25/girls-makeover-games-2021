﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

///<summary>
///<para>Scene:All/NameOfScene/NameOfScene1,NameOfScene2,NameOfScene3...</para>
///<para>Object:N/A</para>
///<para>Description: Sample Description </para>
///</summary>

public class AnimEventsDressUp : MonoBehaviour {

	public void TurnOfLoading()
	{
		transform.parent.parent.gameObject.SetActive(false);
	}

	public void ChangeZoomIcon()
	{
		if(GlobalVariables.ZoomedIn)
		{
            transform.GetChild(0).gameObject.SetActive(false);
            transform.GetChild(1).gameObject.SetActive(true);
        }
        else
        {
            transform.GetChild(0).gameObject.SetActive(true);
            transform.GetChild(1).gameObject.SetActive(false);
		}
	}

    public void ChangeParticleIcon()
    {
        for (int i = 0; i < transform.childCount-1; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }

        transform.GetChild(DressUpAnima2D.currentParticle).gameObject.SetActive(true);
    }

	public void DisableCustomButton()
	{
        transform.parent.GetChild(3).GetComponent<CustomButton>().interactable=false;
        transform.parent.parent.Find("ButtonArrowRight").GetComponentInChildren<Button>().interactable=false;
    }

    public void EnableCustomButton()
    {
        transform.parent.GetChild(3).GetComponent<CustomButton>().interactable=true;
        transform.parent.parent.Find("ButtonArrowRight").GetComponentInChildren<Button>().interactable=true;
	}
}
