﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

///<summary>
///<para>Scene:All/NameOfScene/NameOfScene1,NameOfScene2,NameOfScene3...</para>
///<para>Object:N/A</para>
///<para>Description: Sample Description </para>
///</summary>

public class LevelUp : MonoBehaviour {

	public delegate void LevelUpDel();
	public static event LevelUpDel OnLevelUp;
	Image imageProgress;
	Text lvlText;
	RectTransform pointerHolder;
	ParticleSystem lvlUpParticle;
	float buttonAngle;
	// Use this for initialization
	void Awake () {
		imageProgress = transform.Find("YelowPart").GetComponent<Image>();
		lvlText = transform.Find("Text").GetComponent<Text>();
		pointerHolder = transform.Find("YelowPart/PointerHolder").GetComponent<RectTransform>();
		lvlUpParticle = transform.Find("LvlUpParticle").GetComponent<ParticleSystem>();
    }

    void Start()
    {
        lvlText.text = GlobalVariables.currentLvl.ToString();
        ExpChange((float)GlobalVariables.experience/GlobalVariables.nextLvlExpNeeded);
    }

	public void AddExperience(int experienceToAdd)
	{
		GlobalVariables.experience+=experienceToAdd;
		//FIX dodaj deo da se cuva u Prefs
		if(GlobalVariables.experience>=GlobalVariables.nextLvlExpNeeded)
		{
			GlobalVariables.currentLvl++;
            if(OnLevelUp != null)
                OnLevelUp();
			lvlUpParticle.Play(true);
			GlobalVariables.currentLvlExpNeeded = GlobalVariables.nextLvlExpNeeded;
			GlobalVariables.nextLvlExpNeeded = GlobalVariables.currentLvlExpNeeded + (GlobalVariables.currentLvl+1)*50;
			if(GlobalVariables.experience>GlobalVariables.currentLvlExpNeeded)
			{
				if(GlobalVariables.experience-GlobalVariables.currentLvlExpNeeded>GlobalVariables.experience)
				{
					while(GlobalVariables.experience>GlobalVariables.currentLvlExpNeeded) 
						GlobalVariables.experience = GlobalVariables.experience-GlobalVariables.currentLvlExpNeeded;
					
					if(GlobalVariables.experience>GlobalVariables.currentLvlExpNeeded/2)
						GlobalVariables.experience = Random.Range(0,GlobalVariables.currentLvlExpNeeded/2);
				}
				else
				{
					GlobalVariables.experience = GlobalVariables.experience-GlobalVariables.currentLvlExpNeeded;
				}
			}
			else
			{
				GlobalVariables.experience = 0;
			}
		}
		lvlText.text = GlobalVariables.currentLvl.ToString();
		ExpChange((float)GlobalVariables.experience/GlobalVariables.nextLvlExpNeeded);

        PlayerPrefs.SetInt("experience",GlobalVariables.experience);
        PlayerPrefs.SetInt("currentLvl",GlobalVariables.currentLvl);
        PlayerPrefs.SetInt("currentLvlExpNeeded",GlobalVariables.currentLvlExpNeeded);
        PlayerPrefs.Save();
	}
	
	void ExpChange(float amount)
    {
        if (float.IsNaN(amount))
            amount = 0;
        
		imageProgress.fillAmount = amount;
		buttonAngle = amount * 360;
		pointerHolder.eulerAngles = new Vector3(0, 0, -buttonAngle);
	}
}
