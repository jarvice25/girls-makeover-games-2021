﻿using UnityEngine;
using System.Collections;

/**
  * Scene:All
  * Object:SoundManager
  * Description: Skripta zaduzena za zvuke u apliakciji, njihovo pustanje, gasenje itd...
  **/
public class SoundManager : MonoBehaviour
{

    public static int musicOn = 1;
    public static int soundOn = 1;
    public static bool forceTurnOff = false;

    [Header("Music")]
    public AudioSource gameplayMusic;
    //    public AudioSource menuMusic;

    [Header("Sound FX")]
    public AudioSource buttonClick;
    public AudioSource buttonClick2;
    public AudioSource buttonClick3;
    public AudioSource buttonClick4;
    public AudioSource buttonClick5;
    public AudioSource pimplesPop;
    public AudioSource plasterPop;
    public AudioSource eyebowsPop;
    //    public AudioSource scissorsCut;
    //    public AudioSource hairGrowSpray;
    //    public AudioSource hairColorBrush;
    public AudioSource characterSelected;
    public AudioSource characterLocked;
    public AudioSource applyCreme;
    public AudioSource applyShampoo;
    public AudioSource phoneNotification;
    public AudioSource phoneSms;
    public AudioSource voting;

    [Header("Item Sounds")]
    public AudioSource itemPickUp;
    public AudioSource itemShower;
    public AudioSource itemSteamer;
    public AudioSource itemMassage;
    //    public AudioSource itemDryer;

    static SoundManager instance;

    public static SoundManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType(typeof(SoundManager)) as SoundManager;
            }

            return instance;
        }
    }

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    void Start()
    {
        DontDestroyOnLoad(this.gameObject);

        if (PlayerPrefs.HasKey("SoundOn"))
        {
            musicOn = PlayerPrefs.GetInt("MusicOn");
            soundOn = PlayerPrefs.GetInt("SoundOn");
        }

        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    #region Music
    public void Play_MenuMusic()
    {
        //        if(menuMusic.clip != null && musicOn == 1)
        //            menuMusic.Play();
    }

    public void Stop_MenuMusic()
    {
        //        if(menuMusic.clip != null && musicOn == 1)
        //            menuMusic.Stop();
    }

    public void Play_GameplayMusic()
    {
        if (gameplayMusic.clip != null && musicOn == 1)
        {
            gameplayMusic.Play();
        }
    }

    public void Stop_GameplayMusic()
    {
        if (gameplayMusic.clip != null)
        {
            gameplayMusic.Stop();
            //            StartCoroutine(FadeOut(gameplayMusic, 0.005f));
        }
    }
    #endregion //Music

    #region SoundFX
    public void Play_ButtonClick()
    {
        if (buttonClick.clip != null && soundOn == 1)
            buttonClick.Play();
    }

    public void Play_ButtonClick2()
    {
        if (buttonClick2.clip != null && soundOn == 1)
            buttonClick2.Play();
    }

    public void Play_ButtonClick3()
    {
        if (buttonClick3.clip != null && soundOn == 1)
            buttonClick3.Play();
    }

    public void Play_ButtonClick4()
    {
        if (buttonClick4.clip != null && soundOn == 1)
            buttonClick4.Play();
    }

    public void Play_ButtonClick5()
    {
        if (buttonClick5.clip != null && soundOn == 1)
            buttonClick5.Play();
    }

    public void Play_PimplesPop()
    {
        if (pimplesPop.clip != null && soundOn == 1)
            pimplesPop.Play();
    }

    public void Play_PlasterPop()
    {
        if (plasterPop.clip != null && soundOn == 1)
            plasterPop.Play();
    }

    public void Play_EyebowsPop()
    {
        if (eyebowsPop.clip != null && soundOn == 1)
            eyebowsPop.Play();
    }

    public void Play_ScissorsCut()
    {
        //        if(scissorsCut.clip != null && soundOn == 1)
        //            scissorsCut.Play();
    }

    public void Play_HairGrowSpray()
    {
        //        if(hairGrowSpray.clip != null && soundOn == 1)
        //            hairGrowSpray.Play();
    }

    public void Play_HairColorBrush()
    {
        //        if(hairColorBrush.clip != null && soundOn == 1)
        //            hairColorBrush.Play();
    }

    public void Stop_HairColorBrush()
    {
        //        if(hairColorBrush.clip != null && soundOn == 1)
        //            hairColorBrush.Stop();
    }

    public void Play_CharacterSelected()
    {
        if (characterSelected.clip != null && soundOn == 1)
            characterSelected.Play();
    }

    public void Play_CharacterLocked()
    {
        if (characterLocked.clip != null && soundOn == 1)
            characterLocked.Play();
    }

    public void Play_ApplyCreme()
    {
        if (applyCreme.clip != null && soundOn == 1)
            applyCreme.Play();
    }

    public void Stop_ApplyCreme()
    {
        if (applyCreme.clip != null && soundOn == 1)
            applyCreme.Stop();
    }

    public void Play_ApplyShampoo()
    {
        if (applyShampoo.clip != null && soundOn == 1)
            applyShampoo.Play();
    }

    public void Stop_ApplyShampoo()
    {
        if (applyShampoo.clip != null && soundOn == 1)
            applyShampoo.Stop();
    }

    public void Play_PhoneNotification()
    {
        if (phoneNotification.clip != null && soundOn == 1)
            phoneNotification.Play();
    }

    public void Play_PhoneSMS()
    {
        if (phoneSms.clip != null && soundOn == 1)
            phoneSms.Play();
    }

    public void Play_Voting()
    {
        if (voting.clip != null && soundOn == 1)
            voting.Play();
    }
    #endregion //SoundFX

    #region ItemSounds
    public void Play_ItemPickUp()
    {
        Debug.Log("item picked up");
        if (itemPickUp.clip != null && soundOn == 1)
            itemPickUp.Play();
    }

    public void Play_ItemShower()
    {
        if (itemShower.clip != null && musicOn == 1)
            itemShower.Play();
    }

    public void Stop_ItemShower()
    {
        if (itemShower.clip != null && musicOn == 1)
            itemShower.Stop();
    }

    public void Play_ItemSteamer()
    {
        if (itemSteamer.clip != null && musicOn == 1)
            itemSteamer.Play();
    }

    public void Stop_ItemSteamer()
    {
        if (itemSteamer.clip != null && musicOn == 1)
            itemSteamer.Stop();
    }

    public void Play_ItemMassage()
    {
        if (itemMassage.clip != null && musicOn == 1)
            itemMassage.Play();
    }

    public void Stop_ItemMassage()
    {
        if (itemMassage.clip != null && musicOn == 1)
            itemMassage.Stop();
    }

    public void Play_ItemDryer()
    {
        //        if(itemDryer.clip != null && musicOn == 1)
        //            itemDryer.Play();
    }

    public void Stop_ItemDryer()
    {
        //        if(itemDryer.clip != null && musicOn == 1)
        //            itemDryer.Stop();
    }
    #endregion //ItemSounds




    /// <summary>
    /// Corutine-a koja za odredjeni AudioSource, kroz prosledjeno vreme, utisava AudioSource do 0, gasi taj AudioSource, a zatim vraca pocetni Volume na pocetan kako bi AudioSource mogao opet da se koristi
    /// </summary>
    /// <param name="sound">AudioSource koji treba smanjiti/param>
    /// <param name="time">Vreme za koje treba smanjiti Volume/param>
    IEnumerator FadeOut(AudioSource sound, float time)
    {
        float originalVolume = sound.volume;
        while (sound.volume != 0)
        {
            sound.volume = Mathf.MoveTowards(sound.volume, 0, time);
            yield return null;
        }
        sound.Stop();
        sound.volume = originalVolume;
    }

    /// <summary>
    /// F-ja koja Mute-uje sve zvuke koja su deca SoundManager-a
    /// </summary>
    public void MuteAllSounds()
    {
        foreach (Transform t in transform)
        {
            t.GetComponent<AudioSource>().mute = true;
        }
    }

    /// <summary>
    /// F-ja koja Unmute-uje sve zvuke koja su deca SoundManager-a
    /// </summary>
    public void UnmuteAllSounds()
    {
        foreach (Transform t in transform)
        {
            t.GetComponent<AudioSource>().mute = false;
        }
    }

}
