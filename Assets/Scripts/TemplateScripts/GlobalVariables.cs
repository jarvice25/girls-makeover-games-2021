﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

///<summary>
///<para>Scene:All/NameOfScene/NameOfScene1,NameOfScene2,NameOfScene3...</para>
///<para>Object:N/A</para>
///<para>Description: Sample Description </para>
///</summary>

public class GlobalVariables : MonoBehaviour
{

    public static string applicationID = "";

    public static int sceneToLoad = 0; //0 - main scene, 1 - Witches, 2 - GamePlay, 3 - PrinceSelection

    public static bool readyToContest = false;
    public static int currentPlayerIndex = 0;
    public static int[] selectedCharacterIndex = new int[] { 0, 0 };
    public static string[] selectedCharacterName = new string[] { "First", "Second" };

    public static bool DressActive = true;
    public static bool ZoomedIn = false;

    #region jewelry section
    public static int jewelryRingCase = 1;
    //    public static int jewelryLiquidIndex = -1;  //-1-no ring, 0-silver, 1 - gold
    public static int jewelryMoldSize = 0; //1 ,2 ,3 , 4
    public static int jewelrySideDiamonds = 1;
    //    public static int jewelryDiamond = 0;
    public static int jewelryDiamondCase = 1;
    #endregion

    public static CharacterDressUpData[] mainCharacterDressUpData = new CharacterDressUpData[2];
    public static CharacterMakeUpData[] mainCharacterMakeUpData = new CharacterMakeUpData[2];

    #region InnApps
    public static bool fullGameOwned = false;
    public static bool removeAds = false;
    public static bool unlockAll = false;
    #endregion

    #region EXP
    static readonly int expUnlockedMultiplier = 10;
    static readonly int expVideoLockedMultiplier = 50;
    static readonly int expExpLockedMultiplier = 100;
    static readonly int expAddition = 50;
    public static int experience; //trenutni broj experienca
    public static int currentLvl = 1; // trenutni nivo
    public static int nextLvlExpNeeded; // potreban broj poena za sledeci nivo
    public static int currentLvlExpNeeded;//  potreban broj poena za tekuci nivo
    int lvl2Points = 500; // broj poena za lvl2
    static Dictionary<string, int> xpBuffer = new Dictionary<string, int>(); //ovde se smesta experience pre nego sto se dodeli putem LevelUp.AddExperience(xpBuffer);
    static List<ExpLockedItem> expLockedItems;
    #endregion 

    #region Map
    public static bool phoneAnswered = false;
    //    public static bool challengeAccepted = false;
    public static int nextLevelPointer = 0;
    //    public static EItemStyles currentContestQuest = (EItemStyles)0;
    #endregion

    #region Contest Votes
    static Dictionary<string, int>[] contestVotesBufffers = new Dictionary<string, int>[2];
    #endregion 

    public static int numberOfItemsUsed;    //Number of items used in BeautySalon scene

    public static string offlineNativeAdUrl = "https://play.google.com/store/apps/details?id=com.kitty.hospital.veterinarian.clinic&hl=en";
    //    public static FacebookNativeAd curentNativeAd;

    static string videoUnlockedItemsSaveString = "";
    //    static string expUnlockedItemsSaveString = "";

    static GlobalVariables instance;

    //ExpLockedItemsGetter
    public static List<ExpLockedItem> GetExpLockedItemsList()
    {
        return expLockedItems;
    }

    // Use this for initialization
    void Awake()
    {

        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
        Input.multiTouchEnabled = false;
#if UNITY_ANDROID
        applicationID = "com.High.School.Dress.Up.Game";
#elif UNITY_IOS
		applicationID = "bundle.ID";
#endif

        LoadUnlockedItemsString();

        if (PlayerPrefs.HasKey("experience"))
        {
            experience = PlayerPrefs.GetInt("experience");
            currentLvl = PlayerPrefs.GetInt("currentLvl");
            currentLvlExpNeeded = PlayerPrefs.GetInt("currentLvlExpNeeded");
        }
        else
        {
            currentLvlExpNeeded = 0;
            experience = 0;
            currentLvl = 1;
        }

        if (currentLvl < 2)
        {
            nextLvlExpNeeded = lvl2Points;
        }
        else
        {
            nextLvlExpNeeded = currentLvlExpNeeded + (currentLvl + 1) * 50;
        }

        if (PlayerPrefs.HasKey("removeAds"))
        {
            int tripetsedam = PlayerPrefs.GetInt("removeAds");
            if (tripetsedam == 357)
            {
                GlobalVariables.removeAds = true;
            }
        }

        if (PlayerPrefs.HasKey("unlockAll"))
        {
            int tripetsedam = PlayerPrefs.GetInt("unlockAll");
            if (tripetsedam == 357)
            {
                GlobalVariables.unlockAll = true;
            }
        }

        if (PlayerPrefs.HasKey("fullGame"))
        {
            int tripetsedam = PlayerPrefs.GetInt("fullGame");
            if (tripetsedam == 357)
            {
                GlobalVariables.fullGameOwned = true;
                GlobalVariables.unlockAll = true;
                GlobalVariables.removeAds = true;
            }
        }

        contestVotesBufffers[0] = new Dictionary<string, int>();
        contestVotesBufffers[1] = new Dictionary<string, int>();

        InitializeMainCharacterMakeUpData();
        InitializeMainCharacterDressUpData();

        InitializeExpLockedItems();
    }

    void LoadUnlockedItemsString()
    {
        if (PlayerPrefs.HasKey("VideoUnlockedItems"))
            videoUnlockedItemsSaveString = PlayerPrefs.GetString("VideoUnlockedItems");

        //        if (PlayerPrefs.HasKey("ExpUnlockedItems"))
        //            expUnlockedItemsSaveString = PlayerPrefs.GetString("ExpUnlockedItems");
    }

    public static void SaveVideoUnlockedItemsString(string itemName)
    {
        if (videoUnlockedItemsSaveString == "")
            videoUnlockedItemsSaveString = itemName;
        else
            videoUnlockedItemsSaveString += "#" + itemName;

        PlayerPrefs.SetString("VideoUnlockedItems", videoUnlockedItemsSaveString);
        PlayerPrefs.Save();
    }

    //    public static void SaveExpUnlockedItemsString(string itemName)
    //    {
    //        if(expUnlockedItemsSaveString == "")
    //            expUnlockedItemsSaveString = itemName;
    //        else
    //            expUnlockedItemsSaveString += "#"+itemName;
    //
    //        PlayerPrefs.SetString("expUnlockedItems", expUnlockedItemsSaveString);
    //        PlayerPrefs.Save();
    //    }

    public static bool CheckIfItemIsUnlockedForVideo(string itemName)
    {
        return videoUnlockedItemsSaveString.Contains(itemName);
    }

    //    public static bool CheckIfItemIsUnlockedForExp(string itemName)
    //    {
    //        return expUnlockedItemsSaveString.Contains(itemName);
    //    }

    public static int GetItemUnlockLevel(string name)
    {
        foreach (var item in expLockedItems)
        {
            if (item.itemName == name)
                return item.unlockLevel;
        }
        Debug.Log("Locked item not in list! " + name);
        return 0;
    }

    public static void ResetGlobalVariables()
    {
        Debug.Log("Reset Global Variables");
        currentPlayerIndex = 0;
        readyToContest = false;

        InitializeMainCharacterMakeUpData();
        InitializeMainCharacterDressUpData();

        selectedCharacterIndex = new int[] { 0, 0 };
        selectedCharacterName = new string[] { "", "" };

        DressActive = true;
        ZoomedIn = false;

        jewelryRingCase = 1;
        //        jewelryLiquidIndex = 0;
        jewelryMoldSize = 0;
        jewelrySideDiamonds = 1;
        //        jewelryDiamond = 1;
        jewelryDiamondCase = 1;

        phoneAnswered = false;
        //        challengeAccepted = false;
        nextLevelPointer = 0;

        contestVotesBufffers[0].Clear();
        contestVotesBufffers[1].Clear();
        xpBuffer.Clear();
    }

    public static void AddXpToBuffer(GameObject itemButton)
    {
        if (itemButton.GetComponent<ItemState>() != null)
        {
            //        Debug.Log("Item to check: " + itemName);
            //ItemName = itemType_index
            string itemType = itemButton.name.Split('_')[0];

            if (xpBuffer.ContainsKey(itemType))
                xpBuffer.Remove(itemType);

            if (itemButton.GetComponent<ItemState>().StateOfItem == ItemState.StateOfItemEnum.Experience)
                xpBuffer.Add(itemType, currentLvl * expExpLockedMultiplier + expAddition);
            else if (itemButton.GetComponent<ItemState>().StateOfItem == ItemState.StateOfItemEnum.Video)
                xpBuffer.Add(itemType, currentLvl * expVideoLockedMultiplier + expAddition);
            else
                xpBuffer.Add(itemType, currentLvl * expUnlockedMultiplier + expAddition);
        }
    }

    public static void AddXpToBuffer(string key)
    {
        if (xpBuffer.ContainsKey(key))
            xpBuffer.Remove(key);

        xpBuffer.Add(key, currentLvl * expUnlockedMultiplier + expAddition);
    }

    /// <summary>
    /// Returns the sum of xpBuffer values and clears the buffer
    /// </summary>
    /// <returns>The sum of xpBuffer values.</returns>
    public static int CollectXpFromBuffer()
    {
        int sum = 0;
        foreach (var xp in xpBuffer)
        {
            sum += xp.Value;
        }
        xpBuffer.Clear();
        return sum;
    }

    public static void AddContestVotes(string key, int value)
    {
        if (key.Contains("_"))
            key = key.Split('_')[0];

        Debug.Log("Key: " + key + " Value: " + value);

        if (contestVotesBufffers[currentPlayerIndex].ContainsKey(key))
            contestVotesBufffers[currentPlayerIndex].Remove(key);

        contestVotesBufffers[currentPlayerIndex].Add(key, value);
        Debug.Log("Dictionary ima : " + contestVotesBufffers[currentPlayerIndex].Count);
    }

    public static void AddContestVotesItem(GameObject itemButton)
    {
        string itemType = itemButton.name.Split('_')[0];
        Debug.Log("Item Name: " + itemType);

        if (itemButton.GetComponent<ItemState>().StateOfItem == ItemState.StateOfItemEnum.Experience)
            AddContestVotes(itemType, (int)ECategoryTier.Low * (int)EValueMultiplier.Exp);
        else if (itemButton.GetComponent<ItemState>().StateOfItem == ItemState.StateOfItemEnum.Video)
            AddContestVotes(itemType, (int)ECategoryTier.Low * (int)EValueMultiplier.Video);
        else
            AddContestVotes(itemType, (int)ECategoryTier.Low * (int)EValueMultiplier.Free);
    }

    public static int GetBeautyAndMakeUpVotes(int playerIndex)
    {
        int sum = 0;
        foreach (var cv in contestVotesBufffers[playerIndex])
        {
            sum += cv.Value;
        }
        //        contestVotesBufffer.Clear();
        Debug.Log("BM points: " + sum);
        return sum;
    }

    public void DisableLog()
    {
        Debug.unityLogger.logEnabled = false;
    }

    public static void InitializeMainCharacterDressUpData()
    {
        mainCharacterDressUpData = new CharacterDressUpData[2];

        for (int i = 0; i < mainCharacterDressUpData.Length; i++)
        {
            mainCharacterDressUpData[i] = new CharacterDressUpData();
        }
    }

    public static void MainCharacterDefaultDressUpData()
    {
        try
        {
            Debug.Log("HomeScreenCharacterData/" + currentPlayerIndex.ToString() + "/" + selectedCharacterIndex[currentPlayerIndex].ToString());
            CharacterDressUpData tmp = Resources.Load<CharacterDressUpData>("HomeScreenCharacterData/" + currentPlayerIndex.ToString() + "/" + selectedCharacterIndex[currentPlayerIndex].ToString());

            if (tmp.earringsSprite.item != null)
                mainCharacterDressUpData[currentPlayerIndex].earringsSprite = tmp.earringsSprite;
            else
                mainCharacterDressUpData[currentPlayerIndex].earringsSprite = null;

            if (tmp.neclessSprite.item != null)
                mainCharacterDressUpData[currentPlayerIndex].neclessSprite = tmp.neclessSprite;
            else
                mainCharacterDressUpData[currentPlayerIndex].neclessSprite = null;

            if (tmp.sunglassesSprite.item != null)
                mainCharacterDressUpData[currentPlayerIndex].sunglassesSprite = tmp.sunglassesSprite;
            else
                mainCharacterDressUpData[currentPlayerIndex].sunglassesSprite = null;
            if (tmp.tiaraSprite.item != null)
                mainCharacterDressUpData[currentPlayerIndex].tiaraSprite = tmp.tiaraSprite;
            else
                mainCharacterDressUpData[currentPlayerIndex].tiaraSprite = null;

            if (tmp.hairPrefab.item != null)
                mainCharacterDressUpData[currentPlayerIndex].hairPrefab = tmp.hairPrefab;
            else
                mainCharacterDressUpData[currentPlayerIndex].hairPrefab = null;
            if (tmp.blousePrefab.item != null)
                mainCharacterDressUpData[currentPlayerIndex].blousePrefab = tmp.blousePrefab;
            else
                mainCharacterDressUpData[currentPlayerIndex].blousePrefab = null;
            if (tmp.jacketPrefab.item != null)
                mainCharacterDressUpData[currentPlayerIndex].jacketPrefab = tmp.jacketPrefab;
            else
                mainCharacterDressUpData[currentPlayerIndex].jacketPrefab = null;
            if (tmp.dressPrefab.item != null)
                mainCharacterDressUpData[currentPlayerIndex].dressPrefab = tmp.dressPrefab;
            else
                mainCharacterDressUpData[currentPlayerIndex].dressPrefab = null;
            if (tmp.pantsPrefab.item != null)
                mainCharacterDressUpData[currentPlayerIndex].pantsPrefab = tmp.pantsPrefab;
            else
                mainCharacterDressUpData[currentPlayerIndex].pantsPrefab = null;
            if (tmp.shoesPrefab.item != null)
                mainCharacterDressUpData[currentPlayerIndex].shoesPrefab = tmp.shoesPrefab;
            else
                mainCharacterDressUpData[currentPlayerIndex].shoesPrefab = null;
            if (tmp.shirtPrefab.item != null)
                mainCharacterDressUpData[currentPlayerIndex].shirtPrefab = tmp.shirtPrefab;
            else
                mainCharacterDressUpData[currentPlayerIndex].shirtPrefab = null;
            if (tmp.skirtPrefab.item != null)
                mainCharacterDressUpData[currentPlayerIndex].skirtPrefab = tmp.skirtPrefab;
            else
                mainCharacterDressUpData[currentPlayerIndex].skirtPrefab = null;
            if (tmp.shortsPrefab.item != null)
                mainCharacterDressUpData[currentPlayerIndex].shortsPrefab = tmp.shortsPrefab;
            else
                mainCharacterDressUpData[currentPlayerIndex].shortsPrefab = null;
            if (tmp.socksPrefab.item != null)
                mainCharacterDressUpData[currentPlayerIndex].socksPrefab = tmp.socksPrefab;
            else
                mainCharacterDressUpData[currentPlayerIndex].socksPrefab = null;
            if (tmp.bagSprite.item != null)
                mainCharacterDressUpData[currentPlayerIndex].bagSprite = tmp.bagSprite;
            else
                mainCharacterDressUpData[currentPlayerIndex].bagSprite = null;

        }
        catch (System.Exception ex)
        {
            Debug.Log("Nema resursa: " + ex.Message);
        }

        Resources.UnloadUnusedAssets();
    }

    public static void InitializeMainCharacterMakeUpData()
    {
        mainCharacterMakeUpData = new CharacterMakeUpData[2];

        for (int i = 0; i < mainCharacterMakeUpData.Length; i++)
        {
            mainCharacterMakeUpData[i] = new CharacterMakeUpData();
            mainCharacterMakeUpData[i].shimmers = new bool[6];
        }
    }

    void InitializeExpLockedItems()
    {
        int totalItems = 0;
        expLockedItems = new List<ExpLockedItem>();
        DressUpItemSpriteScriptableObject[] tmpSpriteSOs = Resources.LoadAll("CharacterData", typeof(DressUpItemSpriteScriptableObject)).Cast<DressUpItemSpriteScriptableObject>().ToArray();
        DressUpItemSprite[] tmpSpriteItems;
        foreach (var so in tmpSpriteSOs)
        {
            tmpSpriteItems = so.elements.ToArray();

            foreach (var element in tmpSpriteItems)
            {
                if (element.valueMultiplier == EValueMultiplier.Exp)
                    expLockedItems.Add(new ExpLockedItem(so.name + "_" + System.Array.IndexOf(tmpSpriteItems, element), so.sceneName, element.unlockLevel, element.thumbnail));
            }
            totalItems += tmpSpriteItems.Length;
        }
        tmpSpriteSOs = null;
        tmpSpriteItems = null;

        DressUpItemPrefabScriptableObject[] tmpPrefabSOs = Resources.LoadAll("CharacterData", typeof(DressUpItemPrefabScriptableObject)).Cast<DressUpItemPrefabScriptableObject>().ToArray();
        DressUpItemPrefab[] tmpPrefabItems;
        foreach (var so in tmpPrefabSOs)
        {
            tmpPrefabItems = so.elements.ToArray();

            foreach (var element in tmpPrefabItems)
            {
                if (element.valueMultiplier == EValueMultiplier.Exp)
                    expLockedItems.Add(new ExpLockedItem(so.name + "_" + System.Array.IndexOf(tmpPrefabItems, element), so.sceneName, element.unlockLevel, element.thumbnail));
            }
            totalItems += tmpPrefabItems.Length;
        }
        tmpPrefabSOs = null;
        tmpPrefabItems = null;

        //        IListExtensions.Shuffle(expLockedItems,23);
        //
        //        int expLvl = 2;
        //        for (int i = 0; i < expLockedItems.Count; i++)
        //        {
        //            expLockedItems[i].unlockLevel = expLvl;
        //            if (i!=0 && i%2 != 0)
        //            {
        //                expLvl += 1;
        //            }
        //        }

        Debug.Log("Total items: " + totalItems);
        Debug.Log("Exp locked items count: " + expLockedItems.Count);

        Resources.UnloadUnusedAssets();
    }
}

