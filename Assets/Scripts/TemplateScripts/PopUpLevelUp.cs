﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpLevelUp : MonoBehaviour {

    public bool popUpLevelUpShown;
    public GameObject unlockedItemPrefab;
    public Transform scrollView;
    
    MenuManager menuManager;
    List<ExpLockedItem> expLockedItems;
    GridLayoutGroup gridLayout;
    GameObject go;

    void Awake()
    {
        menuManager = GameObject.Find("Canvas").GetComponent<MenuManager>();
        expLockedItems = GlobalVariables.GetExpLockedItemsList();
        gridLayout = scrollView.GetComponent<GridLayoutGroup>();

        if(!GlobalVariables.unlockAll && !GlobalVariables.fullGameOwned)
            LevelUp.OnLevelUp += ShowRewardedItems;
    }

    void ShowRewardedItems()
    { 
        while (scrollView.childCount > 0)
        {
            DestroyImmediate(scrollView.GetChild(0).gameObject);
        }

        foreach (ExpLockedItem item in expLockedItems)
        {
            //TODO da se instanciraju kao offerwall
            if (item.unlockLevel == GlobalVariables.currentLvl)
            {
                go = Instantiate(unlockedItemPrefab,scrollView) as GameObject;
                go.transform.Find("Name").GetComponent<Text>().text = item.sceneName;
                go.transform.Find("Icon").GetComponent<Image>().sprite = item.itemImage;
                go.transform.localPosition = Vector3.zero;
            }
        }

        float scrollContentHeight = ((scrollView.childCount) * gridLayout.cellSize.y) + ((scrollView.childCount - 1) * gridLayout.spacing.y)+50;
        scrollView.GetComponent<RectTransform>().sizeDelta = new Vector2(gridLayout.gameObject.GetComponent<RectTransform>().sizeDelta.x, scrollContentHeight);

        if (scrollView.childCount != 0)
        {
            Debug.Log("levelap popap");
            popUpLevelUpShown = true;
            menuManager.ShowPopUpMenu(gameObject);
        }
    }

    void OnDisable()
    {
        popUpLevelUpShown = false;
    }

    void OnDestroy()
    {
        LevelUp.OnLevelUp -= ShowRewardedItems;
    }
}
