﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ButtonUnlockTimer : MonoBehaviour {

    public int hoursToWait;
    public int minutesToWait;
    public int secondsToWait;

    public Text timerText;

    DateTime currentTime;
    DateTime endTime;

    TimeSpan timeToWait;
    TimeSpan timeDifference;

    TimeSpan timerTick = new TimeSpan(0,0,1);   //timerTick and waitForSec should be in sync
    TimeSpan rewardTick = new TimeSpan (0,30,0);
    WaitForSeconds waitForSec = new WaitForSeconds(1f);

    string prefsTempTime;
    string tempTimeDifString;
    int notifTimer;

    void Awake()
    {
        Debug.Log("Timer Awake");
        timeToWait = new TimeSpan(hoursToWait,minutesToWait,secondsToWait);
        notifTimer = secondsToWait + (60 * minutesToWait) + (3600 * hoursToWait);

        if (PlayerPrefs.HasKey("ButtonUnlockTimer" + name.ToString()))
        {
            Debug.Log("resume timer");
            prefsTempTime = PlayerPrefs.GetString("ButtonUnlockTimer" + name.ToString());
            DateTime.TryParse(prefsTempTime, out endTime);
//            Debug.Log("Timer loaded: " + endTime + " as: " + prefsTempTime);
            currentTime = DateTime.Now;
            timeDifference = endTime.Subtract(currentTime);
//            Debug.Log("Time difference: " + timeDifference);
            if (timeDifference < TimeSpan.Zero)
            {
                TimerFinished();
//                StopCoroutine(CUnlockAnim());
//                StartCoroutine(CUnlockAnim());
            }
            else
                StartCoroutine("CTimer");
        }
        else
            StartButtonTimer();

    }

    public void StartButtonTimer()
    {
        Debug.Log("Timer Start");
        if (PlayerPrefs.HasKey("ButtonUnlockTimer" + name.ToString()))
        {
            return;
        }

        currentTime = DateTime.Now;
        endTime = currentTime + timeToWait;
        PlayerPrefs.SetString("ButtonUnlockTimer" + name.ToString(), endTime.ToString());
        PlayerPrefs.Save();
        timeDifference = endTime.Subtract(currentTime);
        StartCoroutine("CTimer");
    }

    public void RewardTime()
    {
        Debug.Log("Time rewarded");
        if (endTime.Subtract(currentTime.Add(rewardTick)) < TimeSpan.Zero)
            TimerFinished();
        else
        {
            endTime = endTime.Subtract(rewardTick);
            PlayerPrefs.SetString("ButtonUnlockTimer" + name.ToString(), endTime.ToString());
            PlayerPrefs.Save();
        }
    }

    void TimerFinished()
    {
        Debug.Log("CTimer finished");
        StopCoroutine("CTimer");
//        if (PlayerPrefs.HasKey("ButtonUnlockTimer" + name.ToString()))
//        {
//            PlayerPrefs.DeleteKey("ButtonUnlockTimer" + name.ToString());
//            PlayerPrefs.Save();
//        }
    }

    IEnumerator CTimer()
    {
        GetComponent<Animator>().Play("NameLockedIdle");
        Debug.Log("CTimer started");
        do
        {

            timeDifference = endTime.Subtract(currentTime);
            //calculate time
            currentTime = currentTime.Add(timerTick);
            timeDifference = endTime.Subtract(currentTime);
            //Update time text
            tempTimeDifString = timeDifference.ToString();
            if (tempTimeDifString.Length > 8)
            {
                tempTimeDifString = tempTimeDifString.Remove(8);
            }
//            Debug.Log("Current time: " + currentTime + " Difference: " + timeDifference);
            timerText.text = tempTimeDifString;

            //Wait for seconds
            yield return waitForSec;
            
        }
        while (timeDifference >= timerTick);

        GetComponent<Animator>().Play("NameLockedUnlocking");
        TimerFinished();
    }

//    IEnumerator CUnlockAnim()
//    {
//        transform.Find("Locked").GetComponentInChildren<CustomButton>().enabled = false;
//        yield return new WaitForSeconds(1f);
//        GetComponent<Animator>().Play("NameLockedUnlocking");
//    }
}
