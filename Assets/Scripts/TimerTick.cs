﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class TimerTick : MonoBehaviour {

	public Text[] timerTexts;

	private int[] remainingTimesInSeconds;
	public int[] totalTimesToWaitInSeconds;

	public string[] playerPrefsNamesForTime;

	public static int timerScene = -1;

	private IEnumerator[] coroutines;

	public bool fillAmountPresent;
	[HideInInspector] public float[] fillAmounts; 

	static TimerTick instance;
	public static TimerTick Instance
	{
		get
		{
			if(instance == null)
				instance = GameObject.FindObjectOfType(typeof(TimerTick)) as TimerTick;
			return instance;
		}
	}

	void Awake ()
	{
		if(totalTimesToWaitInSeconds != null)
		{
			coroutines = new IEnumerator[totalTimesToWaitInSeconds.Length];

			if(totalTimesToWaitInSeconds.Length > 0)
			{
				remainingTimesInSeconds = new int[totalTimesToWaitInSeconds.Length];
				if(playerPrefsNamesForTime.Length == 0)
				{
					playerPrefsNamesForTime = new string[totalTimesToWaitInSeconds.Length];
				}
				if(timerTexts.Length == 0)
				{
					timerTexts = new Text[totalTimesToWaitInSeconds.Length];
				}

				if(fillAmountPresent)
				{
					fillAmounts = new float[totalTimesToWaitInSeconds.Length];
				}
			}

		}

//		if(GlobalVariables.allowNotifications == 1 && PlayerPrefs.HasKey("LastDateTR"))
//			NotifficationsManager.CancelNotiffication(11223312);

	}

	void Start ()
	{
		Debug.Log("OKAY start");
		if(PlayerPrefs.HasKey("LastDateTR")) // game has at least once been paused
		{
			Debug.Log("IMA LastDateTR, i treba da ima");
			for(int i=0;i<totalTimesToWaitInSeconds.Length;i++)
			{
				int lastTime = PlayerPrefs.GetInt(playerPrefsNamesForTime[i]);
				Debug.Log(i + ": vrednost last time: " + lastTime);
				if(lastTime > 0) // Timer has still been ticked
				{
					string pausedTime = PlayerPrefs.GetString("LastDateTR");
					string resumedTime = DateTime.Now.ToString();
					TimeSpan difference = DateTime.Parse(resumedTime) - DateTime.Parse(pausedTime);
					Debug.Log("PAUSED TIME: " + pausedTime + ", RESUMED TIME: " + resumedTime + ", DIFFERENCE in secs: " + difference.TotalSeconds + ", total time: " + totalTimesToWaitInSeconds[i]);

					//security check, if date time on device was manipulated
					int differenceInSeconds = (int)difference.TotalSeconds;
					differenceInSeconds = Mathf.Clamp(differenceInSeconds,0,totalTimesToWaitInSeconds[i]);
					lastTime = Mathf.Clamp(lastTime,0,totalTimesToWaitInSeconds[i]);
					
					if(differenceInSeconds >= totalTimesToWaitInSeconds[i]) // coroutine is done
					{
						TimerDone(i);
//						GlobalVariables.timeRewardStatus = 2;
					}
					else
					{
						remainingTimesInSeconds[i] = lastTime - differenceInSeconds;
						StartTicking(true, i);
//						GlobalVariables.timeRewardStatus = 1;
					}
				}
				else
				{
					TimerDone(i);
//					GlobalVariables.timeRewardStatus = 2;
				}
			}
		}
	}

	public void InitializeText(int index, Text text)
	{
		timerTexts[index] = text;
	}

	public void InitializeTimers(int[] times)
	{
		remainingTimesInSeconds = new int[times.Length];
		totalTimesToWaitInSeconds = new int[times.Length];
		for(int i=0;i<times.Length;i++)
		{
			totalTimesToWaitInSeconds[i] = times[i];
		}

		coroutines = new IEnumerator[totalTimesToWaitInSeconds.Length];
	}

	public void ResetTimers(int index = -1)
	{
		if(index == -1)
		{
			for(int i=0;i<remainingTimesInSeconds.Length;i++)
				remainingTimesInSeconds[i] = totalTimesToWaitInSeconds[i];
		}
		else
			remainingTimesInSeconds[index] = totalTimesToWaitInSeconds[index];
	}

	public void StartTicking(bool continueTicking, int index = -1)
	{
		if(index == -1)
		{
			for(int i=0;i<totalTimesToWaitInSeconds.Length;i++)
			{
				if(!continueTicking)
				{
					remainingTimesInSeconds[i] = totalTimesToWaitInSeconds[i];
					if(fillAmountPresent)
						fillAmounts[i] = 0;
				}
				coroutines[i] = Ticking(i);
				StartCoroutine(coroutines[i]);
			}
		}
		else
		{
			if(!continueTicking)
			{
				remainingTimesInSeconds[index] = totalTimesToWaitInSeconds[index];
				if(fillAmountPresent)
					fillAmounts[index] = 0;
			}
			coroutines[index] = Ticking(index);
			StartCoroutine(coroutines[index]);
		}
		//coroutines[index] = StartCoroutine(Ticking(index));
	}

	public void StartTicking(int remainingTimeInSeconds, Text timer)
	{
		StartCoroutine(Ticking(remainingTimeInSeconds, timer));
	}

	public void StopTicking(int index = -1)
	{
		if(index == -1)
		{
			for(int i=0;i<coroutines.Length;i++)
			{
				if(remainingTimesInSeconds[i] > 0)
					StopCoroutine(coroutines[i]);
			}
		}
		else
			if(remainingTimesInSeconds[index] > 0)
				StopCoroutine(coroutines[index]);
	}

	public IEnumerator Ticking(int index)
	{
		if(remainingTimesInSeconds[index] > 0)
		{
			DateTime time = new DateTime();

			int hours = remainingTimesInSeconds[index]/3600;
			int minutes = remainingTimesInSeconds[index]/60 - hours*60;
			int seconds = remainingTimesInSeconds[index]%60;
			
			Debug.Log("HOURS : " + hours + ", MINUTES: " + minutes + ", SECONDS: " + seconds);
			
			string timerString = System.String.Empty;

			Image progress = null;
			if(fillAmountPresent && Application.loadedLevel == timerScene)
				progress = timerTexts[index].transform.parent.parent.Find("ProgressBar").GetComponent<Image>();
			
			
			while(remainingTimesInSeconds[index] > 0)
			{
				timerString = hours.ToString() + ":" + minutes.ToString() + ":" + seconds.ToString();
//				Debug.Log("timerString "+timerString);
				time = DateTime.Parse(timerString);
				if(timerTexts[index] != null)
					timerTexts[index].text = time.ToString("HH:mm:ss");

				if(fillAmountPresent)
				{


					fillAmounts[index] = 1f - (float)remainingTimesInSeconds[index]/totalTimesToWaitInSeconds[index];
					if(Application.loadedLevel == timerScene)
					{
						if(progress == null)
							progress = timerTexts[index].transform.parent.parent.Find("ProgressBar").GetComponent<Image>();
						progress.fillAmount = fillAmounts[index];
					}
				}

				yield return new WaitForSeconds(1f);
				seconds--;
				remainingTimesInSeconds[index]--;
				
				if(seconds < 0)
				{
					seconds = 59;
					minutes--;
				}
				if(minutes < 0)
				{
					minutes = 59;
					hours--;
				}
			}
			if(fillAmountPresent)
			{
				if(Application.loadedLevel == timerScene)
					progress.fillAmount = 1;
			}
		}
		else yield return null;

		TimerDone(index);
	}

	public IEnumerator Ticking(int remainingTimeInSeconds, Text timer)
	{
		DateTime time = new DateTime();
		
		int minutes = remainingTimeInSeconds/60;
		int hours = remainingTimeInSeconds/3600;
		int seconds = remainingTimeInSeconds%60;
		
		Debug.Log("HOURS : " + hours + ", MINUTES: " + minutes + ", SECONDS: " + seconds);
		
		string timerString = System.String.Empty;
		
		
		while(remainingTimeInSeconds > 0)
		{
			yield return new WaitForSeconds(1f);
			seconds--;
			remainingTimeInSeconds--;

			if(seconds < 0)
			{
				seconds = 59;
				minutes--;
			}
			if(minutes < 0)
			{
				minutes = 59;
				hours--;
			}

			timerString = hours.ToString() + ":" + minutes.ToString() + ":" + seconds.ToString();
			Debug.Log("timer string: " + timerString);
			time = DateTime.Parse(timerString);
			timer.text = time.ToString("HH:mm:ss");
		}

		TimerDone(timer);
	}

	void TimerDone(Text timer)
	{
		timer.transform.parent.parent.gameObject.SetActive(false);
	}

	void TimerDone(int index)
	{
		Debug.Log("TIMER DONE: " + index);
//		if(timerScene == Application.loadedLevel)
//		{
//			VariablesManager.Instance.TimeRewardDone();
//		}
//		GlobalVariables.timeRewardStatus = 2;
	}

	public void SaveParameters()
	{
		for(int i=0;i<playerPrefsNamesForTime.Length;i++)
		{
			PlayerPrefs.SetInt(playerPrefsNamesForTime[i], remainingTimesInSeconds[i]);
		}
		PlayerPrefs.SetString("LastDateTR",DateTime.Now.ToString());
		PlayerPrefs.Save();
		StopTicking();
		Debug.Log("PAUSED TIME u pause: " + DateTime.Now.ToString() + ", remaining time in seconds: " + remainingTimesInSeconds[0]);
	}
	
	void OnApplicationPause(bool status)
	{
		if(status) // game paused
		{
			if(Application.loadedLevel != 0)
			{
				SaveParameters();
//				if(GlobalVariables.allowNotifications == 1 && remainingTimesInSeconds[0] > 0)
//					NotifficationsManager.SetNottification(remainingTimesInSeconds[0], "Your reward is ready, come back to collect it!", 11223312);
			}
		}
		else // game resumed
		{
			for(int i=0;i<remainingTimesInSeconds.Length;i++)
			{
				if(remainingTimesInSeconds[i] > 0)
				{
					string pausedTime = PlayerPrefs.GetString("LastDateTR");
					string resumedTime = DateTime.Now.ToString();
					Debug.Log("1 - PAUSED TIME: " + pausedTime + ", RESUMED TIME: " + resumedTime);
					TimeSpan difference = DateTime.Parse(resumedTime) - DateTime.Parse(pausedTime);
					Debug.Log("2 - PAUSED TIME: " + pausedTime + ", RESUMED TIME: " + resumedTime + ", DIFFERENCE: " + difference + ", total time: " + totalTimesToWaitInSeconds[i]);

					//security check, if date time on device was manipulated
					int differenceInSeconds = (int)difference.TotalSeconds;
					differenceInSeconds = Mathf.Clamp(differenceInSeconds,0,totalTimesToWaitInSeconds[i]);


					if(differenceInSeconds >= totalTimesToWaitInSeconds[i]) // coroutine is done
					{
						TimerDone(i);
					}
					else
					{
						int lastTime = PlayerPrefs.GetInt(playerPrefsNamesForTime[i]);
						Debug.Log("LAST TIME FOR " + i + ": " + lastTime); //93564
						//security check, if date time on device was manipulated
						lastTime = Mathf.Clamp(lastTime,0,totalTimesToWaitInSeconds[i]);

						Debug.Log("LAST TIME FOR " + i + " AFTER CLAMP : " + lastTime); //93564
						remainingTimesInSeconds[i] = lastTime - differenceInSeconds;
						StartTicking(true, i);
					}
				}
			}
//			if(GlobalVariables.allowNotifications == 1)
//				NotifficationsManager.CancelNotiffication(11223312);
		}
	}

	void OnApplicationQuit()
	{
		SaveParameters();
//		if(GlobalVariables.allowNotifications == 1 && remainingTimesInSeconds[0] > 0)
//			NotifficationsManager.SetNottification(remainingTimesInSeconds[0], "Your reward is ready, come back to collect it!", 11223312);
	}
}
