﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectedParticleHandler : MonoBehaviour {

    Transform topLimit, botLimit, particleHolder;

    void Awake()
    {
        particleHolder = transform.GetChild(1);
        topLimit = transform.parent.parent.parent.parent.parent.Find("UpperArrow");
        botLimit = transform.parent.parent.parent.parent.parent.Find("LowerArrow");
    }

    void Update()
    {
        if (particleHolder.gameObject.activeSelf)
        {
            if (particleHolder.position.y < botLimit.position.y || particleHolder.position.y > topLimit.position.y)
                particleHolder.gameObject.SetActive(false);
        }
        else
        {
            if (particleHolder.position.y > botLimit.position.y && particleHolder.position.y < topLimit.position.y)
                particleHolder.gameObject.SetActive(true);
        }
    }
}
