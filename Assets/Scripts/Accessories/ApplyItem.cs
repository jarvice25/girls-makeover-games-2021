﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ApplyItem : MonoBehaviour
{
	Button button;
	int indexOfItem;
	int indexOfCategory;

	void Start()
	{
		button = GetComponent<Button>();
		indexOfItem = transform.parent.GetComponent<IndexOfItem>().indexOfItem;
		indexOfCategory = (int)transform.parent.parent.parent.GetComponent<IndexOfCategory>().Category;
		button.onClick.AddListener(() => {Apply();});
	}

	void Apply()
	{
		AccessoriesManager.Instance.ApplyItem(indexOfCategory, indexOfItem);
	}
}

