﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class AccessoriesManager : MonoBehaviour
{

    public Transform categoryHolderSelection, contentHolder;
    [Header("Earrings")]
    //	public Transform LeftEaring;
    //	public Transform RightEaring;
    public GameObject lastEarrings;
    //	public Sprite[] Earrings;
    public DressUpItemSpriteScriptableObject earrings;

    [Header("Necless")]
    //	public Transform Necless;
    public GameObject lastNecless;
    //	public Sprite[] Neclesses;
    public DressUpItemSpriteScriptableObject neclesses;

    [Header("Sunglasses")]
    //	public Transform Sunglass;
    public GameObject lastSunglasses;
    //	public Sprite[] Sunglasses;
    public DressUpItemSpriteScriptableObject sunglasses;

    [Header("Tiaras")]
    //	public Transform Tiara;
    public GameObject lastTiara;
    //	public Sprite[] Tiaras;
    public DressUpItemSpriteScriptableObject tiaras;

    [Header("Character")]
    public Transform characterHolder;
    public GameObject[] characterPrefabs;
    GameObject character;

    [Header("Sounds Holder")]
    public Transform soundsHolder;

    CharacterSetUp characterSetUp;
    Animator tutorialAnimator;

    static AccessoriesManager instance;

    public static AccessoriesManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType(typeof(AccessoriesManager)) as AccessoriesManager;
            }

            return instance;
        }
    }

    IEnumerator Start()
    {
        tutorialAnimator = GameObject.Find("Canvas/HandTutorialHolder/AnimationHolder").GetComponent<Animator>();

        if (PlayerPrefs.HasKey("AccessoriesTutorialShown"))
        {
            if (PlayerPrefs.GetString("AccessoriesTutorialShown") == "true")
                tutorialAnimator.gameObject.SetActive(false);
        }

        character = Instantiate(characterPrefabs[GlobalVariables.selectedCharacterIndex[GlobalVariables.currentPlayerIndex]], characterHolder) as GameObject;
        character.transform.localScale = new Vector3(3f, 3f, 1f);
        character.transform.localPosition = new Vector3(5000f, 0f, 0f);

        characterSetUp = character.GetComponent<CharacterSetUp>();
        GameObject.Find("Canvas/PopUps/PopUpDialogRepeat/AnimationHolder/Body/ButtonsHolder/ButtonYes").GetComponent<UnityEngine.UI.Button>().onClick.AddListener(GlobalVariables.MainCharacterDefaultDressUpData);

        //Init GlobalVariables
        yield return null;
        if (GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].eyeColor == Color.blue)
            ColorUtility.TryParseHtmlString("#2EC2FEFF", out GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].eyeColor);
        GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].leftMascaraOpenSprite = characterSetUp.eyeOutlines.eyeOutlinesOpen[0];
        GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].leftMascaraClosedSprite = characterSetUp.eyeOutlines.eyeOutlinesClosed[0];
        GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].rightMascaraOpenSprite = characterSetUp.eyeOutlines.eyeOutlinesOpen[0];
        GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].rightMascaraClosedSprite = characterSetUp.eyeOutlines.eyeOutlinesClosed[0];
        if (GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].upperLipColor == Color.red)
            GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].upperLipColor = characterSetUp.upperLipHolder.GetComponent<Anima2D.SpriteMeshInstance>().color;
        if (GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].lowerLipColor == Color.red)
            GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex].lowerLipColor = characterSetUp.lowerLipHolder.GetComponent<Anima2D.SpriteMeshInstance>().color;

        yield return null;
        characterSetUp.SetUpCharacter(GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex], GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex]);
        character.transform.localPosition = Vector3.zero;
        character.transform.GetChild(0).GetComponent<Animator>().Play("CharacterArriving");

        //        GlobalVariables.curentNativeAd = GameObject.Find("Canvas/Accessories").GetComponentInChildren<FacebookNativeAd>();
        //        if (GlobalVariables.curentNativeAd != null)
        //            GlobalVariables.curentNativeAd.LoadAdWithDelay(1f);
    }

    public void SetCategory(int indexOfCategory)
    {
        tutorialAnimator.gameObject.SetActive(false);
        PlayerPrefs.SetString("AccessoriesTutorialShown", "true");
        PlayerPrefs.Save();

        //      Debug.Log("indexOfCategory "+indexOfCategory);
        categoryHolderSelection.GetChild(0).Find("EarringsHolder/AnimationHolder").GetChild(1).gameObject.SetActive(false);
        categoryHolderSelection.GetChild(0).Find("NecklessHolder/AnimationHolder").GetChild(1).gameObject.SetActive(false);
        categoryHolderSelection.GetChild(0).Find("SunglassesHolder/AnimationHolder").GetChild(1).gameObject.SetActive(false);
        categoryHolderSelection.GetChild(0).Find("TiaraHolder/AnimationHolder").GetChild(1).gameObject.SetActive(false);
        contentHolder.GetChild(0).GetChild(0).gameObject.SetActive(false);
        contentHolder.GetChild(0).GetChild(1).gameObject.SetActive(false);
        contentHolder.GetChild(0).GetChild(2).gameObject.SetActive(false);
        contentHolder.GetChild(0).GetChild(3).gameObject.SetActive(false);
        switch (indexOfCategory)
        {

            case 1:
                categoryHolderSelection.GetChild(0).Find("EarringsHolder/AnimationHolder").GetChild(1).gameObject.SetActive(true);
                break;
            case 2:
                categoryHolderSelection.GetChild(0).Find("NecklessHolder/AnimationHolder").GetChild(1).gameObject.SetActive(true);
                break;
            case 3:
                categoryHolderSelection.GetChild(0).Find("SunglassesHolder/AnimationHolder").GetChild(1).gameObject.SetActive(true);
                break;
            case 4:
                categoryHolderSelection.GetChild(0).Find("TiaraHolder/AnimationHolder").GetChild(1).gameObject.SetActive(true);
                break;
        }
        contentHolder.GetChild(0).GetChild(indexOfCategory - 1).gameObject.SetActive(true);
    }

    public void ApplyItem(int indexOfCategory, int indexOfItem)
    {
        //We shift item index by one, if adjusted intex iz -1, then item is empty
        int adjustedIndex = indexOfItem - 1;

        switch (indexOfCategory)
        {
            case 0: //Earrings
                tutorialAnimator.gameObject.SetActive(false);
                PlayerPrefs.SetString("AccessoriesTutorialShown", "true");
                PlayerPrefs.Save();

                SelectEarrings();
                if (adjustedIndex >= 0)
                {
                    characterSetUp.LeftEaring.SetActive(true);
                    characterSetUp.RightEaring.SetActive(true);
                    characterSetUp.LeftEaring.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = earrings.elements[adjustedIndex].item;//Earrings[adjustedIndex];
                    characterSetUp.RightEaring.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = earrings.elements[adjustedIndex].item;//Earrings[adjustedIndex];
                    GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].earringsSprite = earrings.elements[adjustedIndex];
                }
                else
                {
                    characterSetUp.LeftEaring.SetActive(false);
                    characterSetUp.RightEaring.SetActive(false);
                    GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].earringsSprite = null;
                }
                break;
            case 1: //Necless
                SelectNecless();
                if (adjustedIndex >= 0)
                {
                    characterSetUp.Necless.SetActive(true);
                    characterSetUp.Necless.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = neclesses.elements[adjustedIndex].item;//Neclesses[adjustedIndex];
                    GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].neclessSprite = neclesses.elements[adjustedIndex];
                }
                else
                {
                    characterSetUp.Necless.SetActive(false);
                    GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].neclessSprite = null;
                }
                break;
            case 2: //Sunglasses
                SelectSunglasses();
                if (adjustedIndex >= 0)
                {
                    characterSetUp.Sunglass.SetActive(true);
                    characterSetUp.Sunglass.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = sunglasses.elements[adjustedIndex].item;//Sunglasses[adjustedIndex];
                    GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].sunglassesSprite = sunglasses.elements[adjustedIndex];
                }
                else
                {
                    characterSetUp.Sunglass.SetActive(false);
                    GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].sunglassesSprite = null;
                }
                break;
            case 3: //Tiara
                SelectTiara();
                if (adjustedIndex >= 0)
                {
                    characterSetUp.Tiara.SetActive(true);
                    characterSetUp.Tiara.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = tiaras.elements[adjustedIndex].item;//Tiaras[adjustedIndex];
                    GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].tiaraSprite = tiaras.elements[adjustedIndex];
                }
                else
                {
                    characterSetUp.Tiara.SetActive(false);
                    GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex].tiaraSprite = null;
                }
                break;
        }

        GlobalVariables.AddXpToBuffer(EventSystem.current.currentSelectedGameObject.transform.parent.gameObject);

        soundsHolder.GetChild(Random.Range(0, soundsHolder.childCount)).GetComponent<AudioSource>().Play();
    }

    public void SelectEarrings()
    {
        if (lastEarrings != null)
        {
            lastEarrings.SetActive(false);
            lastEarrings.transform.parent.GetChild(2).gameObject.SetActive(false);
        }
        EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).gameObject.SetActive(true);
        EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(2).gameObject.SetActive(true);
        lastEarrings = EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).gameObject;
    }

    public void SelectNecless()
    {
        if (lastNecless != null)
        {
            lastNecless.SetActive(false);
            lastNecless.transform.parent.GetChild(2).gameObject.SetActive(false);
        }
        EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).gameObject.SetActive(true);
        EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(2).gameObject.SetActive(true);
        lastNecless = EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).gameObject;
    }

    public void SelectSunglasses()
    {
        if (lastSunglasses != null)
        {
            lastSunglasses.SetActive(false);
            lastSunglasses.transform.parent.GetChild(2).gameObject.SetActive(false);
        }
        EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).gameObject.SetActive(true);
        EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(2).gameObject.SetActive(true);
        lastSunglasses = EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).gameObject;
    }

    public void SelectTiara()
    {
        if (lastTiara != null)
        {
            lastTiara.SetActive(false);
            lastTiara.transform.parent.GetChild(2).gameObject.SetActive(false);
        }
        EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).gameObject.SetActive(true);
        EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(2).gameObject.SetActive(true);
        lastTiara = EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).gameObject;
    }

    public void LoadNextScene()
    {
        //        GameObject.Find("Canvas").GetComponent<MenuManager>().AddExp(GlobalVariables.CollectXpFromBuffer());

        if (AdsManager.Instance != null)
            AdsManager.Instance.ShowInterstitial();
        Debug.Log("Ucitaj novu scenu");
        //        if(GlobalVariables.challengeAccepted)
        //            GameObject.Find("Canvas").GetComponent<MenuManager>().LoadAdequateScene("Contest");
        //        else
        //            GameObject.Find("Canvas").GetComponent<MenuManager>().LoadAdequateScene("Map");
        if (GlobalVariables.readyToContest || GlobalVariables.currentPlayerIndex >= 1)
        {
            GameObject.Find("Canvas").GetComponent<MenuManager>().LoadAdequateScene("Battle");
            GlobalVariables.currentPlayerIndex = 0;
        }
        else
        {
            GlobalVariables.currentPlayerIndex = 1;
            GameObject.Find("Canvas").GetComponent<MenuManager>().LoadAdequateScene("HomeScreen");
        }
    }
}
