﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/**
* Scene:All
* Object:Canvas
* Description: Skripta zaduzena za hendlovanje(prikaz i sklanjanje svih Menu-ja,njihovo paljenje i gasenje, itd...)
**/
public class MenuManager : MonoBehaviour
{
    bool buttonsExpanded;
    public Menu currentMenu;
    Menu currentPopUpMenu;
    public static string nameOfSceneToLoad;
    public GameObject[] disabledObjects;


    GameObject ratePopUp;
    GameObject LevelUpHolder;
    Transform levelUpPopUp;
    static bool mainSceneLoaded;

    void Awake()
    {
        LevelUpHolder = GameObject.Find("LevelUpHolder");

        transform.Find("LoadingMenu").gameObject.SetActive(true);
        if (Application.loadedLevelName != "Contest")
        {
            transform.Find("LoadingMenu/LoadingHolder/AnimationHolder").GetComponent<Animator>().Play("LoadingGameplayDeparting");
        }

        levelUpPopUp = transform.Find("PopUps/PopUpLevelUp");
        if (levelUpPopUp != null)
            levelUpPopUp.transform.Find("AnimationHolder/Body/ButtonExit").GetComponent<Button>().onClick.RemoveListener(StartLoading);

        if (GameObject.Find("Canvas/PopUps/PopUpDialogHome") != null)
        {
            GameObject.Find("Canvas/PopUps/PopUpDialogHome/AnimationHolder/Body/ButtonsHolder/ButtonYes").GetComponent<Button>().onClick.RemoveAllListeners();
            GameObject.Find("Canvas/PopUps/PopUpDialogHome/AnimationHolder/Body/ButtonsHolder/ButtonYes").GetComponent<Button>().onClick.AddListener(() => { GlobalVariables.ResetGlobalVariables(); });
        }
    }

    void Start()
    {
        try
        {
        }
        catch (System.Exception ex)
        {
            Debug.Log("There is no ButtonScriptMoreGames button on the scene " + ex);
        }

        if (GameObject.Find("CanvasPromotion") != null)
        {
           
        }


        if (Application.loadedLevelName == "ChoseGirlScreen")
        {
            //			TimerTick.Instance.InitializeText(0,GameObject.Find("NameHolderRight/AnimationHolder/Locked/TimeTextBG/ImeText").GetComponent<Text>());
            //			TimerTick.Instance.InitializeText(1,GameObject.Find("NameHolderLeft/AnimationHolder/Locked/TimeTextBG/ImeText").GetComponent<Text>());
            //			TimerTick.Instance.StartTicking(false);
        }
        else if (Application.loadedLevelName == "HomeScreen")
        {
            ratePopUp = GameObject.Find("PopUps/PopUpRate");
            if (!SoundManager.Instance.gameplayMusic.isPlaying)
                SoundManager.Instance.Play_GameplayMusic();
        }

        if (disabledObjects != null)
        {
            for (int i = 0; i < disabledObjects.Length; i++)
            {
                disabledObjects[i].SetActive(false);
            }
        }

        //		if(Application.loadedLevelName!= "JewelryDesign")
        //			ShowMenu(currentMenu.gameObject);	

        if (Application.loadedLevelName == "HomeScreen")
        {


            if (PlayerPrefs.HasKey("alreadyRated"))
            {
                Rate.alreadyRated = PlayerPrefs.GetInt("alreadyRated");
                //                transform.Find("ButtonsHolderBS FINAL/ButtonRate").gameObject.SetActive(false);
            }
            else
            {
                Rate.alreadyRated = 0;
            }

            if (Rate.alreadyRated == 0)
            {
                Rate.appStartedNumber = PlayerPrefs.GetInt("appStartedNumber");
                Debug.Log("appStartedNumber " + Rate.appStartedNumber);

                if (Rate.appStartedNumber >= 6)
                {
                    Rate.appStartedNumber = 0;
                    PlayerPrefs.SetInt("appStartedNumber", Rate.appStartedNumber);
                    PlayerPrefs.Save();
                    ShowPopUpMenu(ratePopUp);

                }
                else
                {
                    if (mainSceneLoaded)
                    {
                    }
                }
            }
            else
            {
                if (mainSceneLoaded)
                {
                }
            }
            mainSceneLoaded = true;
        }
        else if (Application.loadedLevelName == "BeautySalon")
        {
            ShowPopUpBattle(EItemStyles.Classy.ToString(), "Let's show everyone elegant style is the most beautiful! I need a perfect classy outfit to win this fashion battle!");
        }
        else if (Application.loadedLevelName == "BeautySalonPunk")
        {
            ShowPopUpBattle(EItemStyles.Punk.ToString(), "Rock chicks rule the world! We must find the coolest punk rock clothes to get the first prize!");
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
        }

        if (Input.GetButtonDown("Cancel"))
        {
            Debug.Log("Back pressed");

            

            for (int i = disabledObjects.Length - 1; i >= 0; i--)
            {
                if (disabledObjects[i].activeInHierarchy)
                {
                    if (disabledObjects[i].name.Contains("PopUpDialog"))
                    {
                        disabledObjects[i].transform.Find("AnimationHolder/Body/ButtonsHolder/ButtonNo").GetComponent<Button>().onClick.Invoke();
                        return;
                    }
                    else if (disabledObjects[i].name == "PopUpMessage" || disabledObjects[i].name == "PopUpBattle")
                    {
                        disabledObjects[i].transform.Find("AnimationHolder/Body/ButtonsHolder/ButtonOk").GetComponent<Button>().onClick.Invoke();
                        return;
                    }
                    else
                    {
                        if (disabledObjects[i].transform.Find("AnimationHolder/Body/ButtonExit").GetComponent<Button>() != null)
                            disabledObjects[i].transform.Find("AnimationHolder/Body/ButtonExit").GetComponent<Button>().onClick.Invoke();
                        return;
                    }
                }
            }
            if (Application.loadedLevelName == "HomeScreen")
            {
                transform.Find("PopUps/PopUpDialog/AnimationHolder/Body/ButtonsHolder/ButtonYes").GetComponent<Button>().onClick.RemoveAllListeners();
                transform.Find("PopUps/PopUpDialog/AnimationHolder/Body/ButtonsHolder/ButtonYes").GetComponent<Button>().onClick.AddListener(() => { Application.Quit(); });
                ShowPopUpDialog("Exit Game", "Are sure you want to exit the game?");
            }
            else
            {
                if (Application.loadedLevelName != "ChooseGirlScreen")
                    ShowPopUpMenu(GameObject.Find("Canvas/PopUps/PopUpDialogHome"));
            }
        }
    }

    /// <summary>
    /// Funkcija koja pali(aktivira) objekat
    /// </summary>
    /// /// <param name="gameObject">Game object koji se prosledjuje i koji treba da se upali</param>
    public void EnableObject(GameObject gameObject)
    {

        if (gameObject != null)
        {
            if (!gameObject.activeSelf)
            {
                gameObject.SetActive(true);
            }
        }
    }

    /// <summary>
    /// Funkcija koja gasi objekat
    /// </summary>
    /// /// <param name="gameObject">Game object koji se prosledjuje i koji treba da se ugasi</param>
    public void DisableObject(GameObject gameObject)
    {
        Debug.Log("Disable Object");
        if (gameObject != null)
        {
            if (gameObject.activeSelf)
            {
                gameObject.SetActive(false);
            }
        }
    }

    /// <summary>
    /// F-ja koji poziva ucitavanje Scene
    /// </summary>
    /// <param name="levelName">Level name.</param>
    public void LoadScene(string levelName)
    {
        if (levelName != "")
        {
            try
            {
                Application.LoadLevel(levelName);
            }
            catch (System.Exception e)
            {
                Debug.Log("Can't load scene: " + e.Message);
            }
        }
        else
        {
            Debug.Log("Can't load scene: Level name to set");
        }
    }

    /// <summary>
    /// F-ja koji poziva asihrono ucitavanje Scene
    /// </summary>
    /// <param name="levelName">Level name.</param>
    public void LoadSceneAsync(string levelName)
    {
        if (levelName != "")
        {
            try
            {
                Application.LoadLevelAsync(levelName);
            }
            catch (System.Exception e)
            {
                Debug.Log("Can't load scene: " + e.Message);
            }
        }
        else
        {
            Debug.Log("Can't load scene: Level name to set");
        }
    }

    /// <summary>
    /// Funkcija za prikaz Menu-ja koji je pozvan kao Menu
    /// </summary>
    /// /// <param name="menu">Game object koji se prosledjuje i treba da se skloni, mora imati na sebi skriptu Menu.</param>
    public void ShowMenu(GameObject menu)
    {
        if (currentMenu != null)
        {
            currentMenu.IsOpen = false;
            currentMenu.gameObject.SetActive(false);
        }

        currentMenu = menu.GetComponent<Menu>();
        menu.gameObject.SetActive(true);
        currentMenu.IsOpen = true;
    }

    /// <summary>
    /// Funkcija za zatvaranje Menu-ja koji je pozvan kao Meni
    /// </summary>
    /// /// <param name="menu">Game object koji se prosledjuje za prikaz, mora imati na sebi skriptu Menu.</param>
    public void CloseMenu(GameObject menu)
    {
        if (menu != null)
        {
            menu.GetComponent<Menu>().IsOpen = false;
            menu.SetActive(false);
        }
    }

    /// <summary>
    /// Funkcija za prikaz Menu-ja koji je pozvan kao PopUp-a
    /// </summary>
    /// /// <param name="menu">Game object koji se prosledjuje za prikaz, mora imati na sebi skriptu Menu.</param>
    public void ShowPopUpMenu(GameObject menu)
    {

        menu.gameObject.SetActive(true);
        currentPopUpMenu = menu.GetComponent<Menu>();
        currentPopUpMenu.IsOpen = true;

    }

    /// <summary>
    /// Funkcija za zatvaranje Menu-ja koji je pozvan kao PopUp-a, poziva inace coroutine-u, ima delay zbog animacije odlaska Menu-ja
    /// </summary>
    /// /// <param name="menu">Game object koji se prosledjuje i treba da se skloni, mora imati na sebi skriptu Menu.</param>
    public void ClosePopUpMenu(GameObject menu)
    {
      
        StartCoroutine("HidePopUp", menu);
    }

    /// <summary>
    /// Couorutine-a za zatvaranje Menu-ja koji je pozvan kao PopUp-a
    /// </summary>
    /// /// <param name="menu">Game object koji se prosledjuje, mora imati na sebi skriptu Menu.</param>
    IEnumerator HidePopUp(GameObject menu)
    {
        menu.GetComponent<Menu>().IsOpen = false;
        yield return new WaitForSeconds(1.2f);
        //        if (GlobalVariables.curentNativeAd != null)
        //            GlobalVariables.curentNativeAd.LoadAd();
        menu.SetActive(false);
    }

    /// <summary>
    /// Funkcija za prikaz poruke preko Log-a, prilikom klika na dugme
    /// </summary>
    /// /// <param name="message">poruka koju treba prikazati.</param>
    public void ShowMessage(string message)
    {
        Debug.Log(message);
    }

    /// <summary>
    /// Funkcija koja podesava naslov dialoga kao i poruku u dialogu i ova f-ja se poziva iz skripte
    /// </summary>
    /// <param name="messageTitleText">naslov koji treba prikazati.</param>
    /// <param name="messageText">custom poruka koju treba prikazati.</param>
    public void ShowPopUpMessage(string messageTitleText, string messageText)
    {
        transform.Find("PopUps/PopUpMessage/AnimationHolder/Body/HeaderHolder/TextHeader").GetComponent<Text>().text = messageTitleText;
        transform.Find("PopUps/PopUpMessage/AnimationHolder/Body/ContentHolder/TextBG/TextHeader").GetComponent<Text>().text = messageText;
        ShowPopUpMenu(transform.Find("PopUps/PopUpMessage").gameObject);

    }

    /// <summary>
    /// Funkcija koja podesava naslov CustomMessage-a, i ova f-ja se poziva preko button-a zajedno za f-jom ShowPopUpMessageCustomMessageText u redosledu: 1-ShowPopUpMessageTitleText 2-ShowPopUpMessageCustomMessageText
    /// </summary>
    /// <param name="messageTitleText">naslov koji treba prikazati.</param>
    public void ShowPopUpMessageTitleText(string messageTitleText)
    {
        transform.Find("PopUps/PopUpMessage/AnimationHolder/Body/HeaderHolder/TextHeader").GetComponent<Text>().text = messageTitleText;
    }

    /// <summary>
    /// Funkcija koja podesava poruku CustomMessage, i poziva meni u vidu pop-upa, ova f-ja se poziva preko button-a zajedno za f-jom ShowPopUpMessageTitleText u redosledu: 1-ShowPopUpMessageTitleText 2-ShowPopUpMessageCustomMessageText
    /// </summary>
    /// <param name="messageText">custom poruka koju treba prikazati.</param>
    public void ShowPopUpMessageCustomMessageText(string messageText)
    {
        transform.Find("PopUps/PopUpMessage/AnimationHolder/Body/ContentHolder/TextBG/TextHeader").GetComponent<Text>().text = messageText;
        ShowPopUpMenu(transform.Find("PopUps/PopUpMessage").gameObject);
    }

    /// <summary>
    /// Funkcija koja podesava naslov dialoga kao i poruku u dialogu i ova f-ja se poziva iz skripte
    /// </summary>
    /// <param name="dialogTitleText">naslov koji treba prikazati.</param>
    /// <param name="dialogMessageText">custom poruka koju treba prikazati.</param>
    public void ShowPopUpDialog(string dialogTitleText, string dialogMessageText)
    {
        transform.Find("PopUps/PopUpDialog/AnimationHolder/Body/HeaderHolder/TextHeader").GetComponent<Text>().text = dialogTitleText;
        transform.Find("PopUps/PopUpDialog/AnimationHolder/Body/ContentHolder/TextBG/TextHeader").GetComponent<Text>().text = dialogMessageText;
        ShowPopUpMenu(transform.Find("PopUps/PopUpDialog").gameObject);
    }

    /// <summary>
    /// Funkcija koja podesava naslov dialoga kao i poruku u dialogu i ova f-ja se poziva iz skripte
    /// </summary>
    /// <param name="dialogTitleText">naslov koji treba prikazati.</param>
    /// <param name="dialogMessageText">custom poruka koju treba prikazati.</param>
    public void ShowPopUpBattle(string dialogTitleText, string dialogMessageText)
    {
        transform.Find("PopUps/PopUpBattle/AnimationHolder/Body/HeaderHolder/TextHeader").GetComponent<Text>().text = dialogTitleText;
        transform.Find("PopUps/PopUpBattle/AnimationHolder/Body/ContentHolder/TextBG/TextHeader").GetComponent<Text>().text = dialogMessageText;
        ShowPopUpMenu(transform.Find("PopUps/PopUpBattle").gameObject);
    }

    /// <summary>
    /// Funkcija koja podesava naslov dialoga, ova f-ja se poziva preko button-a zajedno za f-jom ShowPopUpDialogCustomMessageText u redosledu: 1-ShowPopUpDialogTitleText 2-ShowPopUpDialogCustomMessageText
    /// </summary>
    /// <param name="dialogTitleText">naslov koji treba prikazati.</param>
    public void ShowPopUpDialogTitleText(string dialogTitleText)
    {
        transform.Find("PopUps/PopUpMessage/AnimationHolder/Body/HeaderHolder/HeaderTextBG/TextHeader").GetComponent<Text>().text = dialogTitleText;
    }

    /// <summary>
    /// Funkcija koja podesava poruku dialoga i poziva meni u vidu pop-upa, ova f-ja se poziva preko button-a zajedno za f-jom ShowPopUpDialogTitleText u redosledu: 1-ShowPopUpDialogTitleText 2-ShowPopUpDialogCustomMessageText
    /// </summary>
    /// <param name="dialogMessageText">custom poruka koju treba prikazati.</param>
    public void ShowPopUpDialogCustomMessageText(string dialogMessageText)
    {
        transform.Find("PopUps/PopUpMessage/AnimationHolder/Body/ContentHolder/TextBG/TextMessage").GetComponent<Text>().text = dialogMessageText;
        ShowPopUpMenu(transform.Find("PopUps/PopUpMessage").gameObject);
    }

    public void StartLoading()
    {
        transform.Find("LoadingMenu").gameObject.SetActive(true);
        transform.Find("LoadingMenu/LoadingHolder/AnimationHolder").GetComponent<Animator>().Play("LoadingGameplayArriving");
    }

    public void LoadAdequateScene(string nameOfScene)
    {
        nameOfSceneToLoad = nameOfScene;

        if (levelUpPopUp == null)
            Debug.Log("Lvl up popup je null");
        else
            Debug.Log("Lvl up popup je aktivan? " + levelUpPopUp.GetComponent<PopUpLevelUp>().popUpLevelUpShown);

        if (levelUpPopUp != null && levelUpPopUp.GetComponent<PopUpLevelUp>().popUpLevelUpShown)
        {
            levelUpPopUp.transform.Find("AnimationHolder/Body/ButtonExit").GetComponent<Button>().onClick.AddListener(StartLoading);
        }
        else
        {
            StartLoading();
        }
    }

    //	public void AddExp(int addExp)
    //	{
    //		LevelUpHolder.GetComponent<LevelUp>().AddExperience(addExp);
    //	}

    public void ShowHideButtonsMenuHolder()
    {
        //		if(buttonsExpanded)
        //		{
        //			GameObject.Find("ButtonsMenuHolder").GetComponent<Animator>().Play("MenuButtonsHide");
        //		}
        //		else
        //		{
        //			GameObject.Find("ButtonsMenuHolder").GetComponent<Animator>().Play("MenuButtonsExpand");
        //		}
        //		buttonsExpanded = !buttonsExpanded;
    }

    public void ButtonClickSound()
    {
        if (SoundManager.Instance != null)
            SoundManager.Instance.Play_ButtonClick();
    }

    public void ButtonClickSound2()
    {
        if (SoundManager.Instance != null)
            SoundManager.Instance.Play_ButtonClick2();
    }

    public void ButtonClickSound3()
    {
        if (SoundManager.Instance != null)
            SoundManager.Instance.Play_ButtonClick3();
    }

    public void ButtonClickSound4()
    {
        if (SoundManager.Instance != null)
            SoundManager.Instance.Play_ButtonClick4();
    }

    public void ButtonClickSound5()
    {
        if (SoundManager.Instance != null)
            SoundManager.Instance.Play_ButtonClick5();
    }

    public void OpenURL(string url)
    {
        Application.OpenURL(url);
    }

    public void ShowVideoReward()
    {
        if (AdsManager.Instance != null)
            AdsManager.Instance.ShowVideoReward();
    }



    public void ActionInterstitialHome()
    {
        //        AdsManager.Instance.ShowInterstitial();
    }

    public void ActionInterstitialReplay()
    {
        //        AdsManager.Instance.ShowInterstitial();
    }

    public void OpenPrivacyPolicyLink()
    {
        Application.OpenURL(AdsManager.Instance.privacyPolicyLink);
    }
}
