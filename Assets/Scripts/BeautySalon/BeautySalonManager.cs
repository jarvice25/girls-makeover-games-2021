﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
//using System.Diagnostics;

public class BeautySalonManager : MonoBehaviour
{

    int currentNubOfScene = 0;
    int currentSlideToolIndex = -1;
    public static bool canPlayAnimation = true;
    public static GameObject lastObject;
    public GameObject lastFruit;
    public Transform ToolsHolder, SlideToolTransform;
    public GameObject ButtonLeft;
    //FIXME Nidza
    [Header("Character")]
    public Transform characterHolder;
    public GameObject[] characterPrefabs;
    GameObject character;

    //Delegates
    public delegate void NextSceneDel();
    public event NextSceneDel OnNextScene;
    public delegate void PreviousSceneDel();
    public event PreviousSceneDel OnPreviousScene;

    public GameObject Finger;

    // Use this for initialization
    //FIXME Nidza
    IEnumerator Start()
    {
        Debug.Log("BSM Start");
        character = Instantiate(characterPrefabs[GlobalVariables.selectedCharacterIndex[GlobalVariables.currentPlayerIndex]], characterHolder) as GameObject;
        character.transform.localScale = new Vector3(3.5f, 3.5f, 1f);
        character.transform.localPosition = Vector3.zero;
        //Set character for BeautySalon
        yield return null;
        character.GetComponent<CharacterSetUp>().SetUpCharacter(GlobalVariables.mainCharacterDressUpData[GlobalVariables.currentPlayerIndex], GlobalVariables.mainCharacterMakeUpData[GlobalVariables.currentPlayerIndex]);

        yield return null;
        //        yield return new WaitForSeconds(1.2f);
        character.transform.GetChild(0).GetComponent<Animator>().Play("CharacterArriving");
        //        GlobalVariables.curentNativeAd = ToolsHolder.GetChild(currentNubOfScene).GetComponentInChildren<FacebookNativeAd>();
        //        if (GlobalVariables.curentNativeAd != null)
        //            GlobalVariables.curentNativeAd.LoadAdWithDelay(1f);
        ButtonLeft.SetActive(false);
        Debug.Log("BSM Start zavrsio");

        Finger.SetActive(false);
    }

    public void NextScene()
    {
        //        GameObject.Find("Canvas").GetComponent<MenuManager>().AddExp(GlobalVariables.CollectXpFromBuffer());

        //FIXME Nidza
        DisableButtons();

        canPlayAnimation = true;


        if (lastObject != null)
            lastObject.SetActive(false);

        if (SlideToolTransform.GetChild(0).gameObject.activeSelf)
        {
            SlideToolTransform.parent.parent.GetComponent<Animator>().Play("SideLeftToolDeparting");
        }
        if (currentSlideToolIndex != -1)
            SlideToolTransform.GetChild(currentSlideToolIndex).gameObject.SetActive(false);
        currentSlideToolIndex = -1;
        if (currentNubOfScene < ToolsHolder.childCount - 2)
        {
            //            if (GlobalVariables.curentNativeAd != null)
            //                GlobalVariables.curentNativeAd.HideNativeAd();

            ToolsHolder.GetChild(currentNubOfScene).GetComponent<Animator>().Play("ScenesDepart");
            currentNubOfScene++;

            //            GlobalVariables.curentNativeAd = ToolsHolder.GetChild(currentNubOfScene).GetComponentInChildren<FacebookNativeAd>();
            //            if (GlobalVariables.curentNativeAd != null)
            //                GlobalVariables.curentNativeAd.LoadAdWithDelay(1f);

            if (currentNubOfScene == 1)
                ButtonLeft.SetActive(true);
            ToolsHolder.GetChild(currentNubOfScene).GetComponent<Animator>().Play("ScenesArrival");

            if (OnNextScene != null)
                OnNextScene();
        }
        else
        {
            //            if (GlobalVariables.curentNativeAd != null)
            //                GlobalVariables.curentNativeAd.HideBothAds();
            if (AdsManager.Instance != null)
                AdsManager.Instance.ShowInterstitial();

            Debug.Log("Ucitaj novu scenu");
            if (GlobalVariables.numberOfItemsUsed <= 0)
                GlobalVariables.AddContestVotes("BeautySalon", Random.Range(25, 50));
            else if (GlobalVariables.numberOfItemsUsed < 5)
                GlobalVariables.AddContestVotes("BeautySalon", Random.Range(51, 75));
            else
                GlobalVariables.AddContestVotes("BeautySalon", Random.Range(76, 100));

            if (GlobalVariables.currentPlayerIndex == 0)
            {
                if (AdsManager.Instance != null)
                    AdsManager.Instance.ShowInterstitial();
                GameObject.Find("Canvas").GetComponent<MenuManager>().LoadAdequateScene("HairAndMakeUp");
            }
            else
            {
                if (AdsManager.Instance != null)
                    AdsManager.Instance.ShowInterstitial();
                GameObject.Find("Canvas").GetComponent<MenuManager>().LoadAdequateScene("HairAndMakeUpPunk");
            }
        }

        if (currentNubOfScene == 3)
        {
            if (AdsManager.Instance != null)
            {
                AdsManager.Instance.ShowInterstitial();
            }
        }

        if (currentNubOfScene > 0)
        {
            Finger.SetActive(false);
        }

        Invoke("EnableButtons", 1f);
    }

    void EnableButtons()
    {
        ButtonLeft.transform.parent.Find("ButtonArrowRight").GetComponentInChildren<Button>().enabled = true;
        ButtonLeft.GetComponentInChildren<Button>().enabled = true;
    }

    void DisableButtons()
    {
        ButtonLeft.transform.parent.Find("ButtonArrowRight").GetComponentInChildren<Button>().enabled = false;
        ButtonLeft.GetComponentInChildren<Button>().enabled = false;
    }

    public void PreviousScene()
    {
        DisableButtons();
        //FIXME Nidza
        canPlayAnimation = true;


        if (lastObject != null)
            lastObject.SetActive(false);

        if (SlideToolTransform.GetChild(0).gameObject.activeSelf)
        {
            SlideToolTransform.parent.parent.GetComponent<Animator>().Play("SideLeftToolDeparting");
        }
        if (currentSlideToolIndex != -1)
            SlideToolTransform.GetChild(currentSlideToolIndex).gameObject.SetActive(false);
        currentSlideToolIndex = -1;
        if (currentNubOfScene > 0)
        {
            ToolsHolder.GetChild(currentNubOfScene).GetComponent<Animator>().Play("ScenesArrivalReverse");

            //            if (GlobalVariables.curentNativeAd != null)
            //                GlobalVariables.curentNativeAd.HideNativeAd();

            currentNubOfScene--;

            //            GlobalVariables.curentNativeAd = ToolsHolder.GetChild(currentNubOfScene).GetComponentInChildren<FacebookNativeAd>();
            //            if (GlobalVariables.curentNativeAd != null)
            //                GlobalVariables.curentNativeAd.LoadAdWithDelay(1f);

            if (currentNubOfScene == 0)
            {
                ButtonLeft.SetActive(false);
            }
            ToolsHolder.GetChild(currentNubOfScene).GetComponent<Animator>().Play("ScenesDepartReverse");

            if (OnPreviousScene != null)
                OnPreviousScene();
        }
        //        GameObject.Find("Canvas").GetComponent<MenuManager>().AddExp(GlobalVariables.CollectXpFromBuffer());

        Invoke("EnableButtons", 1f);
    }

    public void SelectObject()
    {
        if (lastObject != null)
            lastObject.SetActive(false);
        EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).GetChild(0).gameObject.SetActive(true);
        lastObject = EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).GetChild(0).gameObject;
    }

    public void DeselectObject()
    {
        if (lastObject != null)
        {
            lastObject.SetActive(false);
            lastObject = null;
        }
        currentSlideToolIndex = -1;
    }

    public void ChangeSlideTool(int index)
    {
        Debug.Log("ChangeSlideTool index:" + index + " currentSlideToolIndex:" + currentSlideToolIndex);
        if (index != currentSlideToolIndex)
            StartCoroutine("ChangeSlideToolCoorutine", index);

        if (SoundManager.Instance != null)
            SoundManager.Instance.Play_ButtonClick5();
    }

    IEnumerator ChangeSlideToolCoorutine(int index)
    {
        while (!canPlayAnimation)
            yield return null;

        currentSlideToolIndex = index;
        SlideToolTransform.GetChild(currentSlideToolIndex).gameObject.SetActive(true);
        canPlayAnimation = false;

        SlideToolTransform.parent.parent.GetComponent<Animator>().Play("SideLeftToolArriving");

    }

    public void SelectEyeFruit()
    {
        if (lastFruit != null)
        {
            lastFruit.SetActive(false);
            lastFruit.transform.parent.GetChild(2).gameObject.SetActive(false);
        }
        EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).gameObject.SetActive(true);
        EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(2).gameObject.SetActive(true);
        lastFruit = EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).gameObject;

        GlobalVariables.AddXpToBuffer(EventSystem.current.currentSelectedGameObject.transform.parent.name);
    }


    public void HideSideMenu()
    {
        //FIXME Nidza
        canPlayAnimation = true;

        if (SlideToolTransform.GetChild(0).gameObject.activeSelf)
        {
            SlideToolTransform.parent.parent.GetComponent<Animator>().Play("SideLeftToolDeparting");
        }
        currentSlideToolIndex = -1;
    }

    public int GetCurrentScene()
    {
        return currentNubOfScene;
    }

    void OnDisable()
    {
        OnNextScene = null;
    }
}
