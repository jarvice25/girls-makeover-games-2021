﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutuorialNextScene : MonoBehaviour
{
    public GameObject Finger;
    public CustomButton wash;


    private void Start()
    {
        Finger.SetActive(false);
        wash.onUp.AddListener(() =>
        {
            ShowTutorial();
        });
    }

    void ShowTutorial()
    {
        Finger.SetActive(true);
    }
}
