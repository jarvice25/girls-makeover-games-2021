﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundOnColliderEnter : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.parent.GetChild(0).name.Contains("Sponge") || other.transform.parent.GetChild(0).name.Contains("Green"))
        {
            SoundManager.Instance.Play_ApplyCreme();
            GlobalVariables.AddXpToBuffer(other.transform.parent.name);
        }

        if (other.transform.parent.GetChild(0).name.Contains("Hand"))
        {
            SoundManager.Instance.Play_ApplyShampoo();
            GlobalVariables.AddXpToBuffer(other.transform.parent.name);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        SoundManager.Instance.Stop_ApplyCreme();
        SoundManager.Instance.Stop_ApplyShampoo();
    }

}
