﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using AdvancedMobilePaint;

public class BeautySalonLogic : MonoBehaviour
{

    public AdvancedMobilePaint.AdvancedMobilePaint paintEngineFaceMask;
    public Transform instanceHolder;
    public Texture2D[] patterns;
    [Space()]
    //    public Transform pimplesHolder;
    public Transform mythsHolder;
    [Space()]
    // fruits and vegies that you put on eyes
    public SpriteRenderer fruitLeftEye;
    public SpriteRenderer fruitRightEye;
    public Sprite[] fruitsForEyes;

    [Header("Sounds Holder")]
    public Transform soundsHolder;


    BeautySalonManager bsm;
    ItemDragHandeler[] allDraggableItems;
    Transform itemPointOfAction;
    GameObject removeFruitButton;
    GameObject removeMythsButton;
    CharacterSetUp characterSetUp;
    Animator tutorialAnimator;
    AudioSource currentAudioSource;
    UnityEngine.UI.Button btn;

    // Initialize depending on scene
    void Awake()
    {
        GlobalVariables.numberOfItemsUsed = 0;

        tutorialAnimator = GameObject.Find("Canvas/HandTutorialHolder/AnimationHolder").GetComponent<Animator>();

        if (PlayerPrefs.HasKey("BeautyTutorialShown"))
        {
            if (PlayerPrefs.GetString("BeautyTutorialShown") == "true")
                tutorialAnimator.gameObject.SetActive(false);
        }

        bsm = GameObject.Find("BeautySalonManager").GetComponent<BeautySalonManager>();
        bsm.OnNextScene += delegate
            {
                tutorialAnimator.gameObject.SetActive(false);
                PlayerPrefs.SetString("BeautyTutorialShown", "true");
                PlayerPrefs.Save();
            };

        //Get references to all draggable items in scene
        allDraggableItems = FindObjectsOfType<ItemDragHandeler>();
        //        Debug.Log("Draggable items in scene: " + allDraggableItems.Length);

        //        //Set up Pimples
        //        foreach (Transform pimple in pimplesHolder)
        //        {
        //            pimple.gameObject.AddComponent<PimpleClass>();
        //        }
        //Set up Fruit Buttons()
        foreach (Transform fruitButton in bsm.SlideToolTransform.GetChild(0).GetChild(0))
        {
            fruitButton.GetChild(4).GetComponent<UnityEngine.UI.Button>().onClick.AddListener(SetFruitOnEyes);
        }

    }

    IEnumerator Start()
    {
        yield return null;
        //Set up Myths
        mythsHolder.gameObject.AddComponent<MythsClass>();

        yield return null;
        // Set up FaceMask 
        paintEngineFaceMask.GetComponent<MeshRenderer>().sortingOrder = 24;
        paintEngineFaceMask.SetDrawingTexture(GenerateColoredTex(256, 256, new Color(1, 1, 1, 0)));

        yield return null;
        //Creating itemPointForAction
        itemPointOfAction = new GameObject().transform;
        itemPointOfAction.SetParent(instanceHolder);
        itemPointOfAction.localPosition = Vector3.zero;
        itemPointOfAction.gameObject.AddComponent<Rigidbody2D>();
        itemPointOfAction.GetComponent<Rigidbody2D>().isKinematic = true;
        itemPointOfAction.gameObject.AddComponent<CircleCollider2D>();
        itemPointOfAction.GetComponent<CircleCollider2D>().radius = 0.1f;
        itemPointOfAction.GetComponent<Collider2D>().enabled = false;
        itemPointOfAction.GetComponent<Collider2D>().isTrigger = true;
        itemPointOfAction.name = "ItemPointOfAction";

        yield return null;
        //Creating RemoveFruit button
        removeFruitButton = new GameObject();
        removeFruitButton.AddComponent<RectTransform>();
        removeFruitButton.transform.SetParent(GameObject.Find("Canvas/BeautySalon/AnimationHolder/ToolsHolder/AnimationHolder/Scene4").transform);
        removeFruitButton.transform.localScale = Vector3.one;
        removeFruitButton.transform.localPosition = Vector3.zero;
        removeFruitButton.GetComponent<RectTransform>().sizeDelta = new Vector2(500, 200);
        removeFruitButton.AddComponent<RaycastTarget>();
        removeFruitButton.AddComponent<UnityEngine.UI.Button>();
        removeFruitButton.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(RemoveFruitFromEyes);
        removeFruitButton.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(SoundManager.Instance.Play_PimplesPop);
        removeFruitButton.name = "RemoveFruitButton";

        yield return null;
        //Creating RemoveMyths button
        removeMythsButton = new GameObject();
        removeMythsButton.AddComponent<RectTransform>();
        removeMythsButton.transform.SetParent(GameObject.Find("Canvas/BeautySalon/AnimationHolder/ToolsHolder/AnimationHolder/Scene5").transform);
        removeMythsButton.transform.localScale = Vector3.one;
        removeMythsButton.transform.localPosition = new Vector2(0, -100);
        removeMythsButton.GetComponent<RectTransform>().sizeDelta = new Vector2(200, 200);
        removeMythsButton.AddComponent<RaycastTarget>();
        removeMythsButton.AddComponent<UnityEngine.UI.Button>();
        removeMythsButton.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(RemoveMyths);
        removeMythsButton.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(SoundManager.Instance.Play_PlasterPop);
        removeMythsButton.name = "RemoveMythsButton";

        yield return null;
        characterSetUp = bsm.characterHolder.GetChild(0).GetComponent<CharacterSetUp>();
        Debug.Log("ima character setup " + characterSetUp);
        //Set up brow hairs
        foreach (Transform browHair in characterSetUp.leftBrowHairsHolder)
        {
            browHair.gameObject.AddComponent<BrowHairsClass>();
            browHair.gameObject.AddComponent<CircleCollider2D>();
            browHair.GetComponent<CircleCollider2D>().radius = 0.01f;
        }
        Debug.Log("Zavrsio levu obrvu");
        yield return null;
        foreach (Transform browHair in characterSetUp.rightBrowHairsHolder)
        {
            browHair.gameObject.AddComponent<BrowHairsClass>();
            browHair.gameObject.AddComponent<CircleCollider2D>();
            browHair.GetComponent<CircleCollider2D>().radius = 0.01f;
        }
        Debug.Log("Zavrsio desnu obrvu, ce setuje work space");

        //Activate BeautySalonTargetsHolder
        //        yield return new WaitForSeconds(2.2f);
        yield return null;
        Debug.Log("Samo sto nije...");
        SetUpWorkSpace();
    }

    void OnEnable()
    {
        //TODO Subscribe
        SubscribeMethods();
    }

    // Methods that we subscribe to each item to be called when item is picked up
    #region OnPickedUpMethods
    public void ItemFacialWashPickedUp()
    {
        paintEngineFaceMask.SetVectorBrush(AdvancedMobilePaint.VectorBrush.Circle, 22, 22, new Color(1, 1, 1, 1), patterns[0], false, false, false, false);
        paintEngineFaceMask.raySource = itemPointOfAction;
        paintEngineFaceMask.useAlternativeRay = true;
        paintEngineFaceMask.drawEnabled = true;
        itemPointOfAction.localPosition = Vector3.zero;
        itemPointOfAction.GetComponent<Collider2D>().enabled = true;

        tutorialAnimator.SetInteger("TutorialState", 1);
    }

    public void ItemShowerPickedUp()
    {
        paintEngineFaceMask.SetVectorBrush(AdvancedMobilePaint.VectorBrush.Circle, 33, 33, new Color(1, 1, 1, 0), null, false, false, false, false);
        itemPointOfAction.position += new Vector3(-1f, 0f, 0);
        paintEngineFaceMask.raySource = itemPointOfAction;
        paintEngineFaceMask.useAlternativeRay = true;
        paintEngineFaceMask.drawEnabled = true;

        tutorialAnimator.gameObject.SetActive(false);

        GlobalVariables.AddXpToBuffer("Shower");
        GlobalVariables.numberOfItemsUsed++;
    }

    public void ItemFacialScrubPickedUp()
    {
        paintEngineFaceMask.SetVectorBrush(AdvancedMobilePaint.VectorBrush.Circle, 22, 22, new Color(1, 1, 1, 1), patterns[1], false, false, false, false);
        paintEngineFaceMask.raySource = itemPointOfAction;
        paintEngineFaceMask.useAlternativeRay = true;
        paintEngineFaceMask.drawEnabled = true;
        itemPointOfAction.localPosition = Vector3.zero;
        itemPointOfAction.GetComponent<Collider2D>().enabled = true;
    }

    public void ItemGreenFacialPickedUp()
    {
        paintEngineFaceMask.SetVectorBrush(AdvancedMobilePaint.VectorBrush.Circle, 11, 11, new Color(1, 1, 1, 1), patterns[2], false, false, false, false);
        itemPointOfAction.position += new Vector3(-0.7f, 0.7f, 0);
        itemPointOfAction.GetComponent<Collider2D>().enabled = true;
        paintEngineFaceMask.raySource = itemPointOfAction;
        paintEngineFaceMask.useAlternativeRay = true;
        paintEngineFaceMask.drawEnabled = true;

        PlaySentence("TheMaskWillMakeTheSkinGlow");
    }

    public void ItemFaceMassageMachinePickedUp()
    {
        paintEngineFaceMask.SetVectorBrush(AdvancedMobilePaint.VectorBrush.Circle, 16, 16, new Color(1, 1, 1, 0), null, false, false, false, false);
        itemPointOfAction.position += new Vector3(-0.95f, 0.3f, 0);
        paintEngineFaceMask.raySource = itemPointOfAction;
        paintEngineFaceMask.useAlternativeRay = true;
        paintEngineFaceMask.drawEnabled = true;

        InvokeRepeating("Vibrate", 0, 0.5f);

        GlobalVariables.AddXpToBuffer("Massage");
        GlobalVariables.numberOfItemsUsed++;
    }

    //    public void EnablePimplesSteam()
    //    {
    //        itemPointOfAction.position += new Vector3(-1,0.4f,0);
    //        itemPointOfAction.GetComponent<Collider2D>().enabled = true;
    //
    //        foreach (Transform pimple in pimplesHolder)
    //        {
    //            pimple.GetComponent<Collider2D>().enabled = true;
    //        }
    //    }

    //    public void EnablePimplesForPop()
    //    {
    //        itemPointOfAction.position += new Vector3(-0.7f,0.4f,0);
    //        itemPointOfAction.GetComponent<Collider2D>().enabled = true;
    //
    //        foreach (Transform pimple in pimplesHolder)
    //        {
    //            pimple.GetComponent<Collider2D>().enabled = true;
    //        }
    //    }

    public void EnableMyths()
    {
        bsm.OnNextScene += RemoveMyths;
        bsm.OnPreviousScene += RemoveMyths;
        itemPointOfAction.GetComponent<Collider2D>().enabled = true;
        mythsHolder.GetComponent<Collider2D>().enabled = true;

        PlaySentence("LetsGetRidOfThisNastyBlackheads");
    }

    public void EnableBrowHairs()
    {
        itemPointOfAction.position += new Vector3(-0.52f, 0.38f, 0);
        itemPointOfAction.GetComponent<CircleCollider2D>().radius = 0.05f;
        itemPointOfAction.GetComponent<Collider2D>().enabled = true;

        foreach (Transform browHair in characterSetUp.leftBrowHairsHolder)
        {
            browHair.gameObject.GetComponent<Collider2D>().enabled = true;
        }
        foreach (Transform browHair in characterSetUp.rightBrowHairsHolder)
        {
            browHair.gameObject.GetComponent<Collider2D>().enabled = true;
        }
    }
    #endregion  //OnPickedUpMethods

    #region OnReleasedMethods
    public void FaceMaskItemReleased()
    {
        itemPointOfAction.localPosition = Vector3.zero;
        paintEngineFaceMask.drawEnabled = false;
        paintEngineFaceMask.useAlternativeRay = false;
        CancelInvoke("Vibrate");
        if (tutorialAnimator.gameObject.activeSelf)
        {
            if (tutorialAnimator.GetInteger("TutorialState") == 1)
                tutorialAnimator.SetInteger("TutorialState", 2);
            else
            {
                tutorialAnimator.gameObject.SetActive(false);
                PlayerPrefs.SetString("BeautyTutorialShown", "true");
                PlayerPrefs.Save();
            }
        }
    }

    //    public void DisablePimples()
    //    {
    //        itemPointOfAction.localPosition = Vector3.zero;
    //        itemPointOfAction.GetComponent<Collider2D>().enabled = false;
    //
    //        foreach (Transform pimple in pimplesHolder)
    //        {
    //            pimple.GetComponent<Collider2D>().enabled = false;
    //        }
    //    }

    public void DisableMyths()
    {
        itemPointOfAction.localPosition = Vector3.zero;
        itemPointOfAction.GetComponent<Collider2D>().enabled = false;
        mythsHolder.GetComponent<Collider2D>().enabled = false;
    }

    public void DisableBrowHairs()
    {
        itemPointOfAction.localPosition = Vector3.zero;
        itemPointOfAction.GetComponent<CircleCollider2D>().radius = 0.1f;
        itemPointOfAction.GetComponent<Collider2D>().enabled = false;

        foreach (Transform browHair in characterSetUp.leftBrowHairsHolder)
        {
            browHair.gameObject.GetComponent<Collider2D>().enabled = false;
        }
        foreach (Transform browHair in characterSetUp.rightBrowHairsHolder)
        {
            browHair.gameObject.GetComponent<Collider2D>().enabled = false;
        }
    }
    #endregion //OnReleaseMethods

    #region ButtonCalls
    public void SetFruitOnEyes()
    {
        bsm.OnNextScene -= RemoveFruitFromEyes;
        bsm.OnPreviousScene -= RemoveFruitFromEyes;
        fruitLeftEye.sprite = fruitsForEyes[EventSystem.current.currentSelectedGameObject.transform.parent.GetSiblingIndex()];
        fruitRightEye.sprite = fruitsForEyes[EventSystem.current.currentSelectedGameObject.transform.parent.GetSiblingIndex()];
        fruitLeftEye.GetComponent<SpriteRenderer>().enabled = true;
        fruitRightEye.GetComponent<SpriteRenderer>().enabled = true;
        fruitLeftEye.transform.GetChild(0).gameObject.SetActive(false);
        fruitRightEye.transform.GetChild(0).gameObject.SetActive(false);

        bsm.OnNextScene += RemoveFruitFromEyes;
        bsm.OnPreviousScene += RemoveFruitFromEyes;
    }

    public void RemoveFruitFromEyes()
    {
        fruitLeftEye.transform.GetChild(0).gameObject.SetActive(true);
        if (fruitLeftEye.GetComponent<SpriteRenderer>().enabled)
            fruitLeftEye.transform.GetChild(0).GetComponent<ParticleSystem>().Play(true);
        fruitRightEye.transform.GetChild(0).gameObject.SetActive(true);
        if (fruitLeftEye.GetComponent<SpriteRenderer>().enabled)
            fruitRightEye.transform.GetChild(0).GetComponent<ParticleSystem>().Play(true);
        fruitLeftEye.GetComponent<SpriteRenderer>().enabled = false;
        fruitRightEye.GetComponent<SpriteRenderer>().enabled = false;

        bsm.OnNextScene -= RemoveFruitFromEyes;
        bsm.OnPreviousScene -= RemoveFruitFromEyes;
    }

    public void RemoveMyths()
    {
        mythsHolder.GetComponent<MythsClass>().RemoveMyths();
        bsm.OnNextScene -= RemoveMyths;
        bsm.OnPreviousScene -= RemoveMyths;
    }
    #endregion  //ButtonCalls

    void OnDisable()
    {
        //TODO Unsubscribe
        UnsubscribeMethods();
    }

    void OnDestroy()
    {
        //TODO Unsubscribe
        UnsubscribeMethods();
    }

    #region helperMethods
    void SetItemOnSceneSwitch()
    {
        Debug.Log("SetUtenIbScebeSwutcgasdasdasd " + bsm.GetCurrentScene());
        switch (bsm.GetCurrentScene())
        {
            case 1:

                break;
            case 2:

                break;
            case 3:

                break;
            default:
                Debug.Log("Scene not handeled: " + bsm.GetCurrentScene());
                break;
        }
    }

    Texture2D GenerateColoredTex(int width, int height, Color c)
    {
        Texture2D tex = new Texture2D(width, height, TextureFormat.RGBA32, false);
        Color[] pix = tex.GetPixels();
        for (int i = 0; i < pix.Length; i++)
        {
            pix[i] = c;
        }
        tex.SetPixels(pix);
        tex.Apply();
        tex.filterMode = FilterMode.Point;
        return tex;
    }

    void SubscribeMethods()
    {
        bsm.OnNextScene += SetItemOnSceneSwitch;
        bsm.OnPreviousScene += SetItemOnSceneSwitch;

        foreach (ItemDragHandeler item in allDraggableItems)
        {
            if (SoundManager.Instance != null)
                item.OnPickedUp += SoundManager.Instance.Play_ItemPickUp;

            switch (item.name)
            {
                case "FacialWashCreamHolder":
                case "FaceMassageCreamHolder":
                    item.OnPickedUp += ItemFacialWashPickedUp;
                    item.OnReleased += FaceMaskItemReleased;
                    break;
                case "ShowerHolder":
                    item.OnPickedUp += ItemShowerPickedUp;
                    item.OnReleased += FaceMaskItemReleased;
                    item.OnPickedUp += SoundManager.Instance.Play_ItemShower;
                    item.OnReleased += SoundManager.Instance.Stop_ItemShower;
                    break;
                case "FacialScrubCreamHolder":
                    item.OnPickedUp += ItemFacialScrubPickedUp;
                    item.OnReleased += FaceMaskItemReleased;
                    break;
                case "GreenFacialCreamHolder":
                    item.OnPickedUp += ItemGreenFacialPickedUp;
                    item.OnReleased += FaceMaskItemReleased;
                    break;
                //                case "SteamerHolder":
                //                    item.OnPickedUp += EnablePimplesSteam;
                //                    item.OnReleased += DisablePimples;
                //                    item.OnPickedUp += SoundManager.Instance.Play_ItemSteamer;
                //                    item.OnReleased += SoundManager.Instance.Stop_ItemSteamer;
                //                    break;
                //                case "PimpleRemoverHolder":
                //                    item.OnPickedUp += EnablePimplesForPop;
                //                    item.OnReleased += DisablePimples;
                //                    break;
                case "NoseStripsHolder":
                    item.OnPickedUp += EnableMyths;
                    item.OnReleased += DisableMyths;
                    break;
                case "FaceMassageMachineHolder":
                    item.OnPickedUp += ItemFaceMassageMachinePickedUp;
                    item.OnReleased += FaceMaskItemReleased;
                    item.OnPickedUp += SoundManager.Instance.Play_ItemMassage;
                    item.OnReleased += SoundManager.Instance.Stop_ItemMassage;
                    break;
                case "TweezersHolder":
                    item.OnPickedUp += EnableBrowHairs;
                    item.OnReleased += DisableBrowHairs;
                    break;
                default:
                    Debug.Log("Item not handeled: " + item.name);
                    break;
            }
        }
    }

    void UnsubscribeMethods()
    {
        bsm.OnNextScene -= RemoveFruitFromEyes;
        bsm.OnPreviousScene -= RemoveFruitFromEyes;

        bsm.OnNextScene -= SetItemOnSceneSwitch;
        bsm.OnPreviousScene -= SetItemOnSceneSwitch;

        foreach (ItemDragHandeler item in allDraggableItems)
        {
            if (SoundManager.Instance != null)
                item.OnPickedUp -= SoundManager.Instance.Play_ItemPickUp;

            switch (item.name)
            {
                case "FacialWashCreamHolder":
                case "FaceMassageCreamHolder":
                    item.OnPickedUp -= ItemFacialWashPickedUp;
                    item.OnReleased -= FaceMaskItemReleased;
                    break;
                case "ShowerHolder":
                    item.OnPickedUp -= ItemShowerPickedUp;
                    item.OnReleased -= FaceMaskItemReleased;
                    break;
                case "FacialScrubCreamHolder":
                    item.OnPickedUp -= ItemFacialScrubPickedUp;
                    item.OnReleased -= FaceMaskItemReleased;
                    break;
                case "GreenFacialCreamHolder":
                    item.OnPickedUp -= ItemGreenFacialPickedUp;
                    item.OnReleased -= FaceMaskItemReleased;
                    break;
                //                case "SteamerHolder":
                //                    item.OnPickedUp -= EnablePimplesSteam;
                //                    item.OnReleased -= DisablePimples;
                //                    break;
                //                case "PimpleRemoverHolder":
                //                    item.OnPickedUp -= EnablePimplesForPop;
                //                    item.OnReleased -= DisablePimples;
                //                    break;
                case "NoseStripsHolder":
                    item.OnPickedUp -= EnableMyths;
                    item.OnReleased -= DisableMyths;
                    break;
                case "FaceMassageMachineHolder":
                    item.OnPickedUp -= ItemFaceMassageMachinePickedUp;
                    item.OnReleased -= FaceMaskItemReleased;
                    break;
                case "TweezersHolder":
                    item.OnPickedUp -= EnableBrowHairs;
                    item.OnReleased -= DisableBrowHairs;
                    break;
                default:
                    break;
            }
        }
        Debug.Log("Time end: " + Time.deltaTime.ToString());
    }
    /// <summary>
    /// Method that hides myths and pimples on character and enables targets for BeautySalon scene.
    /// </summary>
    void SetUpWorkSpace()
    {
        paintEngineFaceMask.transform.parent.gameObject.SetActive(true);
        characterSetUp.pimplesHolder.SetActive(false);
        characterSetUp.mythsHolder.SetActive(false);
        Debug.Log("Setovao work space");
    }

    void Vibrate()
    {
        Handheld.Vibrate();
    }

    void PlaySentence(string sentence)
    {
        if (soundsHolder.Find(sentence) != null)
        {
            if (currentAudioSource != null)
                currentAudioSource.Stop();

            currentAudioSource = soundsHolder.Find(sentence).GetComponent<AudioSource>();
            currentAudioSource.Play();
        }
    }
    #endregion //HelperMethods
}

///// <summary>
///// Pimple class.
///// </summary>
//public class PimpleClass : MonoBehaviour{
//
//    bool pimpleReadyToPop = false;
//    bool pimplePopped = false;
//    SpriteRenderer sr;
//    Color colorGain = new Color(0,0,0,0.1f);
//    WaitForEndOfFrame weof = new WaitForEndOfFrame();
//
//    void Awake()
//    {
//        sr = GetComponent<SpriteRenderer>();
//    }
//
//    void OnTriggerEnter2D(Collider2D other)
//    {
//        if (pimplePopped)
//            return;
////        Debug.Log("Other " + other.name + "i Pimple" + transform.GetSiblingIndex().ToString());
//        Debug.Log("Other item " + other.transform.parent.GetChild(0).name.Contains("Steamer"));
//        if (other.transform.parent.GetChild(0).name.Contains("Steamer"))
//        {
//            StopCoroutine(CSteamOnPimples());
//            StartCoroutine(CSteamOnPimples());
//            GlobalVariables.AddXpToBuffer("Steamer");
//            GlobalVariables.numberOfItemsUsed++;
//        }
//
//        if (other.transform.parent.GetChild(0).name.Contains("PimpleRemover"))
//        {
//            if (pimpleReadyToPop)
//            {
//                //PimplePop
//                transform.GetChild(0).gameObject.SetActive(true);
//                transform.GetChild(0).GetComponent<ParticleSystem>().Play(true);
//                sr.enabled = false;
//                pimplePopped = true;
//                SoundManager.Instance.Play_PimplesPop();
//                GlobalVariables.AddXpToBuffer("Pimple");
//                GlobalVariables.numberOfItemsUsed++;
//            }
//        }
//
//    }
//
//    IEnumerator CSteamOnPimples()
//    {
//        while (sr.color.a < 1)
//        {
//            sr.color += colorGain;
//            yield return weof;
//        }
//        pimpleReadyToPop = true;
//    }
//}
/// <summary>
/// Myths class.
/// </summary>
public class MythsClass : MonoBehaviour
{

    bool mythsRemoved = false;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (transform.GetChild(0).gameObject.activeSelf || mythsRemoved)
            return;

        //        Debug.Log("Collision on Myths: " + other.transform.parent.GetChild(0).name);
        //Set strip
        if (other.transform.parent.GetChild(0).name.Contains("NoseStrip"))
        {
            other.transform.parent.GetChild(0).GetComponent<UnityEngine.UI.Image>().enabled = false;
            transform.GetChild(0).gameObject.SetActive(true);
        }
    }

    public void RemoveMyths()
    {
        Debug.Log("RemoveMyths: " + transform.GetChild(0).gameObject.activeSelf);
        if (transform.GetChild(0).gameObject.activeSelf)
        {
            mythsRemoved = true;
            GetComponent<SpriteRenderer>().enabled = false;
            transform.GetChild(0).gameObject.SetActive(false);
            transform.GetChild(1).gameObject.SetActive(true);
            transform.GetChild(1).GetComponent<ParticleSystem>().Play(true);
            GlobalVariables.AddXpToBuffer("Myths");
            GlobalVariables.numberOfItemsUsed++;
        }
    }
}
/// <summary>
/// Brow hairs class.
/// </summary>
public class BrowHairsClass : MonoBehaviour
{

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.parent.GetChild(0).name.Contains("Tweezers"))
        {
            other.transform.parent.GetChild(0).GetChild(0).GetComponent<ParticleSystem>().Emit(1);
            gameObject.SetActive(false);
            SoundManager.Instance.Play_EyebowsPop();
            GlobalVariables.AddXpToBuffer("Tweezers");
            GlobalVariables.numberOfItemsUsed++;
        }
    }
}