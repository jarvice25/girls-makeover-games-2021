﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

///<summary>
///<para>Scene:All/NameOfScene/NameOfScene1,NameOfScene2,NameOfScene3...</para>
///<para>Object:N/A</para>
///<para>Description: Sample Description </para>
///</summary>

public class AnimEventsNew : MonoBehaviour {

	public void LoadScene()
	{
		Application.LoadLevel("Transition");
	}

	public void LoadingTurnOffObject()
	{
		transform.parent.parent.gameObject.SetActive(false);
	}

	public void ShowLeaderBoard()
	{
//		GameObject.Find("Canvas/Bg").GetComponent<Contest>().GenerateLeaderBoard();
	}

    public void PhoneRinging()
    {
        SoundManager.Instance.Play_PhoneNotification();
    }

    public void PhoneSms()
    {
        SoundManager.Instance.Play_PhoneSMS();
    }
}
