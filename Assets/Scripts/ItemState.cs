﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemState: MonoBehaviour  {

	public enum StateOfItemEnum {Normal,Video,InApp,Experience,Reward, Currency}; // video, exp, inapps, reward, moneta
	[Tooltip("State Of Item ima 6 stanja: 1 - Normal(item je otkljucan), 2 - Video(item zakljucan za video), 3 - InApp(item zakljucan za inapp), 4 - Experience(item zakljucan za experience ili progres(lvl u svetu)), 5-Reward(item koji se dobija kao reward), 6-Currency(item zakljucan za neku monetu(coins, zlato, dijamant,banana))")]
	/// <summary>enum za koji se otkljucava item, moze uzeti jednu od vrednosti iznad </summary>
	public StateOfItemEnum StateOfItem;
	/// <summary> potreban lvl da se otkljuca item </summary>
//	[ShowOnlyAttribute]
	public int ExpLevelToUnlock;
//	[ShowOnlyAttribute]
	public int Cost;
//	[ShowOnlyAttribute]
	public string InAppIDThatUnlocksItem;
	public string VideoID;

    public void WatchVideoForItem()
    {
//        #if UNITY_EDITOR
//        UnlockItemForVideo();
//        #else
        AdsManager.Instance.IsVideoRewardAvailable();
//        #endif
        AdsManager.OnVideoRewarded += this.UnlockItemForVideo;
    }

    /// <summary>
    /// Funkcija koja se poziva kada treba otkljucati item za odgledani video, takodje ova f-ja hendluje event kada se desi
    /// </summary>
    void UnlockItemForVideo()
    {
        Debug.Log("UnlockItemForVideo");
        AdsManager.OnVideoRewarded -= this.UnlockItemForVideo;
        transform.Find("LockedHolder").gameObject.SetActive(false);
        GlobalVariables.SaveVideoUnlockedItemsString(this.name);
    }
    
    /// <summary>
    /// Funkcija koja se poziva kada treba otkljucati item za postignut experience ili lvl necega(nivo, lik...), takodje ova f-ja hendluje event kada se desi
    /// </summary>
    void UnlockItemForExperience()
    {
        //FIXME zakomentarisano zbog testa Unity apk
//        if(GlobalVariables.currentLvl==ExpLevelToUnlock)
//        {
            Debug.Log("Otkljucaj "+ExpLevelToUnlock+" "+this.name);
            transform.Find("LockedHolder").gameObject.SetActive(false);
//            GlobalVariables.SaveExpUnlockedItemsString(this.name);
//        }
    }
    
    /// <summary>
    /// Funkcija koja se poziva kada treba otkljucati item za kupljen inapp, takodje ova f-ja hendluje event kada se desi
    /// </summary>
    void UnlockItemForInApp()
    {
        Debug.Log("UnlockItemForAds");
    }
    
    /// <summary>
    /// Funkcija koja se poziva kada treba otkljucati item za reward(daily,chest...), takodje ova f-ja hendluje event kada se desi
    /// </summary>
    void UnlockItemForReward()
    {
        Debug.Log("UnlockItemForAds");
    }
    
    /// <summary>
    /// Funkcija koja se poziva kada treba otkljucati item za neki currency, takodje ova f-ja hendluje event kada se desi
    /// </summary>
    void UnlockItemForCurrency()
    {
        Debug.Log("UnlockItemForAds");
    }

    void OnEnable()
    {
        switch(StateOfItem.ToString())
        {
            case "Experience":
                Debug.Log("Experience " + this.name);
                //FIXME da ne bude set u runtime
                ExpLevelToUnlock = GlobalVariables.GetItemUnlockLevel(name);

                if(GlobalVariables.currentLvl>=ExpLevelToUnlock || GlobalVariables.unlockAll || GlobalVariables.fullGameOwned)
                {
                    UnlockItemForExperience();
                }
                else
                {
                    LevelUp.OnLevelUp += UnlockItemForExperience;
                    transform.Find("LockedHolder/LevelUnlock").gameObject.SetActive(true);
                    transform.Find("LockedHolder/LevelUnlock/TextNumber").GetComponent<Text>().text = ExpLevelToUnlock.ToString();
                }
                break;
            case "InApp":
                Debug.Log("InApp");
                break;
            case "Video": 
                Debug.Log("Video");
                if (GlobalVariables.unlockAll || GlobalVariables.fullGameOwned || GlobalVariables.CheckIfItemIsUnlockedForVideo(this.name))
                {
                    UnlockItemForVideo();
                }
                else
                {
                    if (transform.Find("LockedHolder").gameObject.activeSelf)
                    {
                        transform.Find("LockedHolder").GetComponent<Button>().onClick.RemoveAllListeners();
                        transform.Find("LockedHolder").GetComponent<Button>().onClick.AddListener(WatchVideoForItem);
                    }

                    transform.Find("LockedHolder/VideoIconImage").gameObject.SetActive(true);
                }
                break;
            case "Reward":
                Debug.Log("Reward");
                break;
            case "Currency":
                Debug.Log("Currency");
                break;
            default:
                transform.Find("LockedHolder").gameObject.SetActive(false);
                break;
        }
        
    }
    
    void OnDisable()
    {
        switch(StateOfItem.ToString())
        {
            case "Experience":
                LevelUp.OnLevelUp -= UnlockItemForExperience;
                break;
            case "InApp":
                
                Debug.Log("InApp");
                break;
            case "Video":
                Debug.Log("Video");
                AdsManager.OnVideoRewarded -= this.UnlockItemForVideo;
                transform.Find("LockedHolder").GetComponent<Button>().onClick.RemoveListener(WatchVideoForItem);
                break;
            case "Reward":
                Debug.Log("Reward");
                break;
            case "Currency":
                Debug.Log("Currency");
                break;
        }
    }
    
    void OnDestroy()
    {
        switch(StateOfItem.ToString())
        {
            case "Experience":
                LevelUp.OnLevelUp -= UnlockItemForExperience;
                break;
            case "InApp":
                Debug.Log("InApp");
                break;
            case "Video":
                Debug.Log("Video");
                AdsManager.OnVideoRewarded -= this.UnlockItemForVideo;
                transform.Find("LockedHolder").GetComponent<Button>().onClick.RemoveListener(WatchVideoForItem);
                break;
            case "Reward":
                Debug.Log("Reward");
                break;
            case "Currency":
                Debug.Log("Currency");
                break;
        }
    }
}

