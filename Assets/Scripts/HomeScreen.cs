﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

public class HomeScreen : MonoBehaviour {


	public GameObject selectedParticle;
    public Animator[] charactersAnimators;
    [Space(5)]
    public List <CharacterName> characterNames;
    [Space(5)]
    public GameObject[] girlsHolders;
    [Space(5)]
    public Text[] character0NameHolders;
    public Text[] character1NameHolders;
    public Text[] character2NameHolders;
    CharacterName[] selectedCharacterNames;

    bool characterSelected = false;

    void Awake()
    {
        if (GlobalVariables.currentPlayerIndex != 0)
        {
            foreach (CharacterName item in characterNames)
            {
                if (item.name == GlobalVariables.selectedCharacterName[0])
                {
                    characterNames.Remove(item);
                    break;
                }
            }
        }

        selectedCharacterNames = new CharacterName[3];
        for (int i = 0; i < selectedCharacterNames.Length; i++)
        {
            selectedCharacterNames[i] = PullNameFromList();
        }
        
        foreach (var txt in character0NameHolders)
        {
            txt.text = selectedCharacterNames[0].name;
        }
        
        foreach (var txt in character1NameHolders)
        {
            txt.text = selectedCharacterNames[1].name;
        }
        
        foreach (var txt in character2NameHolders)
        {
            txt.text = selectedCharacterNames[2].name;
        }
        
        girlsHolders[GlobalVariables.currentPlayerIndex].SetActive(true);
    }

    CharacterName PullNameFromList()
    {
        CharacterName cn = characterNames[Random.Range(0, characterNames.Count)];
        characterNames.Remove(cn);
        return cn;
    }

    void Start()
    {
        //Randomize animation
        if (charactersAnimators != null)
        {
            foreach (Animator anim in charactersAnimators)
            {
                if(anim != null)
                    anim.Play("CharacterIdle",0,Random.Range(0f,1f));
            }
        }
    }

    public void SelectCharacter(int index)
	{
        if(!characterSelected)
        {
            characterSelected = true;
            Debug.Log("SelectCharacter");
            GlobalVariables.selectedCharacterIndex[GlobalVariables.currentPlayerIndex] = index;
            GlobalVariables.selectedCharacterName[GlobalVariables.currentPlayerIndex] = selectedCharacterNames[index].name;
            GlobalVariables.MainCharacterDefaultDressUpData();
            StartCoroutine("SelectCh");
            if (SoundManager.Instance != null)
                SoundManager.Instance.Play_CharacterSelected();
        }
    }

    IEnumerator SelectCh()
    {
        yield return null;
		selectedParticle.SetActive(true);	
		EventSystem.current.currentSelectedGameObject.transform.parent.GetComponent<Animator>().Play("NameClicked");

        yield return new WaitForSeconds(1);
        selectedCharacterNames[GlobalVariables.selectedCharacterIndex[GlobalVariables.currentPlayerIndex]].nameAudio.Play();

        yield return new WaitForSeconds(2);
        if(GlobalVariables.currentPlayerIndex == 0)
            GameObject.Find("Canvas").GetComponent<MenuManager>().LoadAdequateScene("BeautySalon");
        else
            GameObject.Find("Canvas").GetComponent<MenuManager>().LoadAdequateScene("BeautySalonPunk");
	}

	public void StartLoading()
	{
		transform.Find("LoadingMenu").gameObject.SetActive(true);
		transform.Find("LoadingMenu/LoadingHolder/AnimationHolder").GetComponent<Animator>().Play("LoadingGameplayArriving");
	}

    public void LockedCharacterSelected()
    {
        if (!characterSelected)
        {
            Debug.Log("LockedCharacterSelected");
            EventSystem.current.currentSelectedGameObject.transform.parent.parent.GetComponent<Animator>().Play("NameLockedClicked",0,0);
            if (SoundManager.Instance != null)
                SoundManager.Instance.Play_CharacterLocked();
        }
    }
}

[System.Serializable]
public class CharacterName
{
    public string name;
    public AudioSource nameAudio;
}
