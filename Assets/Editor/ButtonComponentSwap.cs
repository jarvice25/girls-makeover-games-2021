﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEditor;
using UnityEditorInternal;
using System.Linq;
using UnityEditor.Events;
using UnityEditor.SceneManagement;
//using System.Reflection;

public class ButtonComponentSwap : EditorWindow {

    static ButtonComponentSwap window;

    CustomButton [] allButtonsOfType;
    GameObject [] allButtonsGameObjects;

    bool doingWork;

    [MenuItem("Custom Tools/Swap Button Components")]
    static void ShowWindow()
    {
        EditorWindow.GetWindow<ButtonComponentSwap>();
    }

    void OnGUI()
    {
        if (GUILayout.Button("Swap") && !doingWork)
        {
            doingWork = true;
            SwapComponents();
        }
    }

    void SwapComponents()
    {
        //TODO probati sa system.reflection
        allButtonsOfType = FindObjectsOfTypeAll<CustomButton>().ToArray();

        GameObject tempCustomButtonHolder = new GameObject();
        GameObject tempWbButtonHolder;

        for (int i = 0; i < allButtonsOfType.Length; i++)
        {
            //Get wb button GO
            tempWbButtonHolder = allButtonsOfType[i].gameObject;
            //Refresh CustomButton component
            if(tempCustomButtonHolder.GetComponent<CustomButton>() != null)
                DestroyImmediate(tempCustomButtonHolder.GetComponent<CustomButton>());
            tempCustomButtonHolder.AddComponent<CustomButton>();
            //Assign values 
            tempCustomButtonHolder.GetComponent<CustomButton>().interactable = allButtonsOfType[i].interactable;
            tempCustomButtonHolder.GetComponent<CustomButton>().transition = allButtonsOfType[i].transition;
//            tempCustomButtonHolder.GetComponent<CustomButton>().onClick = allButtonsOfType[i].onClick;
            tempCustomButtonHolder.GetComponent<CustomButton>().onUp = allButtonsOfType[i].onUp;
            tempCustomButtonHolder.GetComponent<CustomButton>().onDown = allButtonsOfType[i].onDown;
            //Destroy wb component
//            allButtonsOfType[i].onClick = new Button.ButtonClickedEvent();
            allButtonsOfType[i].onUp = new CustomButton.ButtonOnUpEvent();
            allButtonsOfType[i].onDown = new CustomButton.ButtonOnDownEvent();
            DestroyImmediate(allButtonsOfType[i]);

            //Move new component from temp to destination
            ComponentUtility.CopyComponent(tempCustomButtonHolder.GetComponent<CustomButton>());
            ComponentUtility.PasteComponentAsNew(tempWbButtonHolder);

        }

        DestroyImmediate(tempCustomButtonHolder);

        EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        AssetDatabase.SaveAssets();

        doingWork = false;
    }

    List<T> FindObjectsOfTypeAll<T>()
    {
        List<T> results = new List<T>();
        for(int i = 0; i< EditorSceneManager.sceneCount; i++)
        {
            var s = EditorSceneManager.GetSceneAt(i);
            if (s.isLoaded)
            {
                var allGameObjects = s.GetRootGameObjects();
                for (int j = 0; j < allGameObjects.Length; j++)
                {
                    var go = allGameObjects[j];
                    results.AddRange(go.GetComponentsInChildren<T>(true));
                }
            }
        }
        return results;
    }
}
