﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof (DressUpItemSprite))]
[CustomPropertyDrawer(typeof (DressUpItemPrefab))]
public class DressUpItemPropertyDrawer : PropertyDrawer {

    int lh1 = 18; // line height

    public override float GetPropertyHeight( SerializedProperty property, GUIContent label ) {
        return lh1*3 + ( lh1 * (property.FindPropertyRelative("styles").arraySize+3) );
    }

    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        EditorGUI.BeginProperty(position, label, property);

        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        // Calculate rects
        Rect ctrlAdd =  new Rect ( position.x+70f,    position.y+1f,   20f,    lh1-6f );
        Rect ctrlRem =  new Rect ( position.x+93f,    position.y+1f,   20f,    lh1-6f );
        Rect textR =    new Rect ( position.x,        position.y,      100f,   lh1 );
        Rect valueR;

        // Draw fields - passs GUIContent.none to each so they are drawn without labels
        SerializedProperty stylesProp = property.FindPropertyRelative("styles");
        SerializedProperty stylePointsProp = property.FindPropertyRelative("stylePoints");
        SerializedProperty thumbnailProp = property.FindPropertyRelative("thumbnail");

        EditorGUI.LabelField( textR, "Styles", EditorStyles.label );
        // + and - buttons
        if ( GUI.Button( ctrlAdd, "+" ) ) 
        {
            if(stylesProp.arraySize < 4)
                stylesProp.InsertArrayElementAtIndex(stylesProp.arraySize );
        }
        if ( GUI.Button( ctrlRem, "-" ) ) 
        {
            if(stylesProp.arraySize > 1)
                stylesProp.DeleteArrayElementAtIndex(stylesProp.arraySize-1 );
        }
        //Styles enum List
        for ( int i=0; i < stylesProp.arraySize; i++ ) 
        {
            // draw every element of the array
            valueR = new Rect (position.x + 70f,    position.y + lh1 + (lh1 * i),    100f,   lh1 );
            EditorGUI.PropertyField( valueR, stylesProp.GetArrayElementAtIndex(i), GUIContent.none );
           
        }
        //StylePoints display
        textR = new Rect(position.x, position.y + (stylesProp.arraySize+1) * lh1,    position.width,   lh1 );
        EditorGUI.LabelField( textR, "Style Points", EditorStyles.label );
        valueR = new Rect (position.x + 70, position.y + (stylesProp.arraySize+1) * lh1,    80f,   lh1 );
        EditorGUI.PropertyField(valueR, stylePointsProp, GUIContent.none);
        //ItemValue
        textR = new Rect(position.x, position.y + (stylesProp.arraySize+2) * lh1,    position.width,   lh1 );
        EditorGUI.LabelField( textR, "Item Value", EditorStyles.label );
        valueR = new Rect (position.x + 70f, position.y + (stylesProp.arraySize+2) * lh1,    50f,   lh1 );
        EditorGUI.PropertyField(valueR, property.FindPropertyRelative("categoryTier"), GUIContent.none);
        valueR = new Rect (position.x + 125f, position.y + (stylesProp.arraySize+2) * lh1,    50f,   lh1 );
        EditorGUI.PropertyField(valueR, property.FindPropertyRelative("valueMultiplier"), GUIContent.none);

        if(property.FindPropertyRelative("valueMultiplier").intValue == (int)EValueMultiplier.Exp)
        {
            valueR = new Rect (position.x + 180f, position.y + (stylesProp.arraySize+2) * lh1,    50f,   lh1 );
            EditorGUI.PropertyField(valueR, property.FindPropertyRelative("unlockLevel"), GUIContent.none);
        }
        //Item prefab or image
        textR = new Rect(position.x, position.y + (stylesProp.arraySize+3) * lh1,    position.width,   lh1 );
        EditorGUI.LabelField( textR, "Item", EditorStyles.label );
        valueR = new Rect (position.x + 70f, position.y + (stylesProp.arraySize+3) * lh1,    position.width-70f,   lh1 );
        EditorGUI.PropertyField(valueR, property.FindPropertyRelative("item"), GUIContent.none);
        //Thumb
        valueR = new Rect (position.x + 180f, position.y,    50f,   50f );
        thumbnailProp.objectReferenceValue = EditorGUI.ObjectField(valueR,thumbnailProp.objectReferenceValue, typeof(Sprite),false);

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
//TODO Refreshuje style points itema ukoliko dozvolimo da se naknadno podesavaju CategoryTier i ValueMultiplier

[CustomEditor (typeof (DressUpItemPrefabScriptableObject))]
[CanEditMultipleObjects]
public class DressUpItemPrefabCustomInspector : Editor {


    public override void OnInspectorGUI () 
    {
        base.OnInspectorGUI();

        // Take out this if statement to set the value using setter when ever you change it in the inspector.
        // But then it gets called a couple of times when ever inspector updates
        // By having a button, you can control when the value goes through the setter and getter, your self.
        if (GUI.changed)
        {
            foreach (var item in targets)
            {
                if(item.GetType() == typeof(DressUpItemPrefabScriptableObject))
                {
                    DressUpItemPrefabScriptableObject getterSetter = (DressUpItemPrefabScriptableObject)item;
                    getterSetter.RefreshStylePoints();
                }

            }
        }
    }
}

[CustomEditor (typeof (DressUpItemSpriteScriptableObject))]
[CanEditMultipleObjects]
public class DressUpItemSpriteCustomInspector : Editor {


    public override void OnInspectorGUI () 
    {
        base.OnInspectorGUI();

        // Take out this if statement to set the value using setter when ever you change it in the inspector.
        // But then it gets called a couple of times when ever inspector updates
        // By having a button, you can control when the value goes through the setter and getter, your self.
        if (GUI.changed)
        {
            foreach (var item in targets)
            {
                if(item.GetType() == typeof(DressUpItemSpriteScriptableObject))
                {
                    DressUpItemSpriteScriptableObject getterSetter = (DressUpItemSpriteScriptableObject)item;
                    getterSetter.RefreshStylePoints();
                }

            }
        }
    }
}

[CustomEditor (typeof (CharacterDressUpData))]
[CanEditMultipleObjects]
public class CharacterDressUpDataCustomInspector : Editor {


    public override void OnInspectorGUI () 
    {
        base.OnInspectorGUI();

        // Take out this if statement to set the value using setter when ever you change it in the inspector.
        // But then it gets called a couple of times when ever inspector updates
        // By having a button, you can control when the value goes through the setter and getter, your self.
        if (GUI.changed)
        {
            foreach (var item in targets)
            {
                if(item.GetType() == typeof(CharacterDressUpData))
                {
                    CharacterDressUpData getterSetter = (CharacterDressUpData)item;
                    getterSetter.RefreshStylePoints();
                }

            }
        }
    }
}