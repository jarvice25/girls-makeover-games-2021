﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
/// <summary>
/// Editor skripta za reimenovanje dece svih selektovanih objekta - v1.0. 
/// </summary>
public class RenameChildren : EditorWindow {

    public string baseName = "";
    static Transform[] parents;
	bool addIndexAtEnd;
    static RenameChildren window;
	string newName;

    public static void ShowWindow()
    {
        window = (RenameChildren)EditorWindow.GetWindow(typeof(RenameChildren),false,"Rename Children",true);
        window.minSize = new Vector2(400, 200);
        window.titleContent = new GUIContent("Rename Children");
        window.Show();
    }

    [MenuItem ("GameObject/Rename Children",false,0)]   //Dodaje "Rename Children" opciju u padajucem meniju kada se klikne desni klik na GameObject u Hierarchy prozoru
    public static void NewMenuOptions()
    {
        parents = Selection.transforms;
        if (parents != null && parents.Length > 0)
        {
            ShowWindow();
        }
        else
        {
            EditorUtility.DisplayDialog("Error","This action requires at least one selected GameObject","Ok");
//            Debug.Log("No GameObject-s selected");
        }
    }

    void OnGUI()
    {
        EditorGUILayout.Space();
        addIndexAtEnd = EditorGUILayout.ToggleLeft(new GUIContent(" Append Index","Child index will be appended to the name"), addIndexAtEnd);

        EditorGUILayout.Space();
        baseName = EditorGUILayout.TextField(" Name: ",baseName);

        if (GUILayout.Button("Rename Children") || (Event.current!=null && Event.current.keyCode == KeyCode.Return))
        {
            if(parents != null && parents.Length > 0)
			{
                RenameAllChildren(parents);
            }
			else
			{
				Debug.Log("Double check failed");
			}
        }
    }

    void RenameAllChildren(Transform[] prnt)
    {
        for(int i=0;i<prnt.Length;i++)
        {
            foreach (Transform item in prnt[i])
            {
                if (baseName == "")
                    baseName = "GameObject";

                if(addIndexAtEnd)
                {
                    newName = baseName + "_" + item.transform.GetSiblingIndex();
                }
                else
                {
                    newName = baseName;
                }
                item.name = newName;
                EditorUtility.SetDirty(item.gameObject);
            }

            EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
        }

        window.Close();
    }

    void OnLostFocus()
    {
        window.Close();
    }

    void OnDestroy()
    {
        parents = null;
    }
}
