﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;

public class ParticleSystemOrderInLayer : EditorWindow {


    public Transform particleSystemHolder;
    public int layerOrder;
    ParticleSystemRenderer[] allParticleSystems;
    static ParticleSystemOrderInLayer window;

    [MenuItem ("Custom Tools/ParticleSystemOrderInLayer")]
    public static void ShowWindow()
    {
        window = (ParticleSystemOrderInLayer)EditorWindow.GetWindow(typeof(ParticleSystemOrderInLayer),false,"Particle System Order In Layer",true);
        window.minSize = new Vector2(100, 100);
        window.titleContent = new GUIContent("Particle System Order In Layer");
        window.Show();
    }

    void OnGUI()
    {
        EditorGUILayout.BeginVertical();
        particleSystemHolder = EditorGUILayout.ObjectField("Particle Systems Holder", particleSystemHolder, typeof(Transform), true) as Transform;
        EditorGUILayout.Space();
        layerOrder = EditorGUILayout.IntField("Order in layer ",layerOrder);
        EditorGUILayout.Space();

        if (GUILayout.Button("Set Layer Order"))
        {
            SetOrderInLayer();
        }
        EditorGUILayout.EndVertical();
    }

    void SetOrderInLayer()
    {
        allParticleSystems = particleSystemHolder.gameObject.GetComponentsInChildren<ParticleSystemRenderer>(true);

        foreach (ParticleSystemRenderer particle in allParticleSystems)
        {
            if(particle.transform.parent.name == "SelectedHolder" && particle.transform.parent.GetComponent<SelectedParticleHandler>() == null)
                particle.transform.parent.gameObject.AddComponent<SelectedParticleHandler>();
            particle.sortingOrder = layerOrder;
            EditorUtility.SetDirty(particle.gameObject);
        }

        EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
    }

}
