﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class FindFunctionOnButton : EditorWindow {
	static List<Button> buttonsFinal = new List<Button>();
	static Button[] buttons;
	static int currentButtonIndex;
	static string nameOfFunction="";
    [MenuItem("Custom Tools/Find Function On Button",false)]
	public static void ClearScene()
	{
		
		buttons = (Button[])Resources.FindObjectsOfTypeAll<Button>();
		foreach(Button btn in buttons)
		{
			int ec = btn.onClick.GetPersistentEventCount();
			for(int i=0; i<ec; i++)
			{
				if(btn.onClick.GetPersistentMethodName(i).Contains(nameOfFunction))
				{
					buttonsFinal.Add(btn);
				}
			}
		}
		FindFunctionOnButton window = (FindFunctionOnButton)EditorWindow.GetWindow(typeof(FindFunctionOnButton),false,"FindFunction",true);
		window.minSize = new Vector2(500, 1000);
		window.Show();
	}

	void OnEnable()
	{
		buttonsFinal.Clear();
		currentButtonIndex = 0;
		nameOfFunction = "";
	}

	private void OnGUI()
	{
		
		EditorGUILayout.BeginVertical();
		nameOfFunction = GUILayout.TextField(nameOfFunction);
		bool buttonFind = GUILayout.Button("Find", GUILayout.Width(140f));
		EditorGUILayout.EndVertical();
		EditorGUILayout.BeginHorizontal();
		bool buttonNext = GUILayout.Button("Next", GUILayout.Width(140f));
		bool buttonPrev = GUILayout.Button("Previous", GUILayout.Width(140f));
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.Space();
		EditorGUILayout.BeginVertical();
		EditorGUILayout.LabelField("Buttons founded: ");
		EditorGUILayout.EndVertical();
		if(buttonsFinal.Count>0)
		{
			for(int i=0;i<buttonsFinal.Count;i++)
			{

				if(GUILayout.Button(buttonsFinal[i].name, GUILayout.Width(140f)))
				{
					ButtonReferenceClicked(i);
				}
			}
		}

		if(buttonFind)
		{
			Find();
		}

		if(buttonNext)
		{
			NextButon();
		}

		if(buttonPrev)
		{
			PreviousButton();
		}
	}

	void ButtonReferenceClicked(int index)
	{
		currentButtonIndex = index;
		EditorGUIUtility.PingObject(buttonsFinal[currentButtonIndex].gameObject);
		Highlighter.Highlight("Hierarchy",buttonsFinal[currentButtonIndex].name);
	}

	void NextButon()
	{
		if(currentButtonIndex>=buttonsFinal.Count-1)
		{
			currentButtonIndex = 0;
		}
		else
		{
			currentButtonIndex++;
		}

		EditorGUIUtility.PingObject(buttonsFinal[currentButtonIndex].gameObject);
		Highlighter.Highlight("Hierarchy",buttonsFinal[currentButtonIndex].name);
	}

	void PreviousButton()
	{
		if(currentButtonIndex<=0)
		{
			currentButtonIndex = buttonsFinal.Count-1;
		}
		else
		{
			currentButtonIndex--;
		}

		EditorGUIUtility.PingObject(buttonsFinal[currentButtonIndex].gameObject);
		Highlighter.Highlight("Hierarchy",buttonsFinal[currentButtonIndex].name);
	}

	void Find()
	{
		buttonsFinal.Clear();
		foreach(Button btn in buttons)
		{
			int ec = btn.onClick.GetPersistentEventCount();
			for(int i=0; i<ec; i++)
			{
				if(Search(btn.onClick.GetPersistentMethodName(i)))
				{
					buttonsFinal.Add(btn);
				}
			}
		}
		currentButtonIndex = 0;
		if(buttonsFinal.Count>0)
		{
			EditorGUIUtility.PingObject(buttonsFinal[0].gameObject);
			Highlighter.Highlight("Hierarchy",buttonsFinal[0].name);
		}
		else
		{
			Highlighter.Stop();
		}

	}

	void OnDestroy()
	{
		Highlighter.Stop();
	}


	public bool Search(string firstString)
	{
		bool contains = firstString.IndexOf(nameOfFunction, StringComparison.OrdinalIgnoreCase) >= 0;
		return contains;
	}

}
