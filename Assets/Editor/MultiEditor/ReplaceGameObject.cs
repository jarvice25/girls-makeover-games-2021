﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

public class ReplaceGameObject : EditorWindow {

    public Object prefab;

    static GameObject[] selectedGameObjects;
    static ReplaceGameObject window;

    GameObject tmpGo;

    public static void ShowWindow()
    {
        window = (ReplaceGameObject)EditorWindow.GetWindow(typeof(ReplaceGameObject),false,"Replace Game Objects",true);
        window.minSize = new Vector2(400, 200);
        window.titleContent = new GUIContent("Replace Game Objects");
        window.Show();
    }

    [MenuItem ("GameObject/Replace Game Objects",false,0)]   //Dodaje "Rename Children" opciju u padajucem meniju kada se klikne desni klik na GameObject u Hierarchy prozoru
    public static void NewMenuOptions()
    {
        selectedGameObjects = Selection.gameObjects;
        if (selectedGameObjects != null && selectedGameObjects.Length > 0)
        {
            ShowWindow();
        }
        else
        {
            EditorUtility.DisplayDialog("Error","This action requires at least one selected GameObject","Ok");
            //            Debug.Log("No GameObject-s selected");
        }
    }

    void OnGUI()
    {
        EditorGUILayout.Space();

        prefab = EditorGUILayout.ObjectField("Prefab ",prefab,typeof(Object),false);

        EditorGUILayout.Space();

        if (GUILayout.Button("Replace Game Objects"))
        {
            if(selectedGameObjects != null && selectedGameObjects.Length > 0)
            {
                RenameAllChildren(selectedGameObjects);
            }
            else
            {
                Debug.Log("Double check failed");
            }
        }
    }

    void RenameAllChildren(GameObject[] prnt)
    {
        foreach (GameObject item in prnt)
        {
            tmpGo = (GameObject) PrefabUtility.InstantiatePrefab(prefab);
            tmpGo.transform.SetParent(item.transform.parent);
            tmpGo.transform.position = item.transform.position;
            tmpGo.transform.localScale = item.transform.localScale;
            tmpGo.name = item.name;
            
            EditorUtility.SetDirty(item.gameObject);
        }

        foreach (GameObject item in prnt)
        {
            DestroyImmediate(item);
        }
        
        EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());

        window.Close();
    }

    void OnDestroy()
    {
        selectedGameObjects = null;
    }
}
