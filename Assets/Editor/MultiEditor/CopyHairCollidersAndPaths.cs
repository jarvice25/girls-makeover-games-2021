﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;

public class CopyHairCollidersAndPaths : EditorWindow {
     
    public Transform sourceTransform;
    public int numberOfTargetTransforms;
    public Transform[] targetTransforms;
    static CopyHairCollidersAndPaths window;

    int oldNumberOfTargetTransforms = -1;

    [MenuItem ("Custom Tools/Copy hair colliders and paths")]
    public static void ShowWindow()
    {
        window = (CopyHairCollidersAndPaths)EditorWindow.GetWindow(typeof(CopyHairCollidersAndPaths),false,"Copy hair colliders and paths",true);
        window.minSize = new Vector2(100, 100);
        window.titleContent = new GUIContent("Copy hair colliders and paths");
        window.Show();
    }

    void OnGUI()
    {
        numberOfTargetTransforms = EditorGUILayout.IntField("Number Of targets",numberOfTargetTransforms);
        EditorGUILayout.Space();
        sourceTransform = (Transform) EditorGUILayout.ObjectField("Source", sourceTransform, typeof(Transform));
        EditorGUILayout.Space();

        if(numberOfTargetTransforms != oldNumberOfTargetTransforms)
        {
            targetTransforms = new Transform[numberOfTargetTransforms];
            oldNumberOfTargetTransforms = numberOfTargetTransforms;
        }

        using (new EditorGUILayout.VerticalScope ()) 
        {
            for(int i=0;i<numberOfTargetTransforms;i++)
            {
                targetTransforms[i] = (Transform) EditorGUILayout.ObjectField("Target_"+i.ToString(), targetTransforms[i], typeof(Transform));
                EditorGUILayout.Space();
            }
        }

        if (GUILayout.Button("Copy Edges and Poligons"))
        {
            PasteEdgePaths();
        }
    }

    void PasteEdgePaths()
    {
        foreach (Transform target in targetTransforms)
        {
            for (int i = 0; i < target.childCount; i++)
            {
                for (int j = 0; j < target.GetChild(i).childCount; j++)
                {
                    //Copy edges
                    if (target.GetChild(i).GetChild(j).GetComponent<EdgeCollider2D>() == null)
                        target.GetChild(i).GetChild(j).gameObject.AddComponent<EdgeCollider2D>();

                    EdgeCollider2D sourceEdge = sourceTransform.GetChild(i).GetChild(j).GetComponent<EdgeCollider2D>();
                    EdgeCollider2D targetEdge = target.GetChild(i).GetChild(j).GetComponent<EdgeCollider2D>();

                    targetEdge.points = sourceEdge.points;

                    //Copy polygons
                    if (target.GetChild(i).GetChild(j).GetComponent<PolygonCollider2D>() == null)
                        target.GetChild(i).GetChild(j).gameObject.AddComponent<PolygonCollider2D>();

                    PolygonCollider2D sourcePolygon = sourceTransform.transform.GetChild(i).GetChild(j).GetComponent<PolygonCollider2D>();
                    PolygonCollider2D targetPolygon = target.GetChild(i).GetChild(j).GetComponent<PolygonCollider2D>();

                    targetPolygon.points = sourcePolygon.points;
                    //Save
                    Debug.Log("done");
                    EditorUtility.SetDirty(target.GetChild(i).GetChild(j).gameObject);
                }
            }
        }
    }
}
