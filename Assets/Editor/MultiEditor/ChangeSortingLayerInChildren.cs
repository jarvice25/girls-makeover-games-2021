﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

public class ChangeSortingLayerInChildren : EditorWindow {

    public string sortingLayerName;

    static GameObject[] selectedGameObjects;
    static ChangeSortingLayerInChildren window;

    Canvas[] tmpCanvases;
    SpriteRenderer[] tmpSR;
    Anima2D.SpriteMeshInstance[] tmpSMI;

    public static void ShowWindow()
    {
        window = (ChangeSortingLayerInChildren)EditorWindow.GetWindow(typeof(ChangeSortingLayerInChildren),false,"Change Sorting Layer",true);
        window.minSize = new Vector2(400, 200);
        window.titleContent = new GUIContent("Change Sorting Layer");
        window.Show();
    }

    [MenuItem ("GameObject/Change Sorting Layer",false,0)]
    public static void NewMenuOptions()
    {

        selectedGameObjects = Selection.gameObjects;
        if (selectedGameObjects != null && selectedGameObjects.Length > 0)
        {
            ShowWindow();
        }
        else
        {
            EditorUtility.DisplayDialog("Error","This action requires at least one selected GameObject","Ok");
            //            Debug.Log("No GameObject-s selected");
        }
    }

    void OnGUI()
    {
        EditorGUILayout.Space();

        sortingLayerName = EditorGUILayout.TextField("Layer name ",sortingLayerName);

        EditorGUILayout.Space();

        if (GUILayout.Button("Change Sorting Layer"))
        {
            Debug.Log("ID trazenog layera je " + SortingLayer.NameToID(sortingLayerName));
            if(selectedGameObjects != null && selectedGameObjects.Length > 0)
            {
                ChangeSortingLayer(selectedGameObjects);
            }
            else
            {
                Debug.Log("Double check failed");
            }
        }
    }

    void ChangeSortingLayer(GameObject[] prnt)
    {
        Undo.RecordObjects(selectedGameObjects,"SelectedGOs");
        foreach (GameObject item in prnt)
        {
            Undo.RecordObject(item.gameObject,item.gameObject.name);

            tmpCanvases = item.GetComponentsInChildren<Canvas>(true);

            if (tmpCanvases != null || tmpCanvases.Length < 1)
            {
                foreach (Canvas canvas in tmpCanvases) 
                {
                    canvas.sortingLayerID = SortingLayer.NameToID(sortingLayerName);
                }
            }
            else
            {
                EditorUtility.DisplayDialog("Error","There are no 'Canvas' components in children of selected objecs","Ok");
            }

            tmpSR = item.GetComponentsInChildren<SpriteRenderer>(true);

            if (tmpCanvases != null || tmpCanvases.Length < 1)
            {
                foreach (SpriteRenderer sr in tmpSR) 
                {
                    sr.sortingLayerID =  SortingLayer.NameToID(sortingLayerName);
                }
            }
            else
            {
                EditorUtility.DisplayDialog("Error","There are no 'SpriteRenderer' components in children of selected objecs","Ok");
            }

            tmpSMI = item.GetComponentsInChildren<Anima2D.SpriteMeshInstance>(true);

            if (tmpCanvases != null || tmpCanvases.Length < 1)
            {
                foreach (Anima2D.SpriteMeshInstance smi in tmpSMI) 
                {
                    smi.sortingLayerID = SortingLayer.NameToID(sortingLayerName);
                }
            }
            else
            {
                EditorUtility.DisplayDialog("Error","There are no 'SpriteRenderer' components in children of selected objecs","Ok");
            }

            EditorUtility.SetDirty(item.gameObject);
        }

        foreach (var selected in selectedGameObjects) 
        {
            EditorUtility.SetDirty(selected);
        }

        EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        Debug.Log("Done");

        tmpCanvases = null;
        window.Close();
    }

    void OnDestroy()
    {
        selectedGameObjects = null;
    }
}
