﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class GetHierarchyPath : Editor {

    static Transform selectedItem;
    static List<string> pathElements = new List<string>();
    static Transform nextParent;
    static TextEditor path;
    static string tmpPath;

    [MenuItem ("GameObject/Get Hierarchy Path %&p",false,0)]
    public static void NewMenuOptions()
    {
        selectedItem = Selection.activeTransform;
        if (selectedItem != null)
        {
            CopyPathToClipbord();
        }
        else
        {
            EditorUtility.DisplayDialog("Error","This action requires one selected GameObject","Ok");
        }
    }

    static void CopyPathToClipbord()
    {
        path = new TextEditor();
        tmpPath = "";
        pathElements.Clear();
        nextParent = selectedItem;

        while (true)
        {
            pathElements.Add(nextParent.name);
            if (nextParent.parent != null)
                nextParent = nextParent.parent;
            else
                break;
        }

        for (int i = pathElements.Count-1; i >= 0; i--)
        {
            tmpPath += pathElements[i];
            tmpPath += "/";
        }

        tmpPath = tmpPath.Remove(tmpPath.Length-1);

        path.text = tmpPath;
        path.SelectAll();
        path.Copy();
        Debug.Log("Selected object's path is copied to clipboard: " + path.text);
    }
}