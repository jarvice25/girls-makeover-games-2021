﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

public class ConvertSpritesToImages : Editor {

    static GameObject[] selectedItems;

    [MenuItem ("GameObject/Convert Sprites to Images %&i",false,0)]
    public static void NewMenuOptions()
    {
        selectedItems = Selection.gameObjects;
        if (selectedItems != null)
        {
            Convert();
        }
        else
        {
            EditorUtility.DisplayDialog("Error","This action requires one selected GameObject","Ok");
        }
    }

    static void Convert()
    {
        Sprite tmpSprite;
        //TODO ovo kreira novu instancu materiajla. Treba da se kreira Sprites/Default materijal kao asset pa da se svim Image komponentama dodeli isti materijal
//        Material defaultMaterial = new Material(Shader.Find("Sprites/Default"));

        foreach (GameObject item in selectedItems)
        {
            if (item.GetComponent<Image>() == null)
                item.gameObject.AddComponent<Image>();

            if (item.GetComponent<SpriteRenderer>() != null)
            {
                tmpSprite = item.GetComponent<SpriteRenderer>().sprite;
                item.GetComponent<Image>().sprite = tmpSprite;
                DestroyImmediate(item.GetComponent<SpriteRenderer>());
            }
            
//            if(item.GetComponent<Image>().material.name == "Default UI Material")
//                item.GetComponent<Image>().material = defaultMaterial;
            
            //TODO img scale to 1, size to proper
            EditorUtility.SetDirty(item.gameObject);
//            Debug.Log("Item done: " + item);
        }

        EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
        AssetDatabase.SaveAssets();
    }
}
