﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Linq;
using UnityEngine.UI;

public class ContentCreator : EditorWindow {

    #region WindowVariables
    /// <summary> instanca EditorWindow-a </summary>
    static ContentCreator window;
    /// <summary> zavisno od stanja, GUI renda drugacije stanje:  </summary>
    int stateOfWindow = 1;
    /// <summary> bool flag koji cekiramo ako kategorija ima posebne teksture za thumbnails</summary>
    bool hasCustomThumbnails = false;
    /// <summary> bool flag koji cekiramo ako hocemo da koristimo SpriteMeshInstance umesto Sprite</summary>
    bool useSpriteMeshInstances = false;
    /// <summary> bool flag koji cekiramo ako hocemo da nam jedna tekstura bude jedan item. Ako je Fales, onda je jedan sprite - jedan item</summary>
    bool textureIsItem = false;
    #endregion //WindowVariables

    #region ItemVariables
    /// <summary> Liste sprite-ova u koje ucitavamo spriteove sa odabranih textura, kasnije se castuje u niz, koriscene su liste, radi lakseg baratanja kada se ucitavaju vise textura istovremeno </summary>
    static List<Sprite> listOfSpritesInAtlas=new List<Sprite>(),listOfSpritesInAtlasTemp=new List<Sprite>(); 
    /// <summary> konacan niz sprite-ova koje smo ucitali </summary>
    Sprite[] itemSpritesArray = new Sprite[0];
    /// <summary> konacan niz prefab-ova koje smo ucitali </summary>
    GameObject[] itemPrefabsArray = new GameObject[0];
    /// <summary> svakom itemu iz niza itema bice dodeljen niz stilova kojima taj item pripada </summary>
    public EItemStyles[][] itemStyleArrays;
    /// <summary> svakom itemu iz niza itema bice dodeljen broj poena koje taj item nosi </summary>
    public int[] stylePoints;
    /// <summary> Niz Thumbnail sprajtova za svaki item </summary>
    Sprite[] itemThumbnailsArray = new Sprite[0];
    #endregion //ItemVariables

    #region TexturesLoad
    /// <summary> bool koji pamti da li su ucitani sprite-ovi sa odabranih textura </summary>
    bool loaded = false;
    /// <summary> niz textura iz kojih ucitavamo spriteove kasnije </summary>
    Texture[] sourceTex; 
    /// <summary> niz textura iz kojih ucitavamo thumbnailove kasnije </summary>
    Texture[] thumbnailTex; 
    /// <summary> provera da li su sve texture dodate u niz </summary>
    bool inputOK = true;
    /// <summary> velicina niza textura iz kojih ucitavamo spriteove </summary>
    int numberOfTextures = 1;
    /// <summary> pomocna promenjiva koja sluzi da registruje izmene u duzini niza </summary>
    int oldNumberOfTextures = -1;
    /// <summary> velicina niza textura iz kojih ucitavamo thumbnailove </summary>
    int numberOfThumbnailTextures = 1;
    /// <summary> pomocna promenjiva koja sluzi da registruje izmene u duzini niza </summary>
    int oldNumberOfThumbnailTextures = -1;
    #endregion //TexturesLoad

    /// <summary> promenljiva za polje za unos putanje ukoliko ucitavamo content iz prefabs </summary>
    string contentPrefabsPath = "";
    /// <summary> niz bool-ova, za svaki item se pamti da li se item koristi(ubacuje u niz itema ili ne) </summary>
    bool[] itemChecked;
    /// <summary> za odabir putanje na kojoj ce biti sacuvan asset </summary>
    string assetSavePath = "";
    /// <summary> ime grupe objekata </summary>
    string contentGroupName = "ContentGroupData";
    /// <summary> vektor koji sadrzi poziciju za scroll gde listamo sve iteme </summary>
    Vector2 scrollPos = Vector2.zero;
    /// <summary>niz enum-a koji sadrzi podatak o tipu itema iz ItemState klase </summary>
    public ItemState.StateOfItemEnum[] itemStateArray;
    /// <summary>niz enum-a koji sadrzi podatak o osnovnoj vrednosti itema koja se kasnije mnozi sa faktorom u zavisnosti od dostupnosti itema </summary>
    public ECategoryTier[] itemTierArray;
    /// <summary>niz enum-a koji sadrzi podatak o osnovnoj vrednosti itema koja se kasnije mnozi sa faktorom u zavisnosti od dostupnosti itema </summary>
    public ECategoryTier categoryTier = new ECategoryTier();
    //  /// <summary> niz int-ova, za svaki item se pamti experience potreban da se otkljuca item, ukoliko se item na taj nacin otkljucava  </summary>
    public int[] expArray;
    /// <summary> prefab objekat koji reprezentuje Item, i koji se instancira u odabrani objekat - public referenca itemsHolder</summary>
    GameObject prefab;
    /// <summary> referenca ka objektu koji je holder za iteme </summary>
    Transform itemsHolder;
    /// <summary>  </summary>
    Transform scrollRectHolder;
    /// <summary> bool koji oznacava da li je skroll verticalan </summary>
    bool vertical;
    /// <summary> bool koji oznacava da li je skroll horizontalan </summary>
    bool  horizontal;
    /// <summary>
    /// inicijalni vektor za textures scroll
    /// </summary>
    Vector2 texturesScroll = Vector2.zero;
    /// <summary>
    /// Inicijalni vektor za thumbnails scroll.
    /// </summary>
    Vector2 thumbnailsScroll = Vector2.zero;

    [MenuItem ("Custom Tools/Content Creator")]
    public static void ShowWindow()
    {
        window = (ContentCreator)EditorWindow.GetWindow(typeof(ContentCreator),false,"Content Creator",true);
        window.minSize = new Vector2(800, 800);
        window.titleContent = new GUIContent("ItemsEditorSetup");
        window.Show();
    }

    void OnGUI()
    {
        switch(stateOfWindow)
        {
            case 1:
                LoadContentTextures();
                break;
            case 2:
                SaveAsset();
                break;
        }
    }

    #region WindowStates
    //Window 1
    void LoadContentTextures()
    {
        GUILayout.Space(50);
        

        useSpriteMeshInstances = EditorGUILayout.Toggle("Use SpriteMeshInstance",useSpriteMeshInstances);

        GUILayout.Space(10);

        textureIsItem = EditorGUILayout.Toggle(new GUIContent("Texture Is Item", "Ako je ovo polje stiklirano, svaka tekstura ce se tretirati kao jedan item bez obzira na broj sprajtova.\n\nAko nije stiklirano, svaki sprajt ce se tretirati kao poseban item."),textureIsItem);

        GUILayout.Space(10);

        var optionsTextures = new []{GUILayout.Width (64), GUILayout.Height (64)};

        numberOfTextures = EditorGUILayout.IntSlider("Number Of Item Textures",numberOfTextures,1,30);
        if(numberOfTextures != oldNumberOfTextures)
        {
            Array.Resize(ref sourceTex, numberOfTextures);
            oldNumberOfTextures = numberOfTextures;
        }

        GUILayout.Space(10);

        texturesScroll = EditorGUILayout.BeginScrollView(texturesScroll);
        using (new EditorGUILayout.HorizontalScope ()) 
        {
            GUILayout.Space(10);

            for(int i=0;i<numberOfTextures;i++)
            {
                sourceTex[i] = (Texture) EditorGUILayout.ObjectField("", sourceTex[i], typeof(Texture),false,optionsTextures);
            }

            for(int i=0;i<numberOfTextures;i++)
            {
                if(sourceTex[i]==null)
                {
                    inputOK = false;
                    break;
                }
                else
                {
                    inputOK = true;
                }
            }
        }
        EditorGUILayout.EndScrollView();

        GUILayout.Space(10);

        hasCustomThumbnails = EditorGUILayout.Toggle("Use Custom Thumbnails",hasCustomThumbnails);

        GUILayout.Space(10);

        if (hasCustomThumbnails)
        {
            numberOfThumbnailTextures = EditorGUILayout.IntSlider("Thumbnail Textures",numberOfThumbnailTextures,1,30);
            if(numberOfThumbnailTextures != oldNumberOfThumbnailTextures)
            {
                Array.Resize(ref thumbnailTex, numberOfThumbnailTextures);
                oldNumberOfThumbnailTextures = numberOfThumbnailTextures;
            }

            thumbnailsScroll = EditorGUILayout.BeginScrollView(thumbnailsScroll);
            using (new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Space(10);

                for(int i=0;i<numberOfThumbnailTextures;i++)
                {
                    thumbnailTex[i] = (Texture) EditorGUILayout.ObjectField("", thumbnailTex[i], typeof(Texture),false,optionsTextures);
                }
            }
            EditorGUILayout.EndScrollView();
            GUILayout.FlexibleSpace();
        }

        GUILayout.FlexibleSpace();

        if(inputOK)
        {
            GUI.color = Color.green;
        }
        else
        {
            GUI.color = Color.red;
        }

        GUILayout.Space(10);

        if (GUILayout.Button("Load Items", EditorStyles.toolbarButton)) 
        {
            if(inputOK)
            {
                LoadItemsFromTextures();
                Debug.Log("Load sprites");
            }
        }

        GUI.color = Color.white;

        if (GUILayout.Button("Back", EditorStyles.toolbarButton)) 
        {
//            CancelButton();
            Debug.Log("Back");
        }
    }
    //Window 2
    void SaveAsset()
    {
        if(!loaded)
        {
            return;
        }

        if(window != null)
            scrollPos = EditorGUILayout.BeginScrollView(scrollPos,false,true, GUILayout.Width(window.position.width), GUILayout.Height(window.position.height));

        //        int itemCount = Mathf.Max(listOfSpritesInAtlas.Count,listOfPrefabsInFolder.Count);

        for(int i=0;i<itemChecked.Length;i++) //FIX ogranici na 20-30 elementa po stranici
        {
            EditorGUILayout.BeginHorizontal("box");
            EditorGUILayout.BeginVertical();
            using (new FixedWidthLabel("Use Item"))
            {
                itemChecked[i] = EditorGUILayout.Toggle(itemChecked[i]);
            }
            EditorGUILayout.Space();
            using (new FixedWidthLabel("Index Of Item"))
            {
                EditorGUILayout.LabelField(i.ToString());
            }

            using (new FixedWidthLabel("Item Value Tier"))
            {
                itemTierArray[i] = (ECategoryTier) EditorGUILayout.EnumPopup(itemTierArray[i]);
            }

            using (new FixedWidthLabel("Locked Statys"))
            {
                itemStateArray[i] = (ItemState.StateOfItemEnum) EditorGUILayout.EnumPopup(itemStateArray[i]);
            }

            if(itemStateArray[i] == ItemState.StateOfItemEnum.Experience)
            {
                expArray[i] = (int) EditorGUILayout.IntField(expArray[i]);
            }
            //TODO ostale opcije za ItemState

            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical();
            using (new FixedWidthLabel("ItemStyle"))
            {
                EditorGUILayout.BeginVertical();
                if ( GUILayout.Button( "-" ) ) 
                {
                    if(itemStyleArrays[i].Length > 1)
                        Array.Resize(ref itemStyleArrays[i],itemStyleArrays[i].Length-1);
                }
                if ( GUILayout.Button( "+" ) ) 
                {
                    if(itemStyleArrays[i].Length < 4)
                        Array.Resize(ref itemStyleArrays[i],itemStyleArrays[i].Length+1);
                }
                EditorGUILayout.EndVertical();

                EditorGUILayout.BeginVertical();
                for (int j = 0; j < itemStyleArrays[i].Length; j++) 
                {
                    itemStyleArrays[i][j] = (EItemStyles) EditorGUILayout.EnumPopup(itemStyleArrays[i][j]);
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndVertical();
            EditorGUILayout.BeginVertical();
            EditorGUI.BeginDisabledGroup(true);
            if (itemSpritesArray.Length > 0)
                itemSpritesArray[i] = (Sprite)EditorGUILayout.ObjectField("Item Preview", itemSpritesArray[i], typeof(Sprite), false);
            else
            {
                //                Debug.Log("preview: " + i);
                EditorGUILayout.ObjectField("Item Preview",itemPrefabsArray[i].GetComponentInChildren<Anima2D.SpriteMeshInstance>().spriteMesh.sprite.texture, typeof(Texture),false);    //TODO preview
            }
            EditorGUI.EndDisabledGroup();

            itemThumbnailsArray[i] = (Sprite) EditorGUILayout.ObjectField("Thumbnail",itemThumbnailsArray[i], typeof(Sprite),false);    //TODO preview

            EditorGUILayout.EndVertical();
            EditorGUILayout.BeginVertical();
            GUILayout.Space(20);
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Separator();
        }

        GUILayout.Space(20);

        GUI.color = Color.white;

        EditorGUILayout.BeginHorizontal();//FIXME
        EditorGUILayout.BeginVertical(GUILayout.Width(400));//FIXME

        contentGroupName = EditorGUILayout.TextField(new GUIContent("Name: ", "Enter the name of the asset"),contentGroupName,EditorStyles.textField);

        GUILayout.Space(10);

        if (GUILayout.Button("Select Save folder", EditorStyles.toolbarButton)) {
            assetSavePath = EditorUtility.OpenFolderPanel("Select asset save folder","","");

            if (!assetSavePath.Contains("Assets"))
            {
                if (EditorUtility.DisplayDialog("Greska", "Odabrali ste pogresnu putanju. Folder za cuvanje itema treba da se nalazi u okviru Assets foldera!", "Ok"))
                {
                }
                return;
            }

            assetSavePath = assetSavePath.Substring(assetSavePath.IndexOf("Assets"));
        }

        EditorGUILayout.LabelField(new GUIContent("Asset will be saved at: "+assetSavePath,assetSavePath),EditorStyles.label);

        if (GUILayout.Button("Create Asset", EditorStyles.toolbarButton)) {
//            CreateAsset();
            Debug.Log("Asset saved");
        }

        EditorGUILayout.EndVertical();//FIXME
        GUILayout.Space(10);
        EditorGUILayout.BeginVertical();//FIXME

        prefab = (GameObject) EditorGUILayout.ObjectField("ItemPrefab ",prefab, typeof(GameObject));
        itemsHolder = (Transform) EditorGUILayout.ObjectField("ItemsHolder ", itemsHolder, typeof(Transform));
        scrollRectHolder = (Transform) EditorGUILayout.ObjectField("ScroolRectHolder ",scrollRectHolder, typeof(Transform));
        EditorGUILayout.Separator();
        EditorGUILayout.BeginHorizontal("box");
        EditorGUILayout.LabelField("Choose orientation:");

        using (new FixedWidthLabel("Horizontal"))
        {
            if(horizontal = EditorGUILayout.Toggle(horizontal))
            {
                vertical = false;
            }
            else
            {
                vertical = true;
            }
        }
        using (new FixedWidthLabel("Vertical"))
        {
            if(vertical = EditorGUILayout.Toggle(vertical))
            {
                horizontal = false;
            }
            else
            {
                horizontal = true;
            }
        }

        EditorGUILayout.EndHorizontal();

        //        if(inputOK)
        //        {
        //            GUI.color = Color.green;
        //        }
        //        else
        //        {
        //            GUI.color = Color.red;
        //        }

        if (GUILayout.Button("Create Buttons", EditorStyles.toolbarButton)) {
//            CreateButtons();
            Debug.Log("Buttons created");
        }
        EditorGUILayout.EndVertical();//FIXME
        EditorGUILayout.EndHorizontal();//FIXME

        //        GUI.color = Color.white;
        GUILayout.Space(10);

        if (GUILayout.Button("Cancel", EditorStyles.toolbarButton)) 
        {
//            CancelButton();
            Debug.Log("Cancel");
        }

        GUILayout.Space(30);
        EditorGUILayout.EndScrollView();
    }
    #endregion //WindowStates
    #region ExexuteMethods
    void LoadItemsFromTextures()
    {
        //Load Item Sprites from Item Textures
        listOfSpritesInAtlas.Clear();
        for (int i = 0; i < numberOfTextures; i++)
        {
            listOfSpritesInAtlasTemp.Clear();
            listOfSpritesInAtlasTemp = (AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(sourceTex[i]))).OfType<Sprite>().ToList();
            listOfSpritesInAtlasTemp.Sort(new SpriteNumericComparer());
            listOfSpritesInAtlas.AddRange(listOfSpritesInAtlasTemp);
        }
        itemSpritesArray = listOfSpritesInAtlas.ToArray();
        //Initialize ItemChecked array
        itemChecked = new bool[itemSpritesArray.Length];
        for(int i = 0; i<itemChecked.Length;i++)
        {
            itemChecked[i]= true;
        }
        //Initialize ItemStyleArrays
        itemStyleArrays = new EItemStyles[itemSpritesArray.Length][];
        for (int i = 0; i < itemSpritesArray.Length; i++)
        {
            itemStyleArrays[i] = new EItemStyles[1];
        }
        //Load Item Thumbnail Sprites from Thumbnail Textures
        if (hasCustomThumbnails)
        {
            listOfSpritesInAtlas.Clear();
            for (int i = 0; i < numberOfThumbnailTextures; i++)
            {
                listOfSpritesInAtlasTemp.Clear();
                listOfSpritesInAtlasTemp = (AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(thumbnailTex[i]))).OfType<Sprite>().ToList();
                listOfSpritesInAtlasTemp.Sort(new SpriteNumericComparer());
                listOfSpritesInAtlas.AddRange(listOfSpritesInAtlasTemp);
            }
            itemThumbnailsArray = listOfSpritesInAtlas.ToArray();

            if (itemThumbnailsArray.Length != itemSpritesArray.Length)
                Array.Resize(ref itemThumbnailsArray, itemSpritesArray.Length);
        }
        else
        {
            itemThumbnailsArray = itemSpritesArray;
        }
        //Initialize ItemStateArray
        itemStateArray = new ItemState.StateOfItemEnum[itemSpritesArray.Length];
        expArray = new int[itemSpritesArray.Length];
        //Initialize ItemValueArray
        itemTierArray = new ECategoryTier[itemSpritesArray.Length];
        for (int i = 0; i < itemTierArray.Length; i++)
        {
            itemTierArray[i] = categoryTier;
        }

        Debug.Log("UKUPNO IH IMA "+itemSpritesArray.Length);
        loaded = true;
        stateOfWindow = 2;
    }
    #endregion //ExecuteMethods
}
