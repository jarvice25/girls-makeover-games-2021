﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MakePants {

//	[MenuItem("DressUpAnima2D/Pants")]
	public static void CreateMyAsset()
	{
		Pants asset = ScriptableObject.CreateInstance<Pants>();

		AssetDatabase.CreateAsset(asset, "Assets/Pants.asset");
		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();

		Selection.activeObject = asset;
	}
}
