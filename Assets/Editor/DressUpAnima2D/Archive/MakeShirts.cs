﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MakeShirts {

//	[MenuItem("DressUpAnima2D/Shirts")]
	public static void CreateMyAsset()
	{
		Shirts asset = ScriptableObject.CreateInstance<Shirts>();

		AssetDatabase.CreateAsset(asset, "Assets/Shirts.asset");
		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();

		Selection.activeObject = asset;
	}
}
