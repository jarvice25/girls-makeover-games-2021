﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MakeSkirts {

//	[MenuItem("DressUpAnima2D/Skirts")]
	public static void CreateMyAsset()
	{
		Skirts asset = ScriptableObject.CreateInstance<Skirts>();

		AssetDatabase.CreateAsset(asset, "Assets/Skirts.asset");
		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();

		Selection.activeObject = asset;
	}
}
