﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MakeDress {

//	[MenuItem("DressUpAnima2D/Dress")]
	public static void CreateMyAsset()
	{
		Dress asset = ScriptableObject.CreateInstance<Dress>();

		AssetDatabase.CreateAsset(asset, "Assets/Dress.asset");
		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();

		Selection.activeObject = asset;
	}
}
