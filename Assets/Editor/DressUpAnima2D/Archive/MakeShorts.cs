﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MakeShorts {

//	[MenuItem("DressUpAnima2D/Shorts")]
	public static void CreateMyAsset()
	{
		Shorts asset = ScriptableObject.CreateInstance<Shorts>();

		AssetDatabase.CreateAsset(asset, "Assets/Shorts.asset");
		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();

		Selection.activeObject = asset;
	}
}
