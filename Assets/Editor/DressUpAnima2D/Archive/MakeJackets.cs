﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MakeJackets {

//	[MenuItem("DressUpAnima2D/Jackets")]
	public static void CreateMyAsset()
	{
		Jackets asset = ScriptableObject.CreateInstance<Jackets>();

		AssetDatabase.CreateAsset(asset, "Assets/Jackets.asset");
		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();

		Selection.activeObject = asset;
	}
}
