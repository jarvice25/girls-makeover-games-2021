﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MakePantyHoes {

//	[MenuItem("DressUpAnima2D/PantyHoes")]
	public static void CreateMyAsset()
	{
		PantyHoes asset = ScriptableObject.CreateInstance<PantyHoes>();

		AssetDatabase.CreateAsset(asset, "Assets/PantyHoes.asset");
		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();

		Selection.activeObject = asset;
	}
}
