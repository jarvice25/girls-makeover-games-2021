﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MakeBlouses {

//	[MenuItem("DressUpAnima2D/Blouses")]
	public static void CreateMyAsset()
	{
		Blouses asset = ScriptableObject.CreateInstance<Blouses>();

		AssetDatabase.CreateAsset(asset, "Assets/Blouses.asset");
		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();

		Selection.activeObject = asset;
	}
}
