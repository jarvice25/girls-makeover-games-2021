﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MakeSwimmingSuit {

//	[MenuItem("DressUpAnima2D/SwimmingSuit")]
	public static void CreateMyAsset()
	{
        SwimmingSuit asset = ScriptableObject.CreateInstance<SwimmingSuit>();

		AssetDatabase.CreateAsset(asset, "Assets/SwimmingSuit.asset");
		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();

		Selection.activeObject = asset;
	}
}
