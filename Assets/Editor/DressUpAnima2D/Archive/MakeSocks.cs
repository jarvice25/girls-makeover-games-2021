﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MakeSocks {

//	[MenuItem("DressUpAnima2D/Socks")]
	public static void CreateMyAsset()
	{
		Socks asset = ScriptableObject.CreateInstance<Socks>();

		AssetDatabase.CreateAsset(asset, "Assets/Socks.asset");
		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();

		Selection.activeObject = asset;
	}
}
