﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MakeJeans {

//	[MenuItem("DressUpAnima2D/Jeans")]
	public static void CreateMyAsset()
	{
		Jeans asset = ScriptableObject.CreateInstance<Jeans>();

		AssetDatabase.CreateAsset(asset, "Assets/Jeans.asset");
		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();

		Selection.activeObject = asset;
	}
}
