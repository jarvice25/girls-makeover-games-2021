﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Linq;

public class CreateContentGroup : EditorWindow {
    
    #region General
    /// <summary> za odabir putanje na kojoj ce biti sacuvan asset </summary>
    string assetSavePath = "";
    /// <summary> ime grupe objekata </summary>
    string contentGroupName = "ContentGroupData";
    /// <summary> zavisno od stanja, GUI renda drugacije stanje: 1 - podesavanje imena i izbor nacina ucitavanja, 2 - ucitavanje iz teksture, 3 - ucitavanje prefabs sa putanje,4 - edit i save </summary>
    int stateOfWindow = 1;
    /// <summary> instanca EditorWindow-a </summary>
    static CreateContentGroup window;
    /// <summary> vektor koji sadrzi poziciju za scroll gde listamo sve iteme </summary>
    Vector2 scrollPos = Vector2.zero;
    /// <summary> Liste sprite-ova u koje ucitavamo spriteove sa odabranih textura, kasnije se castuje u niz, koriscene su liste, radi lakseg baratanja kada se ucitavaju vise textura istovremeno </summary>
    static List<Sprite> listOfSpritesInAtlas=new List<Sprite>(),listOfSpritesInAtlasTemp=new List<Sprite>(); 
    static List<GameObject> listOfPrefabsInFolder=new List<GameObject>(),listOfPrefabsInFolderTemp=new List<GameObject>(); 
    /// <summary> konacan niz sprite-ova koje smo ucitali </summary>
    Sprite[] itemSpritesArray = new Sprite[0];
    GameObject[] itemPrefabsArray = new GameObject[0];
    /// <summary> bool koji pamti da li su ucitani sprite-ovi sa odabranih textura </summary>
    bool loaded = false;
    /// <summary> niz textura iz kojih ucitavamo spriteove kasnije </summary>
    Texture[] sourceTex; 
    /// <summary> provera da li su sve texture dodate u niz </summary>
    bool inputOK = true;
    /// <summary> velicina niza textura iz kojih ucitavamo spriteove </summary>
    int numberOfTextures = 1;
    /// <summary> pomocna promenjiva koja sluzi da registruje izmene u duzini niza </summary>
    int oldNumberOfTextures = -1;
    /// <summary> promenljiva za polje za unos putanje ukoliko ucitavamo content iz prefabs </summary>
    string contentPrefabsPath = "";
    /// <summary> niz bool-ova, za svaki item se pamti da li se item koristi(ubacuje u niz itema ili ne) </summary>
    bool[] itemChecked;

    public EItemStyles[][] itemStyleArrays;
    #endregion
    
//    [MenuItem ("DressUpAnima2D/CreateContentGroup")]
    public static void ShowWindow()
    {
        window = (CreateContentGroup)EditorWindow.GetWindow(typeof(CreateContentGroup),false,"CreateContentGroup",true);
        window.minSize = new Vector2(800, 800);
        window.titleContent = new GUIContent("CreateContentGroup");
        window.Show();
    }
    
    void OnGUI()
    {
        

        switch(stateOfWindow)
        {
            case 1:
                ChooseInputType();
                break;
            case 2:
                CreateContentGroupFromTextures();
                break;
            case 3:
                CreateContentGroupFromPrefabs();
                break;
            case 4:
                SaveAsset();
                break;
        }
    }
    
    #region WindowStates
    //Window 1
    void ChooseInputType()
    {
        GUILayout.Space(30);

//        if (GUILayout.Button("Select Save folder", EditorStyles.toolbarButton)) {
//            assetSavePath = EditorUtility.OpenFolderPanel("Select asset save folder","","");
//
//            if (!assetSavePath.Contains("Assets"))
//            {
//                if (EditorUtility.DisplayDialog("Greska", "Odabrali ste pogresnu putanju. Folder za cuvanje itema treba da se nalazi u okviru Assets foldera!", "Ok"))
//                {
//                }
//
//                EditorUtility.FocusProjectWindow();
//                return;
//            }
//
//            assetSavePath = assetSavePath.Substring(assetSavePath.IndexOf("Assets"));
//        }
//
//        EditorGUILayout.LabelField(new GUIContent("Asset will be saved at: "+assetSavePath,assetSavePath),EditorStyles.label);

//        contentGroupName = EditorGUILayout.TextField(new GUIContent("Name: ", "Enter the name of the asset"),contentGroupName,EditorStyles.textField);

        EditorGUILayout.LabelField("Create content group from: ");
        
        if (GUILayout.Button("Textures", EditorStyles.toolbarButton)) 
        {
            stateOfWindow = 2;
        }
        
        if (GUILayout.Button("Prefabs", EditorStyles.toolbarButton)) 
        {
            stateOfWindow = 3;
        }
    }
    //Window 2
    void CreateContentGroupFromTextures()
    {
        GUILayout.Space(50);

        var optionsTextures = new []{GUILayout.Width (64), GUILayout.Height (64)};
        numberOfTextures = EditorGUILayout.IntSlider("Number Of textures",numberOfTextures,1,10);
        if(numberOfTextures != oldNumberOfTextures)
        {
            sourceTex = new Texture[numberOfTextures];
            oldNumberOfTextures = numberOfTextures;
        }
        
        using (new EditorGUILayout.HorizontalScope ()) 
        {
            
            for(int i=0;i<numberOfTextures;i++)
            {
                sourceTex[i] = (Texture) EditorGUILayout.ObjectField("", sourceTex[i], typeof(Texture),false,optionsTextures);
                EditorGUILayout.Space();
            }
            
            for(int i=0;i<numberOfTextures;i++)
            {
                if(sourceTex[i]==null)
                {
                    inputOK = false;
                }
                else
                {
                    inputOK = true;
                }
            }
        }
        
        if(inputOK)
        {
            GUI.color = Color.green;
        }
        else
        {
            GUI.color = Color.red;
        }

        GUILayout.Space(30);

        if (GUILayout.Button("Load Items", EditorStyles.toolbarButton)) {
            if(inputOK)
            {
                LoadItemsFromTexture();
                Debug.Log("Load sprites");
            }
        }
        
        GUI.color = Color.white;
        
        if (GUILayout.Button("Back", EditorStyles.toolbarButton)) {
            stateOfWindow = 1;
            Debug.Log("Back");
        }
    }
    //Window 3
    void CreateContentGroupFromPrefabs()
    {
        GUILayout.Space(50);

        if (GUILayout.Button("Load Items", EditorStyles.toolbarButton)) 
        {
            LoadItemsFromPrefabs();
            Debug.Log("Load prefabs");
        }
        
        if (GUILayout.Button("Back", EditorStyles.toolbarButton)) 
        {
            stateOfWindow = 1;
            Debug.Log("Back");
        }
    }
    //Window 4
    void SaveAsset()
    {
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos,false,true, GUILayout.Width(window.position.width), GUILayout.Height(window.position.height));

//        int itemCount = Mathf.Max(listOfSpritesInAtlas.Count,listOfPrefabsInFolder.Count);

        for(int i=0;i<itemChecked.Length;i++) //FIX ogranici na 20-30 elementa po stranici
        {
            EditorGUILayout.BeginHorizontal("box");
            EditorGUILayout.BeginVertical();
            using (new FixedWidthLabel("Use Item"))
            {
                itemChecked[i] = EditorGUILayout.Toggle(itemChecked[i]);
            }
            EditorGUILayout.Space();
            using (new FixedWidthLabel("indexOfItem"))
            {
                EditorGUILayout.LabelField(i.ToString());
            }

            EditorGUILayout.EndVertical();
            EditorGUILayout.BeginVertical();
            using (new FixedWidthLabel("ItemStyle"))
            {
                EditorGUILayout.BeginVertical();
                if ( GUILayout.Button( "-" ) ) 
                {
                    if(itemStyleArrays[i].Length > 1)
                        Array.Resize(ref itemStyleArrays[i],itemStyleArrays[i].Length-1);
                }
                if ( GUILayout.Button( "+" ) ) 
                {
                    if(itemStyleArrays[i].Length < 4)
                        Array.Resize(ref itemStyleArrays[i],itemStyleArrays[i].Length+1);
                }
                EditorGUILayout.EndVertical();

                EditorGUILayout.BeginVertical();
                for (int j = 0; j < itemStyleArrays[i].Length; j++) 
                {
//                    EditorGUILayout.Space();
                    itemStyleArrays[i][j] = (EItemStyles) EditorGUILayout.EnumPopup(itemStyleArrays[i][j]);
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndVertical();
            EditorGUILayout.BeginVertical();
            if (itemSpritesArray.Length > 0)
                itemSpritesArray[i] = (Sprite)EditorGUILayout.ObjectField("Image", itemSpritesArray[i], typeof(Sprite), false);
            else
            {
                EditorGUI.BeginDisabledGroup(true);
                EditorGUILayout.ObjectField("Preview Image",itemPrefabsArray[i].GetComponentInChildren<Anima2D.SpriteMeshInstance>().spriteMesh.sprite.texture, typeof(Texture),false);    //TODO preview
                EditorGUI.EndDisabledGroup();
            }
            EditorGUILayout.EndVertical();
            EditorGUILayout.BeginVertical();
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Separator();
        }

        GUILayout.Space(20);

        //TODO TODO TODO TODO TODO TODO TODO TODO //


        GUI.color = Color.white;

        contentGroupName = EditorGUILayout.TextField(new GUIContent("Name: ", "Enter the name of the asset"),contentGroupName,EditorStyles.textField);

        GUILayout.Space(10);

        if (GUILayout.Button("Select Save folder", EditorStyles.toolbarButton)) {
            assetSavePath = EditorUtility.OpenFolderPanel("Select asset save folder","","");

            if (!assetSavePath.Contains("Assets"))
            {
                if (EditorUtility.DisplayDialog("Greska", "Odabrali ste pogresnu putanju. Folder za cuvanje itema treba da se nalazi u okviru Assets foldera!", "Ok"))
                {
                }

                EditorUtility.FocusProjectWindow();
                return;
            }

            assetSavePath = assetSavePath.Substring(assetSavePath.IndexOf("Assets"));
        }

        EditorGUILayout.LabelField(new GUIContent("Asset will be saved at: "+assetSavePath,assetSavePath),EditorStyles.label);
        
        if (GUILayout.Button("Save", EditorStyles.toolbarButton)) {
            SaveButton();
            Debug.Log("Save");
        }
        
        if (GUILayout.Button("Cancel", EditorStyles.toolbarButton)) {
            CancelButton();
            stateOfWindow = 1;
            Debug.Log("Cancel");
        }
        EditorGUILayout.EndScrollView();
    }
    #endregion // WindowStates
    
    #region ExexuteMethods
    void LoadItemsFromTexture()
    {
        for(int i=0;i<numberOfTextures;i++)
        {
            listOfSpritesInAtlasTemp.Clear();
            listOfSpritesInAtlasTemp = AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(sourceTex[i])).OfType<Sprite>().ToList();
            listOfSpritesInAtlas.AddRange(listOfSpritesInAtlasTemp);
        }
        itemSpritesArray = listOfSpritesInAtlas.ToArray();
        itemChecked = new bool[itemSpritesArray.Length];
        for(int i = 0; i<itemChecked.Length;i++)
        {
            itemChecked[i]= true;
        }
        
        itemSpritesArray = listOfSpritesInAtlas.ToArray();

        itemStyleArrays = new EItemStyles[itemSpritesArray.Length][];
        for (int i = 0; i < itemSpritesArray.Length; i++)
        {
            itemStyleArrays[i] = new EItemStyles[1];
        }

        Debug.Log("UKUPNO IH IMA "+itemSpritesArray.Length);
      
        stateOfWindow = 4;
    }
    
    void LoadItemsFromPrefabs()
    {
        Debug.Log("Uso u LoadItemsFromPrefabs");
        contentPrefabsPath = EditorUtility.OpenFolderPanel("Load prefabs from folder","","");

        if (!contentPrefabsPath.Contains("Prefabs/ContentDataPrefabs"))
        {
            if (EditorUtility.DisplayDialog("Greska", "Odabrali ste pogresnu putanju. Folder sa aitemima treba da se nalazi u okviru Resources/Prefabs/ContentDataPrefabs foldera!", "Ok"))
            {
            }
            EditorUtility.FocusProjectWindow();
            return;
        }

        contentPrefabsPath = contentPrefabsPath.Substring(contentPrefabsPath.IndexOf("Prefabs/ContentDataPrefabs"));
        Debug.Log(contentPrefabsPath);

        listOfPrefabsInFolderTemp.Clear();
        listOfPrefabsInFolderTemp = Resources.LoadAll(contentPrefabsPath).OfType<GameObject>().ToList();
        Debug.Log(listOfPrefabsInFolderTemp.Count);
        listOfPrefabsInFolder.AddRange(listOfPrefabsInFolderTemp);

        itemPrefabsArray = listOfPrefabsInFolderTemp.ToArray();
        itemChecked = new bool[itemPrefabsArray.Length];
        for(int i = 0; i<itemChecked.Length;i++)
        {
            itemChecked[i]= true;
        }

        itemPrefabsArray = listOfPrefabsInFolderTemp.ToArray();

        itemStyleArrays = new EItemStyles[itemPrefabsArray.Length][];
        for (int i = 0; i < itemPrefabsArray.Length; i++)
        {
            itemStyleArrays[i] = new EItemStyles[1];
        }

        Debug.Log("UKUPNO IH IMA "+itemPrefabsArray.Length);
        stateOfWindow = 4;
    }
    
    void CancelButton()
    {
        contentPrefabsPath = "";
        assetSavePath = "";
        numberOfTextures = 1;
        sourceTex = new Texture[1];
        listOfSpritesInAtlasTemp.Clear();
        listOfSpritesInAtlas.Clear();
        listOfPrefabsInFolderTemp.Clear();
        listOfPrefabsInFolder.Clear();
        itemSpritesArray = new Sprite[0];
        itemPrefabsArray = new GameObject[0];
        itemStyleArrays = new EItemStyles[0][];
        itemChecked = new bool[0];
        inputOK = false;
    }

    void SaveButton()
    {

        if (itemSpritesArray.Length > 0)
        {
            if (assetSavePath == "")
            {
                assetSavePath = EditorUtility.OpenFolderPanel("Select asset save folder","","");
                assetSavePath = assetSavePath.Substring(assetSavePath.IndexOf("Assets"));
            }
            DressUpItemSpriteScriptableObject asset = ScriptableObject.CreateInstance<DressUpItemSpriteScriptableObject>();
            asset.elements = new List<DressUpItemSprite>();
            AssetDatabase.CreateAsset(asset, assetSavePath + "/" + contentGroupName + ".asset");

            foreach (Sprite item in itemSpritesArray)
            {
                int i = System.Array.IndexOf(itemSpritesArray, item);
                if (itemChecked[i])
                {
                    DressUpItemSprite tmp = new DressUpItemSprite();
                    tmp.item = item;
                    tmp.styles = itemStyleArrays[i].ToList();
                    asset.elements.Add(tmp);
                }
            }

            AssetDatabase.SaveAssets();
            Selection.activeObject = asset;

            EditorUtility.SetDirty(asset);
        }
        else if (itemPrefabsArray.Length > 0)
        {
            if (assetSavePath == "")
            {
                assetSavePath = EditorUtility.OpenFolderPanel("Select asset save folder","","");
                assetSavePath = assetSavePath.Substring(assetSavePath.IndexOf("Assets"));
            }
            DressUpItemPrefabScriptableObject asset = ScriptableObject.CreateInstance<DressUpItemPrefabScriptableObject>();
            asset.elements = new List<DressUpItemPrefab>();
            AssetDatabase.CreateAsset(asset, assetSavePath + "/" + contentGroupName + ".asset");

            foreach (GameObject item in itemPrefabsArray)
            {
                int i = System.Array.IndexOf(itemPrefabsArray, item);
                if (itemChecked[i])
                {
                    DressUpItemPrefab tmp = new DressUpItemPrefab();
                    tmp.item = item;
                    tmp.styles = itemStyleArrays[i].ToList();
                    asset.elements.Add(tmp);
                }
            }       

            AssetDatabase.SaveAssets();
            Selection.activeObject = asset;

            EditorUtility.SetDirty(asset);
        }
        else
        {
            if(EditorUtility.DisplayDialog("Greska","Niste odabrali ni jedan itemali i da !", "Ok"))
            {
                EditorUtility.FocusProjectWindow();
                return;
            }
        }
        
        EditorUtility.FocusProjectWindow();
        CancelButton();
        this.Close();
    }
    #endregion // ExecuteMethods
}
