﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.Linq;
using System;

public class ItemsEditor : EditorWindow {

	#region General
	/// <summary> zavisno od stanja, GUI renda drugacije stanje: 1 - odabir i ucitavanje textura, 2 - inicijalizacija itema, 3 - inicijalizacija objekata </summary>
	int stateOfWindow = 1;
	/// <summary> prefab objekat koji reprezentuje Item, i koji se instancira u odabrani objekat - public referenca itemsHolder</summary>
	GameObject prefab;
	/// <summary> referenca ka objektu koji je holder za iteme </summary>
	Transform itemsHolder;
	/// <summary> bool koji pamti da li su ucitani sprite-ovi sa odabranih textura </summary>
	bool loaded = false;
	/// <summary> niz textura iz kojih ucitavamo spriteove kasnije </summary>
	Texture[] sourceTex; 
	/// <summary> provera da li su sve texture dodate u niz </summary>
	bool texturesOK = true;
	/// <summary> velicina niza textura iz kojih ucitavamo spriteove </summary>
	int numberOfTextures = 1;
	/// <summary> pomocna promenjiva koja sluzi da registruje izmene u duzini niza </summary>
	int oldNumberOfTextures = -1;
	/// <summary>  </summary>
	Transform scrollRectHolder;
	/// <summary> instanca EditorWindow-a </summary>
	static ItemsEditor window;
	/// <summary> vektor koji sadrzi poziciju za scroll gde listamo sve iteme </summary>
	Vector2 scrollPos = Vector2.zero;
	/// <summary> Liste sprite-ova u koje ucitavamo spriteove sa odabranih textura, kasnije se castuje u niz, koriscene su liste, radi lakseg baratanja kada se ucitavaju vise textura istovremeno </summary>
	static List<Sprite> listOfSpritesInAtlas=new List<Sprite>(),listOfSpritesInAtlasTemp=new List<Sprite>(); 
	/// <summary> konacan niz sprite-ova koje smo ucitali </summary>
	Sprite[] spriteArray; 
	/// <summary> niz int-ova, za svaki item se pamti experience potreban da se otkljuca item, ukoliko se item na taj nacin otkljucava  </summary>
	int[] expArray;
	/// <summary> niz int-ova, za svaki item se pamti cena potrebna da se otkljuca item, ukoliko se item na taj nacin otkljucava </summary>
	int[] costArray;
	/// <summary> niz string-ova, za svaki item se pamti ID inapp-a potreban da se otkljuca item, ukoliko se item na taj nacin otkljucava </summary>
	string[] inAppIdArray;
	/// <summary> niz string-ova, za svaki item se pamti videoID potreban da se otkljuca item, ukoliko se item na taj nacin otkljucava </summary>
	string[] videoIdArray;
	/// <summary> niz bool-ova, za svaki item se pamti da li se item koristi(ubacuje u niz itema ili ne) </summary>
	bool[] itemChecked;
	/// <summary> bool koji oznacava da li je skroll verticalan </summary>
	bool vertical;
	/// <summary> bool koji oznacava da li je skroll horizontalan </summary>
	bool  horizontal;
	/// <summary>niz enum-a koji sadrzi podatak o tipu itema iz ItemState klase </summary>
	public ItemState.StateOfItemEnum[] itemStateArray;
	#endregion

//	[MenuItem ("ItemsEditor/ItemsEditorSetup")]
	public static void ShowWindow()
	{
		window = (ItemsEditor)EditorWindow.GetWindow(typeof(ItemsEditor),false,"ItemsEditorSetup",true);
		window.minSize = new Vector2(1000, 1000);
		window.titleContent = new GUIContent("ItemsEditorSetup");
		window.Show();
	}

	void OnGUI()
	{
		switch(stateOfWindow)
		{
		case 1:
			LoadTextures();
			break;
		case 2:
			InitializeItems();
			break;
		case 3:
			InitializeObjects();
			break;
		}
	}

	void LoadTextures()
	{
		var optionsTextures = new []{GUILayout.Width (64), GUILayout.Height (64)};
		numberOfTextures = EditorGUILayout.IntSlider("Number Of textures",numberOfTextures,1,10);
		if(numberOfTextures != oldNumberOfTextures)
		{
			sourceTex = new Texture[numberOfTextures];
			oldNumberOfTextures = numberOfTextures;
		}

		using (new EditorGUILayout.HorizontalScope ()) 
		{

			for(int i=0;i<numberOfTextures;i++)
			{
				sourceTex[i] = (Texture) EditorGUILayout.ObjectField("", sourceTex[i], typeof(Texture),false,optionsTextures);
				EditorGUILayout.Space();
			}

			for(int i=0;i<numberOfTextures;i++)
			{
				if(sourceTex[i]==null)
				{
					texturesOK = false;
				}
				else
				{
					texturesOK = true;
				}
			}
		}

		if(texturesOK)
		{
			GUI.color = Color.green;
		}
		else
		{
			GUI.color = Color.red;
		}
		if (GUILayout.Button("Load Items", EditorStyles.toolbarButton)) {
			if(texturesOK)
			{
				stateOfWindow = 2;
				Debug.Log("Load sprites");
			}
		}
		GUI.color = Color.white;
	}

	void InitializeItems()
	{
		if(!loaded)
		{
			for(int i=0;i<numberOfTextures;i++)
			{
				listOfSpritesInAtlasTemp.Clear();
				listOfSpritesInAtlasTemp = AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(sourceTex[i])).OfType<Sprite>().ToList();
				listOfSpritesInAtlas.AddRange(listOfSpritesInAtlasTemp);
			}

			spriteArray = listOfSpritesInAtlas.ToArray();
			expArray = new int[spriteArray.Length];
			costArray = new int[spriteArray.Length];
			inAppIdArray = new string[spriteArray.Length];
			videoIdArray = new string[spriteArray.Length];
			itemChecked = new bool[spriteArray.Length];
			for(int i = 0; i<itemChecked.Length;i++)
			{
				itemChecked[i]= true;
			}
			itemStateArray = new ItemState.StateOfItemEnum[spriteArray.Length];
			Debug.Log("UKUPNO IH IMA "+spriteArray.Length);
			loaded = true;
		}

		scrollPos = EditorGUILayout.BeginScrollView(scrollPos,false,true, GUILayout.Width(window.position.width), GUILayout.Height(window.position.height));
		for(int i=0;i<listOfSpritesInAtlas.Count;i++) //FIX ogranici na 20-30 elementa po stranici
		{
			EditorGUILayout.BeginHorizontal("box");
			EditorGUILayout.BeginVertical();
			using (new FixedWidthLabel("Use Item"))
			{
				itemChecked[i] = EditorGUILayout.Toggle(itemChecked[i]);
			}
			EditorGUILayout.Space();
			using (new FixedWidthLabel("indexOfItem"))
			{
				EditorGUILayout.LabelField(i.ToString());
			}

			EditorGUILayout.EndVertical();
			EditorGUILayout.BeginVertical();
			using (new FixedWidthLabel("StateOfItem"))
			{
				itemStateArray[i] = (ItemState.StateOfItemEnum) EditorGUILayout.EnumPopup(itemStateArray[i]);
			}

			if(itemStateArray[i] == ItemState.StateOfItemEnum.Experience)
			{
				using (new FixedWidthLabel("Experience"))
				{
					expArray[i] = (int) EditorGUILayout.IntField(expArray[i]);
				}
			}
			else if(itemStateArray[i] == ItemState.StateOfItemEnum.Currency)
			{
				using (new FixedWidthLabel("Cost"))
				{
					costArray[i] = (int) EditorGUILayout.IntField(costArray[i]);
				}
			}
			else if(itemStateArray[i] == ItemState.StateOfItemEnum.InApp)
			{
				using (new FixedWidthLabel("InAppID"))
				{
					inAppIdArray[i] = (string) EditorGUILayout.TextField(inAppIdArray[i]);
				}
			}
			else if(itemStateArray[i] == ItemState.StateOfItemEnum.Video)
			{
				using (new FixedWidthLabel("VideoID"))
				{
					videoIdArray[i] = (string) EditorGUILayout.TextField(videoIdArray[i]);
				}
			}
			EditorGUILayout.EndVertical();
			EditorGUILayout.BeginVertical();
			spriteArray[i] = (Sprite) EditorGUILayout.ObjectField("Image", spriteArray[i], typeof(Sprite),false);
			EditorGUILayout.EndVertical();
			EditorGUILayout.BeginVertical();
			EditorGUILayout.Space();
			EditorGUILayout.Space();
			EditorGUILayout.EndVertical();
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
		}
		if (GUILayout.Button("InitializeObjects", EditorStyles.toolbarButton)) {

			if(itemChecked.All(x=>!x))
			{
				if(EditorUtility.DisplayDialog("Greska","Niste odabrali ni jedan itemali i da !", "Ok"))
				{
					
				}
			}
			else
			{
				stateOfWindow = 3;
			}
		}
		EditorGUILayout.Space();
		EditorGUILayout.Space();
		EditorGUILayout.EndScrollView();
	}

	void InitializeObjects()
	{
		prefab = (GameObject) EditorGUILayout.ObjectField("ItemPrefab ",prefab, typeof(GameObject));
		itemsHolder = (Transform) EditorGUILayout.ObjectField("ItemsHolder ", itemsHolder, typeof(Transform));
		scrollRectHolder = (Transform) EditorGUILayout.ObjectField("ScroolRectHolder ",scrollRectHolder, typeof(Transform));
		EditorGUILayout.Separator();
		EditorGUILayout.BeginHorizontal("box");
		EditorGUILayout.LabelField("Choose orientation:");

		using (new FixedWidthLabel("Vertical"))
		{
			if(vertical = EditorGUILayout.Toggle(vertical))
			{
				horizontal = false;
			}
			else
			{
				horizontal = true;
			}
		}
		using (new FixedWidthLabel("Horizontal"))
		{
			if(horizontal = EditorGUILayout.Toggle(horizontal))
			{
				vertical = false;
			}
			else
			{
				vertical = true;
			}
		}
		EditorGUILayout.EndHorizontal();
		if (GUILayout.Button("Test neki", EditorStyles.toolbarButton))
		{
			if(prefab != null && itemsHolder != null && scrollRectHolder != null)
			{
				if(itemsHolder.transform.childCount>0)
				{
					var children = new List<GameObject>();
					foreach (Transform child in itemsHolder) 
						children.Add(child.gameObject);
					children.ForEach(child => DestroyImmediate(child));
				}

				if(itemsHolder.GetComponent<GridLayoutGroup>()==null)
				{
					itemsHolder.gameObject.AddComponent<GridLayoutGroup>();
				}

				if(scrollRectHolder.GetComponent<ScrollRect>()==null)
				{
					scrollRectHolder.gameObject.AddComponent<ScrollRect>();
				}

				if(horizontal)
				{
					itemsHolder.GetComponent<GridLayoutGroup>().constraint = GridLayoutGroup.Constraint.FixedRowCount;
					scrollRectHolder.GetComponent<ScrollRect>().horizontal = true;
					scrollRectHolder.GetComponent<ScrollRect>().vertical = false;
				}
				else
				{
					itemsHolder.GetComponent<GridLayoutGroup>().constraint = GridLayoutGroup.Constraint.FixedColumnCount;
					scrollRectHolder.GetComponent<ScrollRect>().horizontal = false;
					scrollRectHolder.GetComponent<ScrollRect>().vertical = true;
				}

				scrollRectHolder.GetComponent<ScrollRect>().content = itemsHolder.GetComponent<RectTransform>();
				itemsHolder.GetComponent<GridLayoutGroup>().constraintCount = 1;
				float xSpacing = prefab.GetComponent<RectTransform>().rect.width;
				float ySpacing = prefab.GetComponent<RectTransform>().rect.width;
				itemsHolder.GetComponent<GridLayoutGroup>().spacing = new Vector2(xSpacing, ySpacing);

				for(int i=0;i<spriteArray.Length;i++)
				{
					if(itemChecked[i])
					{
						var tmp = Instantiate(prefab);
						tmp.transform.SetParent(itemsHolder);
						tmp.transform.localPosition = Vector3.zero;
						tmp.transform.localScale = Vector3.one;
						tmp.transform.GetChild(2).GetComponent<Image>().sprite = spriteArray[i];
						tmp.AddComponent<ItemState>();
						tmp.GetComponent<ItemState>().StateOfItem = itemStateArray[i];
						if(itemStateArray[i] == ItemState.StateOfItemEnum.Experience)
						{
							tmp.GetComponent<ItemState>().ExpLevelToUnlock = expArray[i];
						}
						else if(itemStateArray[i] == ItemState.StateOfItemEnum.Currency)
						{
							tmp.GetComponent<ItemState>().Cost = costArray[i];
						}
						else if(itemStateArray[i] == ItemState.StateOfItemEnum.InApp)
						{
							tmp.GetComponent<ItemState>().InAppIDThatUnlocksItem = inAppIdArray[i];
						}
						else if(itemStateArray[i] == ItemState.StateOfItemEnum.Video)
						{
							tmp.GetComponent<ItemState>().VideoID = videoIdArray[i];
						}
					}
				}
				SetContentHeightWidth();
			}
			else
			{
				if(EditorUtility.DisplayDialog("Greska","Proverite da li ste dodelili ItemPrefab i/ili ItemsHolder!", "Ok"))
				{
					Debug.Log("OK");
				}
			}
		}
	}

	private void OnEnable()
	{
		
	}

	private void OnDestroy()
	{
		loaded = false;
		stateOfWindow = 1;
		if(spriteArray!=null)
			Array.Clear(spriteArray,0,spriteArray.Length);
		listOfSpritesInAtlasTemp.Clear();
		listOfSpritesInAtlas.Clear();
	}

	void ResetParameters()
	{
		Debug.Log("ResetParameters");
		loaded = false;
	}

	public void SetContentHeightWidth()
	{
		itemsHolder.GetComponent<RectTransform>().sizeDelta = new Vector2(100,100);
		if(horizontal)
		{
			float scrollContentWidth = ((itemsHolder.transform.childCount) * itemsHolder.GetComponent<GridLayoutGroup>().cellSize.x) + ((itemsHolder.transform.childCount - 1) * itemsHolder.GetComponent<GridLayoutGroup>().spacing.x)+50;
			itemsHolder.GetComponent<RectTransform>().sizeDelta = new Vector2(scrollContentWidth, itemsHolder.gameObject.GetComponent<RectTransform>().sizeDelta.y );
		}
		else
		{
			float scrollContentHeight = ((itemsHolder.transform.childCount) * itemsHolder.GetComponent<GridLayoutGroup>().cellSize.y) + ((itemsHolder.transform.childCount - 1) * itemsHolder.GetComponent<GridLayoutGroup>().spacing.y)+50;
			itemsHolder.GetComponent<RectTransform>().sizeDelta = new Vector2(itemsHolder.gameObject.GetComponent<RectTransform>().sizeDelta.x, scrollContentHeight);
		}
	}


	//DEO STO SAM ISPROBAVAO SVE I SVASTA, ostavljen ako treba kao podsetnik

	//			var element = GameObject.Find("Proba").GetComponent<Nesto>();
	//			int test =EditorGUILayout.IntField(element.test.b);
	//			Editor editor = Editor.CreateEditor(element);
	//			editor.DrawDefaultInspector();

	//		EditorGUIUtility.fieldWidth = 20; poradi sa ovime Nesto probaj

	//			itemList = EditorGUILayout.ObjectField (itemList, typeof(ItemStateList)) as ItemStateList;
	//			Sprite test = (Sprite) EditorGUILayout.ObjectField("", spriteArray[i], typeof(Sprite),false);
	//			var element = GameObject.Find("Proba").GetComponent<Nesto>();
	////			int test =EditorGUILayout.IntField(element.test.b);
	//			Editor editor = Editor.CreateEditor(element);
	//			editor.DrawDefaultInspector();
	//			var serialized = new SerializedObject(GameObject.Find("Proba"));
	//			int testInt =  EditorGUILayout.IntField(element.broj);
	//			EditorGUI.PropertyField(serialized.FindProperty("slika"));
}
