﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MakeBags {

//	[MenuItem("DressUpAnima2D/Bags")]
	public static void CreateMyAsset()
	{
		Bags asset = ScriptableObject.CreateInstance<Bags>();

		AssetDatabase.CreateAsset(asset, "Assets/Bags.asset");
		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();

		Selection.activeObject = asset;
	}
}
