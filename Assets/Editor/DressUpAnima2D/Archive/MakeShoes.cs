﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MakeShoes {

//	[MenuItem("DressUpAnima2D/Shoes")]
	public static void CreateMyAsset()
	{
		Shoes asset = ScriptableObject.CreateInstance<Shoes>();

		AssetDatabase.CreateAsset(asset, "Assets/Shoes.asset");
		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();

		Selection.activeObject = asset;
	}
}
