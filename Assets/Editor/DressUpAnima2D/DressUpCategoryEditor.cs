﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Linq;
using UnityEngine.UI;

public class DressUpCategoryEditor : EditorWindow {
    
    #region WindowVariables
    /// <summary> instanca EditorWindow-a </summary>
    static DressUpCategoryEditor window;
    /// <summary> zavisno od stanja, GUI renda drugacije stanje:  </summary>
    int stateOfWindow = 1;
    /// <summary> bool flag koji cekiramo ako kategorija ima posebne teksture za thumbnails</summary>
    bool hasCustomThumbnails = false;
    #endregion //WindowVariables
    
    #region ItemVariables
    /// <summary> Liste sprite-ova u koje ucitavamo spriteove sa odabranih textura, kasnije se castuje u niz, koriscene su liste, radi lakseg baratanja kada se ucitavaju vise textura istovremeno </summary>
    static List<Sprite> listOfSpritesInAtlas=new List<Sprite>(),listOfSpritesInAtlasTemp=new List<Sprite>(); 
    /// <summary> konacan niz sprite-ova koje smo ucitali </summary>
    Sprite[] itemSpritesArray = new Sprite[0];
    /// <summary> konacan niz prefab-ova koje smo ucitali </summary>
    GameObject[] itemPrefabsArray = new GameObject[0];
    /// <summary> svakom itemu iz niza itema bice dodeljen niz stilova kojima taj item pripada </summary>
    public EItemStyles[][] itemStyleArrays;
    /// <summary> svakom itemu iz niza itema bice dodeljen broj poena koje taj item nosi </summary>
    public int[] stylePoints;
    /// <summary> Niz Thumbnail sprajtova za svaki item </summary>
    Sprite[] itemThumbnailsArray = new Sprite[0];
    #endregion //ItemVariables
    
    #region TexturesLoad
    /// <summary> bool koji pamti da li su ucitani sprite-ovi sa odabranih textura </summary>
    bool loaded = false;
    /// <summary> niz textura iz kojih ucitavamo spriteove kasnije </summary>
    Texture[] sourceTex; 
    /// <summary> niz textura iz kojih ucitavamo thumbnailove kasnije </summary>
    Texture[] thumbnailTex; 
    /// <summary> provera da li su sve texture dodate u niz </summary>
    bool inputOK = true;
    /// <summary> velicina niza textura iz kojih ucitavamo spriteove </summary>
    int numberOfTextures = 1;
    /// <summary> pomocna promenjiva koja sluzi da registruje izmene u duzini niza </summary>
    int oldNumberOfTextures = -1;
    /// <summary> velicina niza textura iz kojih ucitavamo thumbnailove </summary>
    int numberOfThumbnailTextures = 1;
    /// <summary> pomocna promenjiva koja sluzi da registruje izmene u duzini niza </summary>
    int oldNumberOfThumbnailTextures = -1;
    #endregion //TexturesLoad
    
    /// <summary> promenljiva za polje za unos putanje ukoliko ucitavamo content iz prefabs </summary>
    string contentPrefabsPath = "";
    /// <summary> niz bool-ova, za svaki item se pamti da li se item koristi(ubacuje u niz itema ili ne) </summary>
    bool[] itemChecked;
    /// <summary> za odabir putanje na kojoj ce biti sacuvan asset </summary>
    string assetSavePath = "";
    /// <summary> ime grupe objekata </summary>
    string contentGroupName = "ContentGroupData";
    /// <summary> vektor koji sadrzi poziciju za scroll gde listamo sve iteme </summary>
    Vector2 scrollPos = Vector2.zero;
    /// <summary>niz enum-a koji sadrzi podatak o tipu itema iz ItemState klase </summary>
    public ItemState.StateOfItemEnum[] itemStateArray;
    /// <summary>niz enum-a koji sadrzi podatak o osnovnoj vrednosti itema koja se kasnije mnozi sa faktorom u zavisnosti od dostupnosti itema </summary>
    public ECategoryTier[] itemTierArray;
    /// <summary>niz enum-a koji sadrzi podatak o osnovnoj vrednosti itema koja se kasnije mnozi sa faktorom u zavisnosti od dostupnosti itema </summary>
    public ECategoryTier categoryTier = new ECategoryTier();
//  /// <summary> niz int-ova, za svaki item se pamti experience potreban da se otkljuca item, ukoliko se item na taj nacin otkljucava  </summary>
    public int[] expArray;
    /// <summary> prefab objekat koji reprezentuje Item, i koji se instancira u odabrani objekat - public referenca itemsHolder</summary>
    GameObject prefab;
    /// <summary> referenca ka objektu koji je holder za iteme </summary>
    Transform itemsHolder;
    /// <summary>  </summary>
    Transform scrollRectHolder;
    /// <summary> bool koji oznacava da li je skroll verticalan </summary>
    bool vertical;
    /// <summary> bool koji oznacava da li je skroll horizontalan </summary>
    bool  horizontal;

    [MenuItem ("Custom Tools/Dress Up Category Editor")]
    public static void ShowWindow()
    {
        window = (DressUpCategoryEditor)EditorWindow.GetWindow(typeof(DressUpCategoryEditor),false,"DressUpCategoryEditor",true);
        window.minSize = new Vector2(800, 800);
        window.titleContent = new GUIContent("ItemsEditorSetup");
        window.Show();
    }

//    [MenuItem ("Custom Tools/Save Character As Oppponent %#o")]
//    public static void SaveOpponent()
//    {
//        CharacterDressUpData asset;
//        string path = "Assets/Resources/ContestOpponents/Temp";
//        DirectoryInfo di = new DirectoryInfo(path);
//        FileInfo[] fi = di.GetFiles();
//        int index = fi.Length/2;    //delimo sa 2 jer racuna i meta fajlove
//        
//        asset = ScriptableObject.CreateInstance<CharacterDressUpData>();
//        AssetDatabase.CreateAsset(asset,path+"/ContestOpponent_" +index.ToString()+".asset");
//        
//        //TODO popuni sve podatke...
//        asset.earringsAdjIndex = GlobalVariables.mainCharacterDressUpData.earringsAdjIndex;
//        asset.neclessAdjIndex = GlobalVariables.mainCharacterDressUpData.neclessAdjIndex;
//        asset.sunglassesAdjIndex = GlobalVariables.mainCharacterDressUpData.sunglassesAdjIndex;
//        asset.tiaraAdjIndex = GlobalVariables.mainCharacterDressUpData.tiaraAdjIndex;
//
//        asset.hair = GlobalVariables.mainCharacterDressUpData.hair;
//        asset.blouse = GlobalVariables.mainCharacterDressUpData.blouse;
//        asset.jacket = GlobalVariables.mainCharacterDressUpData.jacket;
//        asset.dress = GlobalVariables.mainCharacterDressUpData.dress;
//        asset.pants = GlobalVariables.mainCharacterDressUpData.pants;
//        asset.shoes = GlobalVariables.mainCharacterDressUpData.shoes;
//        asset.shirt = GlobalVariables.mainCharacterDressUpData.shirt;
//        asset.skirt = GlobalVariables.mainCharacterDressUpData.skirt;
//        asset.shorts = GlobalVariables.mainCharacterDressUpData.shorts;
//        asset.socks = GlobalVariables.mainCharacterDressUpData.socks;
//        asset.bag = GlobalVariables.mainCharacterDressUpData.bag;
//
//        asset.jewelryDiamond = GlobalVariables.mainCharacterDressUpData.jewelryDiamond;
//        asset.jewelryLiquidIndex = GlobalVariables.mainCharacterDressUpData.jewelryLiquidIndex;
//        
//        AssetDatabase.SaveAssets();
//        Selection.activeObject = asset;
//        EditorUtility.SetDirty(asset);
//        Debug.Log("Character is saved as opponent");
//    }
    
    void OnGUI()
    {
        switch(stateOfWindow)
        {
            case 1:
                ChooseInputType();
                break;
            case 2:
                EditContentGroupFromTextures();
                break;
            case 3:
                EditContentGroupFromPrefabs();
                break;
            case 4:
                SaveAsset();
                break;
        }
    }
    
    #region WindowStates
    //Window 1
    void ChooseInputType()
    {
        GUILayout.Space(30);
        EditorGUILayout.LabelField("Select Category Value Tier (vote points base value): ");

        categoryTier = (ECategoryTier) EditorGUILayout.EnumPopup(categoryTier);
        
        GUILayout.Space(30);
        EditorGUILayout.LabelField("Select input type for this category: ");
        
        if (GUILayout.Button("Textures", EditorStyles.toolbarButton)) 
        {
            stateOfWindow = 2;
        }
        
        if (GUILayout.Button("Prefabs", EditorStyles.toolbarButton)) 
        {
            stateOfWindow = 3;
        }
    }
    //Window 2
    void EditContentGroupFromTextures()
    {
        GUILayout.Space(50);
        
        var optionsTextures = new []{GUILayout.Width (64), GUILayout.Height (64)};
        
        numberOfTextures = EditorGUILayout.IntSlider("Number Of Item Textures",numberOfTextures,1,10);
        if(numberOfTextures != oldNumberOfTextures)
        {
            sourceTex = new Texture[numberOfTextures];
            oldNumberOfTextures = numberOfTextures;
        }
        
        GUILayout.Space(10);
        
        using (new EditorGUILayout.HorizontalScope ()) 
        {
            GUILayout.Space(10);
            
            for(int i=0;i<numberOfTextures;i++)
            {
                sourceTex[i] = (Texture) EditorGUILayout.ObjectField("", sourceTex[i], typeof(Texture),false,optionsTextures);
                EditorGUILayout.Space();
            }
            
            for(int i=0;i<numberOfTextures;i++)
            {
                if(sourceTex[i]==null)
                {
                    inputOK = false;
                }
                else
                {
                    inputOK = true;
                }
            }
        }
        
        GUILayout.Space(10);
        
        hasCustomThumbnails = EditorGUILayout.Toggle("Use Custom Thumbnails",hasCustomThumbnails);
        
        GUILayout.Space(10);
        
        if (hasCustomThumbnails)
        {
            numberOfThumbnailTextures = EditorGUILayout.IntSlider("Thumbnail Textures",numberOfThumbnailTextures,1,10);
            if(numberOfThumbnailTextures != oldNumberOfThumbnailTextures)
            {
                thumbnailTex = new Texture[numberOfThumbnailTextures];
                oldNumberOfThumbnailTextures = numberOfThumbnailTextures;
            }
            
            using (new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Space(10);
                
                for(int i=0;i<numberOfThumbnailTextures;i++)
                {
                    thumbnailTex[i] = (Texture) EditorGUILayout.ObjectField("", thumbnailTex[i], typeof(Texture),false,optionsTextures);
                    EditorGUILayout.Space();
                }
            }
        }
        
        if(inputOK)
        {
            GUI.color = Color.green;
        }
        else
        {
            GUI.color = Color.red;
        }
        
        GUILayout.Space(10);
        
        if (GUILayout.Button("Load Items", EditorStyles.toolbarButton)) 
        {
            if(inputOK)
            {
                LoadItemsFromTextures();
                Debug.Log("Load sprites");
            }
        }
        
        GUI.color = Color.white;
        
        if (GUILayout.Button("Back", EditorStyles.toolbarButton)) 
        {
            CancelButton();
            Debug.Log("Back");
        }
    }
    //Window 3
    void EditContentGroupFromPrefabs()
    {
        var optionsTextures = new []{GUILayout.Width (64), GUILayout.Height (64)};

        GUILayout.Space(50);

        numberOfThumbnailTextures = EditorGUILayout.IntSlider("Thumbnail Textures",numberOfThumbnailTextures,1,10);
        if(numberOfThumbnailTextures != oldNumberOfThumbnailTextures)
        {
            thumbnailTex = new Texture[numberOfThumbnailTextures];
            oldNumberOfThumbnailTextures = numberOfThumbnailTextures;
        }
        
        using (new EditorGUILayout.HorizontalScope())
        {
            GUILayout.Space(10);
            
            for(int i=0;i<numberOfThumbnailTextures;i++)
            {
                thumbnailTex[i] = (Texture) EditorGUILayout.ObjectField("", thumbnailTex[i], typeof(Texture),false,optionsTextures);
                EditorGUILayout.Space();
            }
            
            for(int i=0;i<numberOfThumbnailTextures;i++)
            {
                if(thumbnailTex[i]==null)
                {
                    inputOK = false;
                }
                else
                {
                    inputOK = true;
                }
            }
        }
        
        GUILayout.Space(10);

        if(inputOK)
        {
            GUI.color = Color.green;
        }
        else
        {
            GUI.color = Color.red;
        }
        
        if (GUILayout.Button("Load Items", EditorStyles.toolbarButton)) 
        {
            if(inputOK)
                LoadItemsFromPrefabs();
            Debug.Log("Load prefabs");
        }

        GUI.color = Color.white;

        if (GUILayout.Button("Back", EditorStyles.toolbarButton)) 
        {
            CancelButton();
            Debug.Log("Back");
        }
    }
    //Window 4
    void SaveAsset()
    {
        if(!loaded)
        {
            return;
        }

        if(window != null)
            scrollPos = EditorGUILayout.BeginScrollView(scrollPos,false,true, GUILayout.Width(window.position.width), GUILayout.Height(window.position.height));
        
        //        int itemCount = Mathf.Max(listOfSpritesInAtlas.Count,listOfPrefabsInFolder.Count);
        
        for(int i=0;i<itemChecked.Length;i++) //FIX ogranici na 20-30 elementa po stranici
        {
            EditorGUILayout.BeginHorizontal("box");
            EditorGUILayout.BeginVertical();
            using (new FixedWidthLabel("Use Item"))
            {
                itemChecked[i] = EditorGUILayout.Toggle(itemChecked[i]);
            }
            EditorGUILayout.Space();
            using (new FixedWidthLabel("Index Of Item"))
            {
                EditorGUILayout.LabelField(i.ToString());
            }
            
            using (new FixedWidthLabel("Item Value Tier"))
            {
                itemTierArray[i] = (ECategoryTier) EditorGUILayout.EnumPopup(itemTierArray[i]);
            }
            
            using (new FixedWidthLabel("Locked Statys"))
            {
                itemStateArray[i] = (ItemState.StateOfItemEnum) EditorGUILayout.EnumPopup(itemStateArray[i]);
            }

            if(itemStateArray[i] == ItemState.StateOfItemEnum.Experience)
            {
                expArray[i] = (int) EditorGUILayout.IntField(expArray[i]);
            }
            //TODO ostale opcije za ItemState

            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical();
            using (new FixedWidthLabel("ItemStyle"))
            {
                EditorGUILayout.BeginVertical();
                if ( GUILayout.Button( "-" ) ) 
                {
                    if(itemStyleArrays[i].Length > 1)
                        Array.Resize(ref itemStyleArrays[i],itemStyleArrays[i].Length-1);
                }
                if ( GUILayout.Button( "+" ) ) 
                {
                    if(itemStyleArrays[i].Length < 4)
                        Array.Resize(ref itemStyleArrays[i],itemStyleArrays[i].Length+1);
                }
                EditorGUILayout.EndVertical();
                
                EditorGUILayout.BeginVertical();
                for (int j = 0; j < itemStyleArrays[i].Length; j++) 
                {
                    itemStyleArrays[i][j] = (EItemStyles) EditorGUILayout.EnumPopup(itemStyleArrays[i][j]);
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndVertical();
            EditorGUILayout.BeginVertical();
            EditorGUI.BeginDisabledGroup(true);
            if (itemSpritesArray.Length > 0)
                itemSpritesArray[i] = (Sprite)EditorGUILayout.ObjectField("Item Preview", itemSpritesArray[i], typeof(Sprite), false);
            else
            {
//                Debug.Log("preview: " + i);
                EditorGUILayout.ObjectField("Item Preview",itemPrefabsArray[i].GetComponentInChildren<Anima2D.SpriteMeshInstance>().spriteMesh.sprite.texture, typeof(Texture),false);    //TODO preview
            }
            EditorGUI.EndDisabledGroup();
            
            itemThumbnailsArray[i] = (Sprite) EditorGUILayout.ObjectField("Thumbnail",itemThumbnailsArray[i], typeof(Sprite),false);    //TODO preview

            EditorGUILayout.EndVertical();
            EditorGUILayout.BeginVertical();
            GUILayout.Space(20);
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Separator();
        }
        
        GUILayout.Space(20);
        
        GUI.color = Color.white;

        EditorGUILayout.BeginHorizontal();//FIXME
        EditorGUILayout.BeginVertical(GUILayout.Width(400));//FIXME

        contentGroupName = EditorGUILayout.TextField(new GUIContent("Name: ", "Enter the name of the asset"),contentGroupName,EditorStyles.textField);
        
        GUILayout.Space(10);

        if (GUILayout.Button("Select Save folder", EditorStyles.toolbarButton)) {
            assetSavePath = EditorUtility.OpenFolderPanel("Select asset save folder","","");
            
            if (!assetSavePath.Contains("Assets"))
            {
                if (EditorUtility.DisplayDialog("Greska", "Odabrali ste pogresnu putanju. Folder za cuvanje itema treba da se nalazi u okviru Assets foldera!", "Ok"))
                {
                }
                return;
            }
            
            assetSavePath = assetSavePath.Substring(assetSavePath.IndexOf("Assets"));
        }
      
        EditorGUILayout.LabelField(new GUIContent("Asset will be saved at: "+assetSavePath,assetSavePath),EditorStyles.label);
        
        if (GUILayout.Button("Create Asset", EditorStyles.toolbarButton)) {
            CreateAsset();
            Debug.Log("Asset saved");
        }

        EditorGUILayout.EndVertical();//FIXME
        GUILayout.Space(10);
        EditorGUILayout.BeginVertical();//FIXME

        prefab = (GameObject) EditorGUILayout.ObjectField("ItemPrefab ",prefab, typeof(GameObject));
        itemsHolder = (Transform) EditorGUILayout.ObjectField("ItemsHolder ", itemsHolder, typeof(Transform));
        scrollRectHolder = (Transform) EditorGUILayout.ObjectField("ScroolRectHolder ",scrollRectHolder, typeof(Transform));
        EditorGUILayout.Separator();
        EditorGUILayout.BeginHorizontal("box");
        EditorGUILayout.LabelField("Choose orientation:");

        using (new FixedWidthLabel("Horizontal"))
        {
            if(horizontal = EditorGUILayout.Toggle(horizontal))
            {
                vertical = false;
            }
            else
            {
                vertical = true;
            }
        }
        using (new FixedWidthLabel("Vertical"))
        {
            if(vertical = EditorGUILayout.Toggle(vertical))
            {
                horizontal = false;
            }
            else
            {
                horizontal = true;
            }
        }

        EditorGUILayout.EndHorizontal();

//        if(inputOK)
//        {
//            GUI.color = Color.green;
//        }
//        else
//        {
//            GUI.color = Color.red;
//        }

        if (GUILayout.Button("Create Buttons", EditorStyles.toolbarButton)) {
            CreateButtons();
            Debug.Log("Buttons created");
        }
        EditorGUILayout.EndVertical();//FIXME
        EditorGUILayout.EndHorizontal();//FIXME

//        GUI.color = Color.white;
        GUILayout.Space(10);

        if (GUILayout.Button("Cancel", EditorStyles.toolbarButton)) 
        {
            CancelButton();
            Debug.Log("Cancel");
        }

        GUILayout.Space(30);
        EditorGUILayout.EndScrollView();
    }
    #endregion //WindowStates
    
    #region ExexuteMethods
    void LoadItemsFromTextures()
    {
        //Load Item Sprites from Item Textures
        listOfSpritesInAtlas.Clear();
        for (int i = 0; i < numberOfTextures; i++)
        {
            listOfSpritesInAtlasTemp.Clear();
            listOfSpritesInAtlasTemp = (AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(sourceTex[i]))).OfType<Sprite>().ToList();
            listOfSpritesInAtlasTemp.Sort(new SpriteNumericComparer());
            listOfSpritesInAtlas.AddRange(listOfSpritesInAtlasTemp);
        }
        itemSpritesArray = listOfSpritesInAtlas.ToArray();
        //Initialize ItemChecked array
        itemChecked = new bool[itemSpritesArray.Length];
        for(int i = 0; i<itemChecked.Length;i++)
        {
            itemChecked[i]= true;
        }
        //Initialize ItemStyleArrays
        itemStyleArrays = new EItemStyles[itemSpritesArray.Length][];
        for (int i = 0; i < itemSpritesArray.Length; i++)
        {
            itemStyleArrays[i] = new EItemStyles[1];
        }
        //Load Item Thumbnail Sprites from Thumbnail Textures
        if (hasCustomThumbnails)
        {
            listOfSpritesInAtlas.Clear();
            for (int i = 0; i < numberOfThumbnailTextures; i++)
            {
                listOfSpritesInAtlasTemp.Clear();
                listOfSpritesInAtlasTemp = (AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(thumbnailTex[i]))).OfType<Sprite>().ToList();
                listOfSpritesInAtlasTemp.Sort(new SpriteNumericComparer());
                listOfSpritesInAtlas.AddRange(listOfSpritesInAtlasTemp);
            }
            itemThumbnailsArray = listOfSpritesInAtlas.ToArray();
            
            if (itemThumbnailsArray.Length != itemSpritesArray.Length)
                Array.Resize(ref itemThumbnailsArray, itemSpritesArray.Length);
        }
        else
        {
            itemThumbnailsArray = itemSpritesArray;
        }
        //Initialize ItemStateArray
        itemStateArray = new ItemState.StateOfItemEnum[itemSpritesArray.Length];
        expArray = new int[itemSpritesArray.Length];
        //Initialize ItemValueArray
        itemTierArray = new ECategoryTier[itemSpritesArray.Length];
        for (int i = 0; i < itemTierArray.Length; i++)
        {
            itemTierArray[i] = categoryTier;
        }
        
        Debug.Log("UKUPNO IH IMA "+itemSpritesArray.Length);
        loaded = true;
        stateOfWindow = 4;
    }
    void LoadItemsFromPrefabs()
    {
        //Select folder to load from
        contentPrefabsPath = EditorUtility.OpenFolderPanel("Load prefabs from folder","","");
        
        if (!contentPrefabsPath.Contains("Prefabs/ContentDataPrefabs"))
        {
            if (EditorUtility.DisplayDialog("Greska", "Odabrali ste pogresnu putanju. Folder sa aitemima treba da se nalazi u okviru Resources/Prefabs/ContentDataPrefabs foldera!", "Ok"))
            {
            }
            return;
        }
        //Load Prefabs from folder
        contentPrefabsPath = contentPrefabsPath.Substring(contentPrefabsPath.IndexOf("Prefabs/ContentDataPrefabs"));
        Debug.Log(contentPrefabsPath);
        itemPrefabsArray = new GameObject[0];

        string[] prefabFiles = Directory.GetFiles("Assets/Resources/"+contentPrefabsPath, "*.prefab");
        itemPrefabsArray = new GameObject[prefabFiles.Length];
        System.Array.Sort(prefabFiles, new NumericComparer());
        foreach (string pfb in prefabFiles)
        {
            string path = pfb.Substring(pfb.IndexOf("Prefabs/ContentDataPrefabs"));
            path = path.Remove(path.Length-7);
            itemPrefabsArray[Array.IndexOf(prefabFiles,pfb)] = Resources.Load(path) as GameObject;
        }
        //Initialize ItemChecked array
        itemChecked = new bool[itemPrefabsArray.Length];
        for(int i = 0; i<itemChecked.Length;i++)
        {
            itemChecked[i]= true;
        }
        //Initialize ItemStyleArrays
        itemStyleArrays = new EItemStyles[itemPrefabsArray.Length][];
        for (int i = 0; i < itemPrefabsArray.Length; i++)
        {
            itemStyleArrays[i] = new EItemStyles[1];
        }
        //Load Item Thumbnail Sprites from Thumbnail Textures
        listOfSpritesInAtlas.Clear();
        for (int i = 0; i < numberOfThumbnailTextures; i++)
        {
            listOfSpritesInAtlasTemp.Clear();
            listOfSpritesInAtlasTemp = (AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(thumbnailTex[i]))).OfType<Sprite>().ToList();
            listOfSpritesInAtlasTemp.Sort(new SpriteNumericComparer());
            listOfSpritesInAtlas.AddRange(listOfSpritesInAtlasTemp);
        }
        itemThumbnailsArray = listOfSpritesInAtlas.ToArray();

        if (itemThumbnailsArray.Length != itemPrefabsArray.Length)
            Array.Resize(ref itemThumbnailsArray, itemPrefabsArray.Length);
        //Initialize ItemStateArray
        itemStateArray = new ItemState.StateOfItemEnum[itemPrefabsArray.Length];
        expArray = new int[itemPrefabsArray.Length];
        //Initialize ItemValueArray
        itemTierArray = new ECategoryTier[itemPrefabsArray.Length];
        for (int i = 0; i < itemTierArray.Length; i++)
        {
            itemTierArray[i] = categoryTier;
        }

        loaded = true;
        stateOfWindow = 4;
    }
    
    void CancelButton()
    {
        Debug.Log("cancelovao");
        stateOfWindow = 1;
        contentPrefabsPath = "";
        assetSavePath = "";
        numberOfTextures = 1;
        numberOfThumbnailTextures = 1;
        sourceTex = new Texture[1];
        thumbnailTex = new Texture[1];
        hasCustomThumbnails = false;
        listOfSpritesInAtlasTemp.Clear();
        listOfSpritesInAtlas.Clear();
        itemSpritesArray = new Sprite[0];
        itemPrefabsArray = new GameObject[0];
        itemStyleArrays = new EItemStyles[0][];
        itemThumbnailsArray = new Sprite[0];
        itemChecked = new bool[0];
        inputOK = false;
        loaded = false;
    }
    
    void CreateAsset()
    {
        
        if (itemSpritesArray.Length > 0)
        {
            if (assetSavePath == "")
            {
                assetSavePath = EditorUtility.OpenFolderPanel("Select asset save folder","","");
                assetSavePath = assetSavePath.Substring(assetSavePath.IndexOf("Assets"));
            }
            DressUpItemSpriteScriptableObject asset = ScriptableObject.CreateInstance<DressUpItemSpriteScriptableObject>();
            asset.elements = new List<DressUpItemSprite>();
            AssetDatabase.CreateAsset(asset, assetSavePath + "/" + contentGroupName + ".asset");
            
            foreach (Sprite item in itemSpritesArray)
            {
                int i = Array.IndexOf(itemSpritesArray, item);
                if (itemChecked[i])
                {
                    DressUpItemSprite tmp = new DressUpItemSprite();
                    tmp.item = item;
                    if (hasCustomThumbnails)
                        tmp.thumbnail = itemThumbnailsArray[i];
                    else
                        tmp.thumbnail = item;
                    tmp.styles = itemStyleArrays[i].ToList();
                    tmp.categoryTier = itemTierArray[i];

                    if(itemStateArray[i] == ItemState.StateOfItemEnum.Experience)
                    {
                        tmp.valueMultiplier = EValueMultiplier.Exp;
                        tmp.unlockLevel = expArray[i];
                    }
                    else if(itemStateArray[i] == ItemState.StateOfItemEnum.Video)
                    {
                        tmp.valueMultiplier = EValueMultiplier.Video;
                    }
                    if(itemStateArray[i] == ItemState.StateOfItemEnum.Normal)
                    {
                        tmp.valueMultiplier = EValueMultiplier.Free;
                    }

                    asset.elements.Add(tmp);
                }

                asset.RefreshStylePoints();
            }
            EditorUtility.SetDirty(asset);
            
            AssetDatabase.SaveAssets();
            Selection.activeObject = asset;
            
        }
        else if (itemPrefabsArray.Length > 0)
        {
            if (assetSavePath == "")
            {
                assetSavePath = EditorUtility.OpenFolderPanel("Select asset save folder","","");
                assetSavePath = assetSavePath.Substring(assetSavePath.IndexOf("Assets"));
            }
            DressUpItemPrefabScriptableObject asset = ScriptableObject.CreateInstance<DressUpItemPrefabScriptableObject>();
            asset.elements = new List<DressUpItemPrefab>();
            AssetDatabase.CreateAsset(asset, assetSavePath + "/" + contentGroupName + ".asset");
            
            foreach (GameObject item in itemPrefabsArray)
            {
                int i = Array.IndexOf(itemPrefabsArray, item);
                if (itemChecked[i])
                {
                    DressUpItemPrefab tmp = new DressUpItemPrefab();
                    tmp.item = item;
                    tmp.thumbnail = itemThumbnailsArray[i];
                    tmp.styles = itemStyleArrays[i].ToList();
                    tmp.categoryTier = itemTierArray[i];

                    if(itemStateArray[i] == ItemState.StateOfItemEnum.Experience)
                    {
                        tmp.valueMultiplier = EValueMultiplier.Exp;
                        tmp.unlockLevel = expArray[i];
                    }
                    else if(itemStateArray[i] == ItemState.StateOfItemEnum.Video)
                    {
                        tmp.valueMultiplier = EValueMultiplier.Video;
                    }
                    if(itemStateArray[i] == ItemState.StateOfItemEnum.Normal)
                    {
                        tmp.valueMultiplier = EValueMultiplier.Free;
                    }

                    asset.elements.Add(tmp);
                }

                asset.RefreshStylePoints();

            }       
            EditorUtility.SetDirty(asset);
            
            AssetDatabase.SaveAssets();
            Selection.activeObject = asset;
            
        }
        else
        {
            if(EditorUtility.DisplayDialog("Greska","Niste odabrali ni jedan item!", "Ok"))
            {
                return;
            }
        }
    }

    void CreateButtons()
    {
        if(prefab != null && itemsHolder != null && scrollRectHolder != null)
        {
            if(itemsHolder.transform.childCount>0)
            {
                var children = new List<GameObject>();
                foreach (Transform child in itemsHolder) 
                    children.Add(child.gameObject);
                children.ForEach(child => DestroyImmediate(child));
            }

            if(itemsHolder.GetComponent<GridLayoutGroup>()==null)
            {
                itemsHolder.gameObject.AddComponent<GridLayoutGroup>();
            }

            if(scrollRectHolder.GetComponent<ScrollRect>()==null)
            {
                scrollRectHolder.gameObject.AddComponent<ScrollRect>();
            }

            if(horizontal)
            {
                itemsHolder.GetComponent<GridLayoutGroup>().constraint = GridLayoutGroup.Constraint.FixedRowCount;
                scrollRectHolder.GetComponent<ScrollRect>().horizontal = true;
                scrollRectHolder.GetComponent<ScrollRect>().vertical = false;
            }
            else
            {
                itemsHolder.GetComponent<GridLayoutGroup>().constraint = GridLayoutGroup.Constraint.FixedColumnCount;
                scrollRectHolder.GetComponent<ScrollRect>().horizontal = false;
                scrollRectHolder.GetComponent<ScrollRect>().vertical = true;
            }

            scrollRectHolder.GetComponent<ScrollRect>().content = itemsHolder.GetComponent<RectTransform>();
            itemsHolder.GetComponent<GridLayoutGroup>().constraintCount = 1;
            float xSpacing = prefab.GetComponent<RectTransform>().rect.width;
            float ySpacing = prefab.GetComponent<RectTransform>().rect.width;
            itemsHolder.GetComponent<GridLayoutGroup>().spacing = new Vector2(xSpacing, ySpacing);

            for(int i=0;i<itemThumbnailsArray.Length;i++)
            {
                if(itemChecked[i])
                {
                    var tmp = Instantiate(prefab);
                    tmp.transform.SetParent(itemsHolder);
                    tmp.transform.localPosition = Vector3.zero;
                    tmp.transform.localScale = Vector3.one;
                    tmp.transform.GetChild(2).GetComponent<Image>().sprite = itemThumbnailsArray[i];
                    tmp.AddComponent<ItemState>();
                    tmp.GetComponent<ItemState>().StateOfItem = itemStateArray[i];
                    if(itemStateArray[i] == ItemState.StateOfItemEnum.Experience)
                    {
                        tmp.GetComponent<ItemState>().ExpLevelToUnlock = expArray[i];
                        tmp.transform.Find("LockedHolder/LevelUnlock/TextNumber").GetComponent<Text>().text = expArray[i].ToString();
                    }
//                    else if(itemStateArray[i] == ItemState.StateOfItemEnum.Currency)
//                    {
//                        tmp.GetComponent<ItemState>().Cost = costArray[i];
//                    }
//                    else if(itemStateArray[i] == ItemState.StateOfItemEnum.InApp)
//                    {
//                        tmp.GetComponent<ItemState>().InAppIDThatUnlocksItem = inAppIdArray[i];
//                    }
//                    else if(itemStateArray[i] == ItemState.StateOfItemEnum.Video)
//                    {
//                        tmp.GetComponent<ItemState>().VideoID = videoIdArray[i];
//                    }
                }
            }
            SetContentHeightWidth();
        }
        else
        {
            if(EditorUtility.DisplayDialog("Greska","Proverite da li ste dodelili ItemPrefab i/ili ItemsHolder!", "Ok"))
            {
                Debug.Log("OK");
            }
        }
    }

    void SetContentHeightWidth()
    {
        itemsHolder.GetComponent<RectTransform>().sizeDelta = new Vector2(100,100);
        if(horizontal)
        {
            float scrollContentWidth = ((itemsHolder.transform.childCount) * itemsHolder.GetComponent<GridLayoutGroup>().cellSize.x) + ((itemsHolder.transform.childCount - 1) * itemsHolder.GetComponent<GridLayoutGroup>().spacing.x)+50;
            itemsHolder.GetComponent<RectTransform>().sizeDelta = new Vector2(scrollContentWidth, itemsHolder.gameObject.GetComponent<RectTransform>().sizeDelta.y );
        }
        else
        {
            float scrollContentHeight = ((itemsHolder.transform.childCount) * itemsHolder.GetComponent<GridLayoutGroup>().cellSize.y) + ((itemsHolder.transform.childCount - 1) * itemsHolder.GetComponent<GridLayoutGroup>().spacing.y)+50;
            itemsHolder.GetComponent<RectTransform>().sizeDelta = new Vector2(itemsHolder.gameObject.GetComponent<RectTransform>().sizeDelta.x, scrollContentHeight);
        }
    }
    #endregion //ExecuteMethods

    void OnDestroy()
    {
        CancelButton();
    }
}
//TODO Da se odradi provera kada se promeni neko podesavanje, da izbaci obavestenje da asset, buttons i expLocked lista nisu sinhronizovane

public class NumericComparer : IComparer
{
    public NumericComparer()
    {}

    public int Compare(object x, object y)
    {
        if((x is string) && (y is string))
        {
            return StringLogicalComparer.Compare((string)x, (string)y);
        }
        return -1;
    }
}//EOC
public class SpriteNumericComparer : IComparer<Sprite>
{
    public SpriteNumericComparer()
    {}

    public int Compare(Sprite x, Sprite y)
    {
        if((x is Sprite) && (y is Sprite))
        {
            return StringLogicalComparer.Compare(x.name, y.name);
        }
        return -1;
    }

}//EOC
public class StringLogicalComparer
{
    public static int Compare(string s1, string s2)
    {
        //get rid of special cases
        if ((s1 == null) && (s2 == null)) return 0;
        else if (s1 == null) return -1;
        else if (s2 == null) return 1;

        if ((s1.Equals(string.Empty) && (s2.Equals(string.Empty)))) return 0;
        else if (s1.Equals(string.Empty)) return -1;
        else if (s2.Equals(string.Empty)) return -1;

        //WE style, special case
        bool sp1 = System.Char.IsLetterOrDigit(s1, 0);
        bool sp2 = System.Char.IsLetterOrDigit(s2, 0);
        if (sp1 && !sp2) return 1;
        if (!sp1 && sp2) return -1;

        int i1 = 0, i2 = 0; //current index
        int r = 0; // temp result
        while (true)
        {
            bool c1 = System.Char.IsDigit(s1, i1);
            bool c2 = System.Char.IsDigit(s2, i2);
            if (!c1 && !c2)
            {
                bool letter1 = System.Char.IsLetter(s1, i1);
                bool letter2 = System.Char.IsLetter(s2, i2);
                if ((letter1 && letter2) || (!letter1 && !letter2))
                {
                    if (letter1 && letter2)
                    {
                        r = System.Char.ToLower(s1[i1]).CompareTo(System.Char.ToLower(s2[i2]));
                    }
                    else
                    {
                        r = s1[i1].CompareTo(s2[i2]);
                    }
                    if (r != 0) return r;
                }
                else if (!letter1 && letter2) return -1;
                else if (letter1 && !letter2) return 1;
            }
            else if (c1 && c2)
            {
                r = CompareNum(s1, ref i1, s2, ref i2);
                if (r != 0) return r;
            }
            else if (c1)
            {
                return -1;
            }
            else if (c2)
            {
                return 1;
            }
            i1++;
            i2++;
            if ((i1 >= s1.Length) && (i2 >= s2.Length))
            {
                return 0;
            }
            else if (i1 >= s1.Length)
            {
                return -1;
            }
            else if (i2 >= s2.Length)
            {
                return -1;
            }
        }
    }

    private static int CompareNum(string s1, ref int i1, string s2, ref int i2)
    {
        int nzStart1 = i1, nzStart2 = i2; // nz = non zero
        int end1 = i1, end2 = i2;

        ScanNumEnd(s1, i1, ref end1, ref nzStart1);
        ScanNumEnd(s2, i2, ref end2, ref nzStart2);
        int start1 = i1; i1 = end1 - 1;
        int start2 = i2; i2 = end2 - 1;

        int nzLength1 = end1 - nzStart1;
        int nzLength2 = end2 - nzStart2;

        if (nzLength1 < nzLength2) return -1;
        else if (nzLength1 > nzLength2) return 1;

        for (int j1 = nzStart1, j2 = nzStart2; j1 <= i1; j1++, j2++)
        {
            int r = s1[j1].CompareTo(s2[j2]);
            if (r != 0) return r;
        }
        // the nz parts are equal
        int length1 = end1 - start1;
        int length2 = end2 - start2;
        if (length1 == length2) return 0;
        if (length1 > length2) return -1;
        return 1;
    }

    //lookahead
    private static void ScanNumEnd(string s, int start, ref int end, ref int nzStart)
    {
        nzStart = start;
        end = start;
        bool countZeros = true;
        while (System.Char.IsDigit(s, end))
        {
            if (countZeros && s[end].Equals('0'))
            {
                nzStart++;
            }
            else countZeros = false;
            end++;
            if (end >= s.Length) break;
        }
    }
}//EOC