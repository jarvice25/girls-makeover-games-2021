﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class UpdateCategoryButtons : EditorWindow {

    static UpdateCategoryButtons window;
    static Transform selectedItem;

    DressUpItemSpriteScriptableObject spriteSO;
    DressUpItemPrefabScriptableObject prefabSO;
    GameObject buttonPrefab;

    public static void ShowWindow()
    {
        window = (UpdateCategoryButtons)EditorWindow.GetWindow(typeof(UpdateCategoryButtons),false,"Update buttons",true);
        window.minSize = new Vector2(400, 200);
        window.titleContent = new GUIContent("Update buttons");
        window.Show();
    }

    [MenuItem ("GameObject/Update Category Buttons",false,0)]
    public static void NewMenuOptions()
    {
        selectedItem = Selection.activeTransform;
        if (selectedItem != null)
        {
            ShowWindow();
        }
        else
        {
            EditorUtility.DisplayDialog("Error","This action requires one selected GameObject","Ok");
        }
    }

    void OnGUI()
    {
        EditorGUILayout.Space();
        buttonPrefab = EditorGUILayout.ObjectField(" Prefab Button: ", buttonPrefab, typeof(GameObject),false) as GameObject;
        EditorGUILayout.Space();
        spriteSO = EditorGUILayout.ObjectField(" Sprite SO: ", spriteSO, typeof(DressUpItemSpriteScriptableObject),false) as DressUpItemSpriteScriptableObject;
        EditorGUILayout.Space();
        prefabSO = EditorGUILayout.ObjectField(" Prefab SO: ", prefabSO, typeof(DressUpItemPrefabScriptableObject),false) as DressUpItemPrefabScriptableObject;

        if (GUILayout.Button("Update buttos"))
        {
            
            if(selectedItem == null)
            {
                EditorUtility.DisplayDialog("Error","This action requires one selected GameObject","Ok");
            }
            else
            {
                if (spriteSO != null)
                {
                    UpdateFromSpriteSO();
                }
                else if (prefabSO != null)
                {
                    UpdateFromPrefabSO();
                }
                else
                    EditorUtility.DisplayDialog("Error", "There is no input scriptable object", "Ok");
            }
        }
    }

    void UpdateFromSpriteSO()
    {
        if (selectedItem.childCount != spriteSO.elements.Count)
        {
            if (buttonPrefab == null)
            {
                EditorUtility.DisplayDialog("Error", "Select button prefab", "Ok");
            }
            else
            {
                while (selectedItem.transform.childCount > 0)
                {
                    DestroyImmediate(selectedItem.GetChild(0).gameObject);
                }
                
                for (int i = 0; i < spriteSO.elements.Count; i++)
                {
                    var tmp = Instantiate(buttonPrefab);
                    tmp.transform.SetParent(selectedItem);
                    tmp.transform.localPosition = Vector3.zero;
                    tmp.transform.localScale = Vector3.one;
                    tmp.transform.GetChild(2).GetComponent<UnityEngine.UI.Image>().sprite = spriteSO.elements[i].thumbnail;
                    tmp.AddComponent<ItemState>();
                }
            }
        }

        for (int i = 0; i < selectedItem.childCount; i++)
        {
            switch (spriteSO.elements[i].valueMultiplier)
            {
                case EValueMultiplier.Free:
                    selectedItem.GetChild(i).GetComponent<ItemState>().StateOfItem = ItemState.StateOfItemEnum.Normal;
                    break;
                case EValueMultiplier.Video:
                    selectedItem.GetChild(i).GetComponent<ItemState>().StateOfItem = ItemState.StateOfItemEnum.Video;
                    break;
                case EValueMultiplier.Exp:
                    selectedItem.GetChild(i).GetComponent<ItemState>().StateOfItem = ItemState.StateOfItemEnum.Experience;
                    selectedItem.GetChild(i).Find("LockedHolder/LevelUnlock/TextNumber").GetComponent<UnityEngine.UI.Text>().text = spriteSO.elements[i].unlockLevel.ToString();
                    break;
                default:
                    break;
            }

            selectedItem.GetChild(i).GetChild(2).GetComponent<UnityEngine.UI.Image>().sprite = spriteSO.elements[i].thumbnail;
        }

        Debug.Log("Buttons Updated!");
        this.Close();
    }

    void UpdateFromPrefabSO()
    {
        if (selectedItem.childCount != prefabSO.elements.Count)
        {
            if (buttonPrefab == null)
            {
                EditorUtility.DisplayDialog("Error", "Select button prefab", "Ok");
            }
            else
            {
                while (selectedItem.transform.childCount > 0)
                {
                    DestroyImmediate(selectedItem.GetChild(0).gameObject);
                }

                for (int i = 0; i < prefabSO.elements.Count; i++)
                {
                    var tmp = Instantiate(buttonPrefab);
                    tmp.transform.SetParent(selectedItem);
                    tmp.transform.localPosition = Vector3.zero;
                    tmp.transform.localScale = Vector3.one;
                    tmp.transform.GetChild(2).GetComponent<UnityEngine.UI.Image>().sprite = prefabSO.elements[i].thumbnail;
                    tmp.AddComponent<ItemState>();
                }
            }
        }

        for (int i = 0; i < selectedItem.childCount; i++)
        {
            switch (prefabSO.elements[i].valueMultiplier)
            {
                case EValueMultiplier.Free:
                    selectedItem.GetChild(i).GetComponent<ItemState>().StateOfItem = ItemState.StateOfItemEnum.Normal;
                    break;
                case EValueMultiplier.Video:
                    selectedItem.GetChild(i).GetComponent<ItemState>().StateOfItem = ItemState.StateOfItemEnum.Video;
                    break;
                case EValueMultiplier.Exp:
                    selectedItem.GetChild(i).GetComponent<ItemState>().StateOfItem = ItemState.StateOfItemEnum.Experience;
                    selectedItem.GetChild(i).Find("LockedHolder/LevelUnlock/TextNumber").GetComponent<UnityEngine.UI.Text>().text = prefabSO.elements[i].unlockLevel.ToString();
                    break;
                default:
                    break;
            }
            selectedItem.GetChild(i).GetChild(2).GetComponent<UnityEngine.UI.Image>().sprite = prefabSO.elements[i].thumbnail;
        }

        Debug.Log("Buttons Updated!");
        this.Close();
    }
}
