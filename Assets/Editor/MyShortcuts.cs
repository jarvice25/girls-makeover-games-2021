﻿using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

public class MyShortcuts : Editor
{
	[MenuItem("GameObject/ActiveToggle _ ")]
	static void ToggleActivationSelection()
	{
		foreach(GameObject go in Selection.gameObjects)
        {
            go.SetActive(!go.activeSelf);
            EditorUtility.SetDirty(go);
        }

        EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
	}

	[MenuItem("Shortcuts/Animator #6")]
	static void ActivateAnimatorWindow()
	{
		EditorApplication.ExecuteMenuItem("Window/Animator");
	}
}