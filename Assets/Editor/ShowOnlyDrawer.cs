﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(ShowOnlyAttribute))]
public class ShowOnlyDrawer : PropertyDrawer
{
	public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label)
	{
		string valueStr;

		switch (prop.propertyType)
		{
		case SerializedPropertyType.Integer:
			valueStr = prop.intValue.ToString();
			break;
		case SerializedPropertyType.Boolean:
			valueStr = prop.boolValue.ToString();
			break;
		case SerializedPropertyType.Float:
			valueStr = prop.floatValue.ToString("0.00000");
			break;
		case SerializedPropertyType.String:
			valueStr = prop.stringValue;
			break;
		default:
            GUI.enabled = false;
            EditorGUI.PropertyField(position, prop, label);
            GUI.enabled = true;
                valueStr = "";
            break;
        }

        EditorGUI.LabelField(position,label.text, valueStr);      
    }
}
